package com.e.digilife.Notification_Service;

import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.e.digilife.Activity.DashboardActivity;
import com.e.digilife.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.List;

/**
 * Created by Mayuri on 27/05/19.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = MyFirebaseMessagingService.class.getSimpleName();

    private NotificationUtils notificationUtils;
    private SharedPreferences mSharedPreferences;
    private Intent notificationIntent;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        mSharedPreferences = getSharedPreferences("TownTalkApp", MODE_PRIVATE);
        String refreshedToken = s;
        Log.e(TAG, "Refreshed token:" + refreshedToken);
        // Saving reg id to shared preferences
        storeString(mSharedPreferences, "registration_id", refreshedToken);

        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);
    }



    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);
    }

    public static void storeString(SharedPreferences sharedPreferences, String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Log.e(TAG, "From: " + remoteMessage.getFrom());

        if (remoteMessage == null)
            return;

        String message = remoteMessage.getData().toString();

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            if (!notificationUtils.isAppIsInBackground(getApplicationContext())) {
                Log.e(TAG, "Notification Body: " + remoteMessage.getNotification().getBody());
                Intent pushNotification = new Intent(Config.PUSH_NOTIFICATION);
                pushNotification.putExtra("message", message);
                LocalBroadcastManager.getInstance(this).sendBroadcast(pushNotification);
            }

        }

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {

            Log.e(TAG, "Data Payload : " + remoteMessage.getData().toString());

            String msg = remoteMessage.getData().get("message");
            String id = remoteMessage.getData().get("id");
            String type = remoteMessage.getData().get("type");

            Log.e(TAG, "Data Payload id: " + id);
            Log.e(TAG, "Data Payload message: " + msg);
            Log.e(TAG, "Data Payload type: " + type);

            handleDataMessage(getApplicationContext(), msg);
        }
    }

    // ToDo this code for current running activity
    private String getActivityName() {
        ActivityManager am = (ActivityManager) getApplicationContext().getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
        String name = taskInfo.get(0).topActivity.getClassName();
        Log.i("CURRENT Activity ", name);
        return name;
    }


    private void handleDataMessage(Context context, String message) {
        int icon = R.mipmap.logo;
        long when = System.currentTimeMillis();
//        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);


        String title = context.getString(R.string.app_name);

        try {
            notificationIntent = new Intent(context, DashboardActivity.class);

            PendingIntent intent = PendingIntent.getActivity(context, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);

            NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                mBuilder.setSmallIcon(R.mipmap.logo);
                mBuilder.setColor(Color.parseColor("#7a1417"));
            } else {
                mBuilder.setSmallIcon(icon);
            }

            mBuilder.setTicker(title).setWhen(when);
            mBuilder.setAutoCancel(true);
            mBuilder.setContentTitle(title);
            mBuilder.setContentIntent(intent);
            mBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
            mBuilder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), icon));
            mBuilder.setContentText(message);
            mBuilder.setPriority(Notification.PRIORITY_MAX);
            mBuilder.setStyle(new NotificationCompat.BigTextStyle().bigText(message));

            NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                NotificationChannel notificationChannel = new NotificationChannel("10001", title, importance);
                notificationChannel.enableLights(true);
                notificationChannel.setLightColor(Color.RED);
                notificationChannel.enableVibration(true);
                notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                assert notificationManager != null;
                mBuilder.setChannelId("10001");
                notificationManager.createNotificationChannel(notificationChannel);
            }
            assert notificationManager != null;
            notificationManager.notify(0, mBuilder.build());

        } catch (NullPointerException e) {
            e.printStackTrace();
        }

    }


}
