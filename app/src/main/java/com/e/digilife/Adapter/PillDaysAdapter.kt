package com.e.digilife.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.e.digilife.Model.PillReminderModel
import com.e.digilife.R
import com.e.digilife.View.TextViewPlus
import java.util.ArrayList

// Created by $Mayuri on 21-06-2019.

class PillDaysAdapter() : RecyclerView.Adapter<PillDaysAdapter.ViewHolder>() {
    private var context: Context? = null
    private var dataList = ArrayList<PillReminderModel>()
    lateinit var onItemClick: View.OnClickListener

    constructor(
        context: Context,
        dataList: ArrayList<PillReminderModel>,
        onItemClick: View.OnClickListener
    ) : this() {
        this.context = context
        this.dataList = dataList
        this.onItemClick = onItemClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PillDaysAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.activity_pill_reminder_days_view, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: PillDaysAdapter.ViewHolder, position: Int) {
        holder.bindItems(dataList[position], context!!)

        var isDaySelected = dataList.get(position).isDaySelected
        if (isDaySelected.equals("1")) {
            holder.dayLetterText!!.setBackgroundResource(R.drawable.round_light_blue)
        } else {
            holder.dayLetterText!!.setBackgroundResource(R.drawable.round_thumb)
        }

        holder.dayLetterText!!.tag = position
        holder.dayLetterText!!.setOnClickListener(onItemClick)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var dayLetterText: TextViewPlus? = null

        fun bindItems(data: PillReminderModel, context: Context) {
            dayLetterText = itemView.findViewById(R.id.dayLetterText)

            dayLetterText!!.setText(data.dayLetter)
        }
    }
}
