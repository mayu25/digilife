package com.app.towntalk.Adapter
import android.content.Context
import android.content.Intent
import android.os.Build
import android.provider.Telephony
import android.support.v7.widget.CardView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import com.android.volley.toolbox.ImageLoader
import com.e.digilife.Model.TaskCategoryListModel
import com.e.digilife.View.TextViewPlus
import android.widget.RelativeLayout
import com.e.digilife.R
import java.nio.file.Files.size





class TaskCategoryListAdapter : BaseAdapter {

    private var context: Context? = null
    private var dataList = ArrayList<TaskCategoryListModel>()
    var onItemClick: View.OnClickListener

    constructor(
        context: Context,
        emrList: ArrayList<TaskCategoryListModel>,
        onItemClick: View.OnClickListener

    ) : super() {
        this.context = context
        this.dataList = emrList
        this.onItemClick = onItemClick

    }

    var view: View? = null
    private lateinit var inflater: LayoutInflater


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

        view = convertView
        val vh = ViewHolder()

        if (convertView == null) {
            inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }

       view = inflater.inflate(R.layout.activity_task_category_list_gridview_item, null)

        vh.txt_categoryname = view?.findViewById(R.id.txt_categoryname) as TextViewPlus
        vh.ic_plus = view?.findViewById(R.id.ic_plus) as ImageView
        vh.txt_count = view?.findViewById(R.id.txt_count) as TextViewPlus
        vh.main_card = view?.findViewById(R.id.main_card) as CardView
        vh.ic_plus!!.visibility = View.GONE
        vh.txt_categoryname!!.visibility = View.VISIBLE
        vh.txt_count!!.visibility = View.VISIBLE
        vh.txt_categoryname!!.text = dataList[position].cat_name
        if (dataList[position].count!=null){
            vh.txt_count!!.text = dataList[position].count+" Tasks "
        }
        else{
            vh.txt_count!!.text =" "
        }
        if (position === dataList.size - 1) {
            vh.ic_plus!!.visibility = View.VISIBLE
            vh.txt_categoryname!!.visibility = View.GONE
            vh.txt_count!!.visibility = View.GONE

        }
        vh.main_card!!.tag = position
        vh.main_card!!.setOnClickListener(onItemClick)
        return view
    }


    override fun getItem(position: Int): Any {
        return dataList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dataList.size
    }

    inner class ViewHolder {

        var txt_categoryname: TextViewPlus? = null
        var txt_count: TextViewPlus? = null
        var main_card: CardView? = null
        var ic_plus: ImageView? = null

    }


}




