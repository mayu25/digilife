package com.app.towntalk.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import com.android.volley.toolbox.ImageLoader
import com.e.digilife.Model.DataModel
import com.e.digilife.R
import com.e.digilife.View.CustomVolleyRequest
import com.e.digilife.View.Utils

/**
 * Created by Mayuri on 22/05/19.
 */
class OfflineAdpater : BaseAdapter {

    private var context: Context? = null
    private var dataList = ArrayList<DataModel>()
    var onItemClick: View.OnClickListener


    private var imageLoader: ImageLoader? = null

    constructor(
        context: Context,
        emrList: ArrayList<DataModel>,
        onItemClick: View.OnClickListener
    ) : super() {

        this.context = context
        this.dataList = emrList
        this.onItemClick = onItemClick
        imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
    }

    var view: View? = null
    private lateinit var inflater: LayoutInflater


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

        view = convertView
        val vh = ViewHolder()

        if (convertView == null) {
            inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }

        view = inflater.inflate(R.layout.activity_emr_gridview_item, null)

        vh.imageView = view?.findViewById(R.id.imageView) as ImageView
        vh.imgCheckbox = view?.findViewById(R.id.imgCheckbox) as CheckBox
        vh.imgNameText = view?.findViewById(R.id.imgNameText) as TextView

        var imagePath = dataList.get(position).offlineImagePath

        vh.imgNameText!!.text = dataList.get(position).offlineFileName

        if (!imagePath.equals(""))
            imageLoader!!.get(
                imagePath,
                ImageLoader.getImageListener(vh.imageView!!, R.mipmap.ic_username, R.mipmap.ic_username)
            )


        vh.imageView!!.tag = position
        vh.imageView!!.setOnClickListener(onItemClick)

        return view
    }


    override fun getItem(position: Int): Any {
        return dataList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dataList.size
    }

    inner class ViewHolder {
        var imageView: ImageView? = null
        var imgCheckbox: CheckBox? = null
        var imgNameText: TextView? = null

    }


}




