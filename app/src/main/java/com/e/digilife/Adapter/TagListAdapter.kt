package com.app.towntalk.Adapter
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.RelativeLayout
import com.e.digilife.Model.TagModel
import com.e.digilife.R
import com.e.digilife.View.TextViewPlus

class TagListAdapter : BaseAdapter {

    private var context: Context? = null
    private var tagList = ArrayList<TagModel>()
    lateinit var onItemClick: View.OnClickListener

    constructor(context: Context, tagList: ArrayList<TagModel>, onItemClick: View.OnClickListener) : super() {
        this.context = context
        this.tagList = tagList
        this.onItemClick = onItemClick

    }

    var view: View? = null
    private lateinit var inflater: LayoutInflater


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

        view = convertView
        val vh = ViewHolder()

        if (convertView == null) {

            inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }
        view = inflater.inflate(R.layout.activity_tag_list_item, null)

        vh.txt_tag_name = view?.findViewById(R.id.txt_tag_name) as TextViewPlus


        vh.txt_tag_name!!.text = tagList[position].tag_title

        vh.itemLayout = view?.findViewById(R.id.itemLayout)

        vh.itemLayout!!.tag = position
        vh.itemLayout!!.setOnClickListener(onItemClick)


        if (tagList[position].tag_title == "Priority") {
            vh.txt_tag_name!!.setBackgroundResource(R.color.lightyellow);
        } else if (tagList[position].tag_title == "Important") {
            vh.txt_tag_name!!.setBackgroundResource(R.color.darkpink);
        } else if (tagList[position].tag_title == "In Progress") {
            vh.txt_tag_name!!.setBackgroundResource(R.color.brown);
        } else if (tagList[position].tag_title == "Deadline") {
            vh.txt_tag_name!!.setBackgroundResource(R.color.yellow);
        } else if (tagList[position].tag_title == "Family") {
            vh.txt_tag_name!!.setBackgroundResource(R.color.darkgreen);
        } else if (tagList[position].tag_title == "Trackbook") {
            vh.txt_tag_name!!.setBackgroundResource(R.color.lightgreen);
        } else if (tagList[position].tag_title == "Science project") {
            vh.txt_tag_name!!.setBackgroundResource(R.color.l_blue);
        } else if (tagList[position].tag_title == "Low priority") {
            vh.txt_tag_name!!.setBackgroundResource(R.color.darkblue);
        } else if (tagList[position].tag_title == "Not Critical") {
            vh.txt_tag_name!!.setBackgroundResource(R.color.slatblue);
        } else if (tagList[position].tag_title == "Top Priority") {
            vh.txt_tag_name!!.setBackgroundResource(R.color.greyColor);
        } else {
            vh.txt_tag_name!!.setBackgroundResource(R.color.blackColor);
        }



        view!!.tag = position

        return view
    }

    override fun getItem(position: Int): Any {
        return tagList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()

    }

    override fun getCount(): Int {
        return tagList.size
    }

    private inner class ViewHolder {

        var txt_tag_name: TextViewPlus? = null
        var itemLayout: RelativeLayout? = null

    }

}




