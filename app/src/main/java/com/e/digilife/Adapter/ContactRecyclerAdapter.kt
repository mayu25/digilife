package com.app.towntalk.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.daimajia.swipe.SwipeLayout
import com.e.digilife.Model.ContactModel
import com.e.digilife.Model.WalletModel
import com.e.digilife.R
import com.e.digilife.View.TextViewPlus


/**
 * Created by Mayuri on 06/06/19.
 */


class ContactRecyclerAdapter() : RecyclerView.Adapter<ContactRecyclerAdapter.MyViewHolder>() {
    private var context: Context? = null
    private var dataList = ArrayList<ContactModel>()
    lateinit var onItemClick: View.OnClickListener
    lateinit var onMessageItemClick: View.OnClickListener
    lateinit var onCallItemClick: View.OnClickListener
    lateinit var onWhatsAppItemClick: View.OnClickListener
    lateinit var onDeleteItemClick: View.OnClickListener
    lateinit var onShareItemClick: View.OnClickListener

    constructor(
        context: Context, dataList: ArrayList<ContactModel>,
        onItemClick: View.OnClickListener, onMessageItemClick: View.OnClickListener,
        onCallItemClick: View.OnClickListener,
        onWhatsAppItemClick: View.OnClickListener,
        onDeleteItemClick: View.OnClickListener,
        onShareItemClick: View.OnClickListener
    ) : this() {
        this.context = context
        this.dataList = dataList
        this.onItemClick = onItemClick
        this.onMessageItemClick = onMessageItemClick
        this.onCallItemClick = onCallItemClick
        this.onWhatsAppItemClick = onWhatsAppItemClick
        this.onWhatsAppItemClick = onWhatsAppItemClick
        this.onWhatsAppItemClick = onWhatsAppItemClick
        this.onDeleteItemClick = onDeleteItemClick
        this.onShareItemClick = onShareItemClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.fragment_contact_item_view, parent, false)

        val holder = MyViewHolder(v)
        return holder
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        var groupName = dataList.get(position).groupName
        var isTitle = dataList.get(position).isTitle
        if (isTitle.equals("")) {
            holder.groupNameText!!.visibility = View.GONE
        } else {
            holder.groupNameText!!.text = groupName
            holder.groupNameText!!.visibility = View.VISIBLE
        }

        holder.contactNameText!!.text = dataList.get(position).contactName
        holder.nameLetterText!!.text = dataList.get(position).nameFirstLetter

        holder.messageView!!.tag = position
        holder.messageView!!.setOnClickListener(onMessageItemClick)

        holder.callView!!.tag = position
        holder.callView!!.setOnClickListener(onCallItemClick)

        holder.whatsAppView!!.tag = position
        holder.whatsAppView!!.setOnClickListener(onWhatsAppItemClick)

        holder.shareLayout!!.tag = position
        holder.shareLayout!!.setOnClickListener(onShareItemClick)

        holder.deleteLayout!!.tag = position
        holder.deleteLayout!!.setOnClickListener(onDeleteItemClick)

        holder.itemLayout!!.tag = position
        holder.itemLayout!!.setOnClickListener(onItemClick)
    }


    override fun getItemCount(): Int {
        return dataList.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var messageView: ImageView? = null
        var callView: ImageView? = null
        var whatsAppView: ImageView? = null
        var contactNameText: TextViewPlus? = null
        var nameLetterText: TextViewPlus? = null
        var groupNameText: TextViewPlus? = null
        var shareLayout: FrameLayout? = null
        var deleteLayout: FrameLayout? = null
        var itemLayout: LinearLayout? = null

        init {
            messageView = itemView.findViewById(R.id.messageView)
            callView = itemView.findViewById(R.id.callView)
            whatsAppView = itemView.findViewById(R.id.whatsAppView)
            contactNameText = itemView.findViewById(R.id.contactNameText)
            nameLetterText = itemView.findViewById(R.id.nameLetterText)
            groupNameText = itemView.findViewById(R.id.groupNameText)
            deleteLayout = itemView.findViewById(R.id.deleteLayout)
            shareLayout = itemView.findViewById(R.id.shareLayout)
            itemLayout = itemView.findViewById(R.id.itemLayout)
        }
    }
}
