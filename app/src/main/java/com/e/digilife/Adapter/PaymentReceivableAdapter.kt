package com.app.towntalk.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.toolbox.NetworkImageView
import com.daimajia.swipe.SwipeLayout
import com.e.digilife.Model.WalletModel
import com.e.digilife.R
import com.e.digilife.View.TextViewPlus
import com.e.digilife.View.Utils
import com.google.android.gms.plus.internal.model.people.PersonEntity


/**
 * Created by Mayuri on 15/06/19.
 * */


class PaymentReceivableAdapter() : RecyclerView.Adapter<PaymentReceivableAdapter.ViewHolder>() {
    private var context: Context? = null
    private var dataList = ArrayList<WalletModel>()
    lateinit var onItemClick: View.OnClickListener

    constructor(
        context: Context, dataList: ArrayList<WalletModel>, onItemClick: View.OnClickListener

    ) : this() {
        this.context = context
        this.dataList = dataList
        this.onItemClick = onItemClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentReceivableAdapter.ViewHolder {
        val v =
            LayoutInflater.from(parent.context).inflate(R.layout.activity_payment_receivable_item_view, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: PaymentReceivableAdapter.ViewHolder, position: Int) {
        holder.bindItems(dataList[position], onItemClick)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return dataList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var listTitleText: TextView? = null
        var currencyView: NetworkImageView? = null
        var trasNameText: TextViewPlus? = null
        var trasAmountText: TextViewPlus? = null
        var rightView: ImageView? = null

        fun bindItems(
            data: WalletModel,
            onItemClick: View.OnClickListener
        ) {
            listTitleText = itemView.findViewById(R.id.listTitleText)
            currencyView = itemView.findViewById(R.id.currencyView)
            trasNameText = itemView.findViewById(R.id.trasNameText)
            trasAmountText = itemView.findViewById(R.id.trasAmountText)
            rightView = itemView.findViewById(R.id.rightView)

            var currency = data.currency + " "
            var isSplit = data.p_IsSplit
            var key = data.p_Key

            if (key.equals("")) {
                listTitleText!!.visibility = View.GONE
            } else {
                listTitleText!!.visibility = View.VISIBLE
                listTitleText!!.text = key
            }

            if (isSplit.equals("1")) {
                rightView!!.visibility = View.VISIBLE
            } else {
                rightView!!.visibility = View.INVISIBLE
            }

            trasNameText!!.text = data.p_MemberName
            trasAmountText!!.text = currency + data.p_Amount + ".00"

            itemView.tag = position
            itemView.setOnClickListener(onItemClick)

        }
    }


}
