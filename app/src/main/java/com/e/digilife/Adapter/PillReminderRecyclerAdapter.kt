package com.app.towntalk.Adapter

import android.content.Context
import android.os.Parcel
import android.os.Parcelable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.e.digilife.Model.PillReminderModel
import com.e.digilife.R
import com.e.digilife.View.TextViewPlus


/**
 * Created by Mayuri on 20/06/19.
 */
class PillReminderRecyclerAdapter() : RecyclerView.Adapter<PillReminderRecyclerAdapter.ViewHolder>() {
    private var context: Context? = null
    private var dataList = ArrayList<PillReminderModel>()
    lateinit var onItemClick: View.OnClickListener


    constructor(context: Context, dataList: ArrayList<PillReminderModel>, onItemClick: View.OnClickListener) : this() {
        this.context = context
        this.dataList = dataList
        this.onItemClick = onItemClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PillReminderRecyclerAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.activity_pill_reminder_medicine_list_view, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: PillReminderRecyclerAdapter.ViewHolder, position: Int) {
        holder.bindItems(dataList[position], onItemClick)
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var pillNameText: TextViewPlus? = null
        var pillTimeText: TextViewPlus? = null

        fun bindItems(data: PillReminderModel, onItemClick: View.OnClickListener) {
            pillNameText = itemView.findViewById(R.id.pillNameText)
            pillTimeText = itemView.findViewById(R.id.pillTimeText)

            pillNameText!!.setText(data.pillName)
            pillTimeText!!.setText("Time : " + data.pillRemiderTime)

            itemView.tag = position
            itemView.setOnClickListener(onItemClick)


        }
    }

}




