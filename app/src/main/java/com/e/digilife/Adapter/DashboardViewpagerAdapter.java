package com.e.digilife.Adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import com.e.digilife.R;

public class DashboardViewpagerAdapter extends PagerAdapter {

    private Context mContext;
    private Integer[] listImages;
    private int resId;
    int position;


    public DashboardViewpagerAdapter(Context mContext, Integer[] listImages, int resId) {
        this.mContext = mContext;
        this.listImages = listImages;
        this.resId = resId;
    }

    @Override
    public int getCount() {
        return listImages == null ? 0 : listImages.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }


    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = LayoutInflater.from(mContext).inflate(resId, container, false);
         ImageView ivProducts = itemView.findViewById(R.id.image);


        /*Picasso.get()
                .load(listImages.get(position))
                .placeholder(R.color.colorWhite)
                .into(ivProducts);*/
        container.addView(itemView);


        return itemView;


    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

}
