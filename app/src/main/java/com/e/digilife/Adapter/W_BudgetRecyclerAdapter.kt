package com.app.towntalk.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.toolbox.NetworkImageView
import com.daimajia.swipe.SwipeLayout
import com.e.digilife.Model.WalletModel
import com.e.digilife.R
import com.e.digilife.View.TextViewPlus
import com.e.digilife.View.Utils


/**
 * Created by Mayuri on 05/06/19.
 */


class W_BudgetRecyclerAdapter() : RecyclerView.Adapter<W_BudgetRecyclerAdapter.ViewHolder>() {
    private var context: Context? = null
    private var dataList = ArrayList<WalletModel>()
    lateinit var onItemClick: View.OnClickListener
    lateinit var onDeleteItemClick: View.OnClickListener

    constructor(context: Context, dataList: ArrayList<WalletModel>, onItemClick: View.OnClickListener,
        onDeleteItemClick: View.OnClickListener) : this() {
        this.context = context
        this.dataList = dataList
        this.onItemClick = onItemClick
        this.onDeleteItemClick = onDeleteItemClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): W_BudgetRecyclerAdapter.ViewHolder {
        val v =
            LayoutInflater.from(parent.context).inflate(R.layout.fragment_wallet_budget_item_view, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: W_BudgetRecyclerAdapter.ViewHolder, position: Int) {
        holder.bindItems(dataList[position], context!!, onItemClick, onDeleteItemClick)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return dataList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var transcationTitleLayout: LinearLayout? = null
        var trasTitleText: TextViewPlus? = null

        var swipe: SwipeLayout? = null
        var deleteLayout: FrameLayout? = null
        var transcationItemLayout: LinearLayout? = null
        var currencyView: NetworkImageView? = null
        var trasNameText: TextViewPlus? = null
        var trasAmountText: TextViewPlus? = null

        fun bindItems(
            data: WalletModel,
            context: Context,
            onItemClick: View.OnClickListener,
            onDeleteItemClick: View.OnClickListener
        ) {
            transcationTitleLayout = itemView.findViewById(R.id.transcationTitleLayout)
            trasTitleText = itemView.findViewById(R.id.trasTitleText)

            swipe = itemView.findViewById(R.id.swipe) as SwipeLayout
            deleteLayout = itemView.findViewById(R.id.deleteLayout)
            transcationItemLayout = itemView.findViewById(R.id.transcationItemLayout)
            currencyView = itemView.findViewById(R.id.currencyView)
            trasNameText = itemView.findViewById(R.id.trasNameText)
            trasAmountText = itemView.findViewById(R.id.trasAmountText)

            var isTitle = data.isTitle
            var currency =data.currency + " "

            if (isTitle.equals("")) {
                transcationTitleLayout!!.visibility = View.GONE
            } else {
                transcationTitleLayout!!.visibility = View.VISIBLE
                trasTitleText!!.text = data.isTitle
            }

            var categoryType = data.trasCategoryType
            if(categoryType.equals(Utils.TRANSACTION_EXPENSE)){
                trasAmountText!!.setTextColor(context.resources.getColor(R.color.expenseColor))
                currencyView!!.setDefaultImageResId(R.mipmap.ic_expense)
            }else{
                trasAmountText!!.setTextColor(context.resources.getColor(R.color.lightGreenColor))
                currencyView!!.setDefaultImageResId(R.mipmap.ic_income)
            }




            trasNameText!!.text = data.trasCategoryName
            trasAmountText!!.text = currency + data.tranAmount


            transcationItemLayout!!.tag = position
            transcationItemLayout!!.setOnClickListener(onItemClick)

            deleteLayout!!.tag = position
            deleteLayout!!.setOnClickListener(onDeleteItemClick)
        }
    }


}
