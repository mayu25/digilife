package com.app.towntalk.Adapter

import android.content.Context
import android.graphics.Typeface
import android.os.Parcel
import android.os.Parcelable
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.toolbox.NetworkImageView
import com.daimajia.swipe.SwipeLayout
import com.e.digilife.Model.WalletModel
import com.e.digilife.R
import com.e.digilife.View.TextViewPlus
import com.e.digilife.View.Utils


/**
 * Created by Mayuri on 10/06/19.
 */


class W_GraphRecyclerAdapter() : RecyclerView.Adapter<W_GraphRecyclerAdapter.ViewHolder>() {
    private var context: Context? = null
    private var dataList = ArrayList<WalletModel>()


    constructor(context: Context, dataList: ArrayList<WalletModel>) : this() {
        this.context = context
        this.dataList = dataList

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): W_GraphRecyclerAdapter.ViewHolder {
        val v =
            LayoutInflater.from(parent.context).inflate(R.layout.fragment_wallet_graph_item_view, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: W_GraphRecyclerAdapter.ViewHolder, position: Int) {
        holder.bindItems(dataList[position], context!!)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return dataList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var transcationTitleLayout: LinearLayout? = null
        var trasDateText: TextViewPlus? = null

        var trasPersentageText: TextViewPlus? = null
        var currencyView: NetworkImageView? = null
        var trasNameText: TextViewPlus? = null
        var trasAmountText: TextViewPlus? = null

        fun bindItems(
            data: WalletModel,
            context: Context


        ) {
            transcationTitleLayout = itemView.findViewById(R.id.transcationTitleLayout)
            trasDateText = itemView.findViewById(R.id.trasDateText)

            trasPersentageText = itemView.findViewById(R.id.trasPersentageText)
            currencyView = itemView.findViewById(R.id.currencyView)
            trasNameText = itemView.findViewById(R.id.trasNameText)
            trasAmountText = itemView.findViewById(R.id.trasAmountText)

            var isExpense = data.isExpense
            var currency = data.currency + " "

            if (isExpense.equals("")) {
                transcationTitleLayout!!.visibility = View.GONE
            } else {
                transcationTitleLayout!!.visibility = View.VISIBLE
                trasDateText!!.text = data.transDate
            }
            var colorValue = data.percentageColor
            if (!colorValue.equals("")) {
                trasPersentageText!!.setTextColor(colorValue)
            }

            trasPersentageText!!.text = data.trasPersentage
            trasNameText!!.text = data.trasCategoryName
            trasAmountText!!.text = currency + data.tranAmount

        }
    }


}
