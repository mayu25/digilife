package com.app.towntalk.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import android.widget.FrameLayout
import android.widget.RelativeLayout
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.NetworkImageView
import com.daimajia.swipe.SimpleSwipeListener
import com.daimajia.swipe.SwipeLayout
import com.e.digilife.Model.PersonalModel
import com.e.digilife.R
import com.e.digilife.View.CustomVolleyRequest
import com.e.digilife.View.TextViewPlus




/**
 * Created by Mayuri on 21/05/19.
 */
class PersonalListAdapter : BaseAdapter {

    private var context: Context? = null
    private var personalList = ArrayList<PersonalModel>()
    var onItemClick: View.OnClickListener
    var onDeleteItemClick: View.OnClickListener

    constructor(
        context: Context, personalList: ArrayList<PersonalModel>, onItemClick: View.OnClickListener,
        onDeleteItemClick: View.OnClickListener
    ) : super() {
        this.context = context
        this.personalList = personalList
        this.onItemClick = onItemClick
        this.onDeleteItemClick = onDeleteItemClick
        imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
    }

    var view: View? = null
    private lateinit var inflater: LayoutInflater
    private var imageLoader: ImageLoader? = null

    private var preswipes: SwipeLayout? = null

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

        view = convertView
        val vh = ViewHolder()

        if (convertView == null) {

            inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }

        view = inflater.inflate(R.layout.fragment_personal_list_item, null)

        vh.title = view?.findViewById(R.id.title) as TextViewPlus
        vh.itemLayout = view?.findViewById(R.id.itemLayout)
        vh.description = view?.findViewById(R.id.description) as TextViewPlus
        vh.listImg = view?.findViewById(R.id.listImg) as NetworkImageView
        vh.swipe = view?.findViewById(R.id.swipe) as SwipeLayout
        vh.deleteLayout = view?.findViewById(R.id.deleteLayout)

        vh.title!!.text = personalList[position].personalFolderName


        var description = personalList[position].description
        if (description.equals("")) {
            vh.description!!.text = "Fild or upload your " + personalList[position].personalFolderName + " Here"
        } else {
            vh.description!!.text = description
        }
        vh.swipe!!.setClickToClose(true)
//        vh.swipe!!.addSwipeListener(object : SwipeLayout.SwipeListener {
//            override fun onStartOpen(layout: SwipeLayout) {
//                if(preswipes==null) {
//                    preswipes=layout;
//                }else
//                {
//                    preswipes!!.close(true);
//                    preswipes=layout;
//                }
//            }
//
//            override fun onOpen(layout: SwipeLayout) {}
//
//            override fun onStartClose(layout: SwipeLayout) {}
//
//            override fun onClose(layout: SwipeLayout) {}
//
//            override fun onUpdate(layout: SwipeLayout, leftOffset: Int, topOffset: Int) {}
//
//            override fun onHandRelease(layout: SwipeLayout, xvel: Float, yvel: Float) {}
//        })

        val imagePath = personalList[position].personalImgPath

        if (!imagePath.equals("")) {
            imageLoader!!.get(
                imagePath,
                ImageLoader.getImageListener(vh.listImg!!, R.mipmap.folder_placeholder, R.mipmap.folder_placeholder)
            )
            vh.listImg!!.setImageUrl(imagePath, imageLoader)
        } else {
            vh.listImg!!.setDefaultImageResId(R.mipmap.personal_financial)
        }

        vh.itemLayout!!.tag = position
        vh.itemLayout!!.setOnClickListener(onItemClick)

        vh.deleteLayout!!.tag = position
        vh.deleteLayout!!.setOnClickListener(onDeleteItemClick)


        return view
    }

    override fun getItem(position: Int): Any {
        return personalList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return personalList.size
    }

    private inner class ViewHolder {
        var itemLayout: RelativeLayout? = null
        var listImg: NetworkImageView? = null
        var title: TextViewPlus? = null
        var description: TextViewPlus? = null
        var swipe: SwipeLayout? = null
        var deleteLayout: FrameLayout? = null
    }

}




