package com.app.towntalk.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.android.volley.toolbox.NetworkImageView
import com.daimajia.swipe.SwipeLayout
import com.e.digilife.Model.WalletModel
import com.e.digilife.R
import com.e.digilife.View.TextViewPlus
import com.e.digilife.View.Utils


/**
 * Created by Mayuri on 05/06/19.
 */

class W_BudgetCategoryRecyclerAdapter() : RecyclerView.Adapter<W_BudgetCategoryRecyclerAdapter.ViewHolder>() {
    private var context: Context? = null
    private var dataList = ArrayList<WalletModel>()
    lateinit var onDeleteItemClick: View.OnClickListener


    constructor(context: Context, dataList: ArrayList<WalletModel>, onDeleteItemClick: View.OnClickListener) : this() {
        this.context = context
        this.dataList = dataList
        this.onDeleteItemClick = onDeleteItemClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): W_BudgetCategoryRecyclerAdapter.ViewHolder {
        val v =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_wallet_budget_category_item_view, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: W_BudgetCategoryRecyclerAdapter.ViewHolder, position: Int) {
        holder.bindItems(dataList[position], onDeleteItemClick)
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return dataList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var transcationTitleLayout: LinearLayout? = null
        var trasTitleText: TextViewPlus? = null

        var swipe: SwipeLayout? = null
        var deleteLayout: FrameLayout? = null
        var currencyView: NetworkImageView? = null
        var trasNameText: TextViewPlus? = null
        var trasAmountText: TextViewPlus? = null
        var transProgressBar: ProgressBar? = null

        fun bindItems(
            data: WalletModel,
            onDeleteItemClick: View.OnClickListener
        ) {
            transcationTitleLayout = itemView.findViewById(R.id.transcationTitleLayout)
            trasTitleText = itemView.findViewById(R.id.trasTitleText)

            swipe = itemView.findViewById(R.id.swipe) as SwipeLayout
            deleteLayout = itemView.findViewById(R.id.deleteLayout)
            currencyView = itemView.findViewById(R.id.currencyView)
            trasNameText = itemView.findViewById(R.id.trasNameText)
            trasAmountText = itemView.findViewById(R.id.trasAmountText)
            transProgressBar = itemView.findViewById(R.id.transProgressBar)

            var currency = data.currency + " "
            var trascationType = data.categoryType

            if (!data.budgetAmount.equals("0")) {


                var totalValue = data.budgetAmount.replace(",", "").toDouble()
                var expenseValue = data.budgetExpense.replace(",", "").toDouble()
                var value = (expenseValue / totalValue) * 100
                var progressBarValue = value.toInt()

                transProgressBar!!.setProgress(progressBarValue)
            } else {
                transProgressBar!!.setProgress(0)
            }


            // var progressBarValue = (data.budgetExpense.toInt() / data.budgetAmount.toInt()) * 100

            if (trascationType.equals(Utils.TRANSACTION_EXPENSE)) {
                currencyView!!.setDefaultImageResId(R.mipmap.ic_expense)
            } else {
                currencyView!!.setDefaultImageResId(R.mipmap.ic_income)
            }



            trasNameText!!.text = data.budgetName
            trasAmountText!!.text = currency + data.budgetExpense + "/" + currency + data.budgetAmount

            deleteLayout!!.tag = position
            deleteLayout!!.setOnClickListener(onDeleteItemClick)
        }
    }
}
