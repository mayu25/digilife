package com.app.towntalk.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.CheckBox
import android.widget.ImageView
import com.android.volley.toolbox.ImageLoader
import com.e.digilife.Model.DataModel
import com.e.digilife.Model.EMRModel
import com.e.digilife.Model.NotificationModel
import com.e.digilife.R
import com.e.digilife.View.CustomVolleyRequest
import com.e.digilife.View.TextViewPlus
import com.e.digilife.View.Utils

/**
 * Created by Mayuri on 27/05/19.
 */
class NotificationAdapter : BaseAdapter {

    private var context: Context? = null
    private var dataList = ArrayList<NotificationModel>()
    var onItemClick: View.OnClickListener


    private var imageLoader: ImageLoader? = null

    constructor(
        context: Context, dataList: ArrayList<NotificationModel>,
        onItemClick: View.OnClickListener
    ) : super() {

        this.context = context
        this.dataList = dataList
        this.onItemClick = onItemClick

        imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
    }

    var view: View? = null
    private lateinit var inflater: LayoutInflater

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

        view = convertView
        val vh = ViewHolder()

        if (convertView == null) {
            inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }

        view = inflater.inflate(R.layout.activity_notificaiton_list_item, null)

        vh.titleText = view?.findViewById(R.id.titleText) as TextViewPlus
        vh.notificationNameText = view?.findViewById(R.id.notificationNameText) as TextViewPlus
        vh.notificationTimeText = view?.findViewById(R.id.notificationTimeText) as TextViewPlus

        vh.notificationNameText!!.text = dataList.get(position).notificationTitle
        vh.notificationTimeText!!.text = dataList.get(position).notificationTime

        if (dataList.get(position).notificationType.equals("")) {
            vh.titleText!!.visibility = View.GONE
        } else {
            vh.titleText!!.visibility = View.VISIBLE
            vh.titleText!!.text = dataList.get(position).notificationType
        }

        return view
    }


    override fun getItem(position: Int): Any {
        return dataList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dataList.size
    }

    inner class ViewHolder {
        var titleText: TextViewPlus? = null
        var notificationNameText: TextViewPlus? = null
        var notificationTimeText: TextViewPlus? = null
    }
}




