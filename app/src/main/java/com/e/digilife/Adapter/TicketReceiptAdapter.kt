package com.app.towntalk.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.NetworkImageView
import com.daimajia.swipe.SimpleSwipeListener
import com.daimajia.swipe.SwipeLayout
import com.e.digilife.Model.CustomFolderModel
import com.e.digilife.Model.PersonalModel
import com.e.digilife.R
import com.e.digilife.View.CustomVolleyRequest
import com.e.digilife.View.TextViewPlus


/**
 * Created by Mayuri on 27/05/19.
 */
class TicketReceiptAdapter : BaseAdapter {

    private var context: Context? = null
    private var folderList = ArrayList<PersonalModel>()
    var onItemClick: View.OnClickListener

    constructor(context: Context, folderList: ArrayList<PersonalModel>, onItemClick: View.OnClickListener) : super() {
        this.context = context
        this.folderList = folderList
        this.onItemClick = onItemClick
        imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
    }

    var view: View? = null
    private lateinit var inflater: LayoutInflater
    private var imageLoader: ImageLoader? = null

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

        view = convertView
        val vh = ViewHolder()

        if (convertView == null) {

            inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }

        view = inflater.inflate(R.layout.fragment_emr_list_item, null)

        vh.title = view?.findViewById(R.id.emrTitle) as TextViewPlus
        vh.description = view?.findViewById(R.id.emrDescription) as TextViewPlus
        vh.listImg = view?.findViewById(R.id.emrListImg) as NetworkImageView



        vh.title!!.text = folderList[position].personalFolderName


        var description = folderList[position].description
        if (description.equals("")) {
            vh.description!!.text = "Fild or upload your " + folderList[position].personalFolderName + " Here"
        } else {
            vh.description!!.text = description
        }

        val imagePath = folderList[position].personalImgPath

        if (!imagePath.equals("")) {
            imageLoader!!.get(
                imagePath,
                ImageLoader.getImageListener(vh.listImg!!, R.mipmap.folder_placeholder, R.mipmap.folder_placeholder)
            )
            vh.listImg!!.setImageUrl(imagePath, imageLoader);
        } else {
            vh.listImg!!.setDefaultImageResId(R.mipmap.personal_financial)
        }

        view!!.tag = position
        view!!.setOnClickListener(onItemClick)

        return view
    }

    override fun getItem(position: Int): Any {
        return folderList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return folderList.size
    }

    private inner class ViewHolder {
        var listImg: NetworkImageView? = null
        var title: TextViewPlus? = null
        var description: TextViewPlus? = null
    }

}




