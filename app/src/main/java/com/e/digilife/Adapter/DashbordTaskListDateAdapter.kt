package com.app.towntalk.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.LinearLayout
import com.e.digilife.Model.DashbordtaskListDateModel
import com.e.digilife.R
import com.e.digilife.View.TextViewPlus


class DashbordTaskListDateAdapter : BaseAdapter {

    private var context: Context? = null
    private var taskList = ArrayList<DashbordtaskListDateModel>()
    var onItemClick: View.OnClickListener

    constructor(
        context: Context,
        task_List_date_wise: ArrayList<DashbordtaskListDateModel>,

        onItemClick: View.OnClickListener
    ) : super() {
        this.context = context
        this.taskList = task_List_date_wise
        this.onItemClick = onItemClick

    }

    var view: View? = null
    private lateinit var inflater: LayoutInflater


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

        view = convertView
        val vh = ViewHolder()

        if (convertView == null) {

            inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }

        view = inflater.inflate(R.layout.dashbord_task_item_list_date_layout, null)

        vh.txt_task_title = view?.findViewById(R.id.txt_task_title) as TextViewPlus
        vh.img_circle = view?.findViewById(R.id.img_circle) as ImageView
        vh.img_add_task = view?.findViewById(R.id.img_add_task) as ImageView
        vh.titleLayout = view?.findViewById(R.id.titleLayout)
        vh.titleName = view?.findViewById(R.id.titleName)

        var isTitle = taskList.get(position).isTitle
        if (!isTitle.equals("")) {
            vh.titleLayout!!.visibility = View.VISIBLE
            vh.img_circle!!.visibility = View.GONE
            vh.titleName!!.setText(isTitle)
        } else {
            vh.titleLayout!!.visibility = View.GONE
            vh.img_circle!!.visibility = View.VISIBLE
        }


        vh.txt_task_title!!.text = taskList[position].task_title


        view!!.tag = position
       // view!!.setOnClickListener(onItemClick)
        vh.img_add_task!!.setOnClickListener(onItemClick)


        return view
    }

    override fun getItem(position: Int): Any {
        return taskList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return taskList.size
    }

    private inner class ViewHolder {

        var txt_task_title: TextViewPlus? = null
        var titleLayout: LinearLayout? = null
        var titleName: TextViewPlus? = null
        var img_circle: ImageView? = null
        var img_add_task: ImageView? = null

    }

}




