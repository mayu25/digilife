package com.app.towntalk.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.daimajia.swipe.SwipeLayout
import com.e.digilife.Model.WalletModel
import com.e.digilife.R
import com.e.digilife.View.TextViewPlus


/**
 * Created by Mayuri on 06/06/19.
 */


class TransactionMemberListRecyclerAdapter() : RecyclerView.Adapter<TransactionMemberListRecyclerAdapter.MyViewHolder>() {
    private var context: Context? = null
    private var dataList = ArrayList<WalletModel>()
    lateinit var onDeleteItemClick: View.OnClickListener

    constructor(
        context: Context, dataList: ArrayList<WalletModel>,
        onDeleteItemClick: View.OnClickListener
    ) : this() {
        this.context = context
        this.dataList = dataList
        this.onDeleteItemClick = onDeleteItemClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.fragment_tras_member_item_view, parent, false)

        val holder = MyViewHolder(v)
        return holder
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        if (dataList.size > 0) {

        }

        var name = dataList.get(position).memberName
        if (!name.equals("")) {
            holder.memberNameEditText!!.setText(dataList.get(position).memberName)
        }

        holder.memberNameEditText!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {
                if (dataList.size > 0)
                    dataList.get(position).memberName = holder.memberNameEditText!!.text.toString()
            }
        })


        holder.memberAmountText!!.text = dataList.get(position).memberCount

        holder.deleteLayout!!.tag = position
        holder.deleteLayout!!.setOnClickListener(onDeleteItemClick)
        //holder.bindItems(dataList[position], context!!, onDeleteItemClick)
    }

    fun remove(position: Int) {
        dataList.removeAt(position)
        notifyDataSetChanged()
        notifyItemRangeChanged(position, dataList.size);
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return dataList.size
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var swipe: SwipeLayout? = null
        var deleteLayout: FrameLayout? = null
        var transcationItemLayout: LinearLayout? = null
        var memberNameEditText: EditText? = null
        var memberAmountText: TextViewPlus? = null

        init {
            swipe = itemView.findViewById(R.id.swipe) as SwipeLayout
            deleteLayout = itemView.findViewById(R.id.deleteLayout)
            transcationItemLayout = itemView.findViewById(R.id.transcationItemLayout)
            memberNameEditText = itemView.findViewById(R.id.memberNameEditText)
            memberAmountText = itemView.findViewById(R.id.memberAmountText)

        }
    }
}
