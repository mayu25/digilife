package com.app.towntalk.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.NetworkImageView
import com.e.digilife.Model.EMRModel
import com.e.digilife.R
import com.e.digilife.View.CustomVolleyRequest
import com.e.digilife.View.TextViewPlus

/**
 * Created by Mayuri on 14/05/19.
 */
class EMRListAdapter : BaseAdapter {

    private var context: Context? = null
    private var emrList = ArrayList<EMRModel>()
    var onItemClick: View.OnClickListener

    constructor(context: Context, emrList: ArrayList<EMRModel>, onItemClick: View.OnClickListener) : super() {
        this.context = context
        this.emrList = emrList
        this.onItemClick = onItemClick

    }

    var view: View? = null
    private lateinit var inflater: LayoutInflater


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

        view = convertView
        val vh = ViewHolder()

        if (convertView == null) {

            inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }

        view = inflater.inflate(R.layout.fragment_emr_list_item, null)

        vh.emrTitle = view?.findViewById(R.id.emrTitle) as TextViewPlus
        vh.emrDescription = view?.findViewById(R.id.emrDescription) as TextViewPlus
        vh.emrListImg = view?.findViewById(R.id.emrListImg) as NetworkImageView

        vh.emrTitle!!.text = emrList[position].emrFolderName

        if (position == 0) {
            vh.emrListImg!!.setDefaultImageResId(R.mipmap.emr_lab_report)
            vh.emrDescription!!.text = context!!.resources.getText(R.string.lab_report_text)
        } else if (position == 1) {
            vh.emrListImg!!.setDefaultImageResId(R.mipmap.emr_prescription)
            vh.emrDescription!!.text = context!!.resources.getText(R.string.prescription_text)
        } else if (position == 2) {
            vh.emrListImg!!.setDefaultImageResId(R.mipmap.emr_medical_images)
            vh.emrDescription!!.text = context!!.resources.getText(R.string.medical_img_text)
        } else if (position == 3) {
            vh.emrListImg!!.setDefaultImageResId(R.mipmap.emr_ecg_report)
            vh.emrDescription!!.text = context!!.resources.getText(R.string.ecg_report_text)
        } else if (position == 4) {
            vh.emrListImg!!.setDefaultImageResId(R.mipmap.emr_medical_bills)
            vh.emrDescription!!.text = context!!.resources.getText(R.string.medical_bills_text)
        } else if (position == 5) {
            vh.emrListImg!!.setDefaultImageResId(R.mipmap.emr_consultation)
            vh.emrDescription!!.text = context!!.resources.getText(R.string.consultation_text)
        } else {
            vh.emrListImg!!.setDefaultImageResId(R.mipmap.emr_prescription)
            vh.emrDescription!!.text = "Find ot upload " + emrList[position].emrFolderName  + " Here"
        }


        view!!.tag = position
        view!!.setOnClickListener(onItemClick)

        return view
    }

    override fun getItem(position: Int): Any {
        return emrList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return emrList.size
    }

    private inner class ViewHolder {
        var emrListImg: NetworkImageView? = null
        var emrTitle: TextViewPlus? = null
        var emrDescription: TextViewPlus? = null
    }

}




