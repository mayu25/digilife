package com.app.towntalk.Adapter

import android.content.Context
import android.graphics.Typeface
import android.os.Parcel
import android.os.Parcelable
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.toolbox.NetworkImageView
import com.daimajia.swipe.SwipeLayout
import com.e.digilife.Model.WalletModel
import com.e.digilife.R
import com.e.digilife.View.TextViewPlus
import com.e.digilife.View.Utils


/**
 * Created by Mayuri on 10/06/19.
 */


class ReportAdapter() : RecyclerView.Adapter<ReportAdapter.MyViewHolder>() {

    private var context: Context? = null
    private var dataList = ArrayList<WalletModel>()


    constructor(context: Context, dataList: ArrayList<WalletModel>) : this() {
        this.context = context
        this.dataList = dataList

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReportAdapter.MyViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.activity_report_item_view, parent, false)

        val holder = MyViewHolder(v)
        return holder
    }

    override fun onBindViewHolder(holder: ReportAdapter.MyViewHolder, position: Int) {

        var isTitle = dataList.get(position).isTitle
        var currency = dataList.get(position).currency + " "

        if (isTitle.equals("")) {
            holder.transcationTitleLayout!!.visibility = View.GONE
        } else {
            holder.transcationTitleLayout!!.visibility = View.VISIBLE
            holder.trasDateText!!.text = dataList.get(position).transDate
        }

        var trascationType = dataList.get(position).trasCategoryType

        if (trascationType.equals(Utils.TRANSACTION_EXPENSE)) {
            holder.trasAmountText!!.setTextColor(context!!.resources.getColor(R.color.expenseColor))
            holder.currencyView!!.setImageResource(R.mipmap.ic_expense)
        } else {
            holder.trasAmountText!!.setTextColor(context!!.resources.getColor(R.color.lightGreenColor))
            holder.currencyView!!.setImageResource(R.mipmap.ic_income)
        }


        var isRenewable = dataList.get(position).trasIsRenewable
        if (isRenewable.equals("1")) {
            holder.trasNameText!!.text = dataList.get(position).trasCategoryName + " (RT)"
        } else {
            holder.trasNameText!!.text = dataList.get(position).trasCategoryName
        }

        holder.trasAmountText!!.text = currency + dataList.get(position).tranAmount + ".00"
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var transcationTitleLayout: LinearLayout? = null
        var trasDateText: TextViewPlus? = null

        var currencyView: ImageView? = null
        var trasNameText: TextViewPlus? = null
        var trasAmountText: TextViewPlus? = null

        init {
            transcationTitleLayout = itemView.findViewById(R.id.transcationTitleLayout)
            trasDateText = itemView.findViewById(R.id.trasDateText)

            currencyView = itemView.findViewById(R.id.currencyView)
            trasNameText = itemView.findViewById(R.id.trasNameText)
            trasAmountText = itemView.findViewById(R.id.trasAmountText)

        }
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return dataList.size
    }


}
