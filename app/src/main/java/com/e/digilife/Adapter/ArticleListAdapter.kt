package com.app.towntalk.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.Button
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.NetworkImageView
import com.daimajia.swipe.SimpleSwipeListener
import com.daimajia.swipe.SwipeLayout
import com.e.digilife.Model.ArticleModel
import com.e.digilife.Model.CustomFolderModel
import com.e.digilife.Model.PersonalModel
import com.e.digilife.R
import com.e.digilife.View.CustomVolleyRequest
import com.e.digilife.View.TextViewPlus


/**
 * Created by Mayuri on 03/06/19.
 */
class ArticleListAdapter : BaseAdapter {

    private var context: Context? = null
    private var articleList = ArrayList<ArticleModel>()
    var onItemClick: View.OnClickListener

    constructor(context: Context, articleList: ArrayList<ArticleModel>, onItemClick: View.OnClickListener) : super() {
        this.context = context
        this.articleList = articleList
        this.onItemClick = onItemClick
        imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
    }

    var view: View? = null
    private lateinit var inflater: LayoutInflater
    private var imageLoader: ImageLoader? = null

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

        view = convertView
        val vh = ViewHolder()

        if (convertView == null) {

            inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }

        view = inflater.inflate(R.layout.activity_article_item_view, null)

        vh.articleView = view?.findViewById(R.id.articleView) as NetworkImageView
        vh.articleTimeText = view?.findViewById(R.id.articleTimeText) as TextViewPlus
        vh.articleTitleText = view?.findViewById(R.id.articleTitleText) as TextViewPlus
        vh.articleDescriptionText = view?.findViewById(R.id.articleDescriptionText) as TextViewPlus
        vh.articleDescriptionText = view?.findViewById(R.id.articleDescriptionText) as TextViewPlus

        vh.articleTimeText!!.text = articleList[position].articelDate
        vh.articleTitleText!!.text = articleList[position].articleTitle
        vh.articleDescriptionText!!.text = articleList[position].articleDescription

        var imagePath = articleList[position].articleImage
        if(!imagePath.equals(""))
        {
            imageLoader!!.get(
                imagePath,
                ImageLoader.getImageListener(vh.articleView!!, R.mipmap.place_holder_img, R.mipmap.place_holder_img)
            )
            vh.articleView!!.setImageUrl(imagePath, imageLoader)
        }

        view!!.tag = position
        view!!.setOnClickListener(onItemClick)

        return view
    }

    override fun getItem(position: Int): Any {
        return articleList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return articleList.size
    }

    private inner class ViewHolder {
        var articleView: NetworkImageView? = null
        var articleTimeText: TextViewPlus? = null
        var articleTitleText: TextViewPlus? = null
        var articleDescriptionText: TextViewPlus? = null
    }

}




