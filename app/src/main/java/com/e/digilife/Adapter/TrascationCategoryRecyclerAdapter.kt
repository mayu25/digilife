package com.app.towntalk.Adapter

import android.content.Context
import android.graphics.Typeface
import android.os.Parcel
import android.os.Parcelable
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.toolbox.NetworkImageView
import com.daimajia.swipe.SwipeLayout
import com.e.digilife.Model.WalletModel
import com.e.digilife.R
import com.e.digilife.View.TextViewPlus
import com.e.digilife.View.Utils


/**
 * Created by Mayuri on 06/06/19.
 */


class TrascationCategoryRecyclerAdapter() : RecyclerView.Adapter<TrascationCategoryRecyclerAdapter.ViewHolder>() {
    private var context: Context? = null
    private var dataList = ArrayList<WalletModel>()
    lateinit var onItemClick: View.OnClickListener
    lateinit var onCategoryAddClick: View.OnClickListener
    var TYPE: String = ""
    lateinit var onCategoryEditClick: View.OnClickListener
    lateinit var onCategoryDeleteClick: View.OnClickListener

    constructor(
        context: Context, dataList: ArrayList<WalletModel>, onItemClick: View.OnClickListener,
        onCategoryAddClick: View.OnClickListener, TYPE: String, onCategoryEditClick: View.OnClickListener,
        onCategoryDeleteClick: View.OnClickListener
    ) : this() {
        this.context = context
        this.dataList = dataList
        this.onItemClick = onItemClick
        this.onCategoryAddClick = onCategoryAddClick
        this.TYPE = TYPE
        this.onCategoryEditClick = onCategoryEditClick
        this.onCategoryDeleteClick = onCategoryDeleteClick
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrascationCategoryRecyclerAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.fragment_trans_category_item_view, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: TrascationCategoryRecyclerAdapter.ViewHolder, position: Int) {
        holder.bindItems(
            dataList[position],
            onItemClick,
            onCategoryAddClick,
            TYPE,
            onCategoryEditClick,
            onCategoryDeleteClick
        )
    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return dataList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var categoryTitleLayout: LinearLayout? = null
        var categoryTypeText: TextViewPlus? = null
        var categoryAddView: NetworkImageView? = null

        var categoryLogoView: NetworkImageView? = null
        var categoryNameText: TextViewPlus? = null

        var editCategoryView: ImageView? = null
        var deleteCategoryView: ImageView? = null

        fun bindItems(
            user: WalletModel,
            onDeleteItemClick: View.OnClickListener, onCategoryAddClick: View.OnClickListener, TYPE: String,
            onCategoryEditClick: View.OnClickListener, onCategoryDeleteClick: View.OnClickListener
        ) {

            categoryTitleLayout = itemView.findViewById(R.id.categoryTitleLayout)
            categoryTypeText = itemView.findViewById(R.id.categoryTypeText)
            categoryAddView = itemView.findViewById(R.id.categoryAddView)

            categoryLogoView = itemView.findViewById(R.id.categoryLogoView)
            categoryNameText = itemView.findViewById(R.id.categoryNameText)

            editCategoryView = itemView.findViewById(R.id.editCategoryView)
            deleteCategoryView = itemView.findViewById(R.id.deleteCategoryView)

            var status = user.categoryStatus

            if (TYPE.equals("Setting")) {
                editCategoryView!!.visibility = View.VISIBLE
                if (status.equals("1")) {
                    deleteCategoryView!!.visibility = View.GONE
                } else {
                    deleteCategoryView!!.visibility = View.VISIBLE
                }


            } else {
                editCategoryView!!.visibility = View.GONE
            }

            editCategoryView!!.tag = position
            editCategoryView!!.setOnClickListener(onCategoryEditClick)

            deleteCategoryView!!.tag = position
            deleteCategoryView!!.setOnClickListener(onCategoryDeleteClick)

            var type = user.type
            if (type.equals("")) {
                categoryTitleLayout!!.visibility = View.GONE
            } else {
                categoryTitleLayout!!.visibility = View.VISIBLE
                categoryTypeText!!.text = type

                if (type.equals("Expense")) {
                    categoryAddView!!.setDefaultImageResId(R.mipmap.expense_section)
                    categoryAddView!!.tag = position
                    categoryAddView!!.setOnClickListener(onCategoryAddClick)
                } else {
                    categoryAddView!!.setDefaultImageResId(R.mipmap.income_section)
                    categoryAddView!!.tag = position
                    categoryAddView!!.setOnClickListener(onCategoryAddClick)
                }
            }

            var categoryType = user.categoryType

            if (categoryType.equals(Utils.TRANSACTION_EXPENSE)) {
                categoryLogoView!!.setDefaultImageResId(R.mipmap.ic_expense)
            } else {
                categoryLogoView!!.setDefaultImageResId(R.mipmap.ic_income)
            }

            categoryNameText!!.text = user.categoryName

            itemView.tag = position
            itemView.setOnClickListener(onDeleteItemClick)
        }
    }
}
