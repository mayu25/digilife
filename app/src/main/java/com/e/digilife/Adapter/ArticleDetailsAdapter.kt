package com.app.towntalk.Adapter

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.NetworkImageView
import com.e.digilife.Model.ArticleModel
import com.e.digilife.R
import com.e.digilife.View.CustomVolleyRequest
import com.e.digilife.View.TextViewPlus
import com.google.android.gms.vision.text.Line


/**
 * Created by Mayuri on 03/06/19.
 */
class ArticleDetailsAdapter() : PagerAdapter() {

    private var context: Context? = null
    private var articleList = ArrayList<ArticleModel>()

    var view: View? = null
    private var imageLoader: ImageLoader? = null
    lateinit var onShareClick: View.OnClickListener

    constructor(context: Context, articleList: ArrayList<ArticleModel>,  onShareClick: View.OnClickListener) : this() {
        this.context = context
        this.articleList = articleList
        this.onShareClick = onShareClick
        imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val view = LayoutInflater.from(container.context).inflate(R.layout.activity_article_details_viewpager_item,container,false)

        //view = container
        val vh = ViewHolder()
       // inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        //val view = inflater.inflate(R.layout.activity_article_details_viewpager_item, container, false)

        vh.articleView = view?.findViewById(R.id.articleView) as NetworkImageView
        vh.articleTimeText = view.findViewById(R.id.articleTimeText) as TextViewPlus
        vh.articleTitleText = view.findViewById(R.id.articleTitleText) as TextViewPlus
        vh.articleDescriptionText = view.findViewById(R.id.articleDescriptionText) as TextViewPlus
        vh.articleDescriptionText = view.findViewById(R.id.articleDescriptionText) as TextViewPlus
        vh.articleShareLayout = view.findViewById(R.id.articleShareLayout) as LinearLayout

        vh.articleTimeText!!.text = articleList[position].articelDate
        vh.articleTitleText!!.text = articleList[position].articleTitle
        vh.articleDescriptionText!!.text = articleList[position].articleDescription

        var imagePath = articleList[position].articleImage
        if(!imagePath.equals(""))
        {
            imageLoader!!.get(
                imagePath,
                ImageLoader.getImageListener(vh.articleView!!, R.mipmap.place_holder_img, R.mipmap.place_holder_img)
            )
            vh.articleView!!.setImageUrl(imagePath, imageLoader)
        }

        vh.articleShareLayout!!.tag = position
        vh.articleShareLayout!!.setOnClickListener(onShareClick)

        container.addView(view)

        return view
    }



    override fun isViewFromObject(v: View, `object`: Any): Boolean {
        // Return the current view
        return v === `object` as View
    }

    override fun getCount(): Int {
        return articleList.size
    }

    private inner class ViewHolder {
        var articleView: NetworkImageView? = null
        var articleTimeText: TextViewPlus? = null
        var articleTitleText: TextViewPlus? = null
        var articleDescriptionText: TextViewPlus? = null
        var articleShareLayout:LinearLayout ? =null
    }

    override fun destroyItem(parent: ViewGroup, position: Int, `object`: Any) {
        // Remove the view from view group specified position
        parent.removeView(`object` as View)
    }

    private val disable = false



}




