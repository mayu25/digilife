package com.app.towntalk.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.CheckBox
import android.widget.ImageView
import com.android.volley.toolbox.ImageLoader
import com.e.digilife.Model.DataModel
import com.e.digilife.Model.EMRModel
import com.e.digilife.R
import com.e.digilife.View.CustomVolleyRequest
import com.e.digilife.View.TextViewPlus
import com.e.digilife.View.Utils

/**
 * Created by Mayuri on 22/05/19.
 */
class ImageGridviewAdapter : BaseAdapter {

    private var context: Context? = null
    private var dataList = ArrayList<DataModel>()
    var onItemClick: View.OnClickListener

    var onSelectClick: View.OnClickListener
    var onUnSelectClick: View.OnClickListener
    private var imageLoader: ImageLoader? = null

    constructor(
        context: Context,
        emrList: ArrayList<DataModel>,
        onItemClick: View.OnClickListener,
        onSelectClick: View.OnClickListener,
        onUnSelectClick: View.OnClickListener
    ) : super() {
        this.context = context
        this.dataList = emrList
        this.onItemClick = onItemClick
        this.onSelectClick = onSelectClick
        this.onUnSelectClick = onUnSelectClick

        imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();
    }

    var view: View? = null
    private lateinit var inflater: LayoutInflater


    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

        view = convertView
        val vh = ViewHolder()

        if (convertView == null) {
            inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        }

        view = inflater.inflate(R.layout.activity_emr_gridview_item, null)

        vh.imageView = view?.findViewById(R.id.imageView) as ImageView
        vh.imgNameText = view?.findViewById(R.id.imgNameText) as TextViewPlus
        vh.imgCheckbox = view?.findViewById(R.id.imgCheckbox) as CheckBox

        vh.imgNameText!!.text = dataList.get(position).fileName
        var imagePath = dataList.get(position).filePath
        var fileType = dataList.get(position).fileType

        if (fileType.equals(Utils.FAVOURITE_MUSIC)) {

            imageLoader!!.get("", ImageLoader.getImageListener(vh.imageView!!, R.mipmap.place_holder_music, R.mipmap.place_holder_music))

        } else if (fileType.equals(Utils.DOCUMENT)) {
            if (!imagePath.equals(""))
                imageLoader!!.get(
                    imagePath,
                    ImageLoader.getImageListener(vh.imageView!!, R.mipmap.place_holder_folder, R.mipmap.place_holder_folder)
                )
        } else {
            if (!imagePath.equals(""))
                imageLoader!!.get(
                    imagePath,
                    ImageLoader.getImageListener(vh.imageView!!, R.mipmap.place_holder_img, R.mipmap.place_holder_img)
                )

        }

        vh.imageView!!.tag = position
        vh.imageView!!.setOnClickListener(onItemClick)

        vh.imgCheckbox!!.tag = position
        vh.imgCheckbox!!.setOnClickListener(onSelectClick)


        vh.imgCheckbox!!.setOnCheckedChangeListener { buttonView, isChecked ->

                if (isChecked) {
                    vh.imgCheckbox!!.tag = position
                    vh.imgCheckbox!!.setOnClickListener(onSelectClick)
                    dataList.get(position).setSelected(buttonView.isChecked());
                } else {
                    vh.imgCheckbox!!.tag = position
                    vh.imgCheckbox!!.setOnClickListener(onUnSelectClick)
                    dataList.get(position).setSelected(false);
                }

        }


        if (Utils.VISIBLE_CHECKBOX.equals("1"))
            vh.imgCheckbox!!.visibility = View.VISIBLE


        return view
    }


    override fun getItem(position: Int): Any {
        return dataList[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dataList.size
    }

    inner class ViewHolder {
        var imageView: ImageView? = null
        var imgNameText: TextViewPlus? = null
        var imgCheckbox: CheckBox? = null

    }


}




