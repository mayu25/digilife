package com.e.digilife.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.e.digilife.Model.PillReminderModel
import com.e.digilife.R
import com.e.digilife.View.TextViewPlus
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import com.jaredrummler.materialspinner.MaterialSpinner
import java.text.SimpleDateFormat
import java.util.*
import android.support.design.widget.Snackbar


// Created by $Mayuri on 21-06-2019.

class PillTimeAdapter() : RecyclerView.Adapter<PillTimeAdapter.ViewHolder>() {


    private var context: Context? = null
    private var tabletTimeList = ArrayList<PillReminderModel>()
    val medicineTypes = arrayOf("Tablet", "Capsules", "Tea spoon", "Table spoon", "Drops", "Injections", "Inhalers")
    lateinit var onDeleteItemClick: View.OnClickListener

    constructor(
        context: Context,
        dataList: ArrayList<PillReminderModel>,
        onDeleteItemClick: View.OnClickListener

    ) : this() {
        this.context = context
        this.tabletTimeList = dataList
        this.onDeleteItemClick = onDeleteItemClick

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PillTimeAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.activity_pill_time_item_view, parent, false)
        return ViewHolder(v)
    }

    var isSpinnerInitial: Boolean = true

    override fun onBindViewHolder(holder: PillTimeAdapter.ViewHolder, position: Int) {

        /* val dataAdapter = ArrayAdapter<String>(context, R.layout.spinner_item_view, R.id.dayLetterText, medicineTypes)
         holder.tabletTimepinner!!.setAdapter(dataAdapter)
         holder.tabletTimepinner!!.setSelection(position, false);*/
        holder.tabletTimepinner!!.setItems(
            "Tablet",
            "Capsules",
            "Tea spoon",
            "Table spoon",
            "Drops",
            "Injections",
            "Inhalers"
        )


        var pillType = tabletTimeList.get(position).pillType
        holder.tabletTimepinner!!.setText(pillType)


        holder.tabletTimepinner!!.setOnItemSelectedListener({ view, pos, id, item ->
            Toast.makeText(context, item.toString(), Toast.LENGTH_SHORT).show()
            tabletTimeList.get(position).pillType = item.toString()
        })


        var pillId = tabletTimeList.get(position).pillId
        if (pillId.equals("1")) {
            holder.deleteView!!.visibility = View.GONE
        } else {
            holder.deleteView!!.visibility = View.VISIBLE
        }

        var time = tabletTimeList.get(position).pillRemiderTime

        if (!time.equals("")) {
            holder.timeTextView!!.setText(time)
            holder.am_pmTextView!!.setText(" " + tabletTimeList.get(position).pillTimeAM_PM)
        }
        holder.pillCountET!!.setText(tabletTimeList.get(position).pillCount)

        holder.pillCountET!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {
                tabletTimeList.get(position).pillCount = holder.pillCountET!!.text.toString()
            }
        })

        //holder.tabletTimepinner!!.setOnItemSelectedListener(this)


        holder.deleteView!!.tag = position
        holder.deleteView!!.setOnClickListener(onDeleteItemClick)

        holder.timeLayout!!.setOnClickListener {
            openTimePicker(position)
        }
    }

//    override fun onNothingSelected(parent: AdapterView<*>?) {
//    }
//
//
//    override fun onItemSelected(parent: AdapterView<*>?, view: View?, pos: Int, id: Long) {
//        val selectedItem = parent!!.getItemAtPosition(pos).toString()
//
//        if (isSpinnerInitial) {
//            isSpinnerInitial = false
//            tabletTimeList.get(position).pillType = selectedItem
//        }
//    }


    fun openTimePicker(position: Int) {
        SingleDateAndTimePickerDialog.Builder(context)
            .bottomSheet()
            .displayMinutes(true)
            .displayHours(true)
            .displayDays(false)
            .displayMonth(false)
            .displayYears(false)
            .displayDaysOfMonth(false)
            .displayListener(object : SingleDateAndTimePickerDialog.DisplayListener {
                override fun onDisplayed(picker: SingleDateAndTimePicker) {
                    //retrieve the SingleDateAndTimePicker
                }
            })
            .title("Select Time")
            .titleTextColor(context!!.getResources().getColor(R.color.blackColor))
            .backgroundColor(context!!.getResources().getColor(R.color.whiteColor))
            .mainColor(context!!.getResources().getColor(R.color.blackColor))
            .curved()
            .listener(object : SingleDateAndTimePickerDialog.Listener {
                override fun onDateSelected(date: Date) {

                    tabletTimeList.get(position).pillRemiderTime = timeConvertor(date)
                    tabletTimeList.get(position).pillTimeAM_PM = timeAm_Pm(date)
                    notifyItemChanged(position)
                }

            }).display()
    }


    fun remove(position: Int) {
        tabletTimeList.removeAt(position)
        notifyDataSetChanged()
        //notifyItemRangeChanged(position, tabletTimeList.size)
    }

    override fun getItemCount(): Int {
        return tabletTimeList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var dayLetterText: TextViewPlus? = null
        var tabletTimepinner: MaterialSpinner? = null
        var deleteView: ImageView? = null
        var timeTextView: TextView? = null
        var am_pmTextView: TextView? = null
        var timeLayout: LinearLayout? = null
        var pillCountET: EditText? = null

        init {
            dayLetterText = itemView.findViewById(R.id.dayLetterText)
            tabletTimepinner = itemView.findViewById(R.id.tabletTimepinner)
            deleteView = itemView.findViewById(R.id.deleteView)
            timeTextView = itemView.findViewById(R.id.timeTextView)
            am_pmTextView = itemView.findViewById(R.id.am_pmTextView)
            timeLayout = itemView.findViewById(R.id.timeLayout)
            pillCountET = itemView.findViewById(R.id.pillCountET)
        }
    }

    fun timeConvertor(date: Date): String {
        val outputFormat = SimpleDateFormat("hh:mm", Locale.US)
        val output = outputFormat.format(date)

        return output
    }

    fun timeAm_Pm(date: Date): String {
        val outputFormat = SimpleDateFormat("aa", Locale.US)
        val output = outputFormat.format(date)

        return output
    }
}
