package com.e.digilife.Adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.e.digilife.Model.PillReminderModel
import com.e.digilife.R
import com.google.firebase.database.ThrowOnExtraProperties

// Created by $Mayuri on 21-06-2019.

class PillTodaysReminderRecyclerAdapter() : RecyclerView.Adapter<PillTodaysReminderRecyclerAdapter.ViewHolder>() {
    private var context: Context? = null
    private var dataList = ArrayList<PillReminderModel>()

    constructor(context: Context, dataList: ArrayList<PillReminderModel>) : this() {
        this.context = context
        this.dataList = dataList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PillTodaysReminderRecyclerAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.activity_pill_reminder_todays_reminder_view, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: PillTodaysReminderRecyclerAdapter.ViewHolder, position: Int) {

        holder.titleTextview!!.setText(dataList[position].todayType)


        if (position == 0) {
            holder.timeTextView.setText("09:30 am")
        } else if (position == 1) {
            holder.timeTextView.setText("12:30 pm")
        } else if (position == 2) {
            holder.timeTextView.setText("06:30 pm")
        } else {
            holder.timeTextView.setText("08:30 pm")
        }


        if (dataList[position].isData.equals("1")) {
            holder.pillTV1.setText(dataList[position].pill1)
        } else if (dataList[position].isData.equals("2")) {
            holder.pillTV1.setText(dataList[position].pill1)
            holder.pillTV2.setText(dataList[position].pill2)
        } else {
            holder.pillTV1.setText(dataList[position].pill1)
            holder.pillTV2.setText(dataList[position].pill2)
            holder.pillTV3.setText(dataList[position].pill3)
        }


    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        lateinit var titleTextview: TextView
        lateinit var timeTextView: TextView
        lateinit var pillTV1: TextView
        lateinit var pillTV2: TextView
        lateinit var pillTV3: TextView

        init {
            titleTextview = itemView.findViewById(R.id.titleTextview)
            timeTextView = itemView.findViewById(R.id.timeTextView)
            pillTV1 = itemView.findViewById(R.id.pillTV1)
            pillTV2 = itemView.findViewById(R.id.pillTV2)
            pillTV3 = itemView.findViewById(R.id.pillTV3)
        }

    }
}
