package com.e.digilife.Activity

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.TrascationCategoryRecyclerAdapter
import com.e.digilife.Model.WalletModel
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

class TrascationCategoryActivity : AppCompatActivity() {


    val TAG = "TrascationCategoryActivity"
    val activity = this@TrascationCategoryActivity
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null

    lateinit var categoryRecyclerView: RecyclerView
    lateinit var loaderLayout: LinearLayout
    lateinit var backView: ImageView
    lateinit var titleText: TextView
    lateinit var searchView: ImageView
    lateinit var searchText: EditText

    var client_id: String = ""
    var isSearch: Boolean = false

    private var categoryList = ArrayList<WalletModel>()
    private var searchList = ArrayList<WalletModel>()
    private var tempCategoryList = ArrayList<WalletModel>()
    var adapter: TrascationCategoryRecyclerAdapter? = null


    var TYPE: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_category_trascation)

        requestQueue = Volley.newRequestQueue(this)
        prefs = this.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)

        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }

        init()

        if (intent != null) {
            TYPE = intent.getStringExtra("type")

        }

        val linearLayoutManager = LinearLayoutManager(activity)
        categoryRecyclerView.setLayoutManager(linearLayoutManager)
        categoryRecyclerView.setHasFixedSize(true)
        adapter = TrascationCategoryRecyclerAdapter(
            activity,
            categoryList,
            onItemClick,
            onCategoryAddClick,
            TYPE,
            editCategoryView, deleteCategoryView
        )
        categoryRecyclerView.adapter = adapter

        getCategoryList("")

    }

    fun init() {
        loaderLayout = findViewById(R.id.loaderLayout)
        categoryRecyclerView = findViewById(R.id.categoryRecyclerView)
        backView = findViewById(R.id.backView)
        titleText = findViewById(R.id.titleText)
        searchText = findViewById(R.id.searchText)
        searchView = findViewById(R.id.searchView)

        backView.setOnClickListener(clickListener)
        searchView.setOnClickListener(clickListener)

        searchText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {
                var searchText = editable.toString()

                if (searchText.equals("")) {
                    searchList.clear()
                    tempCategoryList = categoryList
                    adapter = TrascationCategoryRecyclerAdapter(
                        activity,
                        categoryList,
                        onItemClick,
                        onCategoryAddClick,
                        TYPE,
                        editCategoryView, deleteCategoryView
                    )
                    categoryRecyclerView.adapter = adapter
                } else {
                    getSearchValue(searchText)
                }
            }
        })
        val font = Typeface.createFromAsset(assets, resources.getString(R.string.popins_regular))
        searchText.setTypeface(font)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                var intent = Intent()
                setResult(Activity.RESULT_CANCELED, intent)
                finish()
            }
            R.id.searchView -> {
                if (isSearch == false) {
                    isSearch = true
                    titleText.visibility = View.GONE
                    searchText.visibility = View.VISIBLE
                } else {
                    isSearch = false
                    titleText.visibility = View.VISIBLE
                    searchText.visibility = View.GONE
                }
            }
        }
    }

    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            var intent = Intent()
            intent.putExtra("cName", tempCategoryList.get(position).categoryName)
            intent.putExtra("cId", tempCategoryList.get(position).categoryID)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    var onCategoryAddClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            var type = tempCategoryList.get(position).type
            if (type.equals("Expense")) {
                type = "expense"
            } else {
                type = "income"
            }
            showAddCategoryDialog(type)
        }
    }

    var editCategoryView: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            var catId = tempCategoryList.get(position).categoryID
            var catName = tempCategoryList.get(position).categoryName
            var type = tempCategoryList.get(position).categoryType
            showEditCategoryDialog(catId, catName, type)
        }
    }

    var deleteCategoryView: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            var catId = tempCategoryList.get(position).categoryID
            var catName = tempCategoryList.get(position).categoryName
            var type = tempCategoryList.get(position).categoryType
            showDeleteDialog("Are you sure you want to delete ?", catId)
        }
    }


    fun getSearchValue(text: String) {
        searchList.clear()
        for (i in 0..(categoryList.size - 1)) {
            var name = categoryList.get(i).categoryName
            if ((name.toLowerCase()).contains(text.toLowerCase())) {

                searchList.add(categoryList.get(i))
            }
        }
        tempCategoryList = searchList
        adapter = TrascationCategoryRecyclerAdapter(
            activity,
            searchList,
            onItemClick,
            onCategoryAddClick,
            TYPE,
            editCategoryView, deleteCategoryView
        )
        categoryRecyclerView.adapter = adapter

    }


    private fun getCategoryList(type: String) {
        if (type.equals(""))
            loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.GET_CATEGORY,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        val dataArray = jsonObj.getJSONArray("data")
                        if (dataArray.length() > 0) {
                            setCategoryValue(dataArray)
                        }

                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    Log.d("Error.Response", error.toString())
                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                Log.e(" JSONData :", params.toString())
                return params
            }
        }
        Log.e(" API :", EndPoints.GET_CATEGORY)
        requestQueue.add(stringRequest)
    }

    var expenseArray = JSONArray()
    var incomeArray = JSONArray()
    var categoryArray = JSONArray()
    var expenseObj = JSONObject()
    var incomeObj = JSONObject()

    fun setCategoryValue(jsonArray: JSONArray) {
        categoryList.clear()
        tempCategoryList.clear()
        categoryArray = JSONArray()
        expenseArray = JSONArray()
        incomeArray = JSONArray()
        expenseObj = JSONObject()
        incomeObj = JSONObject()

        for (i in 0..(jsonArray.length() - 1)) {
            var jObj = jsonArray.getJSONObject(i)
            var categoryType = jObj.getString("category_type")
            if (categoryType.equals(Utils.TRANSACTION_EXPENSE)) {
                expenseArray.put(jObj)
            } else {
                incomeArray.put(jObj)
            }
        }

        incomeObj.put("title", "Income")
        incomeObj.put("data", incomeArray)
        expenseObj.put("title", "Expense")
        expenseObj.put("data", expenseArray)

        categoryArray.put(incomeObj)
        categoryArray.put(expenseObj)
        Log.e("categoryArray :: ", categoryArray.toString())

        for (j in 0..(categoryArray.length() - 1)) {
            var jObj = categoryArray.getJSONObject(j)
            var title = jObj.getString("title")
            var jArry = jObj.getJSONArray("data")
            for (k in 0..(jArry.length() - 1)) {
                var jObj = jArry.getJSONObject(k)
                var dataModel = WalletModel()
                dataModel.categoryID = jObj.getString("id")
                dataModel.categoryName = jObj.getString("category_name")
                dataModel.categoryType = jObj.getString("category_type")
                dataModel.categoryStatus = jObj.getString("tr_status")
                if (k == 0) {
                    dataModel.type = title
                } else {
                    dataModel.type = ""
                }
                categoryList.add(dataModel)
            }

        }
        tempCategoryList = categoryList

        adapter!!.notifyDataSetChanged()
    }

    fun hideKeyboard() {
        val inputManager: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputManager.hideSoftInputFromWindow(currentFocus.windowToken, InputMethodManager.SHOW_FORCED)
    }

    //TODO: Add Category..............

    private fun addCategory(name: String, type: String) {

        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.ADD_CATEGORY,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    //  loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        getCategoryList("add")

                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    Log.d("Error.Response", error.toString())
                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["category_name"] = name
                params["category_type"] = type
                Log.e(" JSONData :", params.toString())
                return params
            }
        }
        Log.e(" API :", EndPoints.ADD_CATEGORY)
        requestQueue.add(stringRequest)
    }

    //TODO: Edit Category..........

    private fun editCategory(id: String, name: String, type: String) {

        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.EDIT_CATEGORY,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    //  loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        getCategoryList("edit")

                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    Log.d("Error.Response", error.toString())
                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["id"] = id
                params["category_name"] = name
                params["category_type"] = type
                Log.e(" JSONData :", params.toString())
                return params
            }
        }
        Log.e(" API :", EndPoints.EDIT_CATEGORY)
        requestQueue.add(stringRequest)
    }

    //TODO: Delete Category........

    private fun deleteCategory(id: String) {

        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(Request.Method.POST, EndPoints.DELETE_CATEGORY,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        getCategoryList("delete")

                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    Log.d("Error.Response", error.toString())
                }
            }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["id"] = id
                Log.e(" JSONData :", params.toString())
                return params
            }
        }
        Log.e(" API :", EndPoints.DELETE_CATEGORY)
        requestQueue.add(stringRequest)
    }

    fun showAddCategoryDialog(type: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = activity!!.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.create_folder_view, null)
        dialog.setContentView(view)

        val createText = dialog.findViewById(R.id.createText) as TextView
        val folderNameEdittext = dialog.findViewById(R.id.folderNameEdittext) as EditText
        val doneBtn = dialog.findViewById(R.id.doneBtn) as Button
        folderNameEdittext.setHint("Enter Category Name")
        createText.setText("Create Category")

        val font = Typeface.createFromAsset(activity!!.assets, resources.getString(R.string.popins_regular))
        folderNameEdittext.setTypeface(font)
        doneBtn.setTypeface(font)

        doneBtn.setOnClickListener {
            if (folderNameEdittext.text.toString().equals("")) {
                Toast.makeText(activity, "Enter Category Name", Toast.LENGTH_SHORT).show()
            } else {
                if (Utils.checkInternetConnection(activity))
                    addCategory(folderNameEdittext.text.toString(), type)
                else
                    Utils.showMessageDialog(
                        activity!!,
                        getString(R.string.app_name),
                        getString(R.string.check_internet)
                    )
                dialog.dismiss()
            }
        }
        var window = dialog.getWindow();
        window.setLayout(700, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }

    fun showEditCategoryDialog(id: String, name: String, type: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = activity!!.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.create_folder_view, null)
        dialog.setContentView(view)

        val createText = dialog.findViewById(R.id.createText) as TextView
        val folderNameEdittext = dialog.findViewById(R.id.folderNameEdittext) as EditText
        val doneBtn = dialog.findViewById(R.id.doneBtn) as Button
        folderNameEdittext.setHint("Enter Category Name")
        createText.setText("Edit Category")
        folderNameEdittext.setText(name)

        val font = Typeface.createFromAsset(activity!!.assets, resources.getString(R.string.popins_regular))
        folderNameEdittext.setTypeface(font)
        doneBtn.setTypeface(font)

        doneBtn.setOnClickListener {
            if (folderNameEdittext.text.toString().equals("")) {
                Toast.makeText(activity, "Enter Category Name", Toast.LENGTH_SHORT).show()
            } else {
                if (Utils.checkInternetConnection(activity))
                    editCategory(id, folderNameEdittext.text.toString(), type)
                else
                    Utils.showMessageDialog(
                        activity!!,
                        getString(R.string.app_name),
                        getString(R.string.check_internet)
                    )

                dialog.dismiss()
            }
        }
        var window = dialog.getWindow();
        window.setLayout(700, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }

    fun showDeleteDialog(message: String, id: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = activity.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.confirmation_dialog, null)
        dialog.setContentView(view)

        val cancleBtn = dialog.findViewById(R.id.cancleText) as TextView
        val okBtn = dialog.findViewById(R.id.okBtn) as Button
        val dialogText = dialog.findViewById(R.id.dialogText) as TextView
        dialogText.setText(message)

        val font = Typeface.createFromAsset(assets, resources.getString(R.string.popins_semi_bold))
        okBtn.setTypeface(font)

        okBtn.setOnClickListener {
            deleteCategory(id)
            dialog.dismiss()
        }

        cancleBtn.setOnClickListener {
            dialog.dismiss()
        }

        var window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }
}

