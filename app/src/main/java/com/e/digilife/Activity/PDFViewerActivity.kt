package com.e.digilife.Activity

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.*
import com.android.volley.RequestQueue
import com.e.digilife.R
import com.e.digilife.View.Utils
import org.json.JSONArray
import java.io.IOException

class PDFViewerActivity : AppCompatActivity() {

    val TAG = "PDFViewerActivity"
    val activity = this@PDFViewerActivity
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null

    lateinit var loaderLayout: LinearLayout
    lateinit var closeView: ImageView
    lateinit var deleteView: ImageView
    lateinit var fileNameText: TextView

    lateinit var documentLayout: LinearLayout
    lateinit var documentWebView: WebView

    var bundle: Bundle? = null
    lateinit var bitmap: Bitmap
    var filePath: String = ""
    var TYPE: String = ""
    var fileID: String = ""
    var fileName: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pdfviewer)

        bundle = intent.extras
        init()
        prefs = activity.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)

        if (bundle != null) {
            filePath = bundle!!.getString("filePath")
            TYPE = bundle!!.getString(Utils.TYPE)
            fileID = bundle!!.getString("fileID")
            fileName = bundle!!.getString("fileName")
            Log.e("filePath ::: ", filePath)

            if (!filePath.equals("")) {
                val settings = documentWebView.settings
                settings.javaScriptEnabled = true
                settings.allowFileAccessFromFileURLs = true
                settings.allowUniversalAccessFromFileURLs = true
                settings.builtInZoomControls = true

                documentWebView.setWebViewClient(object : WebViewClient() {
                    override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                        view.loadUrl(url)
                        return false
                    }
                })

                if (!filePath.equals("")) {
                    documentWebView.loadUrl("http://docs.google.com/gview?embedded=true&url=" + filePath)
                }
            }


            if (TYPE.equals(Utils.OFFLINE)) {
                deleteView.visibility = View.VISIBLE
            }
        }

        fileNameText.text = fileName
    }


    fun init() {
        loaderLayout = findViewById(R.id.loaderLayout)
        closeView = findViewById(R.id.closeView)
        deleteView = findViewById(R.id.deleteView)
        fileNameText = findViewById(R.id.fileNameText)

        documentWebView = findViewById(R.id.documentWebView)
        documentLayout = findViewById(R.id.documentLayout)

        documentLayout.setOnClickListener(clickListener)

        closeView.setOnClickListener(clickListener)
        deleteView.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.closeView -> {
                finish()

            }
            R.id.deleteView -> {
                deleteOfflineFile()
            }
        }
    }

    var offineArray = JSONArray()

    fun deleteOfflineFile() {
        val offlineStr = prefs!!.getString(Utils.SAVE_OFFLINE, "")
        if (!offlineStr.equals("[]")) {
            Utils.OFFLINE_IMG_ARRAY = JSONArray(offlineStr)
            for (i in 0..(Utils.OFFLINE_IMG_ARRAY.length() - 1)) {
                val jsonObj = Utils.OFFLINE_IMG_ARRAY.getJSONObject(i)
                val id = jsonObj.getString(Utils.OFFLINE_FILE_ID)
                if (fileID.equals(id)) {
                    Utils.OFFLINE_IMG_ARRAY.remove(i)
                    break
                }
            }
        }
        if (offineArray.length() == 0) {
            Utils.storeJSONArraylist(prefs, Utils.SAVE_OFFLINE, null)
        } else {
            Utils.storeJSONArraylist(prefs, Utils.SAVE_OFFLINE, Utils.OFFLINE_IMG_ARRAY)
        }

        finish()
        Toast.makeText(activity, "File Deleted Successfully !", Toast.LENGTH_SHORT).show()
    }

}
