package com.e.digilife.Activity


import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.View
import android.widget.*
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.TagListAdapter
import com.e.digilife.Model.TagModel
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import java.util.*
import kotlin.collections.ArrayList

class TagListActivity : AppCompatActivity() {
    lateinit var toolbar_title: TextView
    lateinit var img_popup: ImageView
    lateinit var img_plus: ImageView
    lateinit var img_close: ImageView
    lateinit var list_menu: ImageView
    lateinit var listview: ListView
    lateinit var edt_search: EditText
    var prefs: SharedPreferences? = null
    var client_id: String = ""
    lateinit var loaderLayout: LinearLayout
    var tagvalue: String = ""
    internal lateinit var requestQueue: RequestQueue
    private var tagList = ArrayList<TagModel>()
    private var tempArrayList = ArrayList<TagModel>()

    var adapter: TagListAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tag_list_activity_layout)
        init()
        requestQueue = Volley.newRequestQueue(this@TagListActivity)
        adapter = TagListAdapter(this@TagListActivity, tagList, onItemClick)
        listview.adapter = adapter
        if (Utils.checkInternetConnection(this@TagListActivity)) {
            getTagList("")
        } else {
            Utils.showMessageDialog(
                this@TagListActivity,
                getString(R.string.app_name),
                getString(R.string.check_internet)
            )
        }
        edt_search.addTextChangedListener(object : TextWatcher {

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                val textlength = s.length

                var tempArrayList = ArrayList<TagModel>()

                for (c in tagList) {

                    if (textlength <= c.tag_title.length) {

                        if (c.tag_title.toLowerCase().contains(s.toString().toLowerCase())) {
                            tempArrayList.add(c)

                        }
                    }
                }

                if (tempArrayList.size == 0) {
                    Toast.makeText(this@TagListActivity, "No tag Available", Toast.LENGTH_SHORT).show()
                }

                adapter = TagListAdapter(this@TagListActivity, tempArrayList, onItemClick)
                listview.setAdapter(adapter)
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {
                // TODO Auto-generated method stub


            }

            override fun afterTextChanged(s: Editable) {
                edt_search.setError(null)
            }
        })


    }

    fun init() {

        toolbar_title = findViewById(R.id.toolbar_title)
        img_popup = findViewById(R.id.img_popup)
        img_plus = findViewById(R.id.img_plus)
        list_menu = findViewById(R.id.list_menu)
        img_close = findViewById(R.id.img_close)
        listview = findViewById(R.id.listview)
        loaderLayout = findViewById(R.id.loaderLayout)
        edt_search = findViewById(R.id.edt_search)
        img_popup.visibility = View.GONE;
        img_close.visibility = View.GONE;
        img_plus.visibility = View.VISIBLE;
        toolbar_title.setText(R.string.tags)
        img_plus.setOnClickListener(clickListener)
        list_menu.setOnClickListener(clickListener)

        prefs = applicationContext!!.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")

        }

    }

    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int


            val previousScreen = Intent(getApplicationContext(), AddTaskActivity::class.java)
            previousScreen.putExtra(Utils.TAG_NAME, tagList.get(position).tag_title)
            setResult(1000, previousScreen)
            finish()
        }
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {

            R.id.img_plus -> {
                Show_Tag_Dialog()
            }
            R.id.list_menu -> {
                finish()
            }

        }
    }

    fun Show_Tag_Dialog() {
        val dialogBuilder = android.app.AlertDialog.Builder(this@TagListActivity)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.custom_create_category_dialog_layout, null)
        dialogBuilder.setView(dialogView)
        val editText = dialogView.findViewById<View>(R.id.edit_category) as EditText
        editText.setMovementMethod(ScrollingMovementMethod())
        val btn_cancle = dialogView.findViewById<View>(R.id.btn_cancle) as Button
        val btn_save = dialogView.findViewById<View>(R.id.btn_save) as Button
        val b = dialogBuilder.create()
        b.show()

        btn_cancle.setOnClickListener {
            b.dismiss()
        }
        btn_save.setOnClickListener {

            b.dismiss()
            if (editText.text.toString().length != 0) {
                tagvalue = editText.text.toString();
                if (Utils.checkInternetConnection(this@TagListActivity)) {
                    ADDTAG()
                } else {
                    Utils.showMessageDialog(
                        this@TagListActivity,
                        getString(R.string.app_name),
                        getString(R.string.check_internet)
                    )
                }

            } else {
                Toast.makeText(this@TagListActivity, "Please enter tag name", Toast.LENGTH_SHORT).show()
            }

        }


    }

    private fun ADDTAG() {

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.ADD_TAG,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        Toast.makeText(this@TagListActivity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                        requestQueue.getCache().clear();
                        if (Utils.checkInternetConnection(this@TagListActivity)) {
                            getTagList("")
                        } else {
                            Utils.showMessageDialog(
                                this@TagListActivity,
                                getString(R.string.app_name),
                                getString(R.string.check_internet)
                            )
                        }
                    } else {
                        Toast.makeText(this@TagListActivity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {

            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["tag_title"] = tagvalue
                params["r"] = "252"
                params["g"] = "218"
                params["b"] = "58"
                return params

            }
        }

        requestQueue.add(stringRequest)
    }

    private fun getTagList(type: String) {

        if (type.equals("")) {
            loaderLayout.visibility = View.VISIBLE
        }

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.GET_TAG_LIST,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    loaderLayout.visibility = View.GONE
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        val dataArray = jsonObj.getJSONArray("data")
                        if (dataArray.length() > 0) {
                            setTagList(dataArray)
                        }
                    } else {
                        Toast.makeText(this@TagListActivity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {

            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id

                return params

            }
        }


        requestQueue.add(stringRequest)
    }


    fun setTagList(jsonArray: JSONArray) {

        tagList.clear()

        val tagModel = TagModel()
        tagModel.tag_title = "Priority"
        tagList.add(tagModel)


        val tagModel1 = TagModel()
        tagModel1.tag_title = "Important"
        tagList.add(tagModel1)

        val tagModel2 = TagModel()
        tagModel2.tag_title = "In Progress"
        tagList.add(tagModel2)


        val tagModel3 = TagModel()
        tagModel3.tag_title = "Deadline"
        tagList.add(tagModel3)

        val tagModel4 = TagModel()
        tagModel4.tag_title = "Family"
        tagList.add(tagModel4)

        val tagModel5 = TagModel()
        tagModel5.tag_title = "Trackbook"
        tagList.add(tagModel5)

        val tagModel6 = TagModel()
        tagModel6.tag_title = "Science project"
        tagList.add(tagModel6)

        val tagModel7 = TagModel()
        tagModel7.tag_title = "Low priority"
        tagList.add(tagModel7)

        val tagModel8 = TagModel()
        tagModel8.tag_title = "Not Critical"
        tagList.add(tagModel8)

        val tagModel9 = TagModel()
        tagModel9.tag_title = "Top Priority"
        tagList.add(tagModel9)



        for (i in 0..(jsonArray.length() - 1)) {
            val jsonObj = jsonArray.getJSONObject(i)
            val tagModel = TagModel()

            tagModel.tag_id = jsonObj.getString("tag_id")
            tagModel.client_id = jsonObj.getString("client_id")
            tagModel.tag_title = jsonObj.getString("tag_title")
            tagModel.r = jsonObj.getString("r")
            tagModel.g = jsonObj.getString("g")
            tagModel.b = jsonObj.getString("b")
            tagModel.create_at = jsonObj.getString("create_at")
            tagModel.update_at = jsonObj.getString("update_at")

            tagList.add(tagModel)

        }
        adapter!!.notifyDataSetChanged()

    }

    override fun onResume() {
        super.onResume()

    }

}





