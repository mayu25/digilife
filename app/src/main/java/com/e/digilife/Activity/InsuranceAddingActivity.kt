package com.e.digilife.Activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaScannerConnection
import android.os.*
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.Volley
import com.e.digilife.CropLibrary.CropScanImageActivity
import com.e.digilife.Model.DataModel
import com.e.digilife.Model.PersonalModel
import com.e.digilife.R
import com.e.digilife.View.InputValidation
import com.e.digilife.View.Utils
import com.e.digilife.View.VolleyMultipartRequest
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import com.shurlock.View.EndPoints
import com.squareup.picasso.Picasso
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.Serializable
import java.net.HttpURLConnection
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*


class InsuranceAddingActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    val TAG = "InsuranceAddingActivity"
    val activity = this@InsuranceAddingActivity
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null
    lateinit var inputValidation: InputValidation

    var bundle: Bundle? = null
    var client_id: String = ""
    var parentFolderId: String = ""


    lateinit var backView: ImageView
    lateinit var titleText: TextView
    lateinit var saveText: TextView
    lateinit var loaderLayout: LinearLayout
    lateinit var imageSelectionView: LinearLayout
    lateinit var cameraView: TextView
    lateinit var galleryView: TextView
    lateinit var cancelView: TextView
    lateinit var documentView: TextView
    lateinit var viewImg:TextView

    lateinit var companyEdittext: EditText
    lateinit var policyNumberEdittext: EditText
    lateinit var amountEdittext: EditText
    lateinit var startDateLayout: RelativeLayout
    lateinit var endDateLayout: RelativeLayout
    lateinit var startDateText: TextView
    lateinit var endDateText: TextView
    lateinit var reminderSpinner: Spinner
    lateinit var attachPolicyView: ImageView
    lateinit var attachReceiptView: ImageView

    var insuranceData: Serializable = ""
    var EDIT_INSURANCE: String = ""
    internal var dataModel = PersonalModel()

    var insurance_id: String = ""
    var edit_companyName: String = ""
    var edit_policyNumber: String = ""
    var edit_amount: String = ""
    var edit_attachementPolicy: String = ""
    var edit_attachementReceipt: String = ""
    var edit_days: String = ""


    private lateinit var runnable: Runnable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_insurance_adding)
        requestQueue = Volley.newRequestQueue(this)

        /* if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
             val builder = StrictMode.ThreadPolicy.Builder().permitAll().build()
             StrictMode.setThreadPolicy(builder);

         }*/

        prefs = this.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }

        init()

        if (intent != null) {

            EDIT_INSURANCE = intent.getStringExtra(Utils.EDIT_INSURANCE)

            if (EDIT_INSURANCE.equals("1")) {
                insuranceData = intent.getSerializableExtra(Utils.INSURANCE_OBJ)
                if (!insuranceData.equals("")) {
                    dataModel = insuranceData as PersonalModel
                    insurance_id = dataModel.i_Id
                    edit_companyName = dataModel.i_PolicyName
                    edit_policyNumber = dataModel.i_PolicyNo
                    edit_amount = dataModel.i_Amount
                    startDate = dataModel.i_StartDate
                    endDate = dataModel.i_EndDate
                    edit_attachementPolicy = dataModel.i_Attachement
                    edit_attachementReceipt = dataModel.i_Receipt
                    edit_days = dataModel.i_Days
                    reminderPosition = dataModel.i_Reminder
                }

                companyEdittext.setText(edit_companyName)
                policyNumberEdittext.setText(edit_policyNumber)
                amountEdittext.setText(edit_amount)
                var sDate = apiDateConvertor(startDate)
                startDateText.setText(sDate)
                var eDate = apiDateConvertor(endDate)
                endDateText.setText(eDate)

                var spinnerSelection = reminderPosition.toInt() - 1
                reminderSpinner.setSelection(spinnerSelection)

                titleText.text = "Edit Policy"
                val handler = Handler()

                if (!edit_attachementPolicy.equals("")) {
                    edit_attachementPolicy = EndPoints.ROOT + edit_attachementPolicy

                    Picasso.with(activity).load(edit_attachementPolicy).placeholder(R.mipmap.ic_username)
                        .into(attachPolicyView)

                    convertPolicyBitmap(edit_attachementPolicy)
                }

                if (!edit_attachementReceipt.equals("")) {
                    edit_attachementReceipt = EndPoints.ROOT + edit_attachementReceipt

                    Picasso.with(activity).load(edit_attachementReceipt).placeholder(R.mipmap.ic_username)
                        .into(attachReceiptView)
                    convertReceiptBitmap(edit_attachementReceipt)

//                    try {
//                        val url = URL(edit_attachementReceipt)
//                        runnable = Runnable {
//                            // do your work
//                            handler.postDelayed(runnable, 2000)
//                            receiptBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream())
//                        }
//                        handler.postDelayed(runnable, 2000)
//
//                    } catch (e: IOException) {
//                        println(e)
//                    }
                }

            } else {
                titleText.text = "Add Policy"
            }
        }
    }


    class policyRunnable(url: URL) : Runnable {

        override fun run() {
            // Moves the current Thread into the background
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND)

        }
    }

    fun init() {
        inputValidation = InputValidation(activity)
        imageSelectionView = findViewById(R.id.imageSelectionView)
        loaderLayout = findViewById(R.id.loaderLayout)
        titleText = findViewById(R.id.titleText)
        backView = findViewById(R.id.backView)
        saveText = findViewById(R.id.saveText)

        companyEdittext = findViewById(R.id.companyEdittext)
        policyNumberEdittext = findViewById(R.id.policyNumberEdittext)
        amountEdittext = findViewById(R.id.amountEdittext)
        startDateLayout = findViewById(R.id.startDateLayout)
        endDateLayout = findViewById(R.id.endDateLayout)
        reminderSpinner = findViewById(R.id.reminderSpinner)
        attachPolicyView = findViewById(R.id.attachPolicyView)
        attachReceiptView = findViewById(R.id.attachReceiptView)
        startDateText = findViewById(R.id.startDateText)
        endDateText = findViewById(R.id.endDateText)

        cameraView = findViewById(R.id.cameraView)
        galleryView = findViewById(R.id.galleryView)
        cancelView = findViewById(R.id.cancelView)
        viewImg = findViewById(R.id.viewImg)
        viewImg.visibility = View.VISIBLE
        documentView = findViewById(R.id.documentView)
        documentView.visibility = View.GONE

        backView.setOnClickListener(clickListener)
        saveText.setOnClickListener(clickListener)
        startDateLayout.setOnClickListener(clickListener)
        endDateLayout.setOnClickListener(clickListener)
        attachPolicyView.setOnClickListener(clickListener)
        attachReceiptView.setOnClickListener(clickListener)
        cameraView.setOnClickListener(clickListener)
        galleryView.setOnClickListener(clickListener)
        cancelView.setOnClickListener(clickListener)
        viewImg.setOnClickListener(clickListener)

        reminderSpinner.setOnItemSelectedListener(this)
        val days = arrayOf("Before 1 day", "Before 5 days", "Before 15 days", "Before 30 days")
        val aa = ArrayAdapter(this, R.layout.spinner_item, days)
        aa.setDropDownViewResource(R.layout.spinner_item)
        reminderSpinner.setAdapter(aa)
    }

    var attachmentType: String = ""

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish()
            }
            R.id.saveText -> {
                validation()
            }
            R.id.startDateLayout -> {
                openDatePicker("start")
            }
            R.id.endDateLayout -> {
                openDatePicker("end")
            }
            R.id.attachPolicyView -> {
                if (ActivityCompat.checkSelfPermission(
                        activity,
                        android.Manifest.permission.CAMERA
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    ActivityCompat.requestPermissions(
                        activity, arrayOf(
                            android.Manifest.permission.CAMERA,
                            android.Manifest.permission.READ_EXTERNAL_STORAGE,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ), 0
                    )
                } else {
                    requirePermissions = false
                    attachmentType = "policy"
                    imageSelectionView.visibility = View.VISIBLE
                }
            }
            R.id.attachReceiptView -> {
                if (ActivityCompat.checkSelfPermission(
                        activity,
                        android.Manifest.permission.CAMERA
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    ActivityCompat.requestPermissions(
                        activity, arrayOf(
                            android.Manifest.permission.CAMERA,
                            android.Manifest.permission.READ_EXTERNAL_STORAGE,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ), 0
                    )
                } else {
                    requirePermissions = false
                    attachmentType = "receipt"
                    imageSelectionView.visibility = View.VISIBLE
                }
            }
            R.id.cameraView -> {
                imageSelectionView.visibility = View.GONE
                preference = Utils.OPEN_CAMERA
                checkStoragePermission()
            }
            R.id.galleryView -> {
                imageSelectionView.visibility = View.GONE
                preference = Utils.OPEN_MEDIA
                checkStoragePermission()
            }
            R.id.cancelView -> {
                imageSelectionView.visibility = View.GONE
            }
            R.id.viewImg ->{
                imageSelectionView.visibility = View.GONE
                val intent = Intent(applicationContext, ZoomImageActivity::class.java)
                val bundle = Bundle()
                if(attachmentType.equals("policy")){
                    bundle.putString("imagePath", edit_attachementPolicy)
                }else{
                    bundle.putString("imagePath", edit_attachementReceipt)
                }

                bundle.putString(Utils.TYPE, "")
                bundle.putString("fileID", "")
                bundle.putString("fileName", "")
                intent.putExtras(bundle)
                startActivity(intent)
            }
        }
    }

    var reminderPosition: String = "1"
    private fun validation() {

        if (!inputValidation.isFieldEmpty(companyEdittext, getString(R.string.i_validation1))) {
            return
        }
        if (!inputValidation.isFieldEmpty(policyNumberEdittext, getString(R.string.i_validation2))) {
            return
        }
        if (!inputValidation.isFieldEmpty(amountEdittext, getString(R.string.i_validation3))) {
            return
        }
        if (!inputValidation.isFieldBlank(startDate, getString(R.string.i_validation4))) {
            return
        }

//        if (policyBitmap == null && edit_attachementPolicy.equals("")) {
//            Toast.makeText(activity, "Please attach policy", Toast.LENGTH_SHORT).show()
//            return
//        }
//
//        if (receiptBitmap == null && edit_attachementReceipt.equals("")) {
//            Toast.makeText(activity, "Please attach receipt", Toast.LENGTH_SHORT).show()
//            return
//        }


        var policyName = companyEdittext.text.toString()
        var policyNo = policyNumberEdittext.text.toString()
        var amount = amountEdittext.text.toString()


        if (EDIT_INSURANCE.equals("1")) {
            if (Utils.checkInternetConnection(this@InsuranceAddingActivity)) {
                editInsurance(policyName, policyNo, amount)
            } else {
                Utils.showMessageDialog(
                    this@InsuranceAddingActivity,
                    getString(R.string.app_name),
                    getString(R.string.check_internet)
                )
            }
        } else {
            if (Utils.checkInternetConnection(this@InsuranceAddingActivity)) {
                addInsurance(policyName, policyNo, amount)
            } else {
                Utils.showMessageDialog(
                    this@InsuranceAddingActivity,
                    getString(R.string.app_name),
                    getString(R.string.check_internet)
                )
            }
        }

    }


    @SuppressLint("StaticFieldLeak")
    private fun convertPolicyBitmap(imageUrl: String) {
        object : AsyncTask<Void, Void, Bitmap>() {
            override fun doInBackground(vararg voids: Void): Bitmap {
                var bitmap: Bitmap? = null
                try {
                    val url = URL(imageUrl)
                    bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream())

                } catch (e: IOException) {
                    println(e)
                }
                return bitmap!!
            }

            override fun onPostExecute(s: Bitmap) {
                policyBitmap = s
                super.onPostExecute(s)
            }
        }.execute()

    }


    @SuppressLint("StaticFieldLeak")
    private fun convertReceiptBitmap(imageUrl: String) {
        object : AsyncTask<Void, Void, Bitmap>() {
            override fun doInBackground(vararg voids: Void): Bitmap {
                var bitmap: Bitmap? = null
                try {
                    val url = URL(imageUrl)
                    bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream())

                } catch (e: IOException) {
                    println(e)
                }
                return bitmap!!
            }

            override fun onPostExecute(s: Bitmap) {
                receiptBitmap = s
                super.onPostExecute(s)
            }
        }.execute()

    }


    fun addInsurance(policyName: String, policyNo: String, amount: String) {
        loaderLayout.visibility = View.VISIBLE
        val volleyMultipartRequest = object : VolleyMultipartRequest(
            Request.Method.POST, EndPoints.ADD_INSURANCE,
            Response.Listener { response ->
                loaderLayout.visibility = View.GONE
                Log.d("response", String(response.data))
                var jsonObj = JSONObject(String(response.data))
                var status = jsonObj.getString("status")
                if (status.equals("1")) {
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                    requestQueue.getCache().clear();
                    finish()
                } else {
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT).show()
            }) {


            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("client_id", client_id)
                params.put("policy_company_name", policyName)
                params.put("policy_no", policyNo)
                params.put("start_date", startDate)
                params.put("end_date", endDate)
                params.put("amount", amount)
                params.put("reminder", reminderPosition)
                Log.e(" JSONData :", params.toString())
                return params


            }

            override fun getByteData(): Map<String, DataPart> {

                val params = HashMap<String, DataPart>()
                val imagename = System.currentTimeMillis()
                if (policyBitmap != null)
                    params["attachment"] = DataPart("$imagename.png", getFileDataFromDrawable(policyBitmap))
                if (receiptBitmap != null)
                    params["receipt"] = DataPart("$imagename.png", getFileDataFromDrawable(receiptBitmap))
                Log.e(" Images :", params.toString())
                return params

            }
        }

        volleyMultipartRequest.retryPolicy = DefaultRetryPolicy(
            0,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(volleyMultipartRequest);
    }

    fun getFileDataFromDrawable(bitmap: Bitmap?): ByteArray {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap!!.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream)
        return byteArrayOutputStream.toByteArray()
    }


    fun editInsurance(policyName: String, policyNo: String, amount: String) {
        loaderLayout.visibility = View.VISIBLE
        val volleyMultipartRequest = object : VolleyMultipartRequest(
            Request.Method.POST, EndPoints.EDIT_INSURANCE,
            Response.Listener { response ->
                loaderLayout.visibility = View.GONE
                Log.d("response", String(response.data))
                var jsonObj = JSONObject(String(response.data))
                var status = jsonObj.getString("status")
                if (status.equals("1")) {
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                    requestQueue.getCache().clear();
                    finish()
                } else {
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT).show()
            }) {


            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("id", insurance_id)
                params.put("client_id", client_id)
                params.put("policy_company_name", policyName)
                params.put("policy_no", policyNo)
                params.put("start_date", startDate)
                params.put("end_date", endDate)
                params.put("amount", amount)
                params.put("reminder", reminderPosition)
                Log.e(" JSONData :", params.toString())
                return params

            }

            override fun getByteData(): Map<String, DataPart> {

                val params = HashMap<String, DataPart>()
                val imagename = System.currentTimeMillis()
                if (policyBitmap != null)
                    params["attachment"] = DataPart("$imagename.png", getFileDataFromDrawable(policyBitmap))
                if (receiptBitmap != null)
                    params["receipt"] = DataPart("$imagename.png", getFileDataFromDrawable(receiptBitmap))
                Log.e(" Images :", params.toString())
                return params

            }
        }

        volleyMultipartRequest.retryPolicy = DefaultRetryPolicy(
            0,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(volleyMultipartRequest);
    }


    //TODO: Spinner selector............

    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {
        reminderPosition = (position + 1).toString()
    }

    override fun onNothingSelected(arg0: AdapterView<*>) {

    }

    //TODO: Date Convertor............

    fun openDatePicker(type: String) {
        SingleDateAndTimePickerDialog.Builder(activity)
            .bottomSheet()
            .displayMinutes(false)
            .displayHours(false)
            .displayDays(false)
            .displayMonth(true)
            .displayYears(true)
            .displayDaysOfMonth(true)
            .displayListener(object : SingleDateAndTimePickerDialog.DisplayListener {
                override fun onDisplayed(picker: SingleDateAndTimePicker) {
                    //retrieve the SingleDateAndTimePicker
                }
            })
            .title("Select Date")
            .titleTextColor(getResources().getColor(R.color.blackColor))
            .backgroundColor(getResources().getColor(R.color.whiteColor))
            .mainColor(getResources().getColor(R.color.blackColor))
            .curved()
            .listener(object : SingleDateAndTimePickerDialog.Listener {
                override fun onDateSelected(date: Date) {
                    var convertDate = dateConvertor(date)
                    if (type.equals("start")) {
                        startDateText.setText(convertDate)
                        endDateText.setText(nextYearDate)
                    } else {
                        endDateText.setText(convertDate)
                    }
                }
            }).display()
    }


    var nextYearDate: String = ""
    var startDate: String = ""
    var endDate: String = ""

    fun dateConvertor(date: Date): String {
        val inputFormat = SimpleDateFormat("E MMM dd yyyy HH:mm:ss 'GMT'z", Locale.ENGLISH)
        // val date = inputFormat.parse(time)
        val outputFormat = SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
        outputFormat.timeZone = TimeZone.getTimeZone("UTC")
        val output = outputFormat.format(date)
        println(output)

        val outputFormat2 = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        outputFormat2.timeZone = TimeZone.getTimeZone("UTC")
        startDate = outputFormat2.format(date)

        val cal = Calendar.getInstance()
        cal.setTime(date)
        cal.add(Calendar.YEAR, 1)
        nextYearDate = nextYearDateCount(cal.getTime())

        return output
    }

    fun nextYearDateCount(date: Date): String {
        val inputFormat = SimpleDateFormat("E MMM dd yyyy HH:mm:ss 'GMT'z", Locale.ENGLISH)
        val outputFormat = SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
        outputFormat.timeZone = TimeZone.getTimeZone("UTC")
        val output = outputFormat.format(date)

        val outputFormat2 = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        outputFormat2.timeZone = TimeZone.getTimeZone("UTC")
        endDate = outputFormat2.format(date)

        return output
    }

    fun apiDateConvertor(strDate: String): String {
        val inputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        val date = inputFormat.parse(strDate)
        val outputFormat = SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
        outputFormat.timeZone = TimeZone.getTimeZone("UTC")
        val output = outputFormat.format(date)
        return output
    }


    //TODO: policy and receipt upload images......

    private val GALLERY = 1
    private val CAMERA = 2
    var requirePermissions = true
    var preference = 0

    private fun checkStoragePermission() {

        if (ContextCompat.checkSelfPermission(
                this@InsuranceAddingActivity,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            ActivityCompat.requestPermissions(
                this@InsuranceAddingActivity,
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ), 100
            )

        } else {
            chooseFileFromDevice(preference)
        }
    }

    private fun chooseFileFromDevice(preference: Int) {

        if (preference == Utils.OPEN_MEDIA) {
            val galleryIntent = Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )

            startActivityForResult(galleryIntent, GALLERY)
        } else {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, CAMERA)
        }
    }

    var policyBitmap: Bitmap? = null
    var receiptBitmap: Bitmap? = null

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == GALLERY && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val contentURI = data.data
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
                    val path = saveImage(bitmap)

                    if (attachmentType.equals("policy")) {
                        policyBitmap = bitmap
                        attachPolicyView.setImageBitmap(bitmap)
                        edit_attachementPolicy = path
                    } else {
                        receiptBitmap = bitmap
                        attachReceiptView.setImageBitmap(bitmap)
                        edit_attachementReceipt = path
                    }


                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(this@InsuranceAddingActivity, "Failed!", Toast.LENGTH_SHORT).show()
                }

            }

        } else if (requestCode == CAMERA && resultCode == Activity.RESULT_OK) {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            if (attachmentType.equals("policy")) {
                policyBitmap = thumbnail
                attachPolicyView.setImageBitmap(thumbnail)
            } else {
                receiptBitmap = thumbnail
                attachReceiptView.setImageBitmap(thumbnail)
            }
            saveImage(thumbnail)
        }
    }

    fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallpaperDirectory = File(
            (Environment.getExternalStorageDirectory()).toString() + "Image_Directory"
        )
        // have the object build the directory structure, if needed.
        Log.d("fee", wallpaperDirectory.toString())
        if (!wallpaperDirectory.exists()) {

            wallpaperDirectory.mkdirs()
        }

        try {
            Log.d("heel", wallpaperDirectory.toString())
            val f = File(
                wallpaperDirectory, ((Calendar.getInstance()
                    .getTimeInMillis()).toString() + ".jpg")
            )
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                this,
                arrayOf(f.getPath()),
                arrayOf("image/jpeg"), null
            )
            fo.close()
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath())

            return f.getAbsolutePath()
        } catch (e1: IOException) {
            e1.printStackTrace()
        }

        return ""
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            Utils.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                chooseFileFromDevice(preference)
            } else {
                Toast.makeText(
                    this@InsuranceAddingActivity,
                    resources.getString(R.string.msg_denied_permission),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

}
