package com.e.digilife.Activity

import android.content.Context
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.view.View
import android.widget.FrameLayout
import android.widget.LinearLayout
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.e.digilife.Fragment.*
import com.e.digilife.R
import com.e.digilife.View.Utils
import org.json.JSONObject

class WalletActivity : AppCompatActivity() {

    val TAG = "WalletActivity"
    val activity = this@WalletActivity

    lateinit var loaderLayout: LinearLayout
    lateinit var walletFrameLayout: FrameLayout
    lateinit var transacationLayout: LinearLayout
    lateinit var budgetLayout: LinearLayout
    lateinit var graphLayout: LinearLayout
    lateinit var settingLayout: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_wallet)

        val navView: BottomNavigationView = findViewById(R.id.nav_view)

        val transactionFragment = Wallet_TransactionFragment()
        replaceFragment(transactionFragment)
        Utils.navItemIndex = 0
        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
    }

    fun init() {
        loaderLayout = findViewById(R.id.loaderLayout)
        walletFrameLayout = findViewById(R.id.walletFrameLayout)

    }


    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_trascation -> {
                val transactionFragment = Wallet_TransactionFragment()
                replaceFragment(transactionFragment)
                Utils.navItemIndex = 0


                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_budget -> {
                val budgetFragment = Wallet_BudgetFragment()
                replaceFragment(budgetFragment)
                Utils.navItemIndex = 1

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_graph -> {
                val graphFragment = Wallet_GraphFragment()
                replaceFragment(graphFragment)
                Utils.navItemIndex = 2

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_setting -> {
                val settingFragment = Wallet_SettingFragment()
                replaceFragment(settingFragment)
                Utils.navItemIndex = 3

                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }


    fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.walletFrameLayout, fragment).commit()
    }
}
