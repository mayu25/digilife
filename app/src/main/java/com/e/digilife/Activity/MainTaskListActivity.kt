package com.e.digilife.Activity

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.View
import android.widget.*
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.TaskCategoryListAdapter
import com.e.digilife.Model.TagModel
import com.e.digilife.Model.TaskCategoryListModel
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.shurlock.View.EndPoints
import kotlinx.android.synthetic.main.activity_article_view.*
import org.json.JSONArray
import org.json.JSONObject
import java.util.HashMap


class MainTaskListActivity : AppCompatActivity() {
    lateinit var backView: ImageView
    var adapter: TaskCategoryListAdapter? = null
    lateinit var loaderLayout: LinearLayout
    lateinit var shareView: ImageView
    lateinit var uploadView: ImageView
    lateinit var searchView: ImageView
    lateinit var notificationView: ImageView
    lateinit var plusView: ImageView
    lateinit var addFolder: ImageView
    lateinit var titleText: TextView
    lateinit var gridview: GridView
    var prefs: SharedPreferences? = null
    var client_id: String = ""
    private var taskList = ArrayList<TaskCategoryListModel>()
    internal lateinit var requestQueue: RequestQueue
    var CAT_NAME: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_task_list_layout)
        init()
        adapter = TaskCategoryListAdapter(this@MainTaskListActivity, taskList, onItemClick)
        gridview.adapter = adapter
        if (Utils.checkInternetConnection(this@MainTaskListActivity))
            getcategoryList("")
        else
            Utils.showMessageDialog(
                this@MainTaskListActivity,
                getString(R.string.app_name),
                getString(R.string.check_internet)
            )

    }

    fun init() {
        backView = findViewById(R.id.backView)
        backView.setOnClickListener(clickListener)
        shareView = findViewById(R.id.shareView)
        uploadView = findViewById(R.id.uploadView)
        searchView = findViewById(R.id.searchView)
        notificationView = findViewById(R.id.notificationView)
        titleText = findViewById(R.id.titleText)
        gridview = findViewById(R.id.gridview)
        titleText.setText(R.string.mylist)
        plusView = findViewById(R.id.plusView)
        shareView.visibility = View.GONE;
        uploadView.visibility = View.GONE;
        searchView.visibility = View.GONE;
        notificationView.visibility = View.GONE;
        plusView.visibility = View.GONE;
        loaderLayout = findViewById(R.id.loaderLayout)
        requestQueue = Volley.newRequestQueue(this@MainTaskListActivity)
        prefs = applicationContext!!.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")

        }

    }


    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish();
            }


        }
    }

    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int

            if (position == taskList.size - 1) {
                Show_category_Dialog()

            } else {
                var cat_id = taskList.get(position).cat_id

            }


        }
    }

    fun Show_category_Dialog() {
        val dialogBuilder = android.app.AlertDialog.Builder(this@MainTaskListActivity)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.custom_create_category_dialog_layout, null)
        dialogBuilder.setView(dialogView)
        val editText = dialogView.findViewById<View>(R.id.edit_category) as EditText
        editText.setMovementMethod(ScrollingMovementMethod())
        val btn_cancle = dialogView.findViewById<View>(R.id.btn_cancle) as Button
        val btn_save = dialogView.findViewById<View>(R.id.btn_save) as Button
        val b = dialogBuilder.create()
        b.show()

        btn_cancle.setOnClickListener {
            b.dismiss()
        }
        btn_save.setOnClickListener {

            b.dismiss()

            if (editText.text.toString().length != 0) {
                CAT_NAME = editText.text.toString();
                if (Utils.checkInternetConnection(this@MainTaskListActivity)) {
                    ADD_CATEGORY()
                } else {
                    Utils.showMessageDialog(
                        this@MainTaskListActivity,
                        getString(R.string.app_name),
                        getString(R.string.check_internet)
                    )
                }

            } else {
                Toast.makeText(this@MainTaskListActivity, "Please enter category", Toast.LENGTH_SHORT).show()
            }

        }


    }


    private fun getcategoryList(type: String) {

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.ADD_TASK_GET_CATEGORY_TASK_LIST,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        val dataArray = jsonObj.getJSONArray("data")
                        if (dataArray.length() > 0) {
                            setCategoryList(dataArray)
                        }

                    } else {

                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {

            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                return params

            }
        }

        requestQueue.add(stringRequest)
    }


    fun setCategoryList(jsonArray: JSONArray) {

        taskList.clear()



        for (i in 0..(jsonArray.length() - 1)) {
            val jsonObj = jsonArray.getJSONObject(i)
            val taskcategorylistmodel = TaskCategoryListModel()
            taskcategorylistmodel.count = jsonObj.getString("count")
            taskcategorylistmodel.cat_id = jsonObj.getString("cat_id")
            taskcategorylistmodel.client_id = jsonObj.getString("client_id")
            taskcategorylistmodel.cat_name = jsonObj.getString("cat_name")

            taskList.add(taskcategorylistmodel)

        }
        val taskcategorylistmodel = TaskCategoryListModel()
        taskcategorylistmodel.cat_name = " "
        taskcategorylistmodel.count = " "
        taskList.add(taskcategorylistmodel)

        adapter!!.notifyDataSetChanged()

    }

    private fun ADD_CATEGORY() {

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.ADD_TASK_CATEGORY,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        Toast.makeText(this@MainTaskListActivity, jsonObj.getString("message"), Toast.LENGTH_SHORT)
                            .show()
                        requestQueue.getCache().clear();
                        if (Utils.checkInternetConnection(this@MainTaskListActivity))
                            getcategoryList("")
                        else
                            Utils.showMessageDialog(
                                this@MainTaskListActivity,
                                getString(R.string.app_name),
                                getString(R.string.check_internet)
                            )

                    } else {
                        Toast.makeText(this@MainTaskListActivity, jsonObj.getString("message"), Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {

            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["cat_name"] = CAT_NAME

                return params

            }
        }

        requestQueue.add(stringRequest)
    }
}



