package com.e.digilife.Activity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.widget.*
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.ReportAdapter
import com.app.towntalk.Adapter.TrascationCategoryRecyclerAdapter
import com.e.digilife.Model.WalletModel
import com.e.digilife.R
import com.e.digilife.View.InputValidation
import com.e.digilife.View.Utils
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class ReportActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

    val TAG = "ReportActivity"
    val activity = this@ReportActivity
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null
    lateinit var inputValidation: InputValidation

    lateinit var loaderLayout: LinearLayout
    lateinit var backView: ImageView
    lateinit var shareView: ImageView
    lateinit var thisMonthText: TextView
    lateinit var allTimeText: TextView
    lateinit var lastThreeMonthText: TextView
    lateinit var startDateLayout: RelativeLayout
    lateinit var endDateLayout: RelativeLayout
    lateinit var startDateText: TextView
    lateinit var endDateText: TextView
    lateinit var reportTypeSpinner: Spinner
    lateinit var searchButton: Button
    lateinit var reportRecyclerView: RecyclerView

    var allType = "all"
    var incomeType = "income"
    var expenseType = "expense"

    var reportType: String = allType
    var client_id: String = ""
    var filterType: String = ""
    var currency: String = ""
    var reportStr: String = ""

    private var reportList = ArrayList<WalletModel>()
    var adapter: ReportAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_report)
        init()

        requestQueue = Volley.newRequestQueue(this)
        prefs = this.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        currency = prefs!!.getString(Utils.CURRENCY, "")
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }

        allTimeText.setTextColor(resources.getColor(R.color.l_blue))
        lastThreeMonthText.setTextColor(resources.getColor(R.color.greyColor))
        thisMonthText.setTextColor(resources.getColor(R.color.greyColor))
        filterType = "all_time"

        val linearLayoutManager = LinearLayoutManager(activity)
        reportRecyclerView.setLayoutManager(linearLayoutManager)
        reportRecyclerView.setHasFixedSize(true)
        adapter = ReportAdapter(activity, reportList)
        reportRecyclerView.adapter = adapter
    }

    fun init() {
        inputValidation = InputValidation(activity)

        loaderLayout = findViewById(R.id.loaderLayout)
        reportRecyclerView = findViewById(R.id.reportRecyclerView)
        backView = findViewById(R.id.backView)
        shareView = findViewById(R.id.shareView)
        thisMonthText = findViewById(R.id.thisMonthText)
        allTimeText = findViewById(R.id.allTimeText)
        lastThreeMonthText = findViewById(R.id.lastThreeMonthText)
        startDateLayout = findViewById(R.id.startDateLayout)
        endDateLayout = findViewById(R.id.endDateLayout)
        startDateText = findViewById(R.id.startDateText)
        endDateText = findViewById(R.id.endDateText)
        reportTypeSpinner = findViewById(R.id.reportTypeSpinner)
        searchButton = findViewById(R.id.searchButton)

        backView.setOnClickListener(clickListener)
        searchButton.setOnClickListener(clickListener)
        startDateLayout.setOnClickListener(clickListener)
        endDateLayout.setOnClickListener(clickListener)
        allTimeText.setOnClickListener(clickListener)
        thisMonthText.setOnClickListener(clickListener)
        lastThreeMonthText.setOnClickListener(clickListener)
        shareView.setOnClickListener(clickListener)

        reportTypeSpinner.setOnItemSelectedListener(this)
        val days = arrayOf("All", "Income", "Expense")
        val aa = ArrayAdapter(this, R.layout.spinner_item, days)
        aa.setDropDownViewResource(R.layout.spinner_item)
        reportTypeSpinner.setAdapter(aa)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish()
            }
            R.id.shareView -> {
                if (reportStr.equals("")) {
                    Toast.makeText(applicationContext, "Please create report ..", Toast.LENGTH_SHORT).show()
                } else {
                    val intent = Intent(activity, ReportShareActivity::class.java)
                    intent.putExtra("ReportStr", reportStr)
                    startActivity(intent)

                }
            }
            R.id.startDateLayout -> {
                openDatePicker("start")
            }
            R.id.endDateLayout -> {
                openDatePicker("end")
            }
            R.id.allTimeText -> {
                allTimeText.setTextColor(resources.getColor(R.color.l_blue))
                lastThreeMonthText.setTextColor(resources.getColor(R.color.greyColor))
                thisMonthText.setTextColor(resources.getColor(R.color.greyColor))
                filterType = "all_time"
                validation()
            }
            R.id.lastThreeMonthText -> {
                allTimeText.setTextColor(resources.getColor(R.color.greyColor))
                thisMonthText.setTextColor(resources.getColor(R.color.greyColor))
                lastThreeMonthText.setTextColor(resources.getColor(R.color.l_blue))
                filterType = "last_three_month"
                validation()
            }
            R.id.thisMonthText -> {
                allTimeText.setTextColor(resources.getColor(R.color.greyColor))
                thisMonthText.setTextColor(resources.getColor(R.color.l_blue))
                lastThreeMonthText.setTextColor(resources.getColor(R.color.greyColor))
                filterType = "this_month"
                validation()
            }

            R.id.searchButton -> {
                validation()
            }
        }
    }


    private fun getReport() {
        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.GET_REPORT,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        val dataArray = jsonObj.getJSONArray("data")
                        if (dataArray.length() > 0) {
                            setReoprtData(dataArray)
                        }

                    } else {
                        reportList.clear()
                        adapter!!.notifyDataSetChanged()
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    Log.d("Error.Response", error.toString())
                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["from_date"] = startDate
                params["to_date"] = endDate
                params["filter_name"] = filterType
                params["type"] = reportType
                Log.e(" JSONData :", params.toString())
                return params
            }
        }
        Log.e(" API :", EndPoints.GET_REPORT)
        requestQueue.add(stringRequest)
    }


    fun setReoprtData(jsonArray: JSONArray) {
        reportList.clear()
        for (i in 0..(jsonArray.length() - 1)) {
            val jsonObj = jsonArray.getJSONObject(i)

            val dataArray = jsonObj.getJSONArray("data")

            for (j in 0..(dataArray.length() - 1)) {
                val dataObj = dataArray.getJSONObject(j)
                val dataModel = WalletModel()
                if (j == 0) {
                    dataModel.isTitle = "1"
                } else {
                    dataModel.isTitle = ""
                }
                dataModel.currency = currency
                dataModel.tranId = dataObj.getString("id")
                dataModel.tranAmount = dataObj.getString("amount")
                var sdDate = dateConvertor(dataObj.getString("date1"))
                dataModel.transDate = sdDate
                dataModel.trasCategoryName = dataObj.getString("category_name")
                dataModel.trasCategoryType = dataObj.getString("category_type")
                dataModel.trasIsRenewable = dataObj.getString("is_renewable")
                dataModel.tranDescription = dataObj.getString("description")
                dataModel.trasPurpose = dataObj.getString("purpose")
                var url = dataObj.getString("attachment")
                dataModel.trasAttachment = EndPoints.ROOT + url
                reportList.add(dataModel)
            }
            adapter!!.notifyDataSetChanged()
        }

        creatHTMLString()
    }


    fun creatHTMLString() {
        var htmlStr: String = ""

        for (i in 0..(reportList.size - 1)) {
            var catName = reportList.get(i).trasCategoryName
            var catType = reportList.get(i).trasCategoryType
            var tranAmount = reportList.get(i).tranAmount
            var description = reportList.get(i).tranDescription
            var date = reportList.get(i).transDate
            var purpose = reportList.get(i).trasPurpose
            var attachement = reportList.get(i).trasAttachment

            htmlStr = htmlStr +
                    "<tr><td style='font-style:bold;font-size:13'>Category Name : </td><td>" + catName + "</td></tr>" +
                    "<tr><td style='font-style:bold;font-size:13'> Category Type : </td><td>" + catType + "</td></tr>" +
                    "<tr><td style='font-style:bold;font-size:13'> Amount : </td><td> " + tranAmount + "</td></tr>" +
                    "<tr><td style='font-style:bold;font-size:13'> Note : </td><td>" + description + "</td></tr><tr>" +
                    "<td style='font-style:bold;font-size:13'>Date Time : </td><td> " + date + " </td></tr><tr>" +
                    "<td style='font-style:bold;font-size:13'>Purpose : </td><td> " + purpose + "</td></tr>" +
                    "<tr><td style='font-style:bold;font-size:13'>Attachment : </td><td><img src=" + attachement + " width='150' height='150'></td></tr><tr><td colspan=2><hr></td></tr>"

        }



        reportStr = "<html><body><p style='font-style:bold;font-size:17'>Report</p>" +
                "<p style='font-size:15;font-style:bold'>Type  : <bold>" + filterType + " </bold>" +
                " <p style='font-style:bold;font-size:15'> Start Date : " + startDate + "</p>" +
                " <p style='font-style:bold;font-size:15'> End Date : " + endDate + " </p>" +
                "<p style='font-size:14;font-style:bold'>Transactions  List :</p>" +
                "<hr><table>" + htmlStr + "</table></body></html>"

        Log.e("reportStr ::", reportStr)


    }


    fun dateConvertor(inputText: String): String {

        val outputFormat = SimpleDateFormat("E, MMM dd yyyy", Locale.US)
        val inputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)

        var date: Date? = null
        try {
            date = inputFormat.parse(inputText)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val outputText = outputFormat.format(date)

        return outputText
    }


    fun validation() {

        if (!inputValidation.isFieldBlank(filterType, getString(R.string.select_filter_validation))
            && !inputValidation.isFieldBlank(startDate, getString(R.string.select_filter_validation))
            && !inputValidation.isFieldBlank(endDate, getString(R.string.select_filter_validation))
        ) {
            return
        }


        if (Utils.checkInternetConnection(this@ReportActivity)) {

            getReport()

        } else {
            Utils.showMessageDialog(
                this@ReportActivity,
                getString(R.string.app_name),
                getString(R.string.check_internet)
            )
        }

    }

    //TODO: Date Convertor............

    var startDate: String = ""
    var endDate: String = ""

    fun openDatePicker(type: String) {
        SingleDateAndTimePickerDialog.Builder(activity)
            .bottomSheet()
            .displayMinutes(false)
            .displayHours(false)
            .displayDays(false)
            .displayMonth(true)
            .displayYears(true)
            .displayDaysOfMonth(true)
            .displayListener(object : SingleDateAndTimePickerDialog.DisplayListener {
                override fun onDisplayed(picker: SingleDateAndTimePicker) {
                    //retrieve the SingleDateAndTimePicker
                }
            })
            .title("Select Date")
            .titleTextColor(getResources().getColor(R.color.blackColor))
            .backgroundColor(getResources().getColor(R.color.whiteColor))
            .mainColor(getResources().getColor(R.color.blackColor))
            .curved()
            .listener(object : SingleDateAndTimePickerDialog.Listener {
                override fun onDateSelected(date: Date) {
                    var convertDate = dateConvertor(date)
                    if (type.equals("start")) {
                        startDate = convertDate
                        startDateText.setText(convertDate)
                    } else {
                        endDate = convertDate
                        endDateText.setText(convertDate)
                    }
                }
            }).display()
    }

    fun dateConvertor(date: Date): String {
        val outputFormat = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
        outputFormat.timeZone = TimeZone.getTimeZone("UTC")
        val output = outputFormat.format(date)
        println(output)
        return output
    }

    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {
        if (position == 0) {
            reportType = allType
        } else if (position == 1) {
            reportType = incomeType
        } else if (position == 2) {
            reportType = expenseType
        }
    }

    override fun onNothingSelected(parent: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


}
