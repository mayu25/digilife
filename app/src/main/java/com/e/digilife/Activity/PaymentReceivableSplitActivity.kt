package com.e.digilife.Activity

import android.content.Context
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.PaymentReceivableAdapter
import com.e.digilife.Model.WalletModel
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import java.util.ArrayList
import java.util.HashMap

class PaymentReceivableSplitActivity : AppCompatActivity() {

    val TAG = "PaymentReceivableSplitActivity"
    val activity = this@PaymentReceivableSplitActivity
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null

    private var dataList = ArrayList<WalletModel>()
    var adapter: PaymentReceivableAdapter? = null

    var client_id: String = ""
    var currency: String = ""
    var tranId: String = ""
    var splitId :String = ""

    lateinit var backView: ImageView
    lateinit var deleteLayout: FrameLayout
    lateinit var trasNameText: TextView
    lateinit var trasAmountText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_receivable_split)
        requestQueue = Volley.newRequestQueue(activity)
        prefs = activity!!.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        currency = prefs!!.getString(Utils.CURRENCY, "")
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }
        init()

        if (intent != null) {
            tranId = intent.getStringExtra("trans_id")
            splitId = intent.getStringExtra("split_id")
            var fileData = intent.getStringExtra(Utils.TRANSCATION_DATA)
            var json = JSONArray(fileData)
            Log.e("json ::", json.toString())


            var trasName = json.getJSONObject(0).getString("category_name")
            var amount = json.getJSONObject(0).getString("amount")


            trasNameText.setText(trasName)
            trasAmountText.setText(currency + " " + amount + ".00")
        }

    }

    fun init() {
        backView = findViewById(R.id.backView)
        deleteLayout = findViewById(R.id.deleteLayout)
        trasNameText = findViewById(R.id.trasNameText)
        trasAmountText = findViewById(R.id.trasAmountText)

        backView.setOnClickListener(clickListener)
        deleteLayout.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish()
            }
            R.id.deleteLayout -> {
                deleteTranscation()
            }
        }
    }


    private fun deleteTranscation() {

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.DELETE_SPLIT_TRASCATION,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        finish()
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["transaction_id"] = tranId
                params["split_id"] = splitId

                Log.e(" JSONData :", params.toString())
                return params
            }
        }

        Log.e(" API :", EndPoints.DELETE_SPLIT_TRASCATION)
        requestQueue.add(stringRequest)
    }

}
