package com.e.digilife.Activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.ImageGridviewAdapter
import com.e.digilife.CropLibrary.CropScanImageActivity
import com.e.digilife.CropLibrary.saveToInternalStorage
import com.e.digilife.CropLibrary.shareCacheDirBitmap
import com.e.digilife.CropLibrary.shareMultipleCacheDirBitmap
import com.e.digilife.Model.DataModel
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.e.digilife.View.VolleyMultipartRequest
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.util.*
import kotlin.collections.ArrayList

class FileListActivity : AppCompatActivity() {

    val TAG = "FileListActivity"
    val activity = this@FileListActivity
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null

    lateinit var backView: ImageView
    lateinit var titleText: TextView
    lateinit var shareView: ImageView
    lateinit var uploadView: ImageView
    lateinit var searchView: ImageView
    lateinit var notificationView: ImageView
    lateinit var plusView: ImageView
    lateinit var emrDocsGridview: GridView
    lateinit var loaderLayout: LinearLayout

    var client_id: String = ""
    var parentFolderId: String = ""
    var folderId: String = ""
    var MOVE_FILE_ID: String = ""
    var COPY_FILE_ID: String = ""
    var TITLE: String = ""

    private var dataList = ArrayList<DataModel>()
    var adapter: ImageGridviewAdapter? = null

    lateinit var pasteHereView: TextView
    lateinit var moveHereView: TextView
    lateinit var cameraView: TextView
    lateinit var galleryView: TextView
    lateinit var documentView: TextView
    lateinit var cancelView: TextView
    lateinit var imageSelectionView: LinearLayout

    lateinit var noDataView: TextView
    var bundle: Bundle? = null

    var musicPdfType: String = "0"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_emr__file_upload)

        requestQueue = Volley.newRequestQueue(this)
        prefs = this.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val builder = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(builder);
        }

        init()

        bundle = intent.extras
        if (bundle != null) {
            parentFolderId = bundle!!.getString(Utils.PARENT_FOLDER_ID)
            folderId = bundle!!.getString(Utils.FOLDER_ID)
            TITLE = bundle!!.getString(Utils.TITLE)
        }

        titleText.text = TITLE

        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }

        MOVE_FILE_ID = prefs!!.getString(Utils.MOVE_FILE_ID, "")
        COPY_FILE_ID = prefs!!.getString(Utils.COPY_FILE_ID, "")

        adapter = ImageGridviewAdapter(activity, dataList, onItemClick, onSelectClick, onUnSelectClick)
        emrDocsGridview.adapter = adapter

    }

    fun init() {
        noDataView = findViewById(R.id.noDataView)
        loaderLayout = findViewById(R.id.loaderLayout)
        titleText = findViewById(R.id.titleText)
        backView = findViewById(R.id.backView)
        plusView = findViewById(R.id.plusView)
        notificationView = findViewById(R.id.notificationView)
        searchView = findViewById(R.id.searchView)
        shareView = findViewById(R.id.shareView)
        uploadView = findViewById(R.id.uploadView)
        emrDocsGridview = findViewById(R.id.emrDocsGridview)

        imageSelectionView = findViewById(R.id.imageSelectionView)
        pasteHereView = findViewById(R.id.pasteHereView)
        moveHereView = findViewById(R.id.moveHereView)
        cameraView = findViewById(R.id.cameraView)
        galleryView = findViewById(R.id.galleryView)
        documentView = findViewById(R.id.documentView)
        cancelView = findViewById(R.id.cancelView)

        backView.setOnClickListener(clickListener)
        plusView.setOnClickListener(clickListener)
        shareView.setOnClickListener(clickListener)
        uploadView.setOnClickListener(clickListener)

        pasteHereView.setOnClickListener(clickListener)
        moveHereView.setOnClickListener(clickListener)
        cameraView.setOnClickListener(clickListener)
        galleryView.setOnClickListener(clickListener)
        documentView.setOnClickListener(clickListener)
        cancelView.setOnClickListener(clickListener)
        notificationView.setOnClickListener(clickListener)
        searchView.setOnClickListener(clickListener)
    }

    override fun onResume() {
        super.onResume()
        shareView.visibility = View.VISIBLE
        uploadView.visibility = View.GONE
        imageSelectionView.visibility = View.GONE
        imageUris.clear()

        if (musicPdfType.equals("0")) {

            if (Utils.checkInternetConnection(this@FileListActivity))
                getDocsList("")
            else
                Utils.showMessageDialog(
                    this@FileListActivity,
                    getString(R.string.app_name),
                    getString(R.string.check_internet)
                )
        }
    }


    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                Utils.storeString(prefs, Utils.FOLDER_ID, "")
                finish()
            }

            R.id.notificationView -> {
                val intent = Intent(activity, NotificationActivity::class.java)
                startActivity(intent)
            }
            R.id.searchView -> {
                val intent = Intent(activity, SearchActivity::class.java)
                startActivity(intent)
            }

            R.id.shareView -> {
                Log.e(TAG, " Click")
                if (dataList.size > 0) {

                    shareView.visibility = View.GONE
                    uploadView.visibility = View.VISIBLE
                    Utils.VISIBLE_CHECKBOX = "1"

                    adapter!!.notifyDataSetChanged()
                }
            }

            R.id.uploadView -> {
                if (sharingFiles.size > 0) {
                    loaderLayout.visibility = View.VISIBLE
                    shareFiles()
                    // shareMultiple(imageUris, activity)

                } else {
                    Toast.makeText(activity, "Please select image first !", Toast.LENGTH_SHORT).show()
                }

            }

            R.id.plusView -> {
                if (ActivityCompat.checkSelfPermission(
                        activity,
                        android.Manifest.permission.CAMERA
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    ActivityCompat.requestPermissions(
                        activity, arrayOf(
                            android.Manifest.permission.CAMERA,
                            android.Manifest.permission.READ_EXTERNAL_STORAGE,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ), 0
                    )
                } else {
                    MOVE_FILE_ID = prefs!!.getString(Utils.MOVE_FILE_ID, "")
                    COPY_FILE_ID = prefs!!.getString(Utils.COPY_FILE_ID, "")

                    requirePermissions = false
                    if (!MOVE_FILE_ID.equals("")) {
                        moveHereView.visibility = View.VISIBLE
                    } else {
                        moveHereView.visibility = View.GONE
                    }
                    if (!COPY_FILE_ID.equals("")) {
                        pasteHereView.visibility = View.VISIBLE
                    } else {
                        pasteHereView.visibility = View.GONE
                    }

                    imageSelectionView.visibility = View.VISIBLE

                    if (TITLE.equals(Utils.FAVOURITE_MUSIC)) {
                        cameraView.visibility = View.GONE
                        galleryView.visibility = View.GONE
                    }
                }
            }
            R.id.pasteHereView -> {
                imageSelectionView.visibility = View.GONE
                if (Utils.checkInternetConnection(this@FileListActivity))
                    copyFileHere()
                else
                    Utils.showMessageDialog(
                        this@FileListActivity,
                        getString(R.string.app_name),
                        getString(R.string.check_internet)
                    )
            }

            R.id.moveHereView -> {
                imageSelectionView.visibility = View.GONE
                if (Utils.checkInternetConnection(this@FileListActivity))
                    moveFileHere()
                else
                    Utils.showMessageDialog(
                        this@FileListActivity,
                        getString(R.string.app_name),
                        getString(R.string.check_internet)
                    )
            }
            R.id.cameraView -> {
                imageSelectionView.visibility = View.GONE
                preference = Utils.OPEN_CAMERA
                checkStoragePermission()
            }
            R.id.galleryView -> {
                imageSelectionView.visibility = View.GONE
                preference = Utils.OPEN_MEDIA
                checkStoragePermission()
            }
            R.id.documentView -> {
                imageSelectionView.visibility = View.GONE
                preference = Utils.OPEN_DOCUMENT
                checkStoragePermission()
            }
            R.id.cancelView -> {
                imageSelectionView.visibility = View.GONE
            }
        }
    }


    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            var fileType = dataList.get(position).fileType

            if (fileType.equals(Utils.FAVOURITE_MUSIC) || TITLE.equals(Utils.FAVOURITE_MUSIC)) {
                val intent = Intent(applicationContext, ImageViewerActivity::class.java)
                intent.putExtra(Utils.FILE_DATA, dataList.get(position))
                intent.putExtra(Utils.TYPE, "")
                intent.putExtra(Utils.FILE_TYPE, Utils.FAVOURITE_MUSIC)
                startActivity(intent)
            } else if (fileType.equals(Utils.DOCUMENT)) {
                val intent = Intent(applicationContext, ImageViewerActivity::class.java)
                intent.putExtra(Utils.FILE_DATA, dataList.get(position))
                intent.putExtra(Utils.TYPE, "")
                intent.putExtra(Utils.FILE_TYPE, Utils.DOCUMENT)
                startActivity(intent)
            } else {
                val intent = Intent(applicationContext, ImageViewerActivity::class.java)
                intent.putExtra(Utils.FILE_DATA, dataList.get(position))
                intent.putExtra(Utils.TYPE, "")
                intent.putExtra(Utils.FILE_TYPE, "")
                startActivity(intent)
            }

        }
    }

    val imageUris = ArrayList<Uri>()
    val finalImageUris = ArrayList<Uri>()
    val sharingFiles = ArrayList<String>()

    var systemPath = "android.resource://com.e.digilife/drawable/"

    var onSelectClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            var filePath = dataList.get(position).filePath
            var fileType = dataList.get(position).fileType


            //imageUris.add(Utils.ImageNameClass.geturi(filePath, activity))
            // var fileUri = Uri.parse(filePath)

            if (fileType.equals("")) {
                if (sharingFiles.size > 0) {
                    if (!sharingFiles.contains(filePath)) {

                        sharingFiles.add(filePath)
                    }
                } else {
                    sharingFiles.add(filePath)
                }

                Log.e(TAG, "Selected Images :::" + sharingFiles)
                dataList.get(position).isSelected = true
            } else {
                dataList.get(position).isSelected = false
                Toast.makeText(activity, "Not supported for sharing !", Toast.LENGTH_SHORT).show()
            }

        }
    }

    var onUnSelectClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            var filePath = dataList.get(position).filePath
            var fileUri = Uri.parse(filePath)

            if (sharingFiles.size > 0) {
                if (sharingFiles.contains(filePath)) {
                    sharingFiles.remove(filePath)
                }
            }


            dataList.get(position).isSelected = false
        }
    }


    fun shareFiles() {

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val builder = StrictMode.VmPolicy.Builder()
            StrictMode.setVmPolicy(builder.build())
        }
//
//        for (i in 0..(sharingFiles.size - 1)) {
//            var filePath = sharingFiles.get(i)
//
//            val file = File(filePath)
//            val uri =FileProvider.getUriForFile(this, this.packageName + ".provider",file);
//
//            imageUris.add(uri)
//        }
//
//
//        val shareIntent = Intent()
//        shareIntent.action = Intent.ACTION_SEND_MULTIPLE
//        shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imageUris)
//        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//        shareIntent.type = "*/*"
//        this.startActivity(Intent.createChooser(shareIntent, "Share Image"))


        for (i in 0..(sharingFiles.size - 1)) {
            var filePath = sharingFiles.get(i)

            val bitmap = Utils.getBitmapFromURL(filePath)
            val uri = bitmap!!.saveToInternalStorage(activity)
            val fileUri = shareMultipleCacheDirBitmap(uri, i.toString())
            imageUris.add(fileUri)
        }


        shareMultiple(imageUris)

//        finalImageUris.clear()
//        for (i in 0..(imageUris.size - 1)){
//            var uri = imageUris.get(i)
//            val fileUri = shareMultipleCacheDirBitmap(uri, i.toString())
//            finalImageUris.add(fileUri)
//        }


    }


    fun shareMultiple(imageUris: ArrayList<Uri>) {
        loaderLayout.visibility = View.GONE

        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND_MULTIPLE
        shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imageUris)
        shareIntent.type = "*/*"
        this.startActivity(Intent.createChooser(shareIntent, "Share Image"))


//        val intent = Intent(Intent.ACTION_SEND_MULTIPLE)
//        // intent.setType("image/*")
//        intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imageUris)
//        intent.type = "*/*"
//        this.startActivity(Intent.createChooser(intent, "Multiple Share"))

//        val shareIntent = Intent(Intent.ACTION_SEND_MULTIPLE)
//        shareIntent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, imageUris)
//        shareIntent.type = "image/*"
//        startActivity(Intent.createChooser(shareIntent, "Share image using"));

        //  this.shareMultipleCacheDirBitmap(imageUris)
    }


    private fun getDocsList(type: String) {

        if (type.equals("")) {
            loaderLayout.visibility = View.VISIBLE
        }

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.GET_EMR_DOCS_LIST,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        val dataArray = jsonObj.getJSONArray("data")
                        if (dataArray.length() > 0) {
                            setEMRDocs(dataArray)
                        }
                        noDataView.visibility = View.GONE

                    } else {
                        dataList.clear()
                        noDataView.visibility = View.VISIBLE
                        // Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                        adapter!!.notifyDataSetChanged()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                    Log.d("Error.Response", error.toString())
                }
            }
        ) {

            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["parent_folder_id"] = parentFolderId
                params["child_folder_id"] = folderId

                return params

                Log.e(" JSONData :", params.toString())
            }
        }

        Log.e(" API :", EndPoints.GET_EMR_DOCS_LIST)
        requestQueue.add(stringRequest)
    }


    fun setEMRDocs(jsonArray: JSONArray) {
        dataList.clear()
        for (i in 0..(jsonArray.length() - 1)) {
            val jsonObj = jsonArray.getJSONObject(i)
            val dataModel = DataModel()
            dataModel.fileId = jsonObj.getString("id")
            dataModel.fileName = jsonObj.getString("file_name")
            var filePath = jsonObj.getString("file_path")
            filePath = filePath.replace(" ", "%20")
            dataModel.filePath = filePath
            var extention = getFileExtention(filePath)
            if (extention.equals("mp3")) {
                dataModel.fileType = Utils.FAVOURITE_MUSIC
            } else if (extention.equals("pdf")) {
                dataModel.fileType = Utils.DOCUMENT
            } else {
                dataModel.fileType = ""
            }

            if (TITLE.equals(Utils.FAVOURITE_MUSIC)) {
                dataModel.fileType = Utils.FAVOURITE_MUSIC
            }

            dataModel.isSelected = false
            dataModel.isFav = jsonObj.getString("is_fav")
            dataModel.fileSize = jsonObj.getString("file_size")
            dataModel.parentFolderId = jsonObj.getString("parent_folder_id")
            dataModel.childFolderId = jsonObj.getString("child_folder_id")
            dataModel.createTime = jsonObj.getString("DateTime")
            dataModel.modifiedTime = jsonObj.getString("ModifiedTime")
            dataList.add(dataModel)
        }
        Utils.VISIBLE_CHECKBOX = "0"
        adapter!!.notifyDataSetChanged()
    }

    fun getFileExtention(fileName: String): String {
        val filenameArray = fileName.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        var extention = filenameArray[filenameArray.size - 1]
        return extention
    }


    //TODO: Move file here API...........

    private fun moveFileHere() {

        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.MOVE_FILE,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    //  loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        Utils.storeString(prefs, Utils.MOVE_FILE_ID, "")
                        getDocsList("Move")

                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                    Log.d("Error.Response", error.toString())
                }
            }) {

            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["parent_folder_id"] = parentFolderId
                params["child_folder_id"] = folderId
                params["file_id"] = MOVE_FILE_ID

                return params

                Log.e(" JSONData :", params.toString())
            }
        }

        Log.e(" API :", EndPoints.MOVE_FILE)
        requestQueue.add(stringRequest)
    }


    //TODO: Copy file here API...........

    private fun copyFileHere() {

        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.COPY_FILE,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    //  loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        Utils.storeString(prefs, Utils.COPY_FILE_ID, "")
                        getDocsList("Copy")

                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                    Log.d("Error.Response", error.toString())
                }
            }) {

            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["parent_folder_id"] = parentFolderId
                params["child_folder_id"] = folderId
                params["file_id"] = COPY_FILE_ID

                return params

                Log.e(" JSONData :", params.toString())
            }
        }

        Log.e(" API :", EndPoints.COPY_FILE)
        requestQueue.add(stringRequest)
    }


    var requirePermissions = true
    var preference = 0

    private fun checkStoragePermission() {

        if (ContextCompat.checkSelfPermission(
                this@FileListActivity,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            ActivityCompat.requestPermissions(
                this@FileListActivity,
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ), 100
            )

        } else {
            chooseFileFromDevice(preference)
        }
    }


    private fun chooseFileFromDevice(preference: Int) {

        if (preference == Utils.OPEN_DOCUMENT) {

            if (TITLE.equals(Utils.FAVOURITE_MUSIC)) {
                selectAudioFile()
            } else if (TITLE.equals(Utils.FAVOURITE_PHOTOS)) {
                selectPhotoFile()
            } else {
                selectPDFFile()
            }


        } else if (preference == Utils.OPEN_MEDIA) {
            startActivityForResult(
                CropScanImageActivity.getJumpIntent(this@FileListActivity, true, folderId, parentFolderId),
                100
            )
        } else {
            startActivityForResult(
                CropScanImageActivity.getJumpIntent(this@FileListActivity, false, folderId, parentFolderId),
                100
            )
        }
    }


    fun selectAudioFile() {
        val intent = Intent()
        intent.setAction(Intent.ACTION_GET_CONTENT)
        intent.setType("audio/*")
        startActivityForResult(Intent.createChooser(intent, "Choose Media File"), Utils.SELECT_FILE)
    }

    fun selectPDFFile() {

        val mimeTypes = arrayOf(
            "image/*",
            "application/pdf",
            "application/msword",
            "application/vnd.ms-powerpoint",
            "application/vnd.ms-excel",
            "text/plain"
        )

        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.addCategory(Intent.CATEGORY_OPENABLE)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.type = if (mimeTypes.size == 1) mimeTypes[0] else "*/*"
            if (mimeTypes.size > 0) {
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
            }
        } else {
            var mimeTypesStr = ""
            for (mimeType in mimeTypes) {
                mimeTypesStr += "$mimeType|"
            }
            intent.type = mimeTypesStr.substring(0, mimeTypesStr.length - 1)
        }
        startActivityForResult(Intent.createChooser(intent, "ChooseFile"), Utils.SELECT_PDF_FILE)
    }

    fun selectPhotoFile() {
        val intent = Intent()
        intent.setAction(Intent.ACTION_GET_CONTENT)
        intent.setType("image/*")
        startActivityForResult(Intent.createChooser(intent, "Select Photos"), Utils.SELECT_PDF_FILE)
    }


    var soundByte: ByteArray? = null
    var pdfByte: ByteArray? = null
    var extention: String = ""
    var fileName: String = ""

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Utils.SELECT_FILE && resultCode == Activity.RESULT_OK) {
            if ((data != null) && (data.getData() != null)) {
                var audioUri: Uri
                audioUri = data.data
                soundByte = Utils.getBytes(audioUri, activity)
                musicPdfType = "1"
                val file = File(audioUri.path)
                fileName = file.name
                uploadAudioFile()

            }
        } else if (requestCode == Utils.SELECT_PDF_FILE && resultCode == Activity.RESULT_OK) {
            if ((data != null) && (data.getData() != null)) {
                var pdfUri: Uri
                pdfUri = data.data
                pdfByte = Utils.getBytes(pdfUri, activity)
                musicPdfType = "1"

                val file = File(pdfUri.path)
                //val filePath = file.absolutePath
                fileName = file.name
                extention = getfileExtension(pdfUri)!!
                uploadPDFFile()
            }
        }
    }

    private fun getfileExtension(uri: Uri): String? {
        val extension: String?
        val contentResolver = contentResolver
        val mimeTypeMap = MimeTypeMap.getSingleton()
        extension = mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri))
        return extension
    }



    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        requirePermissions
        when (requestCode) {
            Utils.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                chooseFileFromDevice(preference)
            } else {
                Toast.makeText(
                    this@FileListActivity,
                    resources.getString(R.string.msg_denied_permission),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    fun uploadAudioFile() {
        loaderLayout.visibility = View.VISIBLE
        val volleyMultipartRequest = object : VolleyMultipartRequest(
            Request.Method.POST, EndPoints.FILE_UPLOAD,
            Response.Listener { response ->
                loaderLayout.visibility = View.GONE
                Log.d("response", String(response.data))
                var jsonObj = JSONObject(String(response.data))
                val status = jsonObj.getString("status")
                if (status.equals("1")) {
                    requestQueue.getCache().clear();
                    getDocsList("upload")
                    musicPdfType = "0"
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                }

            },
            Response.ErrorListener { error ->
                Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT).show()
            }) {


            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("client_id", client_id)
                params.put("parent_folder_id", parentFolderId)
                params.put("child_folder_id", folderId)

                return params

                Log.e(" JSONData :", params.toString())
            }

            override fun getByteData(): Map<String, DataPart> {

                val params = HashMap<String, DataPart>()
                // val filename = System.currentTimeMillis()
                params["file"] = DataPart("$fileName.mp3", soundByte)
                return params

                Log.e(" Image :", params.toString())
            }
        }

        volleyMultipartRequest.retryPolicy = DefaultRetryPolicy(
            0,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(volleyMultipartRequest);
    }

    fun uploadPDFFile() {
        loaderLayout.visibility = View.VISIBLE
        val volleyMultipartRequest = object : VolleyMultipartRequest(
            Request.Method.POST, EndPoints.FILE_UPLOAD,
            Response.Listener { response ->
                loaderLayout.visibility = View.GONE
                Log.d("response", String(response.data))
                var jsonObj = JSONObject(String(response.data))
                val status = jsonObj.getString("status")
                if (status.equals("1")) {
                    requestQueue.getCache().clear();
                    getDocsList("upload")
                    musicPdfType = "0"
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                }

            },
            Response.ErrorListener { error ->
                Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT).show()
            }) {


            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("client_id", client_id)
                params.put("parent_folder_id", parentFolderId)
                params.put("child_folder_id", folderId)

                return params

                Log.e(" JSONData :", params.toString())
            }

            override fun getByteData(): Map<String, DataPart> {

                val params = HashMap<String, DataPart>()
                // val filename = System.currentTimeMillis()
                params["file"] = DataPart("$fileName.$extention", pdfByte)
                return params

                Log.e(" Image :", params.toString())
            }
        }

        volleyMultipartRequest.retryPolicy = DefaultRetryPolicy(
            0,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(volleyMultipartRequest);
    }


}
