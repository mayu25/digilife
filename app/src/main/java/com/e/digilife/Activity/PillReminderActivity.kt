package com.e.digilife.Activity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.PillReminderRecyclerAdapter
import com.e.digilife.Adapter.PillTodaysReminderRecyclerAdapter
import com.e.digilife.Model.PillReminderModel
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.e.digilife.View.VolleyMultipartRequest
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import java.util.HashMap
import kotlin.collections.ArrayList
import kotlin.collections.Map


class PillReminderActivity : AppCompatActivity() {

    val TAG = "PillReminderActivity"
    val activity = this@PillReminderActivity
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null

    private var dataList = ArrayList<PillReminderModel>()
    private var medicineList = ArrayList<PillReminderModel>()
    var adapter: PillTodaysReminderRecyclerAdapter? = null

    private var pillList = ArrayList<PillReminderModel>()
    var pillAdapter: PillReminderRecyclerAdapter? = null

    lateinit var backView: ImageView
    lateinit var addPillReminderView: ImageView
    lateinit var reportTextView: TextView
    lateinit var medicinesRecyclerView: RecyclerView
    lateinit var reminderRecyclerView: RecyclerView
    lateinit var noRecordView: ImageView
    lateinit var loaderLayout: LinearLayout

    var client_id: String = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pill_reminder)

        requestQueue = Volley.newRequestQueue(activity)
        prefs = this.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        init()
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }

        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        reminderRecyclerView.setLayoutManager(layoutManager)
        reminderRecyclerView.setHasFixedSize(true)
        adapter = PillTodaysReminderRecyclerAdapter(activity, dataList)
        reminderRecyclerView.adapter = adapter


        val layoutManager2 = LinearLayoutManager(this)
        medicinesRecyclerView.setLayoutManager(layoutManager2)
        medicinesRecyclerView.setHasFixedSize(true)
        pillAdapter = PillReminderRecyclerAdapter(activity, pillList,onItemClick)
        medicinesRecyclerView.adapter = pillAdapter


    }

    fun init() {
        loaderLayout = findViewById(R.id.loaderLayout)
        backView = findViewById(R.id.backView)
        addPillReminderView = findViewById(R.id.addPillReminderView)
        reportTextView = findViewById(R.id.reportTextView)
        medicinesRecyclerView = findViewById(R.id.medicinesRecyclerView)
        noRecordView = findViewById(R.id.noRecordView)
        reminderRecyclerView = findViewById(R.id.reminderRecyclerView)

        backView.setOnClickListener(clickListener)
        addPillReminderView.setOnClickListener(clickListener)
        reportTextView.setOnClickListener(clickListener)
    }

    override fun onResume() {
        super.onResume()
        getPillList()
        getTodaysReminder()
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish()
            }
            R.id.addPillReminderView -> {
                val intent = Intent(applicationContext, AddPillActivity::class.java)
                intent.putExtra(Utils.EDIT_PILLS, "0")
                intent.putExtra(Utils.PILL_OBJ, "")
                startActivity(intent)
            }
            R.id.reportTextView -> {
                val intent = Intent(applicationContext, PillReminderReportActivity::class.java)
                startActivity(intent)
            }
        }
    }


    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            val intent = Intent(applicationContext, AddPillActivity::class.java)
            intent.putExtra(Utils.EDIT_PILLS, "1")
            intent.putExtra(Utils.PILL_OBJ, pillList.get(position))
            startActivity(intent)
        }
    }

    //TODO: Today's Reminder List.............


    fun getTodaysReminder() {
        loaderLayout.visibility = View.VISIBLE
        val volleyMultipartRequest = object : VolleyMultipartRequest(
            Request.Method.POST, EndPoints.GET_TODAYS_REMINDER,
            Response.Listener { response ->
                loaderLayout.visibility = View.GONE
                requestQueue.getCache().clear()
                Log.d("response", String(response.data))
                var jsonObj = JSONObject(String(response.data))

                var status = jsonObj.getString("status")
                if (status.equals("1")) {
                    var dataArray = jsonObj.getJSONArray("data")
                    setReminder(dataArray)

                } else {
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT).show()
            }) {


            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("client_id", client_id)

                Log.e(" JSONData :", params.toString())
                return params

            }
        }
        volleyMultipartRequest.retryPolicy = DefaultRetryPolicy(
            0,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(volleyMultipartRequest);
    }


    fun setReminder(jsonArray: JSONArray) {
        dataList.clear()
        for (i in 0..(4 - 1)) {
            var jObj = jsonArray.getJSONObject(i)
            var todayType = jObj.getString("name")
            var tabletArray = jObj.getJSONArray("tablet")
            if (tabletArray.length() > 0) {
                setMedicine(tabletArray, todayType)

            } else {
                var reminderModel = PillReminderModel()
                reminderModel.todayType = todayType
                reminderModel.isData = ""
                dataList.add(reminderModel)
            }
        }
        adapter!!.notifyDataSetChanged()
        Log.e("Data List ::", dataList.toString())
    }


    fun setMedicine(tabletArray: JSONArray, todayType: String) {
        medicineList.clear()
        for (i in 0..(tabletArray.length() - 1)) {
            var reminderModel = PillReminderModel()
            var tabletObj = tabletArray.getJSONObject(i)
            if (i == 0) {
                reminderModel.pill1 = tabletObj.getString("medicine_name")
            } else if (i == 1) {
                reminderModel.pill2 = tabletObj.getString("medicine_name")
            } else if (i == 2) {
                reminderModel.pill3 = tabletObj.getString("medicine_name")
            }
            medicineList.add(reminderModel)
        }

        var reminderModel = PillReminderModel()
        reminderModel.todayType = todayType
        var arrayLength = tabletArray.length().toString()
        reminderModel.isData = arrayLength
        if (arrayLength.equals("1")) {
            reminderModel.pill1 = medicineList.get(0).pill1
        } else if (arrayLength.equals("2")) {
            reminderModel.pill1 = medicineList.get(0).pill1
            reminderModel.pill2 = medicineList.get(1).pill2
        }  else{
            reminderModel.pill1 = medicineList.get(0).pill1
            reminderModel.pill2 = medicineList.get(1).pill2
            reminderModel.pill3 = medicineList.get(2).pill3
        }

        dataList.add(reminderModel)


    }

    //TODO: Pill List.............

    fun getPillList() {
        loaderLayout.visibility = View.VISIBLE
        val volleyMultipartRequest = object : VolleyMultipartRequest(
            Request.Method.POST, EndPoints.GET_PILL_LIST,
            Response.Listener { response ->
                loaderLayout.visibility = View.GONE
                requestQueue.getCache().clear()
                Log.d("response", String(response.data))
                var jsonObj = JSONObject(String(response.data))

                var status = jsonObj.getString("status")
                if (status.equals("1")) {
                    var dataArray = jsonObj.getJSONArray("data")
                    setPillList(dataArray)

                } else {
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT).show()
            }) {


            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("client_id", client_id)

                Log.e(" JSONData :", params.toString())
                return params

            }
        }
        volleyMultipartRequest.retryPolicy = DefaultRetryPolicy(
            0,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(volleyMultipartRequest);
    }

    fun setPillList(jsonArray: JSONArray) {
        pillList.clear()
        for (i in 0..(jsonArray.length() - 1)) {
            var jObj = jsonArray.getJSONObject(i)
            var reminderModel = PillReminderModel()
            reminderModel.pillId = jObj.getString("pill_id")
            reminderModel.pillName = jObj.getString("medicine_name")
            reminderModel.fromDate = jObj.getString("from_date")
            reminderModel.toDate = jObj.getString("to_date")
            reminderModel.pillReminderType = jObj.getString("pill_reminder_type")
            reminderModel.pillDays = jObj.getString("pill_days")
            reminderModel.takePills = jObj.getString("take_medicine")
            reminderModel.dailyTime = jObj.getString("daily_time")
            reminderModel.pillRemiderTime = jObj.getString("time")
            reminderModel.doctoreName = jObj.getString("dr_name")
            reminderModel.rxNumber = jObj.getString("rx_number")
            reminderModel.pillStatus = jObj.getString("pill_status")
            reminderModel.note = jObj.getString("description")

            pillList.add(reminderModel)

        }
        pillAdapter!!.notifyDataSetChanged()
    }


}
