package com.e.digilife.Activity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.ArticleListAdapter
import com.app.towntalk.Adapter.InsuranceAdapter
import com.app.towntalk.Adapter.TicketReceiptAdapter
import com.e.digilife.Model.ArticleModel
import com.e.digilife.Model.PersonalModel
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import java.util.ArrayList
import java.util.HashMap

class ArticleListActivity : AppCompatActivity() {

    val TAG = "ArticleListActivity"
    val activity = this@ArticleListActivity

    var bundle: Bundle? = null
    var client_id: String = ""
    var parentFolderId: String = ""

    lateinit var backView: ImageView
    lateinit var titleText: TextView
    lateinit var loaderLayout: LinearLayout
    lateinit var listview: ListView


    var adapter: ArticleListAdapter? = null
    private var articleList = ArrayList<ArticleModel>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article_view)
        init()

        bundle = intent.extras

        if (bundle != null) {
            if (!bundle!!.getSerializable("ArticleList").equals("")) {
                articleList = bundle!!.getSerializable("ArticleList") as ArrayList<ArticleModel>
            }
        }

        adapter = ArticleListAdapter(activity, articleList, onItemClick)
        listview.adapter = adapter

        titleText.text = "Articles"
    }


    override fun onResume() {
        super.onResume()
    }

    fun init() {
        listview = findViewById(R.id.listview)
        loaderLayout = findViewById(R.id.loaderLayout)
        titleText = findViewById(R.id.titleText)
        backView = findViewById(R.id.backView)

        backView.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish()
            }
        }
    }

    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int

            val intent = Intent(activity, ArticleDetailsActivity::class.java)
            val bundle = Bundle()
            bundle.putSerializable("ArticleList", articleList)
            bundle.putInt("position", position)
            intent.putExtras(bundle)
            startActivity(intent)
        }
    }

}
