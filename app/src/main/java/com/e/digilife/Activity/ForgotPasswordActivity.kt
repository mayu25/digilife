package com.e.digilife.Activity

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.e.digilife.R
import com.e.digilife.View.InputValidation
import com.e.digilife.View.Utils
import com.shurlock.View.EndPoints
import org.json.JSONObject
import java.util.*


class ForgotPasswordActivity : AppCompatActivity() {

    val TAG = "ForgotPasswordActivity"
    val activity = this@ForgotPasswordActivity
    lateinit var inputValidation: InputValidation
    internal lateinit var requestQueue: RequestQueue

    lateinit var radioGroup: RadioGroup
    lateinit var radioBtnEmail: RadioButton
    lateinit var radioBtnPhone: RadioButton
    lateinit var backView: ImageView
    lateinit var emailEdittext: EditText
    lateinit var phoneEditText: EditText
    lateinit var forgotpasswordBtn: Button

    var phone: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        init()
        requestQueue = Volley.newRequestQueue(this)
    }

    fun init() {

        inputValidation = InputValidation(activity)

        radioGroup = findViewById(R.id.radioGroup)
        radioBtnEmail = findViewById(R.id.radioBtnEmail)
        radioBtnPhone = findViewById(R.id.radioBtnPhone)
        emailEdittext = findViewById(R.id.emailEdittext)
        phoneEditText = findViewById(R.id.phoneEdittext)
        backView = findViewById(R.id.backView)
        forgotpasswordBtn = findViewById(R.id.forgotpasswordBtn)
        backView.setOnClickListener(clickListener)
        forgotpasswordBtn.setOnClickListener(clickListener)

        radioGroup.setOnCheckedChangeListener(
            RadioGroup.OnCheckedChangeListener { group, checkedId ->
                if (checkedId == R.id.radioBtnEmail) {
                    emailEdittext.visibility = View.VISIBLE
                    phoneEditText.visibility = View.GONE
                } else if (checkedId == R.id.radioBtnPhone) {
                    emailEdittext.visibility = View.GONE
                    phoneEditText.visibility = View.VISIBLE
                }
            })

        val font = Typeface.createFromAsset(assets, resources.getString(R.string.popins_regular))
        emailEdittext.setTypeface(font)
        phoneEditText.setTypeface(font)
        radioBtnPhone.setTypeface(font)
        radioBtnEmail.setTypeface(font)
        forgotpasswordBtn.setTypeface(font)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish()
            }

            R.id.forgotpasswordBtn -> {
                validation()
            }
        }
    }

    private fun forgotPassword(
        type: String,
        email: String,
        phone: String,
        resend: String
    ) {
        val dialog = ProgressDialog(this)
        dialog.setMessage("Please wait...")
        dialog.setCancelable(true)
        dialog.isIndeterminate = true
        dialog.show()

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.FORGOTPASSWORD,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    dialog.dismiss()
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        if (type.equals("email")) {
                            Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                            val intent = Intent(applicationContext, LoginActivity::class.java)
                            startActivity(intent)
                            finish()
                        } else {

                            var dataObj = jsonObj.getJSONObject("data")
                            var otp = dataObj.getString("otp")

                            if (resend.equals("0")) {
                                showOTPDialog(otp)
                                Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                            } else {
                                Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                            }
                        }
                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    // error
                    // Log.d("Error.Response", ${error.message})
                }
            }
        ) {


            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["email"] = email
                params["phone"] = phone
                params["type"] = type

                return params

                Log.e(" JSONData :", params.toString())
            }
        }
        val socketTimeout = 10000//30 seconds - change to what you want
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        stringRequest.setRetryPolicy(policy)
        Log.e(" API :", EndPoints.FORGOTPASSWORD)
        requestQueue.add(stringRequest)
    }


    private fun validation() {
        var type: String = ""
        if (emailEdittext.visibility == View.VISIBLE) {
            type = "email"
            if (!inputValidation.isInputEmail(emailEdittext, getString(R.string.email_validation))) {
                return
            }
        }
        if (phoneEditText.visibility == View.VISIBLE) {
            type = "phone"
            if (!inputValidation.isInputPassword(phoneEditText, getString(R.string.phone_validation))) {
                return
            }
        }
        phone = phoneEditText.text.toString()
        forgotPassword(type, emailEdittext.text.toString(), phone, "0")
    }

    lateinit var otpEdittext: EditText

    fun showOTPDialog(otp: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        val inflater = activity.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.otp_dialog_view, null)
        dialog.setContentView(view)



        otpEdittext = dialog.findViewById(R.id.otpEditText) as EditText
        val otpSubmitBtn = dialog.findViewById(R.id.submitBtn) as Button
        val resendOtpBtn = dialog.findViewById(R.id.resendOTPBtn) as Button


        val font = Typeface.createFromAsset(assets, resources.getString(R.string.popins_regular))
        otpEdittext.setTypeface(font)
        otpSubmitBtn.setTypeface(font)
        resendOtpBtn.setTypeface(font)

        otpSubmitBtn.setOnClickListener {
            otpValidation(otp)

        }
        resendOtpBtn.setOnClickListener {
            forgotPassword("phone", "", phone, "1")
        }
        var window = dialog.getWindow();
        window.setLayout(700, 750);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()

    }

    fun otpValidation(otp: String) {
        if (!inputValidation.isFieldEmpty(otpEdittext, "Enter OTP")) {
            return
        }

        if (!otp.equals(otpEdittext.text.toString())) {
            Toast.makeText(activity, "Enter Valid OTP", Toast.LENGTH_SHORT).show()
            return
        }


        if (Utils.checkInternetConnection(activity)) {
            val intent = Intent(applicationContext, ChangePasswordActivity::class.java)
            val bundle = Bundle()
            bundle.putString("phone", phone)
            intent.putExtras(bundle)
            startActivity(intent)
        } else {
            Utils.showMessageDialog(
                activity,
                getString(R.string.app_name),
                getString(R.string.check_internet)
            )
        }


    }

}
