package com.e.digilife.Activity

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Typeface
import android.media.MediaScannerConnection
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.e.digilife.R
import com.e.digilife.View.InputValidation
import com.e.digilife.View.Utils
import com.e.digilife.View.VolleyMultipartRequest
import com.shurlock.View.EndPoints
import com.squareup.picasso.Picasso
import com.ybs.countrypicker.CountryPicker
import de.hdodenhof.circleimageview.CircleImageView
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.URL
import java.util.*

class ProfileActivity : AppCompatActivity() {

    val TAG = "ProfileActivity"
    val activity = this@ProfileActivity
    private lateinit var inputValidation: InputValidation
    internal lateinit var requestQueue: RequestQueue

    lateinit var backView: ImageView
    lateinit var saveText: TextView
    lateinit var profileImg: CircleImageView
    lateinit var nameText: TextView
    lateinit var emailText: TextView
    lateinit var userTypeText: TextView
    lateinit var userSpaceText: TextView
    lateinit var nameEdittext: EditText
    lateinit var pwEdittext: EditText
    lateinit var emailEdittext: EditText
    lateinit var phoneEdittext: EditText
    lateinit var countrytext: TextView
    lateinit var changePasswordText: TextView
    lateinit var packageLayout: LinearLayout

    lateinit var loaderLayout: LinearLayout
    lateinit var imageSelectionView: LinearLayout
    lateinit var cameraView: TextView
    lateinit var galleryView: TextView
    lateinit var cancelView: TextView
    lateinit var documentView: TextView

    var prefs: SharedPreferences? = null

    var Name: String = ""
    var Password: String = ""
    var Email: String = ""
    var Phone: String = ""
    var City: String = ""
    var Country: String = ""
    var client_id = ""
    var ImagePath = ""
    var usedspace = ""
    var remainspace = ""

    lateinit var picker: CountryPicker

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        prefs = this.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        requestQueue = Volley.newRequestQueue(this)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val builder = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(builder);
        }
        init()
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }

        if (Utils.checkInternetConnection(activity)) {
            getProfile()
        } else {
            Utils.showMessageDialog(
                activity,
                getString(R.string.app_name),
                getString(R.string.check_internet)
            )
        }

        picker = CountryPicker.newInstance("Select Country");

        picker.setListener { name, code, dialCode, flagDrawableResID ->
            countrytext.setText(name)
            picker.dismiss()
        }
    }

    fun init() {
        inputValidation = InputValidation(activity)
        imageSelectionView = findViewById(R.id.imageSelectionView)
        loaderLayout = findViewById(R.id.loaderLayout)
        profileImg = findViewById(R.id.profileImg)
        backView = findViewById(R.id.backView)
        nameText = findViewById(R.id.nameText)
        saveText = findViewById(R.id.saveText)
        emailText = findViewById(R.id.emailText)
        userSpaceText = findViewById(R.id.userSpaceText)
        changePasswordText = findViewById(R.id.changePasswordText)
        packageLayout = findViewById(R.id.packageLayout)

        nameEdittext = findViewById(R.id.nameEdittext)
        pwEdittext = findViewById(R.id.pwEdittext)
        emailEdittext = findViewById(R.id.emailEdittext)
        phoneEdittext = findViewById(R.id.phoneEdittext)
        countrytext = findViewById(R.id.countrytext)

        cameraView = findViewById(R.id.cameraView)
        galleryView = findViewById(R.id.galleryView)
        cancelView = findViewById(R.id.cancelView)
        documentView = findViewById(R.id.documentView)
        documentView.visibility = View.GONE


        profileImg.setOnClickListener(clickListener)
        backView.setOnClickListener(clickListener)
        saveText.setOnClickListener(clickListener)
        countrytext.setOnClickListener(clickListener)
        changePasswordText.setOnClickListener(clickListener)
        packageLayout.setOnClickListener(clickListener)

        cameraView.setOnClickListener(clickListener)
        galleryView.setOnClickListener(clickListener)
        cancelView.setOnClickListener(clickListener)

    }

    fun openPicker() {
        picker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
    }


    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish()
            }
            R.id.countrytext -> {
                openPicker()
            }

            R.id.saveText -> {


                validation()
            }
            R.id.profileImg -> {
                if (ActivityCompat.checkSelfPermission(
                        activity,
                        android.Manifest.permission.CAMERA
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    ActivityCompat.requestPermissions(
                        activity, arrayOf(
                            android.Manifest.permission.CAMERA,
                            android.Manifest.permission.READ_EXTERNAL_STORAGE,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ), 0
                    )
                } else {
                    requirePermissions = false
                    imageSelectionView.visibility = View.VISIBLE
                }
            }
            R.id.cameraView -> {
                imageSelectionView.visibility = View.GONE
                preference = Utils.OPEN_CAMERA
                checkStoragePermission()
            }
            R.id.galleryView -> {
                imageSelectionView.visibility = View.GONE
                preference = Utils.OPEN_MEDIA
                checkStoragePermission()
            }
            R.id.cancelView -> {
                imageSelectionView.visibility = View.GONE
            }
            R.id.changePasswordText -> {

            }
            R.id.packageLayout -> {
                val intent = Intent(applicationContext, PackageActivity::class.java)
                startActivity(intent)
            }
        }
    }

    private fun getProfile() {

        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.GET_PROFILE,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        val dataObj = jsonObj.getJSONObject("data")
                        getProfileValue(dataObj)

                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    Log.d("Error.Response", error.toString())
                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                Log.e(" JSONData :", params.toString())
                return params
            }
        }
        Log.e(" API :", EndPoints.GET_PROFILE)
        requestQueue.add(stringRequest)
    }


    fun getProfileValue(jsonObject: JSONObject) {
        Name = jsonObject.getString("name")
        Email = jsonObject.getString("email")
        Password = jsonObject.getString("password")
        Phone = jsonObject.getString("phone")
        City = jsonObject.getString("city")
        Country = jsonObject.getString("country")

        ImagePath = jsonObject.getString("profile_photo")
        remainspace = jsonObject.getString("remainspace")
        usedspace = jsonObject.getString("usedspace")

        emailText.setText(Email)
        nameText.setText(Name)
        nameEdittext.setText(Name)
        pwEdittext.setText(Password)
        emailEdittext.setText(Email)
        phoneEdittext.setText(Phone)
        countrytext.setText(Country)

        userSpaceText.setText("used space " + usedspace + "/" + remainspace)

        if (!ImagePath.equals("")) {
            try {
                val url = URL(EndPoints.ROOT + ImagePath)
                profilePicBitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream())
            } catch (e: IOException) {
                println(e)
            }
            Picasso.with(activity).load(EndPoints.ROOT + ImagePath).placeholder(R.mipmap.ic_username).into(profileImg)
        }

    }

    internal var dashboardActivity = DashboardActivity()

    fun uploadProfile() {
        loaderLayout.visibility = View.VISIBLE

        val volleyMultipartRequest = object : VolleyMultipartRequest(
            Request.Method.POST, EndPoints.EDIT_PROFILE,
            Response.Listener { response ->
                Log.e("response", String(response.data))
                requestQueue.getCache().clear();

                var jsonObj = JSONObject(String(response.data))
                val status = jsonObj.getString("status")

                if (status.equals("1")) {
                    var dataObj = jsonObj.getJSONObject("data")

                    Utils.storeString(prefs, Utils.LOGIN_OBJ, dataObj.toString())

                    var intent = Intent()
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                }


            },
            Response.ErrorListener { error ->
                Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT).show()
            }) {


            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("client_id", client_id)
                params.put("name", Name)
                params.put("email", Email)
                params.put("password", Password)
                params.put("phone", Phone)
                params.put("city", City)
                params.put("country", Country)

                return params

                Log.e(" JSONData :", params.toString())
            }

            override fun getByteData(): Map<String, DataPart> {
                val params = HashMap<String, DataPart>()
                val imagename = System.currentTimeMillis()
                params["photo"] = DataPart("$imagename.png", getFileDataFromDrawable(profilePicBitmap))

                return params
            }
        }
        volleyMultipartRequest.retryPolicy = DefaultRetryPolicy(
            0,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(volleyMultipartRequest);
    }

    fun getFileDataFromDrawable(bitmap: Bitmap?): ByteArray {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap!!.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream)
        return byteArrayOutputStream.toByteArray()
    }


    private fun sendOTP(phone: String) {
        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.SEND_OTP,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        var dataObj = jsonObj.getJSONObject("data")
                        var otp = dataObj.getString("otp")

                        showOTPDialog(otp)
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()

                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    // error
                    // Log.d("Error.Response", ${error.message})
                }
            }
        ) {

            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["phone"] = phone
                return params

                Log.e(" JSONData :", params.toString())
            }
        }
        val socketTimeout = 10000//30 seconds - change to what you want
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        stringRequest.setRetryPolicy(policy)
        Log.e(" API :", EndPoints.FORGOTPASSWORD)
        requestQueue.add(stringRequest)
    }

    lateinit var otpEdittext: EditText

    fun showOTPDialog(otp: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        val inflater = activity.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.otp_dialog_view, null)
        dialog.setContentView(view)


        otpEdittext = dialog.findViewById(R.id.otpEditText) as EditText
        val otpSubmitBtn = dialog.findViewById(R.id.submitBtn) as Button
        val resendOtpBtn = dialog.findViewById(R.id.resendOTPBtn) as Button


        val font = Typeface.createFromAsset(assets, resources.getString(R.string.popins_regular))
        otpEdittext.setTypeface(font)
        otpSubmitBtn.setTypeface(font)
        resendOtpBtn.visibility = View.GONE

        otpSubmitBtn.setOnClickListener {
            otpValidation(otp)
            dialog.dismiss()
        }

        var window = dialog.getWindow();
        window.setLayout(600, 650);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }

    private fun validation() {

        if (ImagePath.equals("") && profilePicBitmap == null) {
            Toast.makeText(activity, "Please select profile photo", Toast.LENGTH_SHORT).show()
            return
        }
        if (!inputValidation.isFieldEmpty(nameEdittext, getString(R.string.name_validation))) {
            return
        }
        if (!inputValidation.isFieldEmpty(phoneEdittext, getString(R.string.phone_validation))) {
            return
        }

        Name = nameEdittext.text.toString()
        Country = countrytext.text.toString()

        if (Utils.checkInternetConnection(activity)) {
            if (Phone.equals(phoneEdittext.text.toString())) {
                Phone = phoneEdittext.text.toString()
                uploadProfile()
            } else {
                Phone = phoneEdittext.text.toString()
                sendOTP(Phone)
            }

        } else {
            Utils.showMessageDialog(
                activity,
                getString(R.string.app_name),
                getString(R.string.check_internet)
            )
        }
    }


    fun otpValidation(otp: String) {
        if (!inputValidation.isFieldEmpty(otpEdittext, "Enter OTP")) {
            return
        }

        if (!otp.equals(otpEdittext.text.toString())) {
            Toast.makeText(activity, "Enter Valid OTP", Toast.LENGTH_SHORT).show()
            return
        }

        if (Utils.checkInternetConnection(this@ProfileActivity)) {
            uploadProfile()
        } else {
            Utils.showMessageDialog(
                this@ProfileActivity,
                getString(R.string.app_name),
                getString(R.string.check_internet)
            )
        }
    }

    var profilePicBitmap: Bitmap? = null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == GALLERY && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val contentURI = data.data
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
                    val byteArrayOutputStream = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
                    profilePicBitmap = bitmap
                    val path = saveImage(bitmap)
                    profileImg.setImageBitmap(bitmap)


                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(this@ProfileActivity, "Failed!", Toast.LENGTH_SHORT).show()
                }

            }

        } else if (requestCode == CAMERA && resultCode == Activity.RESULT_OK) {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            val byteArrayOutputStream = ByteArrayOutputStream()
            thumbnail.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
            profilePicBitmap = thumbnail
            profileImg.setImageBitmap(thumbnail)
            saveImage(thumbnail)
        }
    }


    //TODO: profile upload images......

    private val GALLERY = 1
    private val CAMERA = 2
    var requirePermissions = true
    var preference = 0

    private fun checkStoragePermission() {

        if (ContextCompat.checkSelfPermission(
                this@ProfileActivity,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            ActivityCompat.requestPermissions(
                this@ProfileActivity,
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ), 100
            )

        } else {
            chooseFileFromDevice(preference)
        }
    }

    private fun chooseFileFromDevice(preference: Int) {

        if (preference == Utils.OPEN_MEDIA) {
            val galleryIntent = Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )

            startActivityForResult(galleryIntent, GALLERY)
        } else {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, CAMERA)
        }
    }


    fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallpaperDirectory = File(
            (Environment.getExternalStorageDirectory()).toString() + "Image_Directory"
        )
        // have the object build the directory structure, if needed.
        Log.d("fee", wallpaperDirectory.toString())
        if (!wallpaperDirectory.exists()) {

            wallpaperDirectory.mkdirs()
        }

        try {
            Log.d("heel", wallpaperDirectory.toString())
            val f = File(
                wallpaperDirectory, ((Calendar.getInstance()
                    .getTimeInMillis()).toString() + ".jpg")
            )
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                this,
                arrayOf(f.getPath()),
                arrayOf("image/jpeg"), null
            )
            fo.close()
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath())

            return f.getAbsolutePath()
        } catch (e1: IOException) {
            e1.printStackTrace()
        }

        return ""
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            Utils.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                chooseFileFromDevice(preference)
            } else {
                Toast.makeText(
                    this@ProfileActivity,
                    resources.getString(R.string.msg_denied_permission),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

}
