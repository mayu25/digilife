package com.e.digilife.Activity

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.Typeface
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.os.StrictMode
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.*
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.*
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.e.digilife.CropLibrary.saveToInternalStorage
import com.e.digilife.CropLibrary.shareCacheDirBitmap
import com.e.digilife.Model.DataModel
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.shurlock.View.EndPoints
import com.squareup.picasso.Picasso
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.io.Serializable
import java.util.*


class ImageViewerActivity : AppCompatActivity() {

    val TAG = "ImageViewerActivity"
    val activity = this@ImageViewerActivity
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null

    lateinit var openOptionLayout: LinearLayout
    lateinit var imgView: ImageView
    lateinit var moreOptionView: LinearLayout
    lateinit var backView: ImageView
    lateinit var shareView: ImageView
    lateinit var titleText: TextView

    var bundle: Bundle? = null
    lateinit var bitmap: Bitmap

    var file_Path: String = ""
    var fileName: String = ""
    var isFav: String = ""
    var client_id: String = ""
    var file_id: String = ""
    var fileCreated: String = ""
    var TYPE: String = ""
    var FILE_TYPE: String = ""


    lateinit var loaderLayout: LinearLayout

    lateinit var favImg: ImageView
    lateinit var favText: TextView
    lateinit var imgeNameText: TextView
    lateinit var imageTimeText: TextView
    lateinit var saveOfflineText: TextView

    lateinit var imageLayout: LinearLayout
    lateinit var musicLayout: LinearLayout

    // Music View.....
    lateinit var playView: ImageView
    lateinit var pauseView: ImageView
    lateinit var seekBar: SeekBar
    lateinit var timePlayingView: TextView
    lateinit var totalPlaytimeView: TextView

    private var mPlayer: MediaPlayer? = null
    private var mHandler: Handler? = null
    private var mRunnable: Runnable? = null

    private val mInterval = 10

    //Document View
    lateinit var documentLayout: LinearLayout
    lateinit var documentWebView: WebView

    lateinit var viewImgLayout: LinearLayout
    lateinit var playMusicLayout: LinearLayout
    lateinit var imgDownloadLayout: LinearLayout
    lateinit var imgFavouriteLayout: LinearLayout
    lateinit var renameImgLayout: LinearLayout
    lateinit var moveImgLayout: LinearLayout
    lateinit var copyImgLayout: LinearLayout
    lateinit var saveOfflineImgLayout: LinearLayout
    lateinit var propertiesLayout: LinearLayout
    lateinit var moveToTrashLayout: LinearLayout
    lateinit var deleteLayout: LinearLayout

    internal var dataModel = DataModel()
    var fileData: Serializable = ""

    private lateinit var runnable: Runnable

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_viewer)

        requestQueue = Volley.newRequestQueue(activity)
        prefs = getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val builder = StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(builder);
        }


        init()
        if (intent != null) {
            fileData = intent.getSerializableExtra(Utils.FILE_DATA)
            TYPE = intent.getStringExtra(Utils.TYPE)
            FILE_TYPE = intent.getStringExtra(Utils.FILE_TYPE)
            if (!fileData.equals("")) {
                dataModel = fileData as DataModel

                file_id = dataModel.fileId
                file_Path = dataModel.filePath
                fileName = dataModel.fileName
                isFav = dataModel.isFav
                fileCreated = dataModel.createTime
            }
        }


        if (FILE_TYPE.equals(Utils.FAVOURITE_MUSIC)) {

            viewImgLayout.visibility = View.GONE
            imageLayout.visibility = View.GONE
            playMusicLayout.visibility = View.VISIBLE
            musicLayout.visibility = View.VISIBLE

            mHandler = Handler()
            mPlayer = MediaPlayer()
            val myUri = Uri.parse(file_Path)

            try {
                mPlayer!!.setAudioStreamType(AudioManager.STREAM_MUSIC)
                mPlayer!!.setDataSource(activity, myUri)
                mPlayer!!.prepare()

            } catch (e: IOException) {
                e.printStackTrace()
            }


        } else if (FILE_TYPE.equals(Utils.DOCUMENT)) {
            viewImgLayout.visibility = View.VISIBLE
            imageLayout.visibility = View.GONE
            documentLayout.visibility = View.VISIBLE

            val settings = documentWebView.settings
            settings.javaScriptEnabled = true
            settings.allowFileAccessFromFileURLs = true
            settings.allowUniversalAccessFromFileURLs = true
            settings.builtInZoomControls = true

            documentWebView.setWebViewClient(object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    view.loadUrl(url)
                    return false
                }
            })

            if (!file_Path.equals("")) {
                documentWebView.loadUrl("http://docs.google.com/gview?embedded=true&url=" + file_Path)
            }

        } else {
            viewImgLayout.visibility = View.VISIBLE
            imageLayout.visibility = View.VISIBLE
            playMusicLayout.visibility = View.GONE
            musicLayout.visibility = View.GONE

            if (!file_Path.equals("")) {
                Picasso.with(activity).load(file_Path).placeholder(R.mipmap.place_holder_img).into(imgView)
            }
        }


        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }

        val offlineStr = prefs!!.getString(Utils.SAVE_OFFLINE, "")

        if (!offlineStr.equals("null")) {
            try {
                Utils.OFFLINE_IMG_ARRAY = JSONArray(offlineStr)
                if (Utils.OFFLINE_IMG_ARRAY.length() > 0) {
                    for (i in 0..(Utils.OFFLINE_IMG_ARRAY.length() - 1)) {
                        val jsonObj = Utils.OFFLINE_IMG_ARRAY.getJSONObject(i)

                        if (file_id.equals(jsonObj.getString(Utils.OFFLINE_FILE_ID))) {
                            isAvailableImg = "1"
                            break
                        }
                    }
                }

                if (isAvailableImg.equals("0")) {
                    saveOfflineText.setText(resources.getString(R.string.save_offline))
                } else {
                    saveOfflineText.setText(resources.getString(R.string.already_saved))
                }
            } catch (excepetion: JSONException) {

            }
        }

        imgeNameText.text = fileName
        imageTimeText.text = fileCreated
        titleText.text = "Back"

        setFavouriteValue()
    }


    internal var thread = Thread(Runnable {

    })

    fun init() {
        loaderLayout = findViewById(R.id.loaderLayout)
        titleText = findViewById(R.id.titleText)
        backView = findViewById(R.id.backView)
        shareView = findViewById(R.id.shareView)
        openOptionLayout = findViewById(R.id.openOptionLayout)
        imgView = findViewById(R.id.imgView)
        moreOptionView = findViewById(R.id.moreOptionView)

        imageLayout = findViewById(R.id.imageLayout)
        musicLayout = findViewById(R.id.musicLayout)

        favImg = findViewById(R.id.favImg)
        favText = findViewById(R.id.favText)
        imageTimeText = findViewById(R.id.imageTimeText)
        imgeNameText = findViewById(R.id.imgeNameText)
        viewImgLayout = findViewById(R.id.viewImgLayout)
        playMusicLayout = findViewById(R.id.playMusicLayout)
        saveOfflineText = findViewById(R.id.saveOfflineText)

        imgDownloadLayout = findViewById(R.id.imgDownloadLayout)
        imgFavouriteLayout = findViewById(R.id.imgFavouriteLayout)
        renameImgLayout = findViewById(R.id.renameImgLayout)
        moveImgLayout = findViewById(R.id.moveImgLayout)
        copyImgLayout = findViewById(R.id.copyImgLayout)
        saveOfflineImgLayout = findViewById(R.id.saveOfflineImgLayout)
        propertiesLayout = findViewById(R.id.propertiesLayout)
        moveToTrashLayout = findViewById(R.id.moveToTrashLayout)
        deleteLayout = findViewById(R.id.deleteLayout)

        backView.setOnClickListener(clickListener)
        shareView.setOnClickListener(clickListener)
        imgView.setOnClickListener(clickListener)
        openOptionLayout.setOnClickListener(clickListener)

        playMusicLayout.setOnClickListener(clickListener)
        viewImgLayout.setOnClickListener(clickListener)
        imgDownloadLayout.setOnClickListener(clickListener)
        imgFavouriteLayout.setOnClickListener(clickListener)
        renameImgLayout.setOnClickListener(clickListener)
        moveImgLayout.setOnClickListener(clickListener)
        copyImgLayout.setOnClickListener(clickListener)
        saveOfflineImgLayout.setOnClickListener(clickListener)
        propertiesLayout.setOnClickListener(clickListener)
        moveToTrashLayout.setOnClickListener(clickListener)
        deleteLayout.setOnClickListener(clickListener)
        imageLayout.setOnClickListener(clickListener)
        musicLayout.setOnClickListener(clickListener)

        //Document View
        documentWebView = findViewById(R.id.documentWebView)
        documentLayout = findViewById(R.id.documentLayout)

        documentLayout.setOnClickListener(clickListener)
        documentWebView.setOnClickListener(clickListener)

        // Music View.....
        playView = findViewById(R.id.playView)
        pauseView = findViewById(R.id.pauseView)
        seekBar = findViewById(R.id.seekBar)
        timePlayingView = findViewById(R.id.timePlayingView)
        totalPlaytimeView = findViewById(R.id.totalPlaytimeView)


        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                if (mPlayer != null && b) {
                    mPlayer!!.seekTo(i * mInterval)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {

            }
        })


        playView.setOnClickListener(clickListener)
        pauseView.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                if (FILE_TYPE.equals(Utils.FAVOURITE_MUSIC)) {
                    stopPlaying()
                    finish()
                } else {
                    finish()
                }

            }
            R.id.openOptionLayout -> {
                if (TYPE.equals(Utils.TRASH)) {
                    moveToTrashLayout.visibility = View.GONE
                    deleteLayout.visibility = View.VISIBLE
                } else {
                    moveToTrashLayout.visibility = View.VISIBLE
                    deleteLayout.visibility = View.GONE
                }

                moreOptionView.visibility = View.VISIBLE
            }
            R.id.imgView -> {

                moreOptionView.visibility = View.GONE

            }
            R.id.documentWebView -> {

                moreOptionView.visibility = View.GONE

            }
            R.id.musicLayout -> {

                moreOptionView.visibility = View.GONE

            }
            R.id.shareView -> {
                val bitmap = Utils.getBitmapFromURL(file_Path)
                val uri = bitmap!!.saveToInternalStorage(this)
                Toast.makeText(applicationContext, uri.toString(), Toast.LENGTH_SHORT).show()
                this.shareCacheDirBitmap(uri)
            }
            R.id.viewImgLayout -> {
                moreOptionView.visibility = View.GONE
                if (FILE_TYPE.equals(Utils.DOCUMENT)) {

                    val intent = Intent(activity, PDFViewerActivity::class.java)
                    val bundle = Bundle()
                    bundle.putString("filePath", file_Path)
                    bundle.putString(Utils.TYPE, "")
                    bundle.putString("fileID", "")
                    bundle.putString("fileName", fileName)
                    intent.putExtras(bundle)
                    startActivity(intent)

                } else {
                    val intent = Intent(applicationContext, ZoomImageActivity::class.java)
                    val bundle = Bundle()
                    bundle.putString("imagePath", file_Path)
                    bundle.putString(Utils.TYPE, "")
                    bundle.putString("fileID", "")
                    bundle.putString("fileName", fileName)
                    intent.putExtras(bundle)
                    startActivity(intent)
                }


            }
            R.id.playMusicLayout -> {
                moreOptionView.visibility = View.GONE
                stopPlaying()
                val intent = Intent(activity, MusicPlayerActivity::class.java)
                val bundle = Bundle()
                bundle.putString("filePath", file_Path)
                bundle.putString(Utils.TYPE, "")
                bundle.putString("fileID", "")
                bundle.putString("fileName", fileName)
                intent.putExtras(bundle)
                startActivity(intent)

            }
            R.id.imgDownloadLayout -> {
                moreOptionView.visibility = View.GONE

                if (!file_Path.equals("")) {
                    val bitmap = Utils.getBitmapFromURL(file_Path)
                    MediaStore.Images.Media.insertImage(getContentResolver(), bitmap, "digilife", "image")
                    Toast.makeText(activity, "File Saved Successfully", Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(activity, "File Not Found", Toast.LENGTH_LONG).show()
                }

            }
            R.id.imgFavouriteLayout -> {
                if (isFav.equals("1")) {
                    isFav = "0"
                    isFavourite()
                } else {
                    isFav = "1"
                    isFavourite()
                }
            }
            R.id.renameImgLayout -> {
                showRenameDialog()
            }
            R.id.moveImgLayout -> {
                showMoveCopyDialog(resources.getString(R.string.move_text), "move")
            }
            R.id.copyImgLayout -> {
                showMoveCopyDialog(resources.getString(R.string.copy_text), "copy")
            }
            R.id.saveOfflineImgLayout -> {
                if (isAvailableImg.equals("0")) {
                    isAvailableImg = "1"
                    saveOffline()
                }
            }
            R.id.propertiesLayout -> {
                val intent = Intent(activity, ImagePropertiesActivity::class.java)
                intent.putExtra(Utils.FILE_DATA, fileData)
                startActivity(intent)
            }
            R.id.moveToTrashLayout -> {
                showMoveTrashDialog(resources.getString(R.string.trash_text), Utils.TRASH)
            }

            R.id.deleteLayout -> {
                showMoveTrashDialog(resources.getString(R.string.delete_text), Utils.DELETE)
            }

            /*.........Music Click........*/

            R.id.playView -> {
                startMusic()
            }
            R.id.pauseView -> {
                pauseMusic()
            }
        }
    }


    fun setFavouriteValue() {
        if (isFav.equals("1")) {
            favImg.setImageResource(R.mipmap.img_favourite_full)
            favText.text = resources.getString(R.string.un_favourite)
        } else {
            favImg.setImageResource(R.mipmap.img_favourite)
            favText.text = resources.getString(R.string.favorite)
        }
    }

    var isAvailableImg: String = "0"

    fun saveOffline() {

        val jsonObj = JSONObject()
        jsonObj.put(Utils.OFFLINE_FILE_CLIENT_ID, client_id)
        jsonObj.put(Utils.OFFLINE_FILE_ID, file_id)
        jsonObj.put(Utils.OFFLINE_FILE_NAME, fileName)
        jsonObj.put(Utils.OFFLINE_FILE_PATH, file_Path)
        jsonObj.put(Utils.OFFLINE_FILE_TYPE, FILE_TYPE)
        Utils.OFFLINE_IMG_ARRAY.put(jsonObj)

        Utils.storeJSONArraylist(prefs, Utils.SAVE_OFFLINE, Utils.OFFLINE_IMG_ARRAY)
        Toast.makeText(activity, "Download Completed !", Toast.LENGTH_SHORT).show()

    }

    //TODO: Favourite Image API.......

    private fun isFavourite() {

        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.IS_FAV,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        setFavouriteValue()
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["file_id"] = file_id
                params["is_fav"] = isFav
                return params
                Log.e(" JSONData :", params.toString())
            }
        }

        Log.e(" API :", EndPoints.IS_FAV)
        requestQueue.add(stringRequest)
    }

    //TODO: Rename Image API.......
    private fun renameImageName(imageName: String) {

        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.EDIT_FILE_NAME,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        imgeNameText.text = imageName
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["file_id"] = file_id
                params["file_name"] = imageName
                return params
                Log.e(" JSONData :", params.toString())
            }
        }

        Log.e(" API :", EndPoints.EDIT_FILE_NAME)
        requestQueue.add(stringRequest)
    }

    //TODO: Trash Image API.......
    private fun trashImage() {

        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.TRASH_FILE,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        finish()
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["file_id"] = file_id
                return params
                Log.e(" JSONData :", params.toString())
            }
        }

        Log.e(" API :", EndPoints.TRASH_FILE)
        requestQueue.add(stringRequest)
    }

    //TODO: Delete Image API........
    private fun deleteImage() {

        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.DELETE_FILE,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        finish()
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["id"] = file_id
                params["type"] = "file"
                return params
                Log.e(" JSONData :", params.toString())
            }
        }

        Log.e(" API :", EndPoints.DELETE_FILE)
        requestQueue.add(stringRequest)
    }

    var extention: String = ""
    var name: String = ""

    fun getFileExtention(fileName: String) {
        val filenameArray = fileName.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        extention = filenameArray[filenameArray.size - 1]
        name = filenameArray[0]
    }


    fun showRenameDialog() {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = activity.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.file_edit_name_dialog, null)
        dialog.setContentView(view)

        val fileNameEditText = dialog.findViewById(R.id.fileNameEditText) as EditText
        val submitBtn = dialog.findViewById(R.id.submitBtn) as Button
        getFileExtention(fileName)
        fileNameEditText.setText(name)

        val font = Typeface.createFromAsset(assets, resources.getString(R.string.popins_regular))
        fileNameEditText.setTypeface(font)
        submitBtn.setTypeface(font)

        submitBtn.setOnClickListener {
            dialog.dismiss()
            renameImageName(fileNameEditText.text.toString() + "." + extention)
        }

        var window = dialog.getWindow();
        window.setLayout(700, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }

    fun showMoveCopyDialog(message: String, type: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = activity.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.file_move_dialog, null)
        dialog.setContentView(view)

        val cancleBtn = dialog.findViewById(R.id.cancleText) as TextView
        val goToBtn = dialog.findViewById(R.id.goToBtn) as Button
        val dialogText = dialog.findViewById(R.id.dialogText) as TextView

        dialogText.setText(message)
        val font = Typeface.createFromAsset(assets, resources.getString(R.string.popins_semi_bold))
        goToBtn.setTypeface(font)

        goToBtn.setOnClickListener {
            if (type.equals("move")) {
                Utils.storeString(prefs, Utils.COPY_FILE_ID, "")
                Utils.storeString(prefs, Utils.MOVE_FILE_ID, file_id)
            } else {
                Utils.storeString(prefs, Utils.MOVE_FILE_ID, "")
                Utils.storeString(prefs, Utils.COPY_FILE_ID, file_id)
            }

            dialog.dismiss()
            finish()
        }

        cancleBtn.setOnClickListener {
            dialog.dismiss()
        }

        var window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }

    fun showMoveTrashDialog(message: String, type: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = activity.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.confirmation_dialog, null)
        dialog.setContentView(view)

        val cancleBtn = dialog.findViewById(R.id.cancleText) as TextView
        val okBtn = dialog.findViewById(R.id.okBtn) as Button
        val dialogText = dialog.findViewById(R.id.dialogText) as TextView
        dialogText.setText(message)

        val font = Typeface.createFromAsset(assets, resources.getString(R.string.popins_semi_bold))
        okBtn.setTypeface(font)

        okBtn.setOnClickListener {

            if (type.equals(Utils.TRASH)) {
                trashImage()
            } else {
                deleteImage()
            }

            dialog.dismiss()
        }

        cancleBtn.setOnClickListener {
            dialog.dismiss()
        }

        var window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }

    //TODO : Music Play - Pause Code.....

    var oneTimeOnly = 0

    fun startMusic() {
        mPlayer!!.start()
        getAudioStats()

        if (oneTimeOnly == 0) {
            initializeSeekBar()
            oneTimeOnly = 1
        }

        playView.visibility = View.GONE
        pauseView.visibility = View.VISIBLE
    }

    fun pauseMusic() {
        if (mPlayer != null && mPlayer!!.isPlaying()) {
            mPlayer!!.pause()
            playView.visibility = View.VISIBLE
            pauseView.visibility = View.GONE
        }
    }


    protected fun getAudioStats() {
        val duration = mPlayer!!.getDuration() / 1000 // In milliseconds
        val due = (mPlayer!!.getDuration() - mPlayer!!.getCurrentPosition()) / 1000
        val pass = duration - due

        timePlayingView.setText("$pass sec")
        totalPlaytimeView.setText("$duration sec")
    }

    protected fun initializeSeekBar() {
        seekBar.setMax(mPlayer!!.getDuration() / mInterval)

        mRunnable = Runnable {
            if (mPlayer != null) {
                val mCurrentPosition = mPlayer!!.getCurrentPosition() / mInterval // In milliseconds
                seekBar.setProgress(mCurrentPosition)
                getAudioStats()
            }
            mHandler!!.postDelayed(mRunnable, mInterval.toLong())
        }
        mHandler!!.postDelayed(mRunnable, mInterval.toLong())
    }

    protected fun stopPlaying() {
        // If media player is not null then try to stop it
        if (mPlayer != null) {
            mPlayer!!.stop()
            mPlayer!!.release()
            mPlayer = null
            if (mHandler != null) {
                mHandler!!.removeCallbacks(mRunnable)
            }
        }

    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (FILE_TYPE.equals(Utils.FAVOURITE_MUSIC)) {
            stopPlaying()
            finish()
        } else {
            finish()
        }
    }

}
