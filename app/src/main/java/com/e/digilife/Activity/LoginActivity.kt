package com.e.digilife.Activity

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Bundle
import android.provider.Settings
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.view.View
import android.widget.*
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.e.digilife.R
import com.e.digilife.View.InputValidation
import com.e.digilife.View.Utils
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginManager.getInstance
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.Auth
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.auth.api.signin.GoogleSignInResult
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.shurlock.View.EndPoints
import org.json.JSONObject
import java.util.*

class LoginActivity : AppCompatActivity(), GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {


    val TAG = "LoginActivity"
    val activity = this@LoginActivity
    private lateinit var inputValidation: InputValidation
    internal lateinit var requestQueue: RequestQueue

    lateinit var emailEditText: EditText
    lateinit var passwordEditText: EditText
    lateinit var signBtn: Button
    lateinit var signUpTextview: TextView
    lateinit var forgotPwTextText: TextView
    lateinit var facebookLoginBtn: FrameLayout
    lateinit var googleLoginBtn: FrameLayout
    lateinit var passwordView: ImageView
    lateinit var loaderLayout: LinearLayout

    var deviceToken = ""
    val deviceType = "android"
    var isPasswordVisible: Boolean = false


    private var callbackManager: CallbackManager? = null
    private val SIGN_IN = 30
    private var mGoogleApiClient: GoogleApiClient? = null

    var prefs: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        init()
        requestQueue = Volley.newRequestQueue(this)
        prefs = this.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        FacebookSdk.sdkInitialize(applicationContext)
        callbackManager = CallbackManager.Factory.create()
        faceBookInitialize()

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()

//        mGoogleApiClient = GoogleApiClient.Builder(this)
//            .enableAutoManage(this, this)
//            .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
//            .build()

        deviceToken = Settings.Secure.getString(
            applicationContext.contentResolver,
            Settings.Secure.ANDROID_ID
        )
    }

    var font: Typeface? = null

    fun init() {
        inputValidation = InputValidation(activity)
        loaderLayout = findViewById(R.id.loaderLayout)

        passwordView = findViewById(R.id.passwordView)
        emailEditText = findViewById(R.id.emailEditText)
        passwordEditText = findViewById(R.id.passwordEditText)
        signBtn = findViewById(R.id.signBtn)
        signUpTextview = findViewById(R.id.signUpTextview)
        forgotPwTextText = findViewById(R.id.forgotPwTextText)
        facebookLoginBtn = findViewById(R.id.facebookLoginBtn)
        googleLoginBtn = findViewById(R.id.googleLoginBtn)


        signBtn.setOnClickListener(clickListener)
        forgotPwTextText.setOnClickListener(clickListener)
        signUpTextview.setOnClickListener(clickListener)
        passwordView.setOnClickListener(clickListener)
        // facebookLoginBtn.setOnClickListener(clickListener)
        // googleLoginBtn.setOnClickListener(clickListener)


        font = Typeface.createFromAsset(assets, resources.getString(R.string.popins_regular))
        emailEditText.setTypeface(font)
        passwordEditText.setTypeface(font)
        signBtn.setTypeface(font)
    }


    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.signBtn -> {
                validation()
            }
            R.id.passwordView -> {
                if (isPasswordVisible == false) {
                    isPasswordVisible = true
                    passwordView.setImageResource(R.mipmap.ic_password)
                    passwordEditText.setTransformationMethod(HideReturnsTransformationMethod.getInstance())
                    passwordEditText.setTypeface(font)
                } else {
                    isPasswordVisible = false
                    passwordView.setImageResource(R.mipmap.ic_password_close)
                    passwordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance())
                    passwordEditText.setTypeface(font)

                }
            }

            R.id.forgotPwTextText -> {
                val intent = Intent(applicationContext, ForgotPasswordActivity::class.java)
                startActivity(intent)
            }
            R.id.signUpTextview -> {
                val intent = Intent(applicationContext, SignUpActivity::class.java)
                startActivity(intent)
            }
            R.id.facebookLoginBtn -> {
                if (Utils.checkInternetConnection(this@LoginActivity))
                    LoginManager.getInstance().logInWithReadPermissions(
                        this@LoginActivity,
                        Arrays.asList("email", "public_profile")
                    )
                else
                    Utils.showMessageDialog(
                        this@LoginActivity,
                        getString(R.string.app_name),
                        getString(R.string.check_internet)
                    )

            }
            R.id.googleLoginBtn -> {
                if (Utils.checkInternetConnection(this@LoginActivity))
                    googlePlusLogin()
                else
                    Utils.showMessageDialog(
                        this@LoginActivity,
                        getString(R.string.app_name),
                        getString(R.string.check_internet)
                    )
            }
        }
    }

    private fun login(
        loginType: String,
        email: String,
        password: String,
        social_id: String,
        social_type: String,
        device_token: String,
        device_type: String
    ) {


        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.LOGIN,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {


                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        val dataObj = jsonObj.getJSONObject("data")

                        Utils.storeString(prefs, Utils.LOGIN_OBJ, dataObj.toString())

                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                        val intent = Intent(applicationContext, DashboardActivity::class.java)
                        startActivity(intent)
                        finish()

                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    // error
                    // Log.d("Error.Response", ${error.message})
                }
            }
        ) {


            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["email"] = email
                params["password"] = password
                params["type"] = loginType
                params["social_type"] = social_type
                params["social_id"] = social_id
                params["device_token"] = device_token
                params["device_type"] = device_type
                Log.e(" JSONData :", params.toString())
                return params


            }
        }

        Log.e(" API :", EndPoints.LOGIN)
        requestQueue.add(stringRequest)
    }

    //TODO: Facebook Login....

    fun faceBookInitialize() {
        getInstance().registerCallback(callbackManager!!, object : FacebookCallback<LoginResult> {
            override fun onSuccess(result: LoginResult) {
                val accessToken = AccessToken.getCurrentAccessToken()
                val request = GraphRequest.newMeRequest(accessToken) { `object`, response ->

                    Log.e("Facebook SignIn: ", response.toString());
                    var SfacebookID = ""
                    var fbFname = ""
                    var fbLname = ""
                    var fbEmail = ""
                    try {
                        if (`object`.has("id")) {
                            SfacebookID = `object`.getString("id")
                        }

                        if (`object`.has("first_name")) {
                            fbFname = `object`.getString("first_name")
                        }

                        if (`object`.has("last_name")) {
                            fbLname = `object`.getString("last_name")
                        }

                        if (`object`.has("email")) {
                            fbEmail = `object`.getString("email")
                        }

                        // if (`object`.has("picture")) {
                        //     Surl = `object`.getJSONObject("picture").getJSONObject("data").getString("url")
                        // }

                        Log.d("SfacebookID", SfacebookID)
                        Log.d(TAG, "fbFname " + fbFname)
                        Log.d(TAG, "fbLname " + fbLname)
                        Log.d(TAG, "fbEmail " + fbEmail)

                        // login("facebook", fbEmail, "", SfacebookID, "facebook", deviceToken, deviceType)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    if (fbLname.equals("null")) {
                        fbLname = ""
                    }

                    if (fbFname.equals("null")) {
                        fbFname = ""
                    }


                }
                val parameters = Bundle()
                parameters.putString("fields", "id, email, first_name, last_name")
                request.parameters = parameters
                request.executeAsync()
            }

            override fun onCancel() {
                //TODO Auto-generated method stub
                println("=========================onCancel")
                //  Toast.makeText(this@LoginActivity, "Cancel", Toast.LENGTH_LONG).show()
            }

            override fun onError(error: FacebookException) {
                //TODO Auto-generated method stub
                println("=========================onError" + error.toString())
//                Toast.makeText(this@LoginActivity, "onError", Toast.LENGTH_LONG).show()
                if (AccessToken.getCurrentAccessToken() != null) {
                    LoginManager.getInstance().logOut()
                }
                LoginManager.getInstance()
                    .logInWithReadPermissions(this@LoginActivity, Arrays.asList("email", "public_profile"))
            }
        })
    }


    //TODO: Google+ Login....

    fun googlePlusLogin() {
        val signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient)
        startActivityForResult(signInIntent, SIGN_IN)
    }

    private fun handleSignInResult(result: GoogleSignInResult) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess)

        if (result.isSuccess) {

            try {

                //Getting google account
                val acct = result.signInAccount
                Log.e("Google SignIn: ", acct.toString());
                val personName = acct!!.displayName
                var personGivenName = acct.givenName
                var personFamilyName = acct.familyName
                val personEmail = acct.email
                val personId = acct.id
                val personPhoto = acct.photoUrl

                Log.d("personName", personName)
                Log.d(TAG, "personGivenName " + personGivenName)
                Log.d(TAG, "personFamilyName " + personFamilyName)
                Log.d(TAG, "personEmail " + personEmail)
                Log.d("personId ", personId)
                Log.d(TAG, "personPhoto " + personPhoto)

                if (personGivenName.equals("null")) {
                    personGivenName = ""
                }

                if (personFamilyName.equals("null")) {
                    personFamilyName = ""
                }
                Toast.makeText(activity, "Successfully google+ login ...", Toast.LENGTH_SHORT).show()

                // login("google", personEmail!!, "", personId!!, "google", deviceToken, deviceType)

            } catch (e: NullPointerException) {
                e.printStackTrace()
            }

        } else run {
            //If login fails
            // Toast.makeText(this, "Login Failed", Toast.LENGTH_LONG).show()
        }
    }

    override fun onConnected(p0: Bundle?) {
    }

    override fun onConnectionSuspended(p0: Int) {
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
        Toast.makeText(applicationContext, "Google Play Services error.", Toast.LENGTH_SHORT).show();
    }


    override fun onActivityResult(requestCode: Int, responseCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, responseCode, data)

        callbackManager!!.onActivityResult(requestCode, responseCode, data)
        if (requestCode == SIGN_IN) {
            val result = Auth.GoogleSignInApi.getSignInResultFromIntent(data)
            handleSignInResult(result)

        }
    }


    private fun validation() {
        if (!inputValidation.isInputEmail(emailEditText, getString(R.string.email_validation))) {
            return
        }
        if (!inputValidation.isInputPassword(passwordEditText, getString(R.string.password_validation))) {
            return
        }

        val email = emailEditText.text.toString()
        val password = passwordEditText.text.toString()

        if (Utils.checkInternetConnection(this@LoginActivity))
            login("manual", email, password, "", "", deviceToken, deviceType)
        else
            Utils.showMessageDialog(
                this@LoginActivity,
                getString(R.string.app_name),
                getString(R.string.check_internet)
            )
    }
}
