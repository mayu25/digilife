package com.e.digilife.Activity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.support.v7.app.AppCompatActivity
import com.e.digilife.R
import com.e.digilife.View.Utils

class SplashActivity : AppCompatActivity() {

    private val SPLASH_DELAY: Long = 2000 //2 seconds
    private val activity = this@SplashActivity

    var prefs: SharedPreferences? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        prefs = this.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        var loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")

        Handler().postDelayed({

            if (!loginRes.equals("")) {
                val i = Intent(this@SplashActivity, DashboardActivity::class.java)
                startActivity(i)
                finish()
            } else {
                val i = Intent(this@SplashActivity, LoginActivity::class.java)
                startActivity(i)
                finish()
            }


        }, SPLASH_DELAY)
    }
}
