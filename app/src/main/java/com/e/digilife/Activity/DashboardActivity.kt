package com.e.digilife.Activity

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v4.widget.DrawerLayout
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.*
import android.widget.Button
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.e.digilife.Fragment.*
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.shurlock.View.EndPoints
import com.squareup.picasso.Picasso
import org.json.JSONObject


class DashboardActivity : AppCompatActivity() {


    val TAG = "DashboardActivity"
    val activity = this@DashboardActivity
    var prefs: SharedPreferences? = null

    lateinit var nav_view: NavigationView
    lateinit var navHeader: View

    lateinit var dashboardLayout: LinearLayout
    lateinit var walletLayout: LinearLayout
    lateinit var taskLayout: LinearLayout
    lateinit var calenderLayout: LinearLayout
    lateinit var emrLayout: LinearLayout
    lateinit var personalLayout: LinearLayout
    lateinit var customFolerLayout: LinearLayout
    lateinit var pillReminderLayout: LinearLayout
    lateinit var fitnessReminderLayout: LinearLayout
    lateinit var tripLayout: LinearLayout
    lateinit var favoriteLayout: LinearLayout
    lateinit var recentLayout: LinearLayout
    lateinit var trashLayout: LinearLayout
    lateinit var offlineLayout: LinearLayout
    lateinit var profileLayout: LinearLayout
    lateinit var logoutLayout: LinearLayout

    lateinit var shareLinkView: ImageView

    lateinit var imageView: ImageView
    lateinit var nameText: TextView
    lateinit var emailText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)

        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)

        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
            this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close
        )
        drawerLayout.addDrawerListener(toggle)
        toggle.syncState()


        prefs = this.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)

        init()

        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            var Name = loginObj.getString("name")
            var Email = loginObj.getString("email")
            // var ImagePath = loginObj.getString("profile_photo")
            Utils.storeString(prefs!!, Utils.CURRENCY, loginObj.getString("currency_logo"))
            nameText.text = Name
            emailText.text = Email
//            if (!ImagePath.equals("")) {
//                Picasso.with(activity).load(EndPoints.ROOT + ImagePath).placeholder(R.mipmap.ic_username)
//                    .into(imageView)
//            }
        }


        val dashboardFragment = DashboardFragment()
        replaceFragment(dashboardFragment)
        Utils.navItemIndex = 0
    }

    fun init() {

        nav_view = findViewById(R.id.nav_view)
        navHeader = nav_view.getHeaderView(0)
        imageView = navHeader.findViewById(R.id.imageView)
        nameText = navHeader.findViewById(R.id.nameText)
        emailText = navHeader.findViewById(R.id.emailText)

        dashboardLayout = navHeader.findViewById(R.id.dashboardLayout)
        walletLayout = navHeader.findViewById(R.id.walletLayout)
        emrLayout = navHeader.findViewById(R.id.emrLayout)
        personalLayout = navHeader.findViewById(R.id.personalLayout)
        customFolerLayout = navHeader.findViewById(R.id.customFolerLayout)
        pillReminderLayout = navHeader.findViewById(R.id.pillReminderLayout)
        fitnessReminderLayout = navHeader.findViewById(R.id.fitnessReminderLayout)
        favoriteLayout = navHeader.findViewById(R.id.favoriteLayout)
        recentLayout = navHeader.findViewById(R.id.recentLayout)
        trashLayout = navHeader.findViewById(R.id.trashLayout)
        offlineLayout = navHeader.findViewById(R.id.offlineLayout)
        profileLayout = navHeader.findViewById(R.id.profileLayout)
        logoutLayout = navHeader.findViewById(R.id.logoutLayout)
        taskLayout = navHeader.findViewById(R.id.taskLayout)

        shareLinkView = navHeader.findViewById(R.id.shareLinkView)

        imageView.setOnClickListener(clickListener)
        dashboardLayout.setOnClickListener(clickListener)
        walletLayout.setOnClickListener(clickListener)
        emrLayout.setOnClickListener(clickListener)
        personalLayout.setOnClickListener(clickListener)
        customFolerLayout.setOnClickListener(clickListener)
        pillReminderLayout.setOnClickListener(clickListener)
        fitnessReminderLayout.setOnClickListener(clickListener)
        favoriteLayout.setOnClickListener(clickListener)
        recentLayout.setOnClickListener(clickListener)
        trashLayout.setOnClickListener(clickListener)
        offlineLayout.setOnClickListener(clickListener)
        profileLayout.setOnClickListener(clickListener)
        logoutLayout.setOnClickListener(clickListener)
        taskLayout.setOnClickListener(clickListener)

        shareLinkView.setOnClickListener(clickListener)

    }

    fun changeValue() {

        prefs = this.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")

        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            var Name = loginObj.getString("name")
            var Email = loginObj.getString("email")
            var ImagePath = loginObj.getString("profile_photo")
            nameText.text = Name
            emailText.text = Email
            if (!ImagePath.equals("")) {
                Picasso.with(activity).load(EndPoints.ROOT + ImagePath).placeholder(R.mipmap.ic_username)
                    .into(imageView)
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            changeValue()
        }
    }


    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.imageView -> {
                val intent = Intent(applicationContext, ProfileActivity::class.java)
                startActivityForResult(intent, 101);
                // startActivity(intent)
            }
            R.id.dashboardLayout -> {
                val dashboardFragment = DashboardFragment()
                replaceFragment(dashboardFragment)
                Utils.navItemIndex = 0
            }
            R.id.walletLayout -> {
                val intent = Intent(applicationContext, WalletActivity::class.java)
                startActivity(intent)
            }
            R.id.taskLayout -> {
//                val taskFragment = TaskFragment()
//                replaceFragment(taskFragment)
//                Utils.navItemIndex = 2
                val intent = Intent(applicationContext, TaskMainActivity::class.java)
                startActivity(intent)
            }
            R.id.emrLayout -> {
                val emrFragment = EMRFragment()
                replaceFragment(emrFragment)
                Utils.navItemIndex = 4
            }
            R.id.personalLayout -> {
                val personalFragment = PersonalFragment()
                replaceFragment(personalFragment)
                Utils.navItemIndex = 5
            }
            R.id.customFolerLayout -> {
                val intent = Intent(applicationContext, CustomFolderActivity::class.java)
                startActivity(intent)
            }
            R.id.pillReminderLayout -> {
                val intent = Intent(applicationContext, PillReminderActivity::class.java)
                startActivity(intent)
            }

            R.id.fitnessReminderLayout -> {
                val intent = Intent(applicationContext, FitnessReminderActivity::class.java)
                startActivity(intent)
            }
            R.id.favoriteLayout -> {
                val favFragment = FavoriteFragment()
                replaceFragment(favFragment)
                Utils.navItemIndex = 10
            }
            R.id.recentLayout -> {
                val recentFragment = RecentFragment()
                replaceFragment(recentFragment)
                Utils.navItemIndex = 11
            }
            R.id.trashLayout -> {
                val trashFragment = TrashFragment()
                replaceFragment(trashFragment)
                Utils.navItemIndex = 12
            }

            R.id.offlineLayout -> {
                val OfflineFragment = OfflineFragment()
                replaceFragment(OfflineFragment)
                Utils.navItemIndex = 13
            }
            R.id.profileLayout -> {
                val intent = Intent(applicationContext, ProfileActivity::class.java)
                startActivityForResult(intent, 101);
            }
            R.id.logoutLayout -> {
                showLogoutMessageDialog(getString(R.string.logout_text))
                Utils.navItemIndex = 15
            }

            R.id.shareLinkView -> {
                try {
                    val shareIntent = Intent(Intent.ACTION_SEND)
                    shareIntent.type = "text/plain"
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Digilife")
                    var shareMessage = "\nLet me recommend you this application\n\n"
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                    startActivity(Intent.createChooser(shareIntent, "choose one"))
                } catch (e: Exception) {
                    //e.toString();
                }

            }
        }
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        drawerLayout.closeDrawer(GravityCompat.START)
    }


    override fun onBackPressed() {
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else if (Utils.navItemIndex != 0) {

            val dashboardFragment = DashboardFragment()
            replaceFragment(dashboardFragment)
            Utils.navItemIndex = 0
        } else {
            super.onBackPressed()
        }
    }


    fun showLogoutMessageDialog(message: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = activity.getSystemService(LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.confirmation_dialog, null)
        dialog.setContentView(view)

        val cancleBtn = dialog.findViewById(R.id.cancleText) as TextView
        val okBtn = dialog.findViewById(R.id.okBtn) as Button
        val dialogText = dialog.findViewById(R.id.dialogText) as TextView
        dialogText.setText(message)

        val font = Typeface.createFromAsset(assets, resources.getString(R.string.popins_semi_bold))
        okBtn.setTypeface(font)

        okBtn.setOnClickListener {
            Utils.storeString(prefs, Utils.LOGIN_OBJ, "")
            Utils.storeString(prefs, Utils.MOVE_FILE_ID, "")
            Utils.storeString(prefs, Utils.COPY_FILE_ID, "")
            Utils.storeJSONArraylist(prefs, Utils.SAVE_OFFLINE, null)
            val intent = Intent(applicationContext, LoginActivity::class.java)
            startActivity(intent)
            finish()

            dialog.dismiss()
        }

        cancleBtn.setOnClickListener {
            dialog.dismiss()
        }

        var window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }

//    override fun onCreateOptionsMenu(menu: Menu): Boolean {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        menuInflater.inflate(R.menu.dashboard, menu)
//        return true
//    }

    fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.frameLayout, fragment).commit()
    }


}
