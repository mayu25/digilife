package com.e.digilife.Activity


import android.Manifest
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.location.*
import android.media.MediaScannerConnection
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.InputType
import android.text.TextWatcher
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.e.digilife.R
import com.e.digilife.View.DateFormat
import com.e.digilife.View.Utils
import com.e.digilife.View.VolleyMultipartRequest
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import com.shurlock.View.EndPoints

import lib.kingja.switchbutton.SwitchMultiButton
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL
import java.net.URLEncoder
import java.text.SimpleDateFormat
import java.util.*
import java.util.ArrayList as ArrayList1


class AddTaskActivity<dialogViewremider> : AppCompatActivity(), AdapterView.OnItemClickListener, View.OnClickListener {

    lateinit var titleText: TextView
    lateinit var backView: ImageView
    lateinit var img_attachment: ImageView
    lateinit var btn_add_note: Button
    lateinit var btn_attachment: Button
    lateinit var reminderLayout: LinearLayout
    private var locationManager: LocationManager? = null
    lateinit var categoryLayout: LinearLayout
    lateinit var img_add_tag: Button
    var picBitmap: Bitmap? = null
    lateinit var dialogViewremider: View
    lateinit var imageSelectionView: LinearLayout
    lateinit var cameraView: TextView
    lateinit var galleryView: TextView
    lateinit var cancelView: TextView
    lateinit var documentView: TextView
    lateinit var txt_tag: TextView
    lateinit var txt_date_start_on: TextView
    var markedButtons: String = ""
    var checked_sunday: String = ""
    var checked_monday: String = ""
    var checked_tuesday: String = ""
    var checked_wednesday: String = ""
    var checked_thursday: String = ""
    var checked_friday: String = ""
    var checked_sat: String = ""
    var finaldayselectionvalue: String = ""

    lateinit var txt_place_addtask: AutoCompleteTextView
    lateinit var txt_remindme_select_date: TextView
    lateinit var img_delete_selected_time: ImageView
    lateinit var edit_sub_task: EditText
    lateinit var txt_note: TextView
    var radio_leave_reminder: RadioButton? = null
    var radio_arrive_reminder: RadioButton? = null
    lateinit var radio_arrive_location: RadioButton
    lateinit var radio_leave_location: RadioButton
    lateinit var radio_group_reminder_location: RadioGroup
    lateinit var edt_task_title: EditText
    lateinit var coder: Geocoder
    lateinit var txt_title_task: TextView
    lateinit var txt_main_time: TextView
    lateinit var img_save: TextView
    lateinit var spinner: Spinner
    var dateStringcurrent: String? = null
    var Category_name: String? = null
    lateinit var b: android.app.AlertDialog
    lateinit var listItemsTxt: Array<String>
    private val PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place"
    private val TYPE_AUTOCOMPLETE = "/autocomplete"
    private val OUT_JSON = "/json"
    private val API_KEY = "AIzaSyDYrsn6tnumUGhJEzbhVo494gNBoC3KQ_o"
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null
    var client_id: String = ""
    var bundle: Bundle? = null
    var TAGNAME: String = ""
    var maintime: String = ""
    var radio_arrive_loc: String = ""
    var ragistertime: String = ""
    var finalragistertime: String = ""
    var remindmetime: String = ""
    var repeat_day: String = ""
    var repeat_month: String = ""
    var repeat_week: String = ""
    var daily_date_time: String = ""
    var weekly_date_time: String = ""
    var monthly_date_time: String = ""
    var yearly_date_time: String = ""
    var repeat_year: String = ""
    var selected: String = ""
    var reminder_type: String = ""
    var onetime_type: String = ""
    var FinalRagistrationTimevalue: String = ""
    var repeat_on: String = ""
    var loc_type: String = ""
    var radio_reminder_final: String = ""
    var repeat_type: String = ""
    var OnStartdate: String = ""
    var cat_id: String = ""
    var longitude_place: Double = 0.toDouble()
    var latitude_place: Double = 0.toDouble()
    var current_lat: Double = 0.toDouble()
    var current_long: Double = 0.toDouble()
    private val REQUEST_CODE_EXAMPLE = 0x9988
    lateinit var loaderLayout: LinearLayout
    lateinit var toggle_sunday: ToggleButton
    lateinit var toggle_monday: ToggleButton
    lateinit var toggle_tusday: ToggleButton
    lateinit var toggle_wednesday: ToggleButton
    lateinit var toggle_thursday: ToggleButton
    lateinit var toggle_friday: ToggleButton
    lateinit var toggle_saturday: ToggleButton
    lateinit var include_1: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_task_layout)
        init()
        requestQueue = Volley.newRequestQueue(this@AddTaskActivity)

        ///get current lat-long

        locationManager = getSystemService(LOCATION_SERVICE) as LocationManager?;
        if (ContextCompat.checkSelfPermission(
                this@AddTaskActivity,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
            != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this@AddTaskActivity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                0x0
            )
        }

        try {
            // Request location updates
            locationManager?.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0L, 0f, locationListener);
        } catch (ex: SecurityException) {
            Log.d("myTag", "Security Exception, no location available");
        }


    }

    private val locationListener: LocationListener = object : LocationListener {
        override fun onLocationChanged(location: Location) {
            Log.d("test", "" + location.longitude + ":" + location.latitude)
            current_lat = location.latitude;
            current_long = location.longitude;
        }

        override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {}
        override fun onProviderEnabled(provider: String) {}
        override fun onProviderDisabled(provider: String) {}
    }

    fun init() {
        titleText = findViewById(R.id.titleText)
        backView = findViewById(R.id.backView)
        txt_title_task = findViewById(R.id.txt_title_task)
        txt_place_addtask = findViewById(R.id.txt_place_addtask)
        img_attachment = findViewById(R.id.img_attachment)
        btn_attachment = findViewById(R.id.btn_attachment)
        radio_arrive_location = findViewById(R.id.radio_arrive_location)
        radio_leave_location = findViewById(R.id.radio_leave_location)
        txt_tag = findViewById(R.id.txt_tag)
        img_save = findViewById(R.id.img_save)
        img_add_tag = findViewById(R.id.img_add_tag)
        loaderLayout = findViewById(R.id.loaderLayout)
        reminderLayout = findViewById(R.id.reminderLayout)
        txt_note = findViewById(R.id.txt_note)
        btn_add_note = findViewById(R.id.btn_add_note)
        categoryLayout = findViewById(R.id.categoryLayout)
        edt_task_title = findViewById(R.id.edt_task_title)
        edit_sub_task = findViewById(R.id.edit_sub_task)
        titleText.setText(R.string.addtask)
        backView.setOnClickListener(clickListener)
        categoryLayout.setOnClickListener(clickListener)
        btn_attachment.setOnClickListener(clickListener)
        btn_add_note.setOnClickListener(clickListener)
        reminderLayout.setOnClickListener(clickListener)
        imageSelectionView = findViewById(R.id.imageSelectionView)
        cameraView = findViewById(R.id.cameraView)
        galleryView = findViewById(R.id.galleryView)
        cancelView = findViewById(R.id.cancelView)
        documentView = findViewById(R.id.documentView)
        txt_main_time = findViewById(R.id.txt_main_time)
        documentView.visibility = View.GONE
        cameraView.setOnClickListener(clickListener)
        galleryView.setOnClickListener(clickListener)
        cancelView.setOnClickListener(clickListener)
        img_add_tag.setOnClickListener(clickListener)
        img_save.setOnClickListener(clickListener)
        txt_tag.visibility = View.INVISIBLE
        txt_place_addtask.setAdapter(GooglePlacesAutocompleteAdapter(this, R.layout.list_item))
        txt_place_addtask.setOnItemClickListener(this@AddTaskActivity)
        txt_place_addtask.setOnFocusChangeListener(View.OnFocusChangeListener { v, hasFocus ->
            txt_place_addtask.setOnItemClickListener(
                this@AddTaskActivity
            )
        })


        txt_place_addtask.setThreshold(1)


        txt_place_addtask.addTextChangedListener(object : TextWatcher {

            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
                txt_place_addtask.setOnItemClickListener(this@AddTaskActivity)

            }


            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {


            }

            override fun afterTextChanged(s: Editable) {


            }
        })


        prefs = applicationContext!!.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")

        }

        txt_place_addtask.setEnabled(false)
        txt_place_addtask.setInputType(InputType.TYPE_CLASS_TEXT)


        currentdatetime()

        txt_main_time.setText(dateStringcurrent)
        radiobutton_checked_listner_main()


    }


    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {

            R.id.backView -> {
                finish()
            }
            R.id.categoryLayout -> {
                if (Utils.checkInternetConnection(this@AddTaskActivity))
                    getcategoryList("", view)
                else
                    Utils.showMessageDialog(
                        this@AddTaskActivity,
                        getString(R.string.app_name),
                        getString(R.string.check_internet)
                    )


            }
            R.id.img_add_tag -> {
                val intent = Intent(applicationContext, TagListActivity::class.java)
                startActivityForResult(intent, 1000)

            }
            R.id.btn_add_note -> {
                Show_Note_Dialog();
            }
            R.id.reminderLayout -> {
                Show_Reminder_Dialog();
            }
            R.id.btn_attachment -> {
                if (ActivityCompat.checkSelfPermission(
                        this@AddTaskActivity,
                        android.Manifest.permission.CAMERA
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    ActivityCompat.requestPermissions(
                        this@AddTaskActivity, arrayOf(
                            android.Manifest.permission.CAMERA,
                            android.Manifest.permission.READ_EXTERNAL_STORAGE,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ), 0
                    )
                } else {
                    requirePermissions = false
                    imageSelectionView.visibility = View.VISIBLE
                }

            }

            R.id.cameraView -> {
                imageSelectionView.visibility = View.GONE
                preference = Utils.OPEN_CAMERA
                checkStoragePermission()
            }
            R.id.galleryView -> {
                imageSelectionView.visibility = View.GONE
                preference = Utils.OPEN_MEDIA
                checkStoragePermission()
            }
            R.id.cancelView -> {
                imageSelectionView.visibility = View.GONE
            }
            R.id.img_save -> {

                validation()
            }


        }
    }

    fun validation() {

        if (edt_task_title.text.toString().isEmpty()) {
            Toast.makeText(this@AddTaskActivity, "Please enter Task Title", Toast.LENGTH_SHORT).show()
        } else {
            if (Utils.checkInternetConnection(this@AddTaskActivity))

                AddTask()
            else
                Utils.showMessageDialog(
                    this@AddTaskActivity,
                    getString(R.string.app_name),
                    getString(R.string.check_internet)
                )


        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == GALLERY) {
            if (data != null) {
                val contentURI = data.data
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
                    val byteArrayOutputStream = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
                    picBitmap = bitmap
                    val path = saveImage(bitmap)
                    img_attachment.setImageBitmap(bitmap)

                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(this@AddTaskActivity, "Failed!", Toast.LENGTH_SHORT).show()
                }
            }

        } else if (requestCode == CAMERA) {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            val byteArrayOutputStream = ByteArrayOutputStream()
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
            picBitmap = thumbnail
            img_attachment.setImageBitmap(thumbnail)
            saveImage(thumbnail)
        } else {
            super.onActivityResult(requestCode, resultCode, data)
            val bla = data?.getStringExtra(Utils.TAG_NAME)
            if (bla != null) {
                txt_tag.visibility = View.VISIBLE
                txt_tag.setText(bla)
            } else {
                txt_tag.visibility = View.INVISIBLE
            }


        }


    }

    fun setMenu(jsonArray: JSONArray, view: View) {
        val menu = PopupMenu(this@AddTaskActivity, view)

        for (i in 0..(jsonArray.length() - 1)) {
            val jsonObj = jsonArray.getJSONObject(i)
            Category_name = jsonObj.getString("cat_name")
//            cat_id = jsonObj.getString("cat_id")

            menu.getMenu().add(Category_name)
            menu.show()

        }
        menu.setOnMenuItemClickListener(object : PopupMenu.OnMenuItemClickListener {
            override fun onMenuItemClick(item: MenuItem): Boolean {
                for (i in 0..(jsonArray.length() - 1)) {
                    val jsonObj = jsonArray.getJSONObject(i)
                    Category_name = jsonObj.getString("cat_name")
                    selected = item.toString()

                    if (Category_name == selected) {
                        cat_id = jsonObj.getString("cat_id")
                    }

                }

                txt_title_task.setText(item.toString());
                return true
            }
        })
    }

    fun Show_Note_Dialog() {
        val dialogBuilder = android.app.AlertDialog.Builder(this@AddTaskActivity)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.custom_addnote_dialog_layout, null)
        dialogBuilder.setView(dialogView)
        val editText = dialogView.findViewById<View>(R.id.et_note) as EditText
        editText.setMovementMethod(ScrollingMovementMethod())
        val btn_cancle = dialogView.findViewById<View>(R.id.btn_cancle) as Button
        val btn_done = dialogView.findViewById<View>(R.id.btn_done) as Button
        val b = dialogBuilder.create()
        b.show()

        btn_cancle.setOnClickListener {
            b.dismiss()
        }
        btn_done.setOnClickListener {
            var note: String = editText.text.toString()
            txt_note.setText(note)
            b.dismiss()
        }
    }

    fun Show_Reminder_Dialog() {
        lateinit var txt_place: AutoCompleteTextView

        val dialogBuilder = android.app.AlertDialog.Builder(this@AddTaskActivity)
        val inflater = this.layoutInflater
        dialogViewremider = inflater.inflate(R.layout.custom_reminder_dialog_layout, null)
        val lay_Repeat = dialogViewremider.findViewById<View>(R.id.lay_Repeat) as LinearLayout
        val lay_OneTime = dialogViewremider.findViewById<View>(R.id.lay_OneTime) as LinearLayout
        val lay_location = dialogViewremider.findViewById<View>(R.id.lay_location) as LinearLayout
        val lay_daily = dialogViewremider.findViewById<View>(R.id.lay_daily) as LinearLayout
        val lay_weekly = dialogViewremider.findViewById<View>(R.id.lay_weekly) as LinearLayout
        val lay_monthly = dialogViewremider.findViewById<View>(R.id.lay_monthly) as LinearLayout
        val lay_remind_me = dialogViewremider.findViewById<View>(R.id.lay_remind_me) as LinearLayout
        val lay_yearly = dialogViewremider.findViewById<View>(R.id.lay_yearly) as LinearLayout
        val lay_later_today = dialogViewremider.findViewById<View>(R.id.lay_later_today) as LinearLayout
        txt_remindme_select_date = dialogViewremider.findViewById(R.id.txt_remindme_select_date)
        include_1 = dialogViewremider.findViewById(R.id.daypicker);
        toggle_sunday = include_1.findViewById(R.id.toggle_sunday)
        toggle_monday = include_1.findViewById(R.id.toggle_monday)
        toggle_tusday = include_1.findViewById(R.id.toggle_tusday)
        toggle_wednesday = include_1.findViewById(R.id.toggle_wednesday)
        toggle_thursday = include_1.findViewById(R.id.toggle_thursday)
        toggle_friday = include_1.findViewById(R.id.toggle_friday)
        toggle_saturday = include_1.findViewById(R.id.toggle_saturday)
        img_delete_selected_time = dialogViewremider.findViewById(R.id.img_delete_selected_time)
        radio_group_reminder_location = dialogViewremider.findViewById(R.id.radio_group_reminder_location)
        val lay_this_evening = dialogViewremider.findViewById<View>(R.id.lay_this_evening) as LinearLayout
        radio_leave_reminder = dialogViewremider.findViewById(R.id.radio_leave_reminder)
        radio_arrive_reminder = dialogViewremider.findViewById(R.id.radio_arrive_reminder)
        val lay_tomorrow = dialogViewremider.findViewById<View>(R.id.lay_tomorrow) as LinearLayout
        val lay_nextweek = dialogViewremider.findViewById<View>(R.id.lay_nextweek) as LinearLayout
        val lay_custom = dialogViewremider.findViewById<View>(R.id.lay_custom) as LinearLayout
        val btn_cancle = dialogViewremider.findViewById<View>(R.id.btn_cancle) as Button
        val btn_done = dialogViewremider.findViewById<View>(R.id.btn_done) as Button
        val lay_repeat_on = dialogViewremider.findViewById<View>(R.id.lay_repeat_on) as LinearLayout
        spinner = dialogViewremider.findViewById(R.id.Spinner_selection)
        txt_date_start_on = dialogViewremider.findViewById(R.id.txt_date_start_on)
        txt_date_start_on = dialogViewremider.findViewById(R.id.txt_date_start_on)
        txt_place = dialogViewremider.findViewById(R.id.txt_place)

        if (radio_arrive_loc.equals("1")) {
            radio_arrive_reminder?.setChecked(true)
            radio_leave_reminder?.setChecked(false)
        } else if (radio_arrive_loc.equals("2")) {
            radio_arrive_reminder?.setChecked(false)
            radio_leave_reminder?.setChecked(true)
        }
        radiobutton_checked_listner_reminder()
//        val widget = dialogViewremider.findViewById(R.id.weekdays) as WeekdaysPicker
//        widget.setOnWeekdaysChangeListener(object : OnWeekdaysChangeListener {
//            override fun onChange(view: View, clickedDayOfWeek: Int, selectedDays: List<Int>) {
//                // Do Something
//                repeat_on = selectedDays.toString().replace("[", "").replace("]", "")
//            }
//        })



        txt_place.setAdapter(GooglePlacesAutocompleteAdapter(this, R.layout.list_item))
        txt_place.setOnItemClickListener(this@AddTaskActivity)
        txt_place.setOnFocusChangeListener(View.OnFocusChangeListener { v, hasFocus ->
            txt_place.setOnItemClickListener(
                this@AddTaskActivity
            )
        })


        txt_place.setThreshold(1)
        txt_place.setText(txt_place_addtask.text.toString())

        txt_place.addTextChangedListener(object : TextWatcher {

            override fun onTextChanged(
                s: CharSequence, start: Int, before: Int,
                count: Int
            ) {
                txt_place.setOnItemClickListener(this@AddTaskActivity)


            }


            override fun beforeTextChanged(
                s: CharSequence, start: Int, count: Int,
                after: Int
            ) {


            }

            override fun afterTextChanged(s: Editable) {

            }
        })

        currentdatetime()
        txt_date_start_on.setText(dateStringcurrent)
        maintime = dateStringcurrent.toString()



        listItemsTxt = arrayOf("1 Day", "2 Day", "3 Day")
        setspinnervalue();

        img_delete_selected_time.setOnClickListener {
            currentdatetime()
            txt_remindme_select_date.setText(dateStringcurrent)

        }

        dialogBuilder.setView(dialogViewremider)

        val mSwitchMultiButton = dialogViewremider.findViewById<View>(R.id.switchmultibutton) as SwitchMultiButton
        mSwitchMultiButton.setText("OneTime", "Repeat", "Location").setOnSwitchListener { position, tabText ->

            // select  one time ,repeat , location tab switch and perform some action
            if (position.equals(0)) {
                lay_Repeat.visibility = View.GONE
                lay_OneTime.visibility = View.VISIBLE
                reminder_type = "1";
                lay_location.visibility = View.GONE
                lay_remind_me.visibility = View.GONE

            } else if (position.equals(1)) {
                lay_Repeat.visibility = View.VISIBLE
                lay_OneTime.visibility = View.GONE
                lay_location.visibility = View.GONE
                lay_remind_me.visibility = View.GONE
                reminder_type = "2";


            } else if (position.equals(2)) {
                lay_Repeat.visibility = View.GONE
                lay_OneTime.visibility = View.GONE
                lay_location.visibility = View.VISIBLE
                lay_remind_me.visibility = View.GONE

            }

        }

        val b = dialogBuilder.create()
        b.show()
        //One time--select latertoday,evening,tomorrow,nextweek , custome layout and perform some action
        lay_later_today.setOnClickListener {
            lay_remind_me.visibility = View.VISIBLE
            lay_OneTime.visibility = View.GONE
            setcurrentdatetimeplus2hours();
            onetime_type = "1";


        }
        lay_this_evening.setOnClickListener {
            lay_OneTime.visibility = View.GONE
            lay_remind_me.visibility = View.VISIBLE
            val dateFormat = SimpleDateFormat("dd MMM yyyy hh:mm a EEE")
            val date = Date()
            date.setHours(18)
            date.setMinutes(0)
            date.setSeconds(0)
            txt_remindme_select_date.setText(dateFormat.format(date))
            onetime_type = "2";
            ragistertime = txt_remindme_select_date.text.toString()
        }
        lay_tomorrow.setOnClickListener {
            lay_OneTime.visibility = View.GONE
            lay_remind_me.visibility = View.VISIBLE
            settomorrowdate()
            onetime_type = "3";


        }
        lay_nextweek.setOnClickListener {
            lay_OneTime.visibility = View.GONE
            lay_remind_me.visibility = View.VISIBLE
            settnextweekdate()
            onetime_type = "4";

        }
        lay_custom.setOnClickListener {
            lay_OneTime.visibility = View.GONE
            lay_remind_me.visibility = View.VISIBLE
            currentdatetime()
            txt_remindme_select_date.setText(dateStringcurrent)
            maintime = dateStringcurrent.toString()
            onetime_type = "5";
            ragistertime = txt_remindme_select_date.text.toString()
        }

        txt_date_start_on.setOnClickListener {
            b.dismiss()
            SingleDateAndTimePickerDialog.Builder(this@AddTaskActivity)
                .displayListener(object : SingleDateAndTimePickerDialog.DisplayListener {
                    override fun onDisplayed(picker: SingleDateAndTimePicker) {
                        //retrieve the SingleDateAndTimePicker
                    }
                })
                .title("Select Date")
                .titleTextColor(getResources().getColor(R.color.whiteColor))
                .backgroundColor(getResources().getColor(R.color.whiteColor))
                .mainColor(getResources().getColor(R.color.blackColor))
                .mustBeOnFuture()
                .curved()
                .listener(object : SingleDateAndTimePickerDialog.Listener {
                    override fun onDateSelected(date: Date) {
                        b.show()
                        val datee = DateFormat.dateFormat(
                            "EEE MMM dd HH:mm:ss zzz yyyy",
                            "dd MMM yyyy hh:mm a EEE",
                            date.toString()
                        )
                        txt_date_start_on.setText(datee);
                        OnStartdate = txt_date_start_on.text.toString()
                        Log.d("hgasdgahgdsgd", OnStartdate)
                        ragistertime = txt_date_start_on.text.toString()

                    }
                }).display()
        }
        txt_remindme_select_date.setOnClickListener {

            b.dismiss()
            SingleDateAndTimePickerDialog.Builder(this@AddTaskActivity)
                .displayListener(object : SingleDateAndTimePickerDialog.DisplayListener {
                    override fun onDisplayed(picker: SingleDateAndTimePicker) {
                        //retrieve the SingleDateAndTimePicker
                    }
                })
                .title("Select Date")
                .titleTextColor(getResources().getColor(R.color.whiteColor))
                .backgroundColor(getResources().getColor(R.color.whiteColor))
                .mainColor(getResources().getColor(R.color.blackColor))
                .mustBeOnFuture()
                .curved()
                .listener(object : SingleDateAndTimePickerDialog.Listener {
                    override fun onDateSelected(date: Date) {
                        b.show()
                        val date = DateFormat.dateFormat(
                            "EEE MMM dd HH:mm:ss zzz yyyy",
                            "dd MMM yyyy hh:mm a EEE",
                            date.toString()
                        )
                        txt_remindme_select_date.setText(date);
                        ragistertime = txt_remindme_select_date.text.toString()

                    }
                }).display()
        }
        //Repeat--select lay_daily,lay_weekly,lay_monthly,lay_yearly and perform some action
        lay_daily.setOnClickListener {

            lay_daily.setBackgroundResource(R.drawable.border_green)
            lay_weekly.setBackgroundResource(R.drawable.border)
            lay_monthly.setBackgroundResource(R.drawable.border)
            lay_yearly.setBackgroundResource(R.drawable.border)
            lay_repeat_on.visibility = View.GONE

            listItemsTxt = arrayOf("1 Day", "2 Day", "3 Day")
            setspinnervalue()
            repeat_type = "1"
            daily_date_time = txt_date_start_on.text.toString()

            ragistertime = txt_date_start_on.text.toString()
        }
        lay_weekly.setOnClickListener {
            weekselection()
            lay_daily.setBackgroundResource(R.drawable.border)
            lay_weekly.setBackgroundResource(R.drawable.border_green)
            lay_monthly.setBackgroundResource(R.drawable.border)
            lay_yearly.setBackgroundResource(R.drawable.border)
            lay_repeat_on.visibility = View.VISIBLE

            listItemsTxt = arrayOf("1 Week", "2 Week", "3 Week")
            setspinnervalue()
            repeat_type = "2"
            weekly_date_time = txt_date_start_on.text.toString()
            ragistertime = txt_date_start_on.text.toString()


        }
        lay_monthly.setOnClickListener {

            lay_daily.setBackgroundResource(R.drawable.border)
            lay_weekly.setBackgroundResource(R.drawable.border)
            lay_monthly.setBackgroundResource(R.drawable.border_green)
            lay_yearly.setBackgroundResource(R.drawable.border)
            lay_repeat_on.visibility = View.GONE
            listItemsTxt = arrayOf("1 Month", "2 Month", "3 Month")
            setspinnervalue()
            repeat_type = "3"
            monthly_date_time = txt_date_start_on.text.toString()
            ragistertime = txt_date_start_on.text.toString()

        }
        lay_yearly.setOnClickListener {
            lay_daily.setBackgroundResource(R.drawable.border)
            lay_weekly.setBackgroundResource(R.drawable.border)
            lay_monthly.setBackgroundResource(R.drawable.border)
            lay_yearly.setBackgroundResource(R.drawable.border_green)
            lay_repeat_on.visibility = View.GONE
            listItemsTxt = arrayOf("1 Year", "2 Year", "3 Year")
            setspinnervalue()
            repeat_type = "4"
            yearly_date_time = txt_date_start_on.text.toString()

            ragistertime = txt_date_start_on.text.toString()

        }
        btn_cancle.setOnClickListener {
            b.dismiss()
            finaldayselectionvalue = ""
        }
        btn_done.setOnClickListener {
            b.dismiss()
//            finaldayselectionvalue=markedButtons
            if (markedButtons.endsWith(",")) {
                finaldayselectionvalue = markedButtons.substring(0, markedButtons.length - 1);
                Log.d("test", finaldayselectionvalue)
            }
            txt_place_addtask.setText(txt_place.text.toString())
            if (!ragistertime.isEmpty()) {
                finalragistertime = ragistertime;

                txt_main_time.setText(finalragistertime)
            } else {
                currentdatetime()
                txt_main_time.setText(dateStringcurrent)
                finalragistertime = txt_main_time.text.toString();
            }



            if (radio_reminder_final.equals("1")) {
                radio_arrive_location?.setChecked(true)
                radio_leave_location?.setChecked(false)

            } else if (radio_reminder_final.equals("2")) {
                radio_arrive_location?.setChecked(false)
                radio_leave_location?.setChecked(true)

            }

        }


//
//        Log.d("tttt",txt_remindme_select_date.text.toString())

    }

    fun currentdatetime() {
        val date = System.currentTimeMillis()
        val sdf = SimpleDateFormat("dd MMM yyyy hh:mm a EEE")
        dateStringcurrent = sdf.format(date)

    }


    fun settomorrowdate() {


        val dateFormat = SimpleDateFormat("dd MMM yyyy hh:mm a EEE")
        var dt = Date()
        dt.setHours(10)
        dt.setMinutes(0)
        dt.setSeconds(0)

        val c = Calendar.getInstance()
        c.setTime(dt)
        c.add(Calendar.DATE, 1)
        dt = c.getTime()
        txt_remindme_select_date.setText(dateFormat.format(dt))
        ragistertime = txt_remindme_select_date.text.toString()

    }

    fun settnextweekdate() {
        val dateFormat = SimpleDateFormat("dd MMM yyyy hh:mm a EEE")
        var dt = Date()
        dt.setHours(10)
        dt.setMinutes(0)
        dt.setSeconds(0)

        val c = Calendar.getInstance()
        c.setTime(dt)
        c.add(Calendar.DATE, 7)
        dt = c.getTime()
        txt_remindme_select_date.setText(dateFormat.format(dt))
        ragistertime = txt_remindme_select_date.text.toString()

    }

    fun setspinnervalue() {
        if (spinner != null) {
            val arrayAdapter = ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, listItemsTxt)
            spinner.adapter = arrayAdapter

            spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {

                    if (listItemsTxt[position].equals("1 Day")) {
                        repeat_day = "1"
                        repeat_week = ""
                        repeat_month = ""
                        repeat_year = ""

                    } else if (listItemsTxt[position].equals("2 Day")) {
                        repeat_day = "2"
                        repeat_week = ""
                        repeat_month = ""
                        repeat_year = ""
                    } else if (listItemsTxt[position].equals("3 Day")) {
                        repeat_day = "3"
                        repeat_week = ""
                        repeat_month = ""
                        repeat_year = ""
                    } else if (listItemsTxt[position].equals("1 Week")) {
                        repeat_week = "1"
                        repeat_day = ""
                        repeat_month = ""
                        repeat_year = ""
                    } else if (listItemsTxt[position].equals("2 Week")) {
                        repeat_week = "2"
                        repeat_day = ""
                        repeat_month = ""
                        repeat_year = ""

                    } else if (listItemsTxt[position].equals("3 Week")) {
                        repeat_week = "3"
                        repeat_day = ""
                        repeat_month = ""
                        repeat_year = ""
                    } else if (listItemsTxt[position].equals("1 Month")) {
                        repeat_month = "1"
                        repeat_day = ""
                        repeat_week = ""
                        repeat_year = ""
                    } else if (listItemsTxt[position].equals("2 Month")) {
                        repeat_month = "2"
                        repeat_day = ""
                        repeat_week = ""
                        repeat_year = ""
                    } else if (listItemsTxt[position].equals("3 Month")) {
                        repeat_month = "3"
                        repeat_day = ""
                        repeat_week = ""
                        repeat_year = ""
                    } else if (listItemsTxt[position].equals("1 Year")) {
                        repeat_year = "1"
                        repeat_day = ""
                        repeat_week = ""
                        repeat_month = ""
                    } else if (listItemsTxt[position].equals("2 Year")) {
                        repeat_year = "2"
                        repeat_day = ""
                        repeat_week = ""
                        repeat_month = ""
                    } else if (listItemsTxt[position].equals("3 Year")) {
                        repeat_year = "3"
                        repeat_day = ""
                        repeat_week = ""
                        repeat_month = ""
                    }


                    //  Toast.makeText(this@AddTaskActivity, getString(R.string.selected_item) + " " + listItemsTxt[position], Toast.LENGTH_SHORT).show()
                }

                override fun onNothingSelected(parent: AdapterView<*>) {
                    // Code to perform some action when nothing is selected
                }
            }
        }
    }

    fun radiobutton_checked_listner_main() {
        val RadioGroup = findViewById(R.id.radio_group_location) as RadioGroup
        RadioGroup.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
                when (checkedId) {
                    R.id.radio_arrive_location -> {
                        txt_place_addtask.setEnabled(true)
                        txt_place_addtask.setInputType(InputType.TYPE_CLASS_TEXT)
                        loc_type = "1"



                        radio_arrive_loc = "1"


                    }
                    R.id.radio_leave_location -> {

                        txt_place_addtask.setEnabled(true)
                        txt_place_addtask.setInputType(InputType.TYPE_CLASS_TEXT)
                        loc_type = "2"


                        radio_arrive_loc = "2"
                    }


                }
            }
        })
    }

    fun radiobutton_checked_listner_reminder() {

        radio_group_reminder_location.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
                when (checkedId) {
                    R.id.radio_arrive_reminder -> {

                        loc_type = "1"
                        radio_reminder_final = "1"
                        Log.d("dsfswfef", "1")


                    }
                    R.id.radio_leave_reminder -> {


                        loc_type = "2"
                        radio_reminder_final = "2"

                    }


                }
            }
        })
    }

    fun weekselection() {
//        if (checked_sunday=="1"){
//            toggle_sunday.setChecked(true)
//        }
//

        toggle_sunday.setOnClickListener(this)
        toggle_monday.setOnClickListener(this)
        toggle_tusday.setOnClickListener(this)
        toggle_wednesday.setOnClickListener(this)
        toggle_thursday.setOnClickListener(this)
        toggle_friday.setOnClickListener(this)
        toggle_saturday.setOnClickListener(this)

    }


    //TODO: upload images......

    private val GALLERY = 1
    private val CAMERA = 2
    var requirePermissions = true
    var preference = 0

    private fun checkStoragePermission() {

        if (ContextCompat.checkSelfPermission(
                this@AddTaskActivity,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            ActivityCompat.requestPermissions(
                this@AddTaskActivity,
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ), 100
            )
        } else {
            chooseFileFromDevice(preference)
        }
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        val str = parent?.getItemAtPosition(position) as String
        coder = Geocoder(this@AddTaskActivity)
        try {
            val adresses = coder.getFromLocationName(str.toString(), 10) as java.util.ArrayList<Address>
            for (add in adresses) {
                longitude_place = add.longitude

                latitude_place = add.latitude

            }
        } catch (e: IOException) {
            e.printStackTrace()

        } catch (e: IllegalArgumentException) {
            e.printStackTrace()

        }


    }

    private fun chooseFileFromDevice(preference: Int) {

        if (preference == Utils.OPEN_MEDIA) {
            val galleryIntent = Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )

            startActivityForResult(galleryIntent, GALLERY)
        } else {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, CAMERA)
        }
    }

    fun setcurrentdatetimeplus2hours() {
        val millisToAdd: Long = 7200000 //two hours
        val date = System.currentTimeMillis() + millisToAdd
        val sdf = SimpleDateFormat("dd MMM yyyy hh:mm a EEE")
        val dateString = sdf.format(date)
        txt_remindme_select_date.setText(dateString)

        maintime = dateString;
        ragistertime = txt_remindme_select_date.text.toString()
    }

    fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallpaperDirectory = File(
            (Environment.getExternalStorageDirectory()).toString() + "Image_Directory"
        )
        // have the object build the directory structure, if needed.
        Log.d("fee", wallpaperDirectory.toString())
        if (!wallpaperDirectory.exists()) {

            wallpaperDirectory.mkdirs()
        }

        try {
            Log.d("heel", wallpaperDirectory.toString())
            val f = File(
                wallpaperDirectory, ((Calendar.getInstance()
                    .getTimeInMillis()).toString() + ".jpg")
            )
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                this,
                arrayOf(f.getPath()),
                arrayOf("image/jpeg"), null
            )
            fo.close()
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath())

            return f.getAbsolutePath()
        } catch (e1: IOException) {
            e1.printStackTrace()
        }
        return ""
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            Utils.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                chooseFileFromDevice(preference)
            } else {
                Toast.makeText(
                    this@AddTaskActivity,
                    resources.getString(R.string.msg_denied_permission),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }

    fun autocomplete(input: String): java.util.ArrayList<Any>? {
        var resultList: java.util.ArrayList<Any>? = null

        var conn: HttpURLConnection? = null
        val jsonResults = StringBuilder()
        try {
            val sb = StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON)
            sb.append("?key=$API_KEY")
            sb.append("&components=country:in")
            sb.append("&input=" + URLEncoder.encode(input, "utf8"))

            val url = URL(sb.toString())
            conn = url.openConnection() as HttpURLConnection
            val `in` = InputStreamReader(conn.inputStream)

            // Load the results into a StringBuilder
            var read: Int
            val buff = CharArray(1024)

            do {

                read = `in`.read(buff)

                if (read == -1) {
                    break
                } else {
                    jsonResults.append(buff, 0, read)
                }

                println(read)

            } while (true)

        } catch (e: MalformedURLException) {

            return resultList
        } catch (e: IOException) {

            return resultList
        } finally {
            conn?.disconnect()
        }

        try {
            // Create a JSON object hierarchy from the results
            val jsonObj = JSONObject(jsonResults.toString())
            val predsJsonArray = jsonObj.getJSONArray("predictions")

            // Extract the Place descriptions from the results
            resultList = java.util.ArrayList<Any>(predsJsonArray.length())
            for (i in 0 until predsJsonArray.length()) {
                println(predsJsonArray.getJSONObject(i).getString("description"))

                println("====")

                resultList.add(predsJsonArray.getJSONObject(i).getString("description"))
            }
        } catch (e: JSONException) {

        }

        return resultList
    }


    internal inner class GooglePlacesAutocompleteAdapter(context: Context, textViewResourceId: Int) :
        ArrayAdapter<Any>(context, textViewResourceId), Filterable {
        private var resultList: java.util.ArrayList<Any>? = null

        override fun getCount(): Int {
            return resultList!!.size
        }

        override fun getItem(index: Int): Any? {
            return resultList!![index]
        }

        override fun getFilter(): Filter {
            return object : Filter() {
                override fun performFiltering(constraint: CharSequence?): FilterResults {
                    val filterResults = FilterResults()

                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = autocomplete(constraint.toString())

                        // Assign the data to the FilterResults
                        filterResults.values = resultList
                        filterResults.count = resultList!!.size
                    }
                    return filterResults
                }

                override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged()
                    } else {
                        notifyDataSetInvalidated()
                    }

                }
            }
        }
    }

    private fun getcategoryList(type: String, view: View) {

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.ADD_TASK_GET_CATEGORY_TASK_LIST,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        val dataArray = jsonObj.getJSONArray("data")
                        if (dataArray.length() > 0) {
                            setMenu(dataArray, view)
                        }

                    } else {

                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {

            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                return params

            }
        }

        requestQueue.add(stringRequest)
    }

    fun AddTask() {
        loaderLayout.visibility = View.VISIBLE

        if (!finalragistertime.isEmpty()) {
            FinalRagistrationTimevalue = DateFormat.dateFormat(
                "dd MMM yyyy hh:mm a EEE",
                "yyyy-MM-dd HH:mm",
                finalragistertime.toString()
            )!!

        } else {
            FinalRagistrationTimevalue = DateFormat.dateFormat(
                "dd MMM yyyy hh:mm a EEE",
                "yyyy-MM-dd HH:mm",
                txt_main_time.text.toString()
            )!!
        }
        Log.d("datetime", FinalRagistrationTimevalue)

        val volleyMultipartRequest = object : VolleyMultipartRequest(
            Request.Method.POST, EndPoints.ADD_TASK,
            Response.Listener { response ->
                Log.e("response", String(response.data))
                requestQueue.getCache().clear();

                var jsonObj = JSONObject(String(response.data))
                val status = jsonObj.getString("status")
                loaderLayout.visibility = View.GONE
                if (status.equals("1")) {

                    Log.e("status", "1")
                    finish()
                    Toast.makeText(this@AddTaskActivity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                } else {
                    Log.e("status", "0")
                    Toast.makeText(this@AddTaskActivity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                }


            },
            Response.ErrorListener { error ->
                Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT).show()
                loaderLayout.visibility = View.GONE
                Log.e("status", error.message)
            }) {


            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()

                params.put("client_id", client_id)
                params.put("task_title", edt_task_title.text.toString())
                params.put("task_tag", txt_tag.text.toString())
                params.put("reminder_type", reminder_type)
                params.put("onetime_type", onetime_type)
                params.put("register_date_time", FinalRagistrationTimevalue)
                params.put("repeat_type", repeat_type)
                params.put("repeat_day", repeat_day)
                params.put("repeat_week", repeat_week)
                params.put("repeat_on", finaldayselectionvalue)
                params.put("repeat_month", repeat_month)
                params.put("repeat_year", repeat_year)
                params.put("loc_address", txt_place_addtask.text.toString())
                params.put("curr_lat", current_lat.toString())
                params.put("curr_long", current_long.toString())
                params.put("add_lat", latitude_place.toString())
                params.put("add_long", longitude_place.toString())
                params.put("loc_reminder_type", loc_type)
                params.put("cat_id", cat_id.toString())
                params.put("loc_type", loc_type)
                params.put("adress", txt_place_addtask.text.toString())
                params.put("latitude", "")
                params.put("longitude", "")
                params.put("sub_task_name", edit_sub_task.text.toString())
                params.put("notes", txt_note.text.toString())
                params.put("task_status", "1")
                if (picBitmap == null) {
                    params.put("attachment", "");
                }

                Log.d("testtt", client_id)
                Log.d("testtt", edt_task_title.text.toString())
                Log.d("testtt", txt_tag.text.toString())
                Log.d("testtt", reminder_type)
                Log.d("testtt", onetime_type)
                Log.d("testtt", FinalRagistrationTimevalue)
                Log.d("testtt", repeat_type)
                Log.d("testtt", repeat_day)
                Log.d("testtt", repeat_week)
                Log.d("testtt", finaldayselectionvalue)
                Log.d("testtt", repeat_month)
                Log.d("testtt", repeat_year)
                Log.d("testtt", txt_place_addtask.text.toString())
                Log.d("testtt", current_lat.toString())
                Log.d("testtt", current_long.toString())
                Log.d("testtt", latitude_place.toString())
                Log.d("testtt", longitude_place.toString())
                Log.d("testtt", loc_type)
                Log.d("testtt", txt_place_addtask.text.toString())
                Log.d("testtt", "nodata")
                Log.d("testtt", "nodata")
                Log.d("testtt", edit_sub_task.text.toString())
                Log.d("testtt", txt_note.text.toString())
                Log.d("testtt", "1")

                return params
            }

            override fun getByteData(): Map<String, DataPart> {
                val params = HashMap<String, DataPart>()
                val imagename = System.currentTimeMillis()

                if (picBitmap != null) {
                    params["attachment"] = DataPart("$imagename.png", getFileDataFromDrawable(picBitmap))


                }
                return params
            }
        }
        volleyMultipartRequest.retryPolicy = DefaultRetryPolicy(
            0,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(volleyMultipartRequest);
    }

    fun getFileDataFromDrawable(bitmap: Bitmap?): ByteArray {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap!!.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream)
        return byteArrayOutputStream.toByteArray()
    }

    override fun onClick(v: View?) {
        when (v?.getId()) {
            R.id.toggle_sunday -> if (toggle_sunday.isChecked()) {
                toggle_sunday.setChecked(true)
                markedButtons += "1,"
                checked_sunday = "1"

            } else {
                toggle_sunday.setChecked(false)
                checked_sunday = "0"
                markedButtons = markedButtons.replace("1,", "")
            }
            R.id.toggle_monday -> if (toggle_monday.isChecked()) {
                toggle_monday.setChecked(true)
                markedButtons += "2,"

            } else {
                toggle_monday.setChecked(false)
                markedButtons = markedButtons.replace("2,", "")

            }
            R.id.toggle_tusday -> if (toggle_tusday.isChecked()) {
                toggle_tusday.setChecked(true)
                markedButtons += "3,"

            } else {
                toggle_tusday.setChecked(false)
                markedButtons = markedButtons.replace("3,", "")

            }
            R.id.toggle_wednesday -> if (toggle_wednesday.isChecked()) {
                toggle_wednesday.setChecked(true)
                markedButtons += "4,"

            } else {
                toggle_wednesday.setChecked(false)
                markedButtons = markedButtons.replace("4,", "")

            }
            R.id.toggle_thursday -> if (toggle_thursday.isChecked()) {
                toggle_thursday.setChecked(true)
                markedButtons += "5,"

            } else {
                toggle_thursday.setChecked(false)
                markedButtons = markedButtons.replace("5,", "")

            }
            R.id.toggle_friday -> if (toggle_friday.isChecked()) {
                toggle_friday.setChecked(true)
                markedButtons += "6,"

            } else {
                toggle_friday.setChecked(false)
                markedButtons = markedButtons.replace("6,", "")

            }
            R.id.toggle_saturday -> if (toggle_saturday.isChecked()) {
                toggle_saturday.setChecked(true)
                markedButtons += "7,"

            } else {
                toggle_saturday.setChecked(false)
                markedButtons = markedButtons.replace("7,", "")

            }

            else -> {

            }


        }
    }


}



