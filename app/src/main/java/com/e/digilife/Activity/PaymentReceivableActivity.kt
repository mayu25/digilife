package com.e.digilife.Activity

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.PaymentReceivableAdapter
import com.app.towntalk.Adapter.RenewableTranscationAdapter
import com.app.towntalk.Adapter.TransactionMemberListRecyclerAdapter
import com.app.towntalk.Adapter.W_BudgetRecyclerAdapter
import com.e.digilife.Model.WalletModel
import com.e.digilife.R
import com.e.digilife.View.InputValidation
import com.e.digilife.View.TextViewPlus
import com.e.digilife.View.Utils
import com.e.digilife.View.VolleyMultipartRequest
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import org.w3c.dom.Text
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class PaymentReceivableActivity : AppCompatActivity() {

    val TAG = "RenewableTranscationActivity"
    val activity = this@PaymentReceivableActivity
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null
    lateinit var inputValidation: InputValidation

    private var dataList = ArrayList<WalletModel>()
    var adapter: PaymentReceivableAdapter? = null

    var client_id: String = ""
    var currency: String = ""

    var amount: String = ""

    lateinit var loaderLayout: LinearLayout
    lateinit var backView: ImageView
    lateinit var totalAmountText: TextView
    lateinit var paymentRecyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_receivable)

        requestQueue = Volley.newRequestQueue(activity)
        prefs = activity!!.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        currency = prefs!!.getString(Utils.CURRENCY, "")
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }

        init()

        val linearLayoutManager = LinearLayoutManager(activity)
        paymentRecyclerView.setLayoutManager(linearLayoutManager)
        adapter = PaymentReceivableAdapter(activity, dataList, onItemClick)
        paymentRecyclerView.adapter = adapter
    }

    fun init() {
        inputValidation = InputValidation(activity)

        loaderLayout = findViewById(R.id.loaderLayout)
        paymentRecyclerView = findViewById(R.id.paymentRecyclerView)
        backView = findViewById(R.id.backView)
        totalAmountText = findViewById(R.id.totalAmountText)

        backView.setOnClickListener(clickListener)
    }

    override fun onResume() {
        super.onResume()
        getPaymentList()
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish()
            }
        }
    }

    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            Log.e("position ::", position.toString())
            var isSplit = dataList.get(position).p_IsSplit
            if (isSplit.equals("1")) {
                var intent = Intent(activity, PaymentReceivableSplitActivity::class.java)
                intent.putExtra(Utils.TRANSCATION_DATA, dataList.get(position).p_Split)
                intent.putExtra("split_id", dataList.get(position).p_Id)
                intent.putExtra("trans_id", dataList.get(position).p_TransID)
                startActivity(intent)
            }
        }
    }

    //TODO: Renewable Transcation.............

    private fun getPaymentList() {

        loaderLayout.visibility = View.VISIBLE
        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.GET_PAYMENT_RECEIVABLE,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    loaderLayout.visibility = View.GONE
                    Log.e("Category List ::", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        val dataAObj = jsonObj.getJSONObject("data")

                        setPaymentList(dataAObj)

                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                Log.e(" JSONData :", params.toString())
                return params
            }
        }

        Log.e(" API :", EndPoints.GET_PAYMENT_RECEIVABLE)
        requestQueue.add(stringRequest)
    }


    fun setPaymentList(jsonObj: JSONObject) {

        var totalAmount = jsonObj.getString("total_amount")
        totalAmountText.setText(currency + " " + totalAmount + ".00")

        var jsonArray = jsonObj.getJSONArray("payment_receivable")

        if (jsonArray.length() > 0) {
            dataList.clear()
            for (i in 0..(jsonArray.length() - 1)) {
                val jsonObj = jsonArray.getJSONObject(i)
                var key = jsonObj.getString("key")
                var paymentArray = jsonObj.getJSONArray("data")

                for (j in 0..(paymentArray.length() - 1)) {
                    val paymentObj = paymentArray.getJSONObject(j)

                    if (i == 0) {
                        val dataModel = WalletModel()
                        dataModel.currency = currency
                        if (j == 0) {
                            dataModel.p_Key = key
                        } else {
                            dataModel.p_Key = ""
                        }
                        dataModel.p_Id = paymentObj.getString("split_id")
                        dataModel.p_MemberName = paymentObj.getString("member_name")
                        dataModel.p_Amount = paymentObj.getString("amount")
                        dataModel.p_TransID = paymentObj.getString("transaction_id")
                        var splitData = paymentObj.getJSONArray("split_data")
                        if (splitData.length() > 0) {
                            dataModel.p_Split = splitData.toString()
                            dataModel.p_IsSplit = "1"
                        } else {
                            dataModel.p_Split = ""
                            dataModel.p_IsSplit = "0"
                        }
                        dataList.add(dataModel)
                    }

                    if (i == 1) {
                        val dataModel = WalletModel()
                        dataModel.currency = currency
                        if (j == 0) {
                            dataModel.p_Key = key
                        } else {
                            dataModel.p_Key = ""
                        }
                        dataModel.p_Id = paymentObj.getString("id")
                        dataModel.p_MemberName = paymentObj.getString("company_name")
                        dataModel.p_Amount = paymentObj.getString("total")

                        var splitData = paymentObj.getJSONArray("reimburse_data")
                        if (splitData.length() > 0) {
                            dataModel.p_Split = splitData.toString()
                            dataModel.p_IsSplit = "1"
                        } else {
                            dataModel.p_Split = ""
                            dataModel.p_IsSplit = "0"
                        }
                        dataList.add(dataModel)
                    }
                }
            }
            adapter!!.notifyDataSetChanged()
        }

    }

}
