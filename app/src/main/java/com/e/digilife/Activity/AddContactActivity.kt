package com.e.digilife.Activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Typeface
import android.media.MediaScannerConnection
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.ContactsContract
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.Volley
import com.e.digilife.Model.ContactModel
import com.e.digilife.R
import com.e.digilife.View.InputValidation
import com.e.digilife.View.Utils
import com.e.digilife.View.VolleyMultipartRequest
import com.shurlock.View.EndPoints
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.net.URL
import java.util.*

class AddContactActivity : AppCompatActivity() {

    val TAG = "AddContactActivity"
    val activity = this@AddContactActivity
    internal lateinit var requestQueue: RequestQueue
    lateinit var inputValidation: InputValidation
    var prefs: SharedPreferences? = null

    lateinit var profileImg: CircleImageView
    lateinit var addContactView: ImageView
    lateinit var addOrganization: ImageView
    lateinit var addPhone: ImageView
    lateinit var addEmail: ImageView
    lateinit var addAddress: ImageView
    lateinit var addWebAddress: ImageView

    lateinit var nameET: EditText
    lateinit var organizationET: EditText
    lateinit var anotherOrganizationET: EditText
    lateinit var phoneET: EditText
    lateinit var anotherPhoneET: EditText
    lateinit var emailET: EditText
    lateinit var anotherEmailET: EditText
    lateinit var addressET: EditText
    lateinit var anotherAddressET: EditText
    lateinit var webAddressET: EditText
    lateinit var anotherWebAddressET: EditText

    lateinit var backView: ImageView
    lateinit var titleText: TextView
    lateinit var saveContact: TextView
    lateinit var groupLayout: FrameLayout
    lateinit var groupTextView: TextView
    lateinit var emergencySwitch: Switch
    lateinit var scanCardBtn: Button
    lateinit var imageSelectionView: LinearLayout
    lateinit var loaderLayout: LinearLayout
    lateinit var cameraView: TextView
    lateinit var galleryView: TextView
    lateinit var cancelView: TextView
    lateinit var documentView: TextView

    var isOrganization: Boolean = false
    var isPhone: Boolean = false
    var isEmail: Boolean = false
    var isAddress: Boolean = false
    var isWebAddress: Boolean = false

    var client_id: String = ""
    var EDIT_CONTACT: String = ""

    var contactId: String = ""
    var contactName: String = ""
    var organization: String = ""
    var anotherOrganization: String = ""
    var phone: String = ""
    var anotherPhone: String = ""
    var email: String = ""
    var anotherEmail: String = ""
    var address: String = ""
    var anotherAddress: String = ""
    var webAddress: String = ""
    var anotherWebAddress: String = ""
    var groupID: String = ""
    var groupName: String = ""
    var isEmergency: String = "0"
    internal var dataModel = ContactModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_contact)
        requestQueue = Volley.newRequestQueue(activity)
        prefs = this.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)

        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }

        init()

        if (intent != null) {
            EDIT_CONTACT = intent.getStringExtra(Utils.EDIT_CONTACT)

            if (EDIT_CONTACT.equals("1")) {
                var contactData = intent.getSerializableExtra(Utils.CONTACT_OBJ)
                dataModel = contactData as ContactModel
                setEditData()
                titleText.setText(resources.getString(R.string.edit_contact))
            } else {
                titleText.setText(resources.getString(R.string.create_contact))
            }
        }
    }

    fun init() {
        inputValidation = InputValidation(activity)

        loaderLayout = findViewById(R.id.loaderLayout)
        imageSelectionView = findViewById(R.id.imageSelectionView)
        profileImg = findViewById(R.id.profileImg)
        addContactView = findViewById(R.id.addContactView)
        addOrganization = findViewById(R.id.addOrganization)
        addPhone = findViewById(R.id.addPhone)
        addEmail = findViewById(R.id.addEmail)
        addAddress = findViewById(R.id.addAddress)
        addWebAddress = findViewById(R.id.addWebAddress)

        nameET = findViewById(R.id.nameET)
        organizationET = findViewById(R.id.organizationET)
        anotherOrganizationET = findViewById(R.id.anotherOrganizationET)
        phoneET = findViewById(R.id.phoneET)
        anotherPhoneET = findViewById(R.id.anotherPhoneET)
        emailET = findViewById(R.id.emailET)
        anotherEmailET = findViewById(R.id.anotherEmailET)
        addressET = findViewById(R.id.addressET)
        anotherAddressET = findViewById(R.id.anotherAddressET)
        webAddressET = findViewById(R.id.webAddressET)
        anotherWebAddressET = findViewById(R.id.anotherWebAddressET)

        backView = findViewById(R.id.backView)
        titleText = findViewById(R.id.titleText)
        saveContact = findViewById(R.id.saveContact)
        groupLayout = findViewById(R.id.groupLayout)
        groupTextView = findViewById(R.id.groupTextView)
        emergencySwitch = findViewById(R.id.emergencySwitch)
        scanCardBtn = findViewById(R.id.scanCardBtn)

        cameraView = findViewById(R.id.cameraView)
        galleryView = findViewById(R.id.galleryView)
        cancelView = findViewById(R.id.cancelView)
        documentView = findViewById(R.id.documentView)
        documentView.visibility = View.GONE

        profileImg.setOnClickListener(clickListener)
        addContactView.setOnClickListener(clickListener)
        addOrganization.setOnClickListener(clickListener)
        addPhone.setOnClickListener(clickListener)
        addEmail.setOnClickListener(clickListener)
        addAddress.setOnClickListener(clickListener)
        addWebAddress.setOnClickListener(clickListener)
        groupLayout.setOnClickListener(clickListener)
        scanCardBtn.setOnClickListener(clickListener)
        cameraView.setOnClickListener(clickListener)
        galleryView.setOnClickListener(clickListener)
        cancelView.setOnClickListener(clickListener)
        saveContact.setOnClickListener(clickListener)
        backView.setOnClickListener(clickListener)

        var font = Typeface.createFromAsset(assets, resources.getString(R.string.popins_regular))
        scanCardBtn.setTypeface(font)
        nameET.setTypeface(font)
        organizationET.setTypeface(font)
        anotherOrganizationET.setTypeface(font)
        phoneET.setTypeface(font)
        anotherPhoneET.setTypeface(font)
        emailET.setTypeface(font)
        anotherEmailET.setTypeface(font)
        addressET.setTypeface(font)
        anotherAddressET.setTypeface(font)
        webAddressET.setTypeface(font)
        anotherWebAddressET.setTypeface(font)

        emergencySwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            Log.e("Switch State=", "" + isChecked);

            if (buttonView.isPressed) {
                if (isChecked == true) {
                    isEmergency = "1"
                } else {
                    isEmergency = "0"
                }
            }
        }
    }


    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                hideKeyboard()
                finish()
            }

            R.id.addContactView -> {

                askForContactPermission()


//                val i = Intent(this@AddContactActivity, ContactListActivity::class.java)
//                startActivityForResult(i, Utils.CONTACT)
            }
            R.id.saveContact -> {
                hideKeyboard()
                validation()
            }
            R.id.addOrganization -> {
                if (isOrganization == false) {
                    isOrganization = true
                    anotherOrganizationET.visibility = View.VISIBLE
                } else {
                    isOrganization = false
                    anotherOrganizationET.visibility = View.GONE
                }
            }
            R.id.addPhone -> {
                if (isPhone == false) {
                    isPhone = true
                    anotherPhoneET.visibility = View.VISIBLE
                } else {
                    isPhone = false
                    anotherPhoneET.visibility = View.GONE
                }
            }
            R.id.addEmail -> {
                if (isEmail == false) {
                    isEmail = true
                    anotherEmailET.visibility = View.VISIBLE
                } else {
                    isEmail = false
                    anotherEmailET.visibility = View.GONE
                }
            }
            R.id.addAddress -> {
                if (isAddress == false) {
                    isAddress = true
                    anotherAddressET.visibility = View.VISIBLE
                } else {
                    isAddress = false
                    anotherAddressET.visibility = View.GONE
                }
            }
            R.id.addWebAddress -> {
                if (isWebAddress == false) {
                    isWebAddress = true
                    anotherWebAddressET.visibility = View.VISIBLE
                } else {
                    isWebAddress = false
                    anotherWebAddressET.visibility = View.GONE
                }
            }
            R.id.groupLayout -> {
                val i = Intent(this@AddContactActivity, ContactGroupListActivity::class.java)
                startActivityForResult(i, Utils.GROUP)
            }
            R.id.scanCardBtn -> {

            }
            R.id.profileImg -> {
                if (ActivityCompat.checkSelfPermission(
                        activity,
                        android.Manifest.permission.CAMERA
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    ActivityCompat.requestPermissions(
                        activity, arrayOf(
                            android.Manifest.permission.CAMERA,
                            android.Manifest.permission.READ_EXTERNAL_STORAGE,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ), 0
                    )
                } else {
                    requirePermissions = false
                    imageSelectionView.visibility = View.VISIBLE
                }
            }

            R.id.cameraView -> {
                imageSelectionView.visibility = View.GONE
                preference = Utils.OPEN_CAMERA
                checkStoragePermission()
            }
            R.id.galleryView -> {
                imageSelectionView.visibility = View.GONE
                preference = Utils.OPEN_MEDIA
                checkStoragePermission()
            }
            R.id.cancelView -> {
                imageSelectionView.visibility = View.GONE
            }
        }
    }

    //TODO: Set Edit data.......

    fun setEditData() {
        contactId = dataModel.contactId
        contactName = dataModel.contactName
        organization = dataModel.organization_1
        anotherOrganization = dataModel.organization_2
        phone = dataModel.phone_1
        anotherPhone = dataModel.phone_2
        email = dataModel.email_1
        anotherEmail = dataModel.email_2
        address = dataModel.address_1
        anotherAddress = dataModel.address_2
        webAddress = dataModel.webAddress_1
        anotherWebAddress = dataModel.webAddress_2
        groupID = dataModel.groupId
        groupName = dataModel.groupName
        isEmergency = dataModel.isEmergency

        var profileImgPath = dataModel.contactPic
        if (!profileImgPath.equals("")) {
            Picasso.with(activity).load(profileImgPath).placeholder(R.mipmap.ic_username)
                .into(profileImg)

            convertBitmap(profileImgPath)
        }

        nameET.setText(contactName)
        organizationET.setText(organization)

        if (!anotherOrganization.equals("")) {
            isOrganization = true
            anotherOrganizationET.visibility = View.VISIBLE
            anotherOrganizationET.setText(anotherOrganization)
        }

        if (!anotherPhone.equals("")) {
            isPhone = true
            anotherPhoneET.visibility = View.VISIBLE
            anotherPhoneET.setText(anotherPhone)
        }

        if (!anotherEmail.equals("")) {
            isEmail = true
            anotherEmailET.visibility = View.VISIBLE
            anotherEmailET.setText(anotherEmail)
        }

        if (!anotherAddress.equals("")) {
            isAddress = true
            anotherAddressET.visibility = View.VISIBLE
            anotherAddressET.setText(anotherAddress)
        }

        if (!anotherWebAddress.equals("")) {
            isWebAddress = true
            anotherWebAddressET.visibility = View.VISIBLE
            anotherWebAddressET.setText(anotherWebAddress)
        }

        phoneET.setText(phone)
        emailET.setText(email)
        addressET.setText(address)
        webAddressET.setText(webAddress)

        if (groupID.equals("1")) {
            groupTextView.setText(groupName)
        }

        if (isEmergency.equals("1")) {
           emergencySwitch.isChecked = true
        }
    }


    @SuppressLint("StaticFieldLeak")
    private fun convertBitmap(imageUrl: String) {
        object : AsyncTask<Void, Void, Bitmap>() {
            override fun doInBackground(vararg voids: Void): Bitmap {
                var bitmap: Bitmap? = null
                try {
                    val url = URL(imageUrl)
                    bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream())

                } catch (e: IOException) {
                    println(e)
                }
                return bitmap!!
            }

            override fun onPostExecute(s: Bitmap) {
                contactBitmap = s
                super.onPostExecute(s)
            }
        }.execute()

    }

    //TODO: Add Contact

    fun addContact() {
        loaderLayout.visibility = View.VISIBLE
        val volleyMultipartRequest = object : VolleyMultipartRequest(
            Request.Method.POST, EndPoints.ADD_CONTACT,
            Response.Listener { response ->
                loaderLayout.visibility = View.GONE
                Log.d("response", String(response.data))
                var jsonObj = JSONObject(String(response.data))
                var status = jsonObj.getString("status")
                if (status.equals("1")) {
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                    requestQueue.getCache().clear();
                    finish()
                } else {
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT).show()
            }) {


            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("client_id", client_id)
                params.put("contact_name", contactName)
                params.put("organization", organization)
                params.put("organization_2", anotherOrganization)
                params.put("phone_no", phone)
                params.put("phone_no_2", anotherPhone)
                params.put("email", email)
                params.put("email_2", anotherEmail)
                params.put("address", address)
                params.put("address_2", anotherAddress)
                params.put("web_address", webAddress)
                params.put("web_address_2", anotherWebAddress)
                params.put("group_id", groupID)
                params.put("emergency", isEmergency)
                Log.e(" JSONData :", params.toString())
                return params

            }

            override fun getByteData(): Map<String, DataPart> {

                val params = HashMap<String, DataPart>()
                val imagename = System.currentTimeMillis()
                if (contactBitmap != null) {
                    params["profile_pic"] = DataPart("$imagename.png", getFileDataFromDrawable(contactBitmap))
                }
                return params

            }
        }

        volleyMultipartRequest.retryPolicy = DefaultRetryPolicy(
            0,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(volleyMultipartRequest);
    }


    fun getFileDataFromDrawable(bitmap: Bitmap?): ByteArray {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap!!.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream)
        return byteArrayOutputStream.toByteArray()
    }


    //TODO: Edit Contact..............

    fun editContact() {
        loaderLayout.visibility = View.VISIBLE
        val volleyMultipartRequest = object : VolleyMultipartRequest(
            Request.Method.POST, EndPoints.EDIT_CONTACT,
            Response.Listener { response ->
                loaderLayout.visibility = View.GONE
                Log.d("response", String(response.data))
                var jsonObj = JSONObject(String(response.data))
                var status = jsonObj.getString("status")
                if (status.equals("1")) {
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                    requestQueue.getCache().clear();
                    finish()
                } else {
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT).show()
            }) {


            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("client_id", client_id)
                params.put("update_id", contactId)
                params.put("contact_name", contactName)
                params.put("organization", organization)
                params.put("organization_2", anotherOrganization)
                params.put("phone_no", phone)
                params.put("phone_no_2", anotherPhone)
                params.put("email", email)
                params.put("email_2", anotherEmail)
                params.put("address", address)
                params.put("address_2", anotherAddress)
                params.put("web_address", webAddress)
                params.put("web_address_2", anotherWebAddress)
                params.put("group_id", groupID)
                params.put("emergency", isEmergency)
                Log.e(" JSONData :", params.toString())
                return params

            }

            override fun getByteData(): Map<String, DataPart> {

                val params = HashMap<String, DataPart>()
                val imagename = System.currentTimeMillis()
                if (contactBitmap != null) {
                    params["profile_pic"] = DataPart("$imagename.png", getFileDataFromDrawable(contactBitmap))
                }
                return params

            }
        }

        volleyMultipartRequest.retryPolicy = DefaultRetryPolicy(
            0,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(volleyMultipartRequest);
    }


    private fun validation() {

        if (!inputValidation.isFieldEmpty(nameET, getString(R.string.name_validation))) {
            return
        }
        if (!inputValidation.isFieldEmpty(phoneET, getString(R.string.phone_validation))) {
            return
        }

        contactName = nameET.text.toString()
        organization = organizationET.text.toString()
        anotherOrganization = anotherOrganizationET.text.toString()
        phone = phoneET.text.toString()
        anotherPhone = anotherPhoneET.text.toString()
        email = emailET.text.toString()
        anotherEmail = anotherEmailET.text.toString()
        address = addressET.text.toString()
        anotherAddress = anotherAddressET.text.toString()
        webAddress = webAddressET.text.toString()
        anotherWebAddress = anotherWebAddressET.text.toString()

        if (Utils.checkInternetConnection(this@AddContactActivity)) {
            if (EDIT_CONTACT.equals("1")) {
                editContact()
            } else {
                addContact()
            }

        } else {
            Utils.showMessageDialog(
                this@AddContactActivity,
                getString(R.string.app_name),
                getString(R.string.check_internet)
            )
        }
    }


    //TODO: Contact images......

    private val GALLERY = 1
    private val CAMERA = 2
    var requirePermissions = true
    var preference = 0
    var contactBitmap: Bitmap? = null

    private fun checkStoragePermission() {

        if (ContextCompat.checkSelfPermission(
                this@AddContactActivity,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            ActivityCompat.requestPermissions(
                this@AddContactActivity,
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ), 100
            )

        } else {
            chooseFileFromDevice(preference)
        }
    }

    private fun chooseFileFromDevice(preference: Int) {

        if (preference == Utils.OPEN_MEDIA) {
            val galleryIntent = Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI
            )

            startActivityForResult(galleryIntent, GALLERY)
        } else {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, CAMERA)
        }
    }


    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && requestCode == Utils.GROUP) {
            var name = data!!.getStringExtra("GroupName")
            var id = data.getStringExtra("GroupId")
            groupID = id
            groupTextView.text = name

        } else if (requestCode == GALLERY && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val contentURI = data.data
                try {
                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
                    val path = saveImage(bitmap)

                    contactBitmap = bitmap
                    profileImg.setImageBitmap(bitmap)


                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(this@AddContactActivity, "Failed!", Toast.LENGTH_SHORT).show()
                }
            }

        } else if (requestCode == CAMERA && resultCode == Activity.RESULT_OK) {
            val thumbnail = data!!.extras!!.get("data") as Bitmap

            contactBitmap = thumbnail
            profileImg.setImageBitmap(thumbnail)

            saveImage(thumbnail)
        } else if (requestCode == Utils.CONTACT) {
            if (resultCode == RESULT_OK) {
                val contactData = data!!.getData()
                var number = ""
                val cursor = contentResolver.query(contactData, null, null, null, null)
                cursor.moveToFirst()
                val hasPhone =
                    cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.HAS_PHONE_NUMBER))
                val contactId = cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts._ID))
                if (hasPhone == "1") {
                    val phones = contentResolver.query(
                        ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Phone.CONTACT_ID
                                + " = " + contactId, null, null
                    )
                    while (phones.moveToNext()) {
                        number = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER))
                            .replace(("[-() ]").toRegex(), "")
                    }
                    phones.close()


                    //Do something with number
                } else {
                    Toast.makeText(applicationContext, "This contact has no phone number", Toast.LENGTH_LONG).show()
                }

                val nameColumnIndex = cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME)
                val name = cursor.getString(nameColumnIndex)


                phoneET.setText(number)
                nameET.setText(name)

                cursor.close()
            }
        }
    }


    var PERMISSIONS_REQUEST_READ_CONTACTS: Int = 203;

    fun askForContactPermission() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.READ_CONTACTS), PERMISSIONS_REQUEST_READ_CONTACTS)
            //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
        } else {
            val intent = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
            startActivityForResult(intent, Utils.CONTACT)
        }

    }


    fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallpaperDirectory = File(
            (Environment.getExternalStorageDirectory()).toString() + "Image_Directory"
        )
        // have the object build the directory structure, if needed.
        Log.d("fee", wallpaperDirectory.toString())
        if (!wallpaperDirectory.exists()) {

            wallpaperDirectory.mkdirs()
        }

        try {
            Log.d("heel", wallpaperDirectory.toString())
            val f = File(
                wallpaperDirectory, ((Calendar.getInstance()
                    .getTimeInMillis()).toString() + ".jpg")
            )
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                this,
                arrayOf(f.getPath()),
                arrayOf("image/jpeg"), null
            )
            fo.close()
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath())

            return f.getAbsolutePath()
        } catch (e1: IOException) {
            e1.printStackTrace()
        }

        return ""
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {

        if (requestCode == Utils.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                chooseFileFromDevice(preference)
            } else {
                Toast.makeText(
                    this@AddContactActivity,
                    resources.getString(R.string.msg_denied_permission),
                    Toast.LENGTH_LONG
                ).show()
            }
        } else if (requestCode == PERMISSIONS_REQUEST_READ_CONTACTS) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission is granted
                val intent = Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(intent, Utils.CONTACT)
            } else {
                Toast.makeText(
                    this,
                    "Until you grant the permission, we canot display the names",
                    Toast.LENGTH_SHORT
                )
                    .show();
            }
        }

    }


    fun hideKeyboard() {
        val view = this.getCurrentFocus()
        if (view != null) {
            val inputManager: InputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(view.windowToken, InputMethodManager.SHOW_FORCED)
        }
    }

}
