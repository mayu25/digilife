package com.e.digilife.Activity

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.e.digilife.Model.ContactModel
import com.e.digilife.Model.WalletModel
import com.e.digilife.R
import com.e.digilife.View.TextViewPlus
import com.e.digilife.View.Utils
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import java.util.HashMap

class ContactGroupListActivity : AppCompatActivity() {

    val TAG = "ContactGroupListActivity"
    val activity = this@ContactGroupListActivity
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null

    lateinit var loaderLayout: LinearLayout
    lateinit var backView: ImageView
    lateinit var listview: ListView
    lateinit var addGroupView: ImageView

    var client_id: String = ""

    private var dataList = ArrayList<ContactModel>()
    var adapter: GroupAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_group_list)
        requestQueue = Volley.newRequestQueue(this)
        prefs = this.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)

        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }

        init()
        adapter = GroupAdapter(activity, dataList, onItemClick)
        listview.adapter = adapter

        getGroupList("")

    }

    fun init() {
        loaderLayout = findViewById(R.id.loaderLayout)
        backView = findViewById(R.id.backView)
        listview = findViewById(R.id.listview)
        addGroupView = findViewById(R.id.addGroupView)

        backView.setOnClickListener(clickListener)
        addGroupView.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                hideKeyboard()
                var intent = Intent()
                setResult(Activity.RESULT_CANCELED, intent)
                finish()
            }
            R.id.addGroupView -> {
                showAddGroupDialog()
            }
        }
    }


    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            var intent = Intent()
            intent.putExtra("GroupName", dataList.get(position).groupName)
            intent.putExtra("GroupId", dataList.get(position).groupId)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    private fun getGroupList(type: String) {

        if (type.equals(""))
            loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.GET_GROUP_LIST,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        val dataArray = jsonObj.getJSONArray("data")
                        if (dataArray.length() > 0) {
                            setGroupList(dataArray)
                        }

                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    Log.d("Error.Response", error.toString())
                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                Log.e(" JSONData :", params.toString())
                return params
            }
        }
        Log.e(" API :", EndPoints.GET_GROUP_LIST)
        requestQueue.add(stringRequest)
    }

    fun setGroupList(jsonArray: JSONArray) {
        dataList.clear()
        for (i in 0..(jsonArray.length() - 1)) {
            var jObj = jsonArray.getJSONObject(i)
            var dataModel = ContactModel()
            dataModel.groupId = jObj.getString("id")
            dataModel.groupName = jObj.getString("group_name")
            dataList.add(dataModel)
        }
        adapter!!.notifyDataSetChanged()
    }

    private fun addGroup(name: String) {

        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.CREATE_GROUP,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                        getGroupList("add")

                    } else {
                        loaderLayout.visibility = View.GONE
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    Log.d("Error.Response", error.toString())
                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["group_name"] = name
                Log.e(" JSONData :", params.toString())
                return params
            }
        }
        Log.e(" API :", EndPoints.CREATE_GROUP)
        requestQueue.add(stringRequest)
    }


    fun showAddGroupDialog() {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = activity!!.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.create_folder_view, null)
        dialog.setContentView(view)

        val createText = dialog.findViewById(R.id.createText) as TextView
        val folderNameEdittext = dialog.findViewById(R.id.folderNameEdittext) as EditText
        val doneBtn = dialog.findViewById(R.id.doneBtn) as Button
        folderNameEdittext.setHint("Enter Group Name")
        createText.setText("Create Group")

        val font = Typeface.createFromAsset(activity!!.assets, resources.getString(R.string.popins_regular))
        folderNameEdittext.setTypeface(font)
        doneBtn.setTypeface(font)

        doneBtn.setOnClickListener {
            if (folderNameEdittext.text.toString().equals("")) {
                Toast.makeText(activity, "Enter Group Name", Toast.LENGTH_SHORT).show()
            } else {
                if (Utils.checkInternetConnection(activity))
                    addGroup(folderNameEdittext.text.toString())
                else
                    Utils.showMessageDialog(
                        activity!!,
                        getString(R.string.app_name),
                        getString(R.string.check_internet)
                    )

                dialog.dismiss()
            }
        }
        var window = dialog.getWindow();
        window.setLayout(700, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }

    fun hideKeyboard() {
        val view = this.getCurrentFocus()
        if (view != null) {
            val inputManager: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(view.windowToken, InputMethodManager.SHOW_FORCED)
        }
    }

    class GroupAdapter : BaseAdapter {

        private var context: Context? = null
        private var dataList = ArrayList<ContactModel>()
        var onItemClick: View.OnClickListener

        constructor(
            context: Context, dataList: ArrayList<ContactModel>,
            onItemClick: View.OnClickListener
        ) : super() {

            this.context = context
            this.dataList = dataList
            this.onItemClick = onItemClick
        }

        var view: View? = null
        private lateinit var inflater: LayoutInflater

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

            view = convertView
            val vh = ViewHolder()

            if (convertView == null) {
                inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            }

            view = inflater.inflate(R.layout.activity_tran_company_item_view, null)

            vh.companyNameText = view?.findViewById(R.id.companyNameText) as TextViewPlus

            vh.companyNameText!!.text = dataList.get(position).groupName

            view!!.tag = position
            view!!.setOnClickListener(onItemClick)


            return view
        }


        override fun getItem(position: Int): Any {
            return dataList[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return dataList.size
        }

        inner class ViewHolder {
            var companyNameText: TextViewPlus? = null
        }
    }


}
