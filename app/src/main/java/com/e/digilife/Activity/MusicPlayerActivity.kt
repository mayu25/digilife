package com.e.digilife.Activity

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.AudioManager
import android.media.MediaPlayer
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.NetworkOnMainThreadException
import android.util.Log
import android.view.View
import android.widget.*
import com.android.volley.RequestQueue
import com.e.digilife.CropLibrary.saveToInternalStorage
import com.e.digilife.CropLibrary.shareCacheDirBitmap
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.e.digilife.View.ZoomableImageView
import org.json.JSONArray
import java.io.IOException
import java.net.HttpURLConnection
import java.net.URL

class MusicPlayerActivity : AppCompatActivity() {

    val TAG = "MusicPlayerActivity"
    val activity = this@MusicPlayerActivity
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null

    lateinit var loaderLayout: LinearLayout
    lateinit var closeView: ImageView
    lateinit var deleteView: ImageView
    lateinit var fileNameText: TextView

    var bundle: Bundle? = null
    lateinit var bitmap: Bitmap
    var filePath: String = ""
    var TYPE: String = ""
    var fileID: String = ""
    var fileName: String = ""


    lateinit var playView: ImageView
    lateinit var pauseView: ImageView
    lateinit var seekBar: SeekBar
    lateinit var timePlayingView: TextView
    lateinit var totalPlaytimeView: TextView

    private var mPlayer: MediaPlayer? = null
    private var mHandler: Handler? = null
    private var mRunnable: Runnable? = null

    private val mInterval = 10


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music_player)

            bundle = intent.extras
            init()
            prefs = activity.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)

            if (bundle != null) {
                filePath = bundle!!.getString("filePath")
                TYPE = bundle!!.getString(Utils.TYPE)
                fileID = bundle!!.getString("fileID")
                fileName = bundle!!.getString("fileName")
                Log.e("filePath ::: ", filePath)

                if (!filePath.equals("")) {
                    mHandler = Handler()
                    mPlayer = MediaPlayer()
                    val myUri = Uri.parse(filePath)

                    try {
                        mPlayer!!.setAudioStreamType(AudioManager.STREAM_MUSIC)
                        mPlayer!!.setDataSource(applicationContext, myUri)
                        mPlayer!!.prepare()

                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }


                if (TYPE.equals(Utils.OFFLINE)) {
                    deleteView.visibility = View.VISIBLE
                }
            }

            fileNameText.text = fileName
    }

    fun init() {
        loaderLayout = findViewById(R.id.loaderLayout)
        closeView = findViewById(R.id.closeView)
        deleteView = findViewById(R.id.deleteView)
        fileNameText = findViewById(R.id.fileNameText)

        playView = findViewById(R.id.playView)
        pauseView = findViewById(R.id.pauseView)
        seekBar = findViewById(R.id.seekBar)
        timePlayingView = findViewById(R.id.timePlayingView)
        totalPlaytimeView = findViewById(R.id.totalPlaytimeView)

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, i: Int, b: Boolean) {
                if (mPlayer != null && b) {
                    mPlayer!!.seekTo(i * mInterval)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {

            }
        })


        playView.setOnClickListener(clickListener)
        pauseView.setOnClickListener(clickListener)

        closeView.setOnClickListener(clickListener)
        deleteView.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.closeView -> {
                stopPlaying()

            }
            R.id.deleteView -> {
                deleteOfflineFile()
            }
            R.id.playView -> {
                startMusic()
            }
            R.id.pauseView -> {
                pauseMusic()
            }
        }
    }

    var offineArray = JSONArray()

    fun deleteOfflineFile() {
        val offlineStr = prefs!!.getString(Utils.SAVE_OFFLINE, "")
        if (!offlineStr.equals("[]")) {
            Utils.OFFLINE_IMG_ARRAY = JSONArray(offlineStr)
            for (i in 0..(Utils.OFFLINE_IMG_ARRAY.length() - 1)) {
                val jsonObj = Utils.OFFLINE_IMG_ARRAY.getJSONObject(i)
                val id = jsonObj.getString(Utils.OFFLINE_FILE_ID)
                if (fileID.equals(id)) {
                    Utils.OFFLINE_IMG_ARRAY.remove(i)
                    break
                }
            }
        }
        if (offineArray.length() == 0) {
            Utils.storeJSONArraylist(prefs, Utils.SAVE_OFFLINE, null)
        } else {
            Utils.storeJSONArraylist(prefs, Utils.SAVE_OFFLINE, Utils.OFFLINE_IMG_ARRAY)
        }

        finish()
        Toast.makeText(activity, "File Deleted Successfully !", Toast.LENGTH_SHORT).show()
    }


    //TODO : Music Play - Pause Code.....

    var oneTimeOnly = 0

    fun startMusic() {
        mPlayer!!.start()
        getAudioStats()

        if (oneTimeOnly == 0) {
            initializeSeekBar()
            oneTimeOnly = 1
        }

        playView.visibility = View.GONE
        pauseView.visibility = View.VISIBLE
    }

    fun pauseMusic() {
        if (mPlayer != null && mPlayer!!.isPlaying()) {
            mPlayer!!.pause()
            playView.visibility = View.VISIBLE
            pauseView.visibility = View.GONE
        }
    }


    protected fun getAudioStats() {
        val duration = mPlayer!!.getDuration() / 1000 // In milliseconds
        val due = (mPlayer!!.getDuration() - mPlayer!!.getCurrentPosition()) / 1000
        val pass = duration - due

        timePlayingView.setText("$pass sec")
        totalPlaytimeView.setText("$duration sec")
    }

    protected fun initializeSeekBar() {
        seekBar.setMax(mPlayer!!.getDuration() / mInterval)

        mRunnable = Runnable {
            if (mPlayer != null) {
                val mCurrentPosition = mPlayer!!.getCurrentPosition() / mInterval // In milliseconds
                seekBar.setProgress(mCurrentPosition)
                getAudioStats()
            }
            mHandler!!.postDelayed(mRunnable, mInterval.toLong())
        }
        mHandler!!.postDelayed(mRunnable, mInterval.toLong())
    }

    protected fun stopPlaying() {
        // If media player is not null then try to stop it
        if (mPlayer != null) {
            mPlayer!!.stop()
            mPlayer!!.release()
            mPlayer = null
            if (mHandler != null) {
                mHandler!!.removeCallbacks(mRunnable)
            }
        }
        finish()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        stopPlaying()
    }

}
