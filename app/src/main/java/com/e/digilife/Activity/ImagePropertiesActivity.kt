package com.e.digilife.Activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.e.digilife.CropLibrary.saveToInternalStorage
import com.e.digilife.CropLibrary.shareCacheDirBitmap
import com.e.digilife.Model.DataModel
import com.e.digilife.R
import com.e.digilife.View.Utils

class ImagePropertiesActivity : AppCompatActivity() {

    val TAG = "ImagePropertiesActivity"
    val activity = this@ImagePropertiesActivity

    lateinit var backView: ImageView
    lateinit var shareView:ImageView

    var bundle: Bundle? = null

    internal var dataModel = DataModel()

    var fileName: String = ""
    var isFav: String = ""
    var file_id: String = ""
    var fileSize:String = ""
    var imagePath:String = ""
    var createTime:String = ""
    var modifiedTime :String = ""

    lateinit var titleText:TextView
    lateinit var fileNameText:TextView
    lateinit var fileSizeText:TextView
    lateinit var fileCreatedDateText:TextView
    lateinit var fileModifiedDateText:TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_properties)

        if (intent != null) {
            var fileData = intent.getSerializableExtra(Utils.FILE_DATA)
            if (!fileData.equals("")) {
                dataModel = fileData as DataModel

                file_id = dataModel.fileId
                imagePath = dataModel.filePath
                fileName = dataModel.fileName
                fileSize = dataModel.fileSize
                isFav =dataModel.isFav
                createTime = dataModel.createTime
                modifiedTime = dataModel.modifiedTime
            }
        }
        init()

        fileNameText.text = fileName
        fileSizeText.text = fileSize
        fileCreatedDateText.text = createTime
        fileModifiedDateText.text = modifiedTime

    }

    fun init() {
        backView = findViewById(R.id.backView)
        titleText = findViewById(R.id.titleText)
        shareView = findViewById(R.id.shareView)
        fileNameText = findViewById(R.id.fileNameText)
        fileSizeText = findViewById(R.id.fileSizeText)
        fileCreatedDateText = findViewById(R.id.fileCreatedDateText)
        fileModifiedDateText = findViewById(R.id.fileModifiedDateText)

        titleText.text = resources.getString(R.string.properties)

        backView.setOnClickListener(clickListener)
        shareView.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish()
            }

            R.id.shareView -> {
               val bitmap = Utils.getBitmapFromURL(imagePath)
               val uri = bitmap!!.saveToInternalStorage(this)
               Toast.makeText(applicationContext, uri.toString(), Toast.LENGTH_SHORT).show()
               this.shareCacheDirBitmap(uri)
            }
        }
    }

}
