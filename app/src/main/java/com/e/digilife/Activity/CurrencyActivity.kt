package com.e.digilife.Activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.app.towntalk.Adapter.TrascationCategoryRecyclerAdapter
import com.e.digilife.Model.CurrencyModel
import com.e.digilife.R
import com.e.digilife.View.TextViewPlus
import org.json.JSONArray
import java.io.InputStream


class CurrencyActivity : AppCompatActivity() {

    val TAG = "SignupActivity"
    val activity = this@CurrencyActivity

    lateinit var listview: ListView
    lateinit var backView: ImageView
    lateinit var searchView: ImageView
    lateinit var searchText: EditText
    lateinit var titleText: TextView

    var adapter: CurrencyAdapter? = null
    private var currencyList = ArrayList<CurrencyModel>()
    private var searchList = ArrayList<CurrencyModel>()
    private var tempCurrencyList = ArrayList<CurrencyModel>()

    var isSearch: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_currency)
        init()


        adapter = CurrencyAdapter(activity, currencyList, onItemClick)
        listview.adapter = adapter

        setCurrencyData()
    }

    fun init() {
        listview = findViewById(R.id.listview)
        backView = findViewById(R.id.backView)
        titleText = findViewById(R.id.titleText)
        searchText = findViewById(R.id.searchText)
        searchView = findViewById(R.id.searchView)

        backView.setOnClickListener(clickListener)
        searchView.setOnClickListener(clickListener)

        searchText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {
                var searchText = editable.toString()

                if (searchText.equals("")) {
                    searchList.clear()
                    tempCurrencyList = currencyList
                    adapter = CurrencyAdapter(activity, currencyList, onItemClick)
                    listview.adapter = adapter
                } else {
                    getSearchValue(searchText)
                }
            }
        })

        val font = Typeface.createFromAsset(assets, resources.getString(R.string.popins_regular))
        searchText.setTypeface(font)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish()
            }
            R.id.searchView -> {
                if (isSearch == false) {
                    isSearch = true
                    titleText.visibility = View.GONE
                    searchText.visibility = View.VISIBLE
                } else {
                    isSearch = false
                    titleText.visibility = View.VISIBLE
                    searchText.visibility = View.GONE
                }
            }
        }
    }

    fun getSearchValue(text: String) {
        searchList.clear()
        for (i in 0..(currencyList.size - 1)) {
            var name = currencyList.get(i).countyName
            if ((name.toLowerCase()).contains(text.toLowerCase())) {
                searchList.add(currencyList.get(i))
            }
        }
        tempCurrencyList = searchList
        adapter = CurrencyAdapter(activity, searchList, onItemClick)
        listview.adapter = adapter
    }

    fun setCurrencyData() {
        var jsonArray = JSONArray(readJSONFromAsset())
        for (i in 0..(jsonArray.length() - 1)) {
            val jsonObj = jsonArray.getJSONObject(i)
            var dataModel = CurrencyModel()
            dataModel.countyName = jsonObj.getString("name")
            dataModel.countryCode = jsonObj.getString("countryCode")
            dataModel.countryCurrency = jsonObj.getString("currencytSymbol")
            currencyList.add(dataModel)
        }

        tempCurrencyList = currencyList
        adapter!!.notifyDataSetChanged()
    }

    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            var currency = tempCurrencyList.get(position).countryCurrency
            var intent = Intent()
            intent.putExtra("currency", currency)
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    fun readJSONFromAsset(): String? {
        var json: String? = null
        try {
            val inputStream: InputStream = assets.open("currency.json")
            json = inputStream.bufferedReader().use { it.readText() }
        } catch (ex: Exception) {
            ex.printStackTrace()
            return null
        }
        return json
    }

    class CurrencyAdapter : BaseAdapter {

        private var context: Context? = null
        private var list = ArrayList<CurrencyModel>()
        var onItemClick: View.OnClickListener

        constructor(
            context: Context,
            list: ArrayList<CurrencyModel>,
            onItemClick: View.OnClickListener
        ) : super() {
            this.context = context
            this.list = list
            this.onItemClick = onItemClick
        }

        var view: View? = null
        private lateinit var inflater: LayoutInflater

        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {

            view = convertView
            val vh = ViewHolder()

            if (convertView == null) {

                inflater = context!!.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            }

            view = inflater.inflate(R.layout.activity_currency_item_view, null)

            vh.countrySymbol = view?.findViewById(R.id.countrySymbol) as TextViewPlus
            vh.countryName = view?.findViewById(R.id.countryName) as TextViewPlus
            vh.countryCode = view?.findViewById(R.id.countryCode) as TextViewPlus



            vh.countrySymbol!!.text = list[position].countryCurrency
            vh.countryName!!.text = list[position].countyName
            vh.countryCode!!.text = list[position].countryCode


            view!!.tag = position
            view!!.setOnClickListener(onItemClick)

            return view
        }

        override fun getItem(position: Int): Any {
            return list[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getCount(): Int {
            return list.size
        }

        private inner class ViewHolder {
            var countrySymbol: TextViewPlus? = null
            var countryName: TextViewPlus? = null
            var countryCode: TextViewPlus? = null
        }

    }

}
