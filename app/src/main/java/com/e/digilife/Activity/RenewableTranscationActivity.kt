package com.e.digilife.Activity

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.RenewableTranscationAdapter
import com.app.towntalk.Adapter.TransactionMemberListRecyclerAdapter
import com.app.towntalk.Adapter.W_BudgetRecyclerAdapter
import com.e.digilife.Model.WalletModel
import com.e.digilife.R
import com.e.digilife.View.InputValidation
import com.e.digilife.View.TextViewPlus
import com.e.digilife.View.Utils
import com.e.digilife.View.VolleyMultipartRequest
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import org.w3c.dom.Text
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class RenewableTranscationActivity : AppCompatActivity() {

    val TAG = "RenewableTranscationActivity"
    val activity = this@RenewableTranscationActivity
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null
    lateinit var inputValidation: InputValidation

    private var dataList = ArrayList<WalletModel>()
    var adapter: RenewableTranscationAdapter? = null

    var client_id: String = ""
    var currency: String = ""
    var stRenewableType: String = ""
    var categoryID: String = ""
    var categoryName: String = ""
    var selectedDateFormat: String = ""
    var amount: String = ""

    lateinit var loaderLayout: LinearLayout
    lateinit var backView: ImageView
    lateinit var saveTextView: TextView
    lateinit var tvYearly: TextView
    lateinit var tvMonthly: TextView
    lateinit var tvWeekly: TextView
    lateinit var transcationRecyclerView: RecyclerView
    lateinit var listTitleText: TextViewPlus
    lateinit var categoryLayout: RelativeLayout
    lateinit var categoryNameText: TextView
    lateinit var amountEdittext: EditText


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_renewable_transcation)

        requestQueue = Volley.newRequestQueue(activity)
        prefs = activity!!.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        currency = prefs!!.getString(Utils.CURRENCY, "")
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }

        init()

        selectedDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(Date())

        val linearLayoutManager = LinearLayoutManager(activity)
        transcationRecyclerView.setLayoutManager(linearLayoutManager)
        adapter = RenewableTranscationAdapter(activity, dataList, onItemClick, onDeleteItemClick)
        transcationRecyclerView.adapter = adapter

        tvYearly.setTextColor(resources.getColor(R.color.blackColor))
        tvMonthly.setTextColor(resources.getColor(R.color.l_blue))
        tvWeekly.setTextColor(resources.getColor(R.color.blackColor))
        stRenewableType = Utils.TRANSACTION_MONTHLY

        getRenewableTranscation()
    }

    fun init() {
        inputValidation = InputValidation(activity)

        loaderLayout = findViewById(R.id.loaderLayout)
        saveTextView = findViewById(R.id.saveTextView)
        listTitleText = findViewById(R.id.listTitleText)
        backView = findViewById(R.id.backView)
        transcationRecyclerView = findViewById(R.id.transcationRecyclerView)
        tvYearly = findViewById(R.id.tvYearly)
        tvMonthly = findViewById(R.id.tvMonthly)
        tvWeekly = findViewById(R.id.tvWeekly)
        categoryLayout = findViewById(R.id.categoryLayout)
        categoryNameText = findViewById(R.id.categoryNameText)
        amountEdittext = findViewById(R.id.amountEdittext)

        backView.setOnClickListener(clickListener)
        saveTextView.setOnClickListener(clickListener)
        tvYearly.setOnClickListener(clickListener)
        tvMonthly.setOnClickListener(clickListener)
        tvWeekly.setOnClickListener(clickListener)
        categoryLayout.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish()
            }
            R.id.saveTextView -> {
                validation()
            }
            R.id.categoryLayout -> {
                val i = Intent(this@RenewableTranscationActivity, TrascationCategoryActivity::class.java)
                i.putExtra("type", "")
                startActivityForResult(i, Utils.CATEGORY)
            }
            R.id.tvYearly -> {
                tvYearly.setTextColor(resources.getColor(R.color.l_blue))
                tvMonthly.setTextColor(resources.getColor(R.color.blackColor))
                tvWeekly.setTextColor(resources.getColor(R.color.blackColor))
                stRenewableType = Utils.TRANSACTION_YEARLY
            }
            R.id.tvWeekly -> {
                tvYearly.setTextColor(resources.getColor(R.color.blackColor))
                tvMonthly.setTextColor(resources.getColor(R.color.blackColor))
                tvWeekly.setTextColor(resources.getColor(R.color.l_blue))
                stRenewableType = Utils.TRANSACTION_WEEKLY
            }
            R.id.tvMonthly -> {
                tvYearly.setTextColor(resources.getColor(R.color.blackColor))
                tvMonthly.setTextColor(resources.getColor(R.color.l_blue))
                tvWeekly.setTextColor(resources.getColor(R.color.blackColor))
                stRenewableType = Utils.TRANSACTION_MONTHLY
            }
        }
    }

    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            Log.e("position ::", position.toString())
            var intent = Intent(activity, AddTrascationActivity::class.java)
            intent.putExtra("transcation", "edit")
            intent.putExtra(Utils.TRANSCATION_DATA, dataList.get(position))
            startActivity(intent)
        }
    }

    var onDeleteItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            Log.e("position:::", position.toString())
            showDeleteMessageDialog("Are you sure want to delete this transcation ?", position)
        }
    }

    //TODO: Renewable Transcation.............

    private fun getRenewableTranscation() {

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.GET_RENEWABLE_TRASCATION,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    Log.e("Category List ::", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        val dataArray = jsonObj.getJSONArray("data")
                        if (dataArray.length() > 0) {
                            listTitleText.visibility = View.VISIBLE
                            setBudgetCategoryList(dataArray)
                        } else {
                            listTitleText.visibility = View.GONE
                            dataList.clear()
                            adapter!!.notifyDataSetChanged()
                        }

                    } else {
                        listTitleText.visibility = View.GONE
                        dataList.clear()
                        adapter!!.notifyDataSetChanged()
                        // Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                Log.e(" JSONData :", params.toString())
                return params
            }
        }

        Log.e(" API :", EndPoints.GET_RENEWABLE_TRASCATION)
        requestQueue.add(stringRequest)
    }


    fun setBudgetCategoryList(jsonArray: JSONArray) {
        dataList.clear()
        for (i in 0..(jsonArray.length() - 1)) {
            val jsonObj = jsonArray.getJSONObject(i)
            val dataArray = jsonObj.getJSONArray("data")
            if (dataArray.length() > 0) {
                for (j in 0..(dataArray.length() - 1)) {
                    val dataObj = dataArray.getJSONObject(j)
                    val dataModel = WalletModel()
                    if (j == 0) {
                        dataModel.isTitle = jsonObj.getString("date")
                    } else {
                        dataModel.isTitle = ""
                    }

                    dataModel.currency = currency
                    dataModel.tranId = dataObj.getString("id")
                    dataModel.tranAmount = getFloatValue(dataObj.getString("amount"))
                    dataModel.tranDescription = dataObj.getString("description")
                    dataModel.trasAttachment = dataObj.getString("attachment")
                    dataModel.tranClaimReimburse = dataObj.getString("is_claim_reimburse")
                    dataModel.trasPurpose = dataObj.getString("purpose")
                    dataModel.trasCategoryId = dataObj.getString("category_id")
                    // dataModel.transCompanyName = dataObj.getString("company_name")
                    dataModel.trasnCompnayId = dataObj.getString("company_id")
                    dataModel.trasRenewableType = dataObj.getString("renewable_type")
                    dataModel.trasIsRenewable = dataObj.getString("is_renewable")
                    dataModel.transFullDateTime = dataObj.getString("date_time")
                    var sdDate = dateConvertor(dataObj.getString("date_time"))
                    dataModel.transDate = sdDate
                    dataModel.trasSplit = dataObj.getString("is_split")
                    // dataModel.trasSplitData = dataObj.getString("split_data")
                    dataModel.trasCategoryName = dataObj.getString("category_name")
                    dataModel.trasCategoryType = dataObj.getString("category_type")

                    dataList.add(dataModel)
                }

            }
        }

        adapter!!.notifyDataSetChanged()
    }


    fun getFloatValue(value: String): String {
        val format = DecimalFormat("#,###,###0.00")
        var newPrice = format.format(java.lang.Double.parseDouble(value))
        return newPrice
    }

    //TODO: Delete Transcation.............

    private fun deleteTranscation(id: String, pos: Int) {

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.DELETE_TRANSCATION,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        getRenewableTranscation()
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["id"] = id

                Log.e(" JSONData :", params.toString())
                return params
            }
        }

        Log.e(" API :", EndPoints.DELETE_TRANSCATION)
        requestQueue.add(stringRequest)
    }


    fun addTranscation() {
        loaderLayout.visibility = View.VISIBLE

        val volleyMultipartRequest = object : VolleyMultipartRequest(
            Request.Method.POST, EndPoints.ADD_TRANSCATION,
            Response.Listener { response ->
                Log.e("response", String(response.data))
                requestQueue.getCache().clear();
                loaderLayout.visibility = View.GONE

                var jsonObj = JSONObject(String(response.data))
                val status = jsonObj.getString("status")
                if (status.equals("1")) {
                    finish()
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                }

            },
            Response.ErrorListener { error ->
                Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT).show()
            }) {


            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("client_id", client_id)
                params.put("amount", amount)
                params.put("category_id", categoryID)
                params.put("purpose", "personal")
                params.put("description", "")
                params.put("date_time", selectedDateFormat)
                params.put("is_claim_reimburse", "0")
                params.put("company_id", "")
                params.put("is_renewable", "1")
                params.put("renewable_type", stRenewableType)
                params.put("is_split", "0")
                params.put("split_data", "")
                params.put("attachment", "")

                Log.e(" JSONData :", params.toString())
                return params
            }

            override fun getByteData(): Map<String, DataPart> {
                val params = HashMap<String, DataPart>()
                // val imagename = System.currentTimeMillis()
                return params
            }
        }
        Log.e(" API :", EndPoints.ADD_TRANSCATION)
        volleyMultipartRequest.retryPolicy = DefaultRetryPolicy(
            0,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(volleyMultipartRequest);
    }


    // delete dialog.......

    fun showDeleteMessageDialog(message: String, position: Int) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = activity!!.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.confirmation_dialog, null)
        dialog.setContentView(view)

        val cancleBtn = dialog.findViewById(R.id.cancleText) as TextView
        val okBtn = dialog.findViewById(R.id.okBtn) as Button
        val dialogText = dialog.findViewById(R.id.dialogText) as TextView
        dialogText.setText(message)

        val font = Typeface.createFromAsset(activity!!.assets, resources.getString(R.string.popins_semi_bold))
        okBtn.setTypeface(font)

        okBtn.setOnClickListener {
            deleteTranscation(dataList.get(position).tranId, position)
            dialog.dismiss()
        }

        cancleBtn.setOnClickListener {
            dialog.dismiss()
        }

        var window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }


    fun dateConvertor(inputText: String): String {

        val outputFormat = SimpleDateFormat("E, MMM dd yyyy", Locale.US)
        val inputFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
        var date: Date? = null
        try {
            date = inputFormat.parse(inputText)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val outputText = outputFormat.format(date)
        return outputText
    }


    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && requestCode == Utils.CATEGORY) {
            var name = data!!.getStringExtra("cName")
            var id = data.getStringExtra("cId")
            categoryID = id
            categoryNameText.text = name
            categoryName = name
        }
    }


    private fun validation() {


        if (!inputValidation.isFieldEmpty(amountEdittext, getString(R.string.monthly_income_validation))) {
            return
        }
        if (!inputValidation.isFieldBlank(categoryName, getString(R.string.category_validation))) {
            return
        }

        if (!inputValidation.isFieldBlank(stRenewableType, getString(R.string.renewable_validation))) {
            return
        }

        amount = amountEdittext.text.toString().trim()

        if (Utils.checkInternetConnection(this@RenewableTranscationActivity)) {
            hideKeyboard()
            addTranscation()

        } else {
            Utils.showMessageDialog(
                this@RenewableTranscationActivity,
                getString(R.string.app_name),
                getString(R.string.check_internet)
            )
        }
    }

    fun hideKeyboard() {
        val view = this.getCurrentFocus()
        if (view != null) {
            val inputManager: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(view.windowToken, InputMethodManager.SHOW_FORCED)
        }
    }


}
