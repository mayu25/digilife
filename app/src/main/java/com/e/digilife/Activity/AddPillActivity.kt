package com.e.digilife.Activity

import android.app.Dialog
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.e.digilife.Adapter.PillDaysAdapter
import com.e.digilife.Adapter.PillTimeAdapter
import com.e.digilife.Model.PillReminderModel
import com.e.digilife.R
import com.e.digilife.View.InputValidation
import com.e.digilife.View.Utils
import com.e.digilife.View.VolleyMultipartRequest
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*


class AddPillActivity : AppCompatActivity() {


    val TAG = "AddPillActivity"
    val activity = this@AddPillActivity
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null
    lateinit var inputValidation: InputValidation


    lateinit var noteSelectionView: LinearLayout
    lateinit var viewNote: TextView
    lateinit var editNote: TextView
    lateinit var cancelView: TextView
    lateinit var loaderLayout: LinearLayout
    lateinit var backView: ImageView
    lateinit var titleText: TextView
    lateinit var statusSwitch: Switch
    lateinit var medicineET: EditText
    lateinit var fromeDateLayout: RelativeLayout
    lateinit var toDateLayout: RelativeLayout
    lateinit var daysGroup: RadioGroup
    lateinit var everydayRadioBtn: RadioButton
    lateinit var daywiseRadioBtn: RadioButton
    lateinit var fromDateText: TextView
    lateinit var toDateText: TextView
    lateinit var dayRecyclerView: RecyclerView
    lateinit var medicineTimeSpinner: Spinner
    lateinit var addtabletView: ImageView
    lateinit var tabletTimeRecyclerView: RecyclerView
    lateinit var doctorNameET: EditText
    lateinit var rxNumberEt: EditText
    lateinit var addNoteLayout: LinearLayout
    lateinit var addPillLayout: LinearLayout
    lateinit var addAnotherReminderView: TextView
    lateinit var doneView: TextView
    lateinit var editPillLayout: LinearLayout
    lateinit var editPillView: TextView
    lateinit var deletePillView: TextView

    internal var pillModel = PillReminderModel()


    private var daysList = ArrayList<PillReminderModel>()
    var daysAdapter: PillDaysAdapter? = null

    private var tabletTimeList = ArrayList<PillReminderModel>()
    var tabletTimeAdapter: PillTimeAdapter? = null

    var client_id: String = ""
    val days = arrayOf("S", "M", "T", "W", "T", "F", "S")
    val medicineTimes = arrayOf("Before Food", "After Food", "Without Water", "No Alcohol")
    var item1: String = "Before Food"
    var item2: String = "After Food"
    var item3: String = "Without Water"
    var item4: String = "No Alcohol"


    var pillId: String = ""
    var EDIT_PILLS: String = ""
    var addPillID: Int = 0

    var medicineName: String = ""
    var fromDate: String = ""
    var toDate: String = ""
    var reminderType: String = "1"
    var pillDays: String = ""
    var dailyTimes: String = ""
    var takeMedicine: String = ""
    var doctorName: String = ""
    var rxNumber: String = ""
    var note: String = ""
    var pillStatus: String = "0"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pill_add_view)
        init()

        requestQueue = Volley.newRequestQueue(this)
        prefs = this.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)

        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }

        if (intent != null) {
            EDIT_PILLS = intent.getStringExtra(Utils.EDIT_PILLS)

            if (EDIT_PILLS.equals("1")) {
                setAdapter()
                var pillData = intent.getSerializableExtra(Utils.PILL_OBJ)
                if (!pillData.equals("")) {
                    pillModel = pillData as PillReminderModel
                    setEditPills()
                }


                addPillLayout.visibility = View.GONE
                editPillLayout.visibility = View.VISIBLE

                titleText.setText(resources.getString(R.string.edit_pill))

            } else {
                setAdapter()
                addPillLayout.visibility = View.VISIBLE
                editPillLayout.visibility = View.GONE

                fromDate = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(Date())
                toDate = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(Date())

                setAddPills()
            }
        }

    }

    fun init() {
        inputValidation = InputValidation(activity)
        noteSelectionView = findViewById(R.id.noteSelectionView)
        viewNote = findViewById(R.id.viewNote)
        editNote = findViewById(R.id.editNote)
        cancelView = findViewById(R.id.cancelView)
        loaderLayout = findViewById(R.id.loaderLayout)
        backView = findViewById(R.id.backView)
        titleText = findViewById(R.id.titleText)
        statusSwitch = findViewById(R.id.statusSwitch)
        medicineET = findViewById(R.id.medicineET)
        fromeDateLayout = findViewById(R.id.fromeDateLayout)
        toDateLayout = findViewById(R.id.toDateLayout)
        fromDateText = findViewById(R.id.fromDateText)
        toDateText = findViewById(R.id.toDateText)
        daysGroup = findViewById(R.id.daysGroup)
        everydayRadioBtn = findViewById(R.id.everydayRadioBtn)
        daywiseRadioBtn = findViewById(R.id.daywiseRadioBtn)
        medicineTimeSpinner = findViewById(R.id.medicineTimeSpinner)
        addtabletView = findViewById(R.id.addtabletView)
        tabletTimeRecyclerView = findViewById(R.id.tabletTimeRecyclerView)
        doctorNameET = findViewById(R.id.doctorNameET)
        rxNumberEt = findViewById(R.id.rxNumberEt)
        addNoteLayout = findViewById(R.id.addNoteLayout)
        addPillLayout = findViewById(R.id.addPillLayout)
        addAnotherReminderView = findViewById(R.id.addAnotherReminderView)
        doneView = findViewById(R.id.doneView)
        editPillLayout = findViewById(R.id.editPillLayout)
        editPillView = findViewById(R.id.editPillView)

        deletePillView = findViewById(R.id.deletePillView)
        dayRecyclerView = findViewById(R.id.dayRecyclerView)

        val font = Typeface.createFromAsset(assets, resources.getString(R.string.popins_regular))
        medicineET.setTypeface(font)
        doctorNameET.setTypeface(font)
        rxNumberEt.setTypeface(font)
        // medicineTimeSpinner.setTypeface(font)

        val font2 = Typeface.createFromAsset(assets, resources.getString(R.string.popins_semi_bold))
        everydayRadioBtn.setTypeface(font2)
        daywiseRadioBtn.setTypeface(font2)

        val aa = ArrayAdapter(this, R.layout.spinner_item_view, R.id.letterText, medicineTimes)
        aa.setDropDownViewResource(R.layout.spinner_item)
        medicineTimeSpinner.setAdapter(aa)


        medicineTimeSpinner.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(adapterView: AdapterView<*>, arg1: View, pos: Int, arg3: Long) {
                takeMedicine = adapterView.getItemAtPosition(pos).toString().trim();
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {
                // TODO Auto-generated method stub
            }

        })

        /* medicineTimeSpinner.setItems("Before Food", "After Food", "Without Water", "No Alcohol")

         medicineTimeSpinner.setOnItemSelectedListener({ view, pos, id, item ->
             Toast.makeText(activity, item.toString(), Toast.LENGTH_SHORT).show()
             takeMedicine = item.toString()
         })*/

        daysGroup.setOnCheckedChangeListener({ group, checkedId ->
            val rb = findViewById<View>(checkedId) as RadioButton
            if (rb.text.equals("Everyday")) {
                resetDays()
                reminderType = "1"
            } else {
                reminderType = "2"
            }
        })

        statusSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            Log.e("Switch State=", "" + isChecked);

            if (buttonView.isPressed) {
                if (isChecked == true) {
                    pillStatus = "1"
                } else {
                    pillStatus = "0"
                }
            }
        }

        backView.setOnClickListener(clickListener)
        fromeDateLayout.setOnClickListener(clickListener)
        toDateLayout.setOnClickListener(clickListener)
        addtabletView.setOnClickListener(clickListener)
        doneView.setOnClickListener(clickListener)
        addAnotherReminderView.setOnClickListener(clickListener)
        addNoteLayout.setOnClickListener(clickListener)
        editPillView.setOnClickListener(clickListener)
        deletePillView.setOnClickListener(clickListener)
        viewNote.setOnClickListener(clickListener)
        editNote.setOnClickListener(clickListener)
        cancelView.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish()
            }
            R.id.fromeDateLayout -> {
                openDatePicker("start")
            }
            R.id.toDateLayout -> {
                openDatePicker("end")
            }
            R.id.addtabletView -> {
                setTabletTimeView()
            }
            R.id.addAnotherReminderView -> {
                validation("add")
            }

            R.id.doneView -> {
                validation("Done")
            }

            R.id.addNoteLayout -> {
                if (EDIT_PILLS.equals("1")) {
                    noteSelectionView.visibility = View.VISIBLE
                } else {
                    showCreateNoteDialog("add")
                }
            }
            R.id.viewNote -> {
                noteSelectionView.visibility = View.GONE
                showNoteDialog()
            }
            R.id.editNote -> {
                noteSelectionView.visibility = View.GONE
                showCreateNoteDialog("edit")
            }
            R.id.cancelView -> {
                noteSelectionView.visibility = View.GONE
            }
            R.id.editPillView -> {
                validation("edit")
            }
            R.id.deletePillView -> {
                showDeleteMessageDialog("Are you sure want to delete ?")
            }
        }
    }


    //TODO : Add Pills...........

    fun addPills(type: String) {
        loaderLayout.visibility = View.VISIBLE

        val volleyMultipartRequest = object : VolleyMultipartRequest(
            Request.Method.POST, EndPoints.ADD_PILLS,
            Response.Listener { response ->
                Log.e("response", String(response.data))
                requestQueue.getCache().clear();
                loaderLayout.visibility = View.GONE

                var jsonObj = JSONObject(String(response.data))
                val status = jsonObj.getString("status")
                if (status.equals("1")) {
                    if (type.equals("Done")) {
                        finish()
                    } else {
                        finish()
                        startActivity(getIntent())
                    }

                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                }

            },
            Response.ErrorListener { error ->
                Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT).show()
            }) {


            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("client_id", client_id)
                params.put("medicine_name", medicineName)
                params.put("from_date", fromDate)
                params.put("to_date", toDate)
                params.put("pill_reminder_type", reminderType)
                params.put("pill_days", pillDays)
                params.put("take_medicine", takeMedicine)
                params.put("daily_pill", pillJsonArray.toString())
                params.put("daily_time", dailyTimes)
                params.put("dr_name", doctorName)
                params.put("rx_number", rxNumber)
                params.put("description", note)
                params.put("pill_status", pillStatus)

                Log.e(" JSONData :", params.toString())
                return params
            }
        }
        Log.e(" API :", EndPoints.ADD_PILLS)
        volleyMultipartRequest.retryPolicy = DefaultRetryPolicy(
            0,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(volleyMultipartRequest);
    }


    //TODO: Edit Pills.......

    fun editPills() {
        loaderLayout.visibility = View.VISIBLE

        val volleyMultipartRequest = object : VolleyMultipartRequest(
            Request.Method.POST, EndPoints.EDIT_PILLS,
            Response.Listener { response ->
                Log.e("response", String(response.data))
                requestQueue.getCache().clear();
                loaderLayout.visibility = View.GONE

                var jsonObj = JSONObject(String(response.data))
                val status = jsonObj.getString("status")
                if (status.equals("1")) {

                    finish()
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                }

            },
            Response.ErrorListener { error ->
                Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT).show()
            }) {


            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("client_id", client_id)
                params.put("pill_id", pillId)
                params.put("medicine_name", medicineName)
                params.put("from_date", fromDate)
                params.put("to_date", toDate)
                params.put("pill_reminder_type", reminderType)
                params.put("pill_days", pillDays)
                params.put("take_medicine", takeMedicine)
                params.put("daily_pill", pillJsonArray.toString())
                params.put("daily_time", dailyTimes)
                params.put("dr_name", doctorName)
                params.put("rx_number", rxNumber)
                params.put("description", note)
                params.put("pill_status", pillStatus)

                Log.e(" JSONData :", params.toString())
                return params
            }
        }
        Log.e(" API :", EndPoints.EDIT_PILLS)
        volleyMultipartRequest.retryPolicy = DefaultRetryPolicy(
            0,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(volleyMultipartRequest);
    }


    //TODO: Delete Pills.........

    private fun deletePill() {

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.DELETE_PILLS,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        finish()
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["pill_id"] = pillId

                Log.e(" JSONData :", params.toString())
                return params
            }
        }

        Log.e(" API :", EndPoints.DELETE_PILLS)
        requestQueue.add(stringRequest)
    }


    //TODO: Add Pills ..........

    fun setAdapter() {
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        dayRecyclerView.setLayoutManager(layoutManager)
        dayRecyclerView.setHasFixedSize(true)
        daysAdapter = PillDaysAdapter(activity, daysList, onDayItemClick)
        dayRecyclerView.adapter = daysAdapter

        val layoutManager2 = LinearLayoutManager(this)
        tabletTimeRecyclerView.setLayoutManager(layoutManager2)
        tabletTimeRecyclerView.setHasFixedSize(true)
        tabletTimeAdapter = PillTimeAdapter(activity, tabletTimeList, onDeleteItemClick)
        tabletTimeRecyclerView.adapter = tabletTimeAdapter
    }

    fun setAddPills() {
        takeMedicine = "Before Food"


        titleText.setText(resources.getString(R.string.add_pill))
        fromDateText.setText(SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault()).format(Date()))
        toDateText.setText(SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault()).format(Date()))
        everydayRadioBtn.isChecked = true

        setDays("", "add")
        tabletTimeList.clear()
        setTabletTimeView()
    }


    //TODO: Edit Pills ............

    fun setEditPills() {
        pillId = pillModel.pillId

        medicineName = pillModel.pillName
        fromDate = pillModel.fromDate
        toDate = pillModel.toDate
        reminderType = pillModel.pillReminderType
        pillDays = pillModel.pillDays
        dailyTimes = pillModel.dailyTime
        takeMedicine = pillModel.takePills
        doctorName = pillModel.doctoreName
        rxNumber = pillModel.rxNumber
        note = pillModel.note
        pillStatus = pillModel.pillStatus

        medicineET.setText(medicineName)
        fromDateText.setText(convertDateFormat(fromDate))
        toDateText.setText(convertDateFormat(toDate))


        if (reminderType.equals("1")) {
            everydayRadioBtn.isChecked = true
        } else {
            daywiseRadioBtn.isChecked = true
        }

        if (takeMedicine.equals(item1)) {
            medicineTimeSpinner.setSelection(0)
        } else if (takeMedicine.equals(item2)) {
            medicineTimeSpinner.setSelection(1)
        } else if (takeMedicine.equals(item3)) {
            medicineTimeSpinner.setSelection(2)
        } else if (takeMedicine.equals(item4)) {
            medicineTimeSpinner.setSelection(3)
        }


        doctorNameET.setText(doctorName)
        rxNumberEt.setText(rxNumber)

        if (pillStatus.equals("0")) {
            statusSwitch.isChecked = false
        } else {
            statusSwitch.isChecked = true
        }

        setDays(pillDays, "edit")
        if (!dailyTimes.equals(""))
            setPillReminderTimes(dailyTimes)
    }

    //Take-1-Tablet-18:04,Take-2-Capsules-19:05

    fun setPillReminderTimes(dailyTimes: String) {


        val elements = dailyTimes.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        val fixedLenghtList = Arrays.asList(*elements)
        val listOfString = ArrayList(fixedLenghtList)

        Log.e("listOfString ::", listOfString.toString())

        for (i in 0..(listOfString.size - 1)) {
            var pillItem = listOfString.get(i)
            val element = pillItem.split("-")
            var item1 = element[1]
            var item2 = element[2]
            var item3 = convert24HoursTO12Format(element[3])
            var item4 = convertAM_PMFormate(element[3])

            var pillModel = PillReminderModel()
            addPillID = addPillID + 1
            pillModel.pillId = addPillID.toString()
            pillModel.pillRemiderTime = item3
            pillModel.pillTimeAM_PM = item4
            pillModel.pillCount = item1
            pillModel.pillType = item2
            tabletTimeList.add(pillModel)

        }
        tabletTimeAdapter!!.notifyDataSetChanged()

    }

    fun convertDateFormat(value: String): String {
        val inputFormat = SimpleDateFormat("yyyy-mm-dd", Locale.ENGLISH)
        val outputFormat = SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
        var date: Date? = null
        date = inputFormat.parse(value)
        val output = outputFormat.format(date)
        println(output)
        return output
    }

    fun convert24HoursTO12Format(value: String): String {
        val inputFormat = SimpleDateFormat("HH:mm", Locale.ENGLISH)
        val outputFormat = SimpleDateFormat("hh:mm", Locale.ENGLISH)
        var date: Date? = null
        date = inputFormat.parse(value)
        val output = outputFormat.format(date)
        println(output)
        return output
    }

    fun convertAM_PMFormate(value: String): String {
        val inputFormat = SimpleDateFormat("HH:mm", Locale.ENGLISH)
        val outputFormat = SimpleDateFormat("aa", Locale.ENGLISH)
        var date: Date? = null
        date = inputFormat.parse(value)
        val output = outputFormat.format(date)
        println(output)
        return output
    }

    //TODO: Day Selection............

    fun setDays(selectedDays: String, type: String) {

        daysList.clear()

        if (type.equals("edit")) {
            for (i in 0..(days.size - 1)) {
                var reminderModel = PillReminderModel()
                reminderModel.dayLetter = days.get(i)
                if (selectedDays.contains(i.toString())) {
                    reminderModel.isDaySelected = "1"
                } else {
                    reminderModel.isDaySelected = "0"
                }
                reminderModel.dayId = i.toString()
                daysList.add(reminderModel)
            }

        } else if (type.equals("add")) {
            for (i in 0..(days.size - 1)) {
                var reminderModel = PillReminderModel()
                reminderModel.dayLetter = days.get(i)
                reminderModel.isDaySelected = "0"
                reminderModel.dayId = i.toString()
                daysList.add(reminderModel)
            }
        }
        daysAdapter!!.notifyDataSetChanged()
    }

    fun resetDays() {
        for (i in 0..(daysList.size - 1)) {
            daysList.get(i).isDaySelected = "0"
        }
        daysAdapter!!.notifyDataSetChanged()
    }

    var onDayItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int

            if (reminderType.equals("2")) {
                var isSelection = daysList.get(position).isDaySelected
                if (isSelection.equals("0")) {
                    daysList.get(position).isDaySelected = "1"
                } else {
                    daysList.get(position).isDaySelected = "0"
                }
                daysAdapter!!.notifyDataSetChanged()
            }
        }
    }


    var pillJsonArray = JSONArray()

    fun validation(type: String) {
        if (!inputValidation.isFieldEmpty(medicineET, getString(R.string.medicine_name_validation))) {
            return
        }


        medicineName = medicineET.text.toString().trim()
        var firstDay: String = "1"

        for (i in 0..(daysList.size - 1)) {
            var isDaySelected = daysList.get(i).isDaySelected
            if (isDaySelected.equals("1")) {
                if (firstDay.equals("1")) {
                    firstDay = "0"
                    pillDays = pillDays + daysList.get(i).dayId
                } else {
                    pillDays = pillDays + "," + daysList.get(i).dayId
                }
            }
        }

        Log.e("pillDays ::", pillDays)
        Log.e("takeMedicine ::", takeMedicine)

        for (i in 0..(tabletTimeList.size - 1)) {

            var pillId = tabletTimeList.get(i).pillId
            var pillType = tabletTimeList.get(i).pillType
            var pillTime = tabletTimeList.get(i).pillRemiderTime + tabletTimeList.get(i).pillTimeAM_PM
            var time24: String = ""
            if (!pillTime.equals("")) {
                time24 = convert24HoursFormat(pillTime)
            } else {
                time24 = ""
            }

            if (i == 0) {
                dailyTimes = "Take-" + pillId + "- " + pillType + "-" + time24
            } else {
                dailyTimes = dailyTimes + "," + "Take-" + pillId + "- " + pillType + "-" + time24
            }

            var jObj = JSONObject()
            jObj.put("tab_no", pillId)
            jObj.put("tab_name", pillType)
            jObj.put("tab_time", pillTime)
            pillJsonArray.put(jObj)
        }

        Log.e("dailyTimes ::", dailyTimes)
        Log.e("pillJsonArray ::", pillJsonArray.toString())

        if (Utils.checkInternetConnection(this@AddPillActivity)) {
            hideKeyboard()

            if (type.equals("add") || type.equals("Done")) {
                addPills(type)
            } else if (type.equals("edit")) {
                editPills()
            }


        } else {
            Utils.showMessageDialog(
                this@AddPillActivity,
                getString(R.string.app_name),
                getString(R.string.check_internet)
            )
        }
        // daily_time: String = ""
        doctorName = doctorNameET.text.toString().trim()
        rxNumber = rxNumberEt.text.toString().trim()


    }


    fun convert24HoursFormat(value: String): String {
        val inputFormat = SimpleDateFormat("hh:mmaa", Locale.ENGLISH)
        val outputFormat = SimpleDateFormat("HH:mm", Locale.ENGLISH)
        var date: Date? = null
        date = inputFormat.parse(value)
        val output = outputFormat.format(date)
        println(output)
        return output
    }


    fun hideKeyboard() {
        val view = this.getCurrentFocus()
        if (view != null) {
            val inputManager: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(view.windowToken, InputMethodManager.SHOW_FORCED)
        }
    }


    //TODO: Tablet Time View..........

    fun setTabletTimeView() {


        var pillModel = PillReminderModel()
        addPillID = addPillID + 1
        pillModel.pillId = addPillID.toString()
        pillModel.pillRemiderTime = ""
        pillModel.pillTimeAM_PM = ""
        pillModel.pillCount = "1"
        pillModel.pillType = "Tablet"
        tabletTimeList.add(pillModel)

        tabletTimeAdapter!!.notifyDataSetChanged()
    }


    var onDeleteItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            tabletTimeAdapter!!.remove(position)
        }
    }


    //TODO: Date Convertor............


    fun openDatePicker(type: String) {
        SingleDateAndTimePickerDialog.Builder(activity)
            .bottomSheet()
            .displayMinutes(false)
            .displayHours(false)
            .displayDays(false)
            .displayMonth(true)
            .displayYears(true)
            .displayDaysOfMonth(true)
            .displayListener(object : SingleDateAndTimePickerDialog.DisplayListener {
                override fun onDisplayed(picker: SingleDateAndTimePicker) {
                    //retrieve the SingleDateAndTimePicker
                }
            })
            .title("Select Date")
            .titleTextColor(getResources().getColor(R.color.blackColor))
            .backgroundColor(getResources().getColor(R.color.whiteColor))
            .mainColor(getResources().getColor(R.color.blackColor))
            .curved()
            .listener(object : SingleDateAndTimePickerDialog.Listener {
                override fun onDateSelected(date: Date) {
                    var convertDate = dateConvertor(date)
                    if (type.equals("start")) {
                        fromDate = APIServerDate(date)
                        fromDateText.setText(convertDate)
                    } else {
                        toDate = APIServerDate(date)
                        toDateText.setText(convertDate)
                    }
                }
            }).display()
    }

    fun dateConvertor(date: Date): String {
        val outputFormat = SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH)
        outputFormat.timeZone = TimeZone.getTimeZone("UTC")
        val output = outputFormat.format(date)
        println(output)
        return output
    }

    fun APIServerDate(date: Date): String {
        val outputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        outputFormat.timeZone = TimeZone.getTimeZone("UTC")
        val output = outputFormat.format(date)
        println(output)
        return output
    }

    fun showCreateNoteDialog(type: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = activity.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.pill_note_view, null)
        dialog.setContentView(view)

        val noteEt = dialog.findViewById(R.id.noteEt) as EditText
        val saveView = dialog.findViewById(R.id.saveView) as TextView
        val cancleView = dialog.findViewById(R.id.cancleView) as TextView
        val title = dialog.findViewById(R.id.title) as TextView


        val font = Typeface.createFromAsset(activity!!.assets, resources.getString(R.string.popins_regular))
        noteEt.setTypeface(font)

        if (type.equals("edit")) {
            noteEt.setText(note)
            title.setText("Edit Note")
        } else {
            title.setText(activity.resources.getString(R.string.create_note))
        }

        saveView.setOnClickListener {
            if (noteEt.text.toString().equals("")) {
                Toast.makeText(activity, "Enter Note", Toast.LENGTH_SHORT).show()
            } else {
                note = noteEt.text.toString()
                dialog.dismiss()
            }
        }

        cancleView.setOnClickListener {
            dialog.dismiss()

        }
        var window = dialog.getWindow();
        window.setLayout(800, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }

    //Show Note
    fun showNoteDialog() {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = activity.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.pill_note_view_dialog, null)
        dialog.setContentView(view)

        val noteTV = dialog.findViewById(R.id.noteTV) as TextView
        val cancleView = dialog.findViewById(R.id.cancleView) as TextView
        noteTV.setText(note)

        cancleView.setOnClickListener {
            dialog.dismiss()
        }
        var window = dialog.getWindow();
        window.setLayout(800, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }


    // delete dialog.......

    fun showDeleteMessageDialog(message: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = activity!!.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.confirmation_dialog, null)
        dialog.setContentView(view)

        val cancleBtn = dialog.findViewById(R.id.cancleText) as TextView
        val okBtn = dialog.findViewById(R.id.okBtn) as Button
        val dialogText = dialog.findViewById(R.id.dialogText) as TextView
        dialogText.setText(message)

        val font = Typeface.createFromAsset(activity!!.assets, resources.getString(R.string.popins_semi_bold))
        okBtn.setTypeface(font)

        okBtn.setOnClickListener {
            deletePill()
            dialog.dismiss()
        }

        cancleBtn.setOnClickListener {
            dialog.dismiss()
        }

        var window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }


}


