package com.e.digilife.Activity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.util.Log
import android.view.View
import android.widget.*
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.ArticleDetailsAdapter
import com.app.towntalk.Adapter.ArticleListAdapter
import com.app.towntalk.Adapter.InsuranceAdapter
import com.app.towntalk.Adapter.TicketReceiptAdapter
import com.e.digilife.Model.ArticleModel
import com.e.digilife.Model.PersonalModel
import com.e.digilife.R
import com.e.digilife.View.NonSwipeableViewPager
import com.e.digilife.View.Utils
import com.google.android.gms.vision.text.Line
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import java.util.ArrayList
import java.util.HashMap

class ArticleDetailsActivity : AppCompatActivity() {

    val TAG = "ArticleListActivity"
    val activity = this@ArticleDetailsActivity

    var bundle: Bundle? = null
    var client_id: String = ""
    var parentFolderId: String = ""

    lateinit var backView: ImageView
    lateinit var titleText: TextView
    lateinit var articleViewPager: NonSwipeableViewPager
    lateinit var previousView: LinearLayout
    lateinit var nextView: LinearLayout

    var adapter: ArticleDetailsAdapter? = null
    private var articleList = ArrayList<ArticleModel>()
    var selectedPosition: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_article_details_view)
        init()

        bundle = intent.extras

        if (bundle != null) {
            selectedPosition = bundle!!.getInt("position")
            if (!bundle!!.getSerializable("ArticleList").equals("")) {
                articleList = bundle!!.getSerializable("ArticleList") as ArrayList<ArticleModel>
            }
        }

        adapter = ArticleDetailsAdapter(activity, articleList, onShareClick)
        articleViewPager.adapter = adapter


        articleViewPager.setCurrentItem(selectedPosition)

        titleText.text = "Article Details"
    }

    fun init() {
        articleViewPager = findViewById(R.id.articleViewPager)
        titleText = findViewById(R.id.titleText)
        backView = findViewById(R.id.backView)
        previousView = findViewById(R.id.previousView)
        nextView = findViewById(R.id.nextView)

        backView.setOnClickListener(clickListener)
        previousView.setOnClickListener(clickListener)
        nextView.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish()
            }
            R.id.nextView -> {
                articleViewPager.setCurrentItem(getItem(+1), true)
            }
            R.id.previousView -> {
                articleViewPager.setCurrentItem(getItem(-1), true)
            }
        }
    }

    fun getItem(i: Int): Int {
        return articleViewPager.currentItem + i
    }

    var onShareClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int

            try {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.type = "text/plain"
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Digilife")
                var shareMessage = articleList.get(position).articleTitle
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                startActivity(Intent.createChooser(shareIntent, "choose one"))
            } catch (e: Exception) {
                //e.toString();
            }
        }
    }



}
