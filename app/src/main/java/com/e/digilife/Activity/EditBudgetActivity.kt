package com.e.digilife.Activity

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.W_BudgetRecyclerAdapter
import com.e.digilife.Model.WalletModel
import com.e.digilife.R
import com.e.digilife.View.InputValidation
import com.e.digilife.View.Utils
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import org.w3c.dom.Text
import java.text.DecimalFormat
import java.util.ArrayList
import java.util.HashMap

class EditBudgetActivity : AppCompatActivity() {

    val TAG = "AddTrascationActivity"
    val activity = this@EditBudgetActivity
    lateinit var inputValidation: InputValidation
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null

    var client_id: String = ""
    var currency: String = ""
    var currentYear: String = ""
    var currentMonth: String = ""
    var saveAmount: String = ""
    var totalAmount: String = ""
    var categoryID: String = ""
    var categoryName: String = ""
    var budgetAmount: String = ""

    lateinit var loaderLayout: LinearLayout
    lateinit var saveTextView: TextView
    lateinit var backView: ImageView
    lateinit var amountEdittext: EditText
    lateinit var saveAmountEditText: EditText
    lateinit var monthYearNameText: TextView

    lateinit var budgetRecyclerView: RecyclerView
    lateinit var categoryLayout: RelativeLayout
    lateinit var categoryNameText: TextView
    lateinit var budgetAmountEdittext: EditText
    lateinit var categoryAddext: TextView


    private var dataList = ArrayList<WalletModel>()
    var adapter: W_BudgetRecyclerAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_edit_budget)

        requestQueue = Volley.newRequestQueue(this)
        prefs = this.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        init()
        currency = prefs!!.getString(Utils.CURRENCY, "")
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }

        if (intent != null) {
            currentMonth = intent.getStringExtra("month")
            currentYear = intent.getStringExtra("year")
            saveAmount = intent.getStringExtra("save_amount")
            totalAmount = intent.getStringExtra("total_amount")
        }

        if (!totalAmount.equals("0")) {
            val format = DecimalFormat("#,###,###0.00")
            var newPrice = format.format(java.lang.Double.parseDouble(totalAmount))
           // newPrice = newPrice.format("%.2f", newPrice)
            amountEdittext.setText(newPrice)
        }

        if (!totalAmount.equals("0")) {
            saveAmountEditText.setText(saveAmount)
        }

        monthYearNameText.setText(currentMonth + "-" + currentYear)

        val linearLayoutManager = LinearLayoutManager(this)
        budgetRecyclerView.setLayoutManager(linearLayoutManager)
        budgetRecyclerView.setHasFixedSize(true)
        adapter = W_BudgetRecyclerAdapter(activity, dataList, onItemClick, onDeleteItemClick)
        budgetRecyclerView.adapter = adapter


        getBudgetCategoryList("")

    }

    fun init() {
        inputValidation = InputValidation(activity)

        loaderLayout = findViewById(R.id.loaderLayout)
        saveTextView = findViewById(R.id.saveTextView)
        backView = findViewById(R.id.backView)
        amountEdittext = findViewById(R.id.amountEdittext)
        saveAmountEditText = findViewById(R.id.saveAmountEditText)
        monthYearNameText = findViewById(R.id.monthYearNameText)

        budgetRecyclerView = findViewById(R.id.budgetRecyclerView)
        categoryLayout = findViewById(R.id.categoryLayout)
        categoryNameText = findViewById(R.id.categoryNameText)
        budgetAmountEdittext = findViewById(R.id.budgetAmountEdittext)
        categoryAddext = findViewById(R.id.categoryAddext)

        backView.setOnClickListener(clickListener)
        saveTextView.setOnClickListener(clickListener)
        categoryLayout.setOnClickListener(clickListener)
        categoryAddext.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish()
            }
            R.id.saveTextView -> {
                validation()
            }
            R.id.categoryLayout -> {
                val i = Intent(this@EditBudgetActivity, TrascationCategoryActivity::class.java)
                i.putExtra("type", "")
                startActivityForResult(i, Utils.CATEGORY)
            }
            R.id.categoryAddext -> {
                categoryValidation()
            }
        }
    }

    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            Log.e("position ::", position.toString())
            var id = dataList.get(position).categoryID
            var catName = dataList.get(position).trasCategoryName
            var catId = dataList.get(position).categoryID
            var amount = dataList.get(position).tranAmount
            showEditCategoryDialog(id, catName, catId, amount)
        }
    }

    var onDeleteItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            Log.e("position:::", position.toString())
            showBudgetDeleteDialog("Are you sure want to delete this transcation ?", position)
        }
    }

    //TODO: Get Budget Category................

    private fun getBudgetCategoryList(type: String) {
        if (type.equals("")) {
            loaderLayout.visibility = View.VISIBLE
        }

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.GET_BUDGET_CATEGORY,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        val dataArray = jsonObj.getJSONArray("data")
                        if (dataArray.length() > 0)
                            setBudgetCategoryList(dataArray)
                    } else {
                        dataList.clear()
                        adapter!!.notifyDataSetChanged()
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["month"] = currentMonth
                params["year"] = currentYear
                params["cat_id"] = "1"

                Log.e(" JSONData :", params.toString())
                return params
            }
        }

        Log.e(" API :", EndPoints.GET_BUDGET_CATEGORY)
        requestQueue.add(stringRequest)
    }

    fun setBudgetCategoryList(jsonArray: JSONArray) {
        dataList.clear()
        for (i in 0..(jsonArray.length() - 1)) {
            val jsonObj = jsonArray.getJSONObject(i)
            val dataModel = WalletModel()
            dataModel.categoryID = jsonObj.getString("id")
            dataModel.trasCategoryId = jsonObj.getString("category_id")
            dataModel.trasCategoryName = jsonObj.getString("cat_name")
            var amount = jsonObj.getString("cat_amount")
            val format = DecimalFormat("#,###,###0.00")
            var newPrice = format.format(java.lang.Double.parseDouble(amount))
            dataModel.tranAmount = newPrice
            dataModel.trasCategoryType = jsonObj.getString("category_type")
            dataModel.isTitle = ""
            dataModel.currency = currency

            dataList.add(dataModel)

        }
        adapter!!.notifyDataSetChanged()
    }

    //TODO: Add Income............

    private fun addIncome() {

        loaderLayout.visibility = View.VISIBLE
        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.ADD_INCOME,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        finish()
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["monthly_income"] = totalAmount
                params["save_amount"] = saveAmount
                params["month"] = currentYear + "-" + currentMonth

                Log.e(" JSONData :", params.toString())
                return params
            }
        }

        Log.e(" API :", EndPoints.ADD_INCOME)
        requestQueue.add(stringRequest)
    }


    //TODO: Add CAtegory............

    private fun addCategoryBudget() {

        loaderLayout.visibility = View.VISIBLE
        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.ADD_BUDGET_CATEGORY,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        budgetAmountEdittext.setText("")
                        categoryName = ""
                        categoryID = ""
                        categoryNameText.setText("Category")
                        getBudgetCategoryList("add")
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["category_id"] = categoryID
                params["cat_name"] = categoryName
                params["cat_amount"] = budgetAmount
                params["year"] = currentYear
                params["month"] = currentMonth

                Log.e(" JSONData :", params.toString())
                return params
            }
        }

        Log.e(" API :", EndPoints.ADD_BUDGET_CATEGORY)
        requestQueue.add(stringRequest)
    }

    //TODO: Edit Category Amount.............

    private fun editCategory(id: String, catName: String, catId: String, amount: String) {

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.EDIT_BUDGET_CATEGORY,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        hideKeyboard()

                        getBudgetCategoryList("edit")
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["id"] = id
                params["cat_id"] = catId
                params["cat_name"] = catName
                params["cat_amount"] = amount
                params["year"] = currentYear
                params["month"] = currentMonth
                Log.e(" JSONData :", params.toString())
                return params
            }
        }

        Log.e(" API :", EndPoints.EDIT_BUDGET_CATEGORY)
        requestQueue.add(stringRequest)
    }


    //TODO: Delete Category.............

    private fun deleteCategory(id: String) {

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.DELETE_BUDGET_CATEGORY,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        // dataList.removeAt(pos)
                        // adapterW!!.notifyDataSetChanged()
                        getBudgetCategoryList("delete")
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["id"] = id

                Log.e(" JSONData :", params.toString())
                return params
            }
        }

        Log.e(" API :", EndPoints.DELETE_BUDGET_CATEGORY)
        requestQueue.add(stringRequest)
    }


    fun showBudgetDeleteDialog(message: String, position: Int) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = activity!!.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.confirmation_dialog, null)
        dialog.setContentView(view)

        val cancleBtn = dialog.findViewById(R.id.cancleText) as TextView
        val okBtn = dialog.findViewById(R.id.okBtn) as Button
        val dialogText = dialog.findViewById(R.id.dialogText) as TextView
        dialogText.setText(message)

        val font = Typeface.createFromAsset(activity!!.assets, resources.getString(R.string.popins_semi_bold))
        okBtn.setTypeface(font)

        okBtn.setOnClickListener {
            deleteCategory(dataList.get(position).categoryID)
            dialog.dismiss()
        }

        cancleBtn.setOnClickListener {
            dialog.dismiss()
        }

        var window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }


    fun showEditCategoryDialog(id: String, cat_name: String, cat_id: String, amount: String) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = activity!!.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.create_folder_view, null)
        dialog.setContentView(view)

        val createText = dialog.findViewById(R.id.createText) as TextView
        val folderEditText = dialog.findViewById(R.id.folderNameEdittext) as EditText
        folderEditText.visibility = View.GONE
        val amountEdittext = dialog.findViewById(R.id.amountEdittext) as EditText
        amountEdittext.visibility = View.VISIBLE
        val doneBtn = dialog.findViewById(R.id.doneBtn) as Button
        amountEdittext.setHint("Enter Amount")
        createText.setText("Edit Category Amount")

        val font = Typeface.createFromAsset(activity.assets, resources.getString(R.string.popins_regular))
        amountEdittext.setTypeface(font)
        doneBtn.setTypeface(font)

        amountEdittext.setText(amount)

        doneBtn.setOnClickListener {
            if (amountEdittext.text.toString().equals("")) {
                Toast.makeText(activity, "Enter Amount", Toast.LENGTH_SHORT).show()
            } else {
                if (Utils.checkInternetConnection(activity)) {
                    var catAmount = amountEdittext.text.toString().trim()
                    editCategory(id, cat_name, cat_id, catAmount)
                } else {
                    Utils.showMessageDialog(
                        activity!!,
                        getString(R.string.app_name),
                        getString(R.string.check_internet)
                    )
                }


                dialog.dismiss()
            }
        }
        var window = dialog.getWindow();
        window.setLayout(700, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }


    private fun validation() {


        if (!inputValidation.isFieldEmpty(amountEdittext, getString(R.string.monthly_income_validation))) {
            return
        }
        if (!inputValidation.isFieldEmpty(saveAmountEditText, getString(R.string.save_amount_validation))) {
            return
        }

        totalAmount = amountEdittext.text.toString().trim()
        saveAmount = saveAmountEditText.text.toString().trim()

        if (Utils.checkInternetConnection(this@EditBudgetActivity)) {
            hideKeyboard()
            addIncome()

        } else {
            Utils.showMessageDialog(
                this@EditBudgetActivity,
                getString(R.string.app_name),
                getString(R.string.check_internet)
            )
        }
    }

    private fun categoryValidation() {

        if (!inputValidation.isFieldEmpty(budgetAmountEdittext, getString(R.string.amount_validation))) {
            return
        }
        if (!inputValidation.isFieldBlank(categoryName, getString(R.string.category_validation))) {
            return
        }

        budgetAmount = budgetAmountEdittext.text.toString().trim()

        if (Utils.checkInternetConnection(this@EditBudgetActivity)) {
            hideKeyboard()
            addCategoryBudget()

        } else {
            Utils.showMessageDialog(
                this@EditBudgetActivity,
                getString(R.string.app_name),
                getString(R.string.check_internet)
            )
        }
    }


    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && requestCode == Utils.CATEGORY) {
            var name = data!!.getStringExtra("cName")
            var id = data.getStringExtra("cId")
            categoryID = id
            categoryNameText.text = name
            categoryName = name
        }
    }

    fun hideKeyboard() {
        val view = this.getCurrentFocus()
        if (view != null) {
            val inputManager: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(view.windowToken, InputMethodManager.SHOW_FORCED)
        }
    }


}
