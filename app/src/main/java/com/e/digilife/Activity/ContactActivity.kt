package com.e.digilife.Activity

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import com.e.digilife.Fragment.Contact_Fragment
import com.e.digilife.R
import com.e.digilife.View.Utils

class ContactActivity : AppCompatActivity() {

    val TAG = "ContactActivity"
    val activity = this@ContactActivity

    lateinit var contactFramelayout: FrameLayout
    lateinit var tabLayout: TabLayout
    lateinit var backView: ImageView
    lateinit var addContactView: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact)
        init()

        var contactFragment = Contact_Fragment()
        replaceFragment(contactFragment)
        Utils.contactIndex = 0
    }

    fun init() {
        contactFramelayout = findViewById(R.id.contactFramelayout)
        tabLayout = findViewById(R.id.tabLayout)
        backView = findViewById(R.id.backView)
        addContactView = findViewById(R.id.addContactView)

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                var position = tab.position
                if (position == 0) {
                    var contactFragment = Contact_Fragment()
                    replaceFragment(contactFragment)
                    Utils.contactIndex = 0
                } else if (position == 1) {
                    var contactFragment = Contact_Fragment()
                    replaceFragment(contactFragment)
                    Utils.contactIndex = 1
                } else if (position == 2) {
                    var contactFragment = Contact_Fragment()
                    replaceFragment(contactFragment)
                    Utils.contactIndex = 2
                }
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

        backView.setOnClickListener(clickListener)
        addContactView.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish()
            }
            R.id.addContactView -> {
                var intent = Intent(activity, AddContactActivity::class.java)
                intent.putExtra(Utils.EDIT_CONTACT, "0")
                intent.putExtra(Utils.CONTACT_OBJ, "")
                startActivity(intent)
            }
        }
    }

    fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.contactFramelayout, fragment).commit()
    }
}