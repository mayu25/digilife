package com.e.digilife.Activity

import android.os.Bundle
import android.support.design.widget.BottomNavigationView
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import com.e.digilife.Fragment.CalenderFragment
import com.e.digilife.Fragment.DashboardFragment
import com.e.digilife.Fragment.SettingFragment
import com.e.digilife.Fragment.TaskFragment
import com.e.digilife.R
import com.e.digilife.View.Utils


class TaskMainActivity : AppCompatActivity() {

    //    private lateinit var toolbar_title: TextView
    var fragment: Fragment? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.activity_task_main)
        val navView: BottomNavigationView = findViewById(R.id.nav_view)


        val taskFragment = TaskFragment()
        replaceFragment(taskFragment)
        Utils.navItemIndex = 0
        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
    }

    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_task -> {
                //  toolbar_title.setText(R.string.task)
                val taskFragment = TaskFragment()
                replaceFragment(taskFragment)
                Utils.navItemIndex = 0


                return@OnNavigationItemSelectedListener true
            }
            // R.string.title_dashboard
            R.id.navigation_calender -> {
                // toolbar_title.setText(R.string.calender)
                val taskFragment = CalenderFragment()
                replaceFragment(taskFragment)
                Utils.navItemIndex = 1

                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_setting -> {
                //  toolbar_title.setText(R.string.setting)
                val taskFragment = SettingFragment()
                replaceFragment(taskFragment)
                Utils.navItemIndex = 2

                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    fun replaceFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.fragment_container, fragment).commit()
    }

}
