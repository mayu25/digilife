package com.e.digilife.Activity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.ImageGridviewAdapter
import com.app.towntalk.Adapter.NotificationAdapter
import com.app.towntalk.Adapter.SearchGridviewAdapter
import com.e.digilife.Model.DataModel
import com.e.digilife.Model.NotificationModel
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import org.w3c.dom.Text
import java.util.HashMap

class NotificationActivity : AppCompatActivity() {

    val TAG = "SearchActivity"
    val activity = this@NotificationActivity
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null

    lateinit var cancelText: TextView
    lateinit var loaderLayout: LinearLayout
    lateinit var listview: ListView
    lateinit var clearAllText: TextView

    var client_id: String = ""
    private var NotificationList = ArrayList<NotificationModel>()
    var adapter: NotificationAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_notification)

        prefs = activity.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }
        requestQueue = Volley.newRequestQueue(activity)
        init()


        adapter = NotificationAdapter(activity, NotificationList, onItemClick)
        listview.adapter = adapter

        getNotification()
    }

    fun init() {
        listview = findViewById(R.id.listview)
        loaderLayout = findViewById(R.id.loaderLayout)
        cancelText = findViewById(R.id.cancelText)
        clearAllText = findViewById(R.id.clearAllText)

        cancelText.setOnClickListener(clickListener)
        clearAllText.setOnClickListener(clickListener)

    }

    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int

        }
    }


    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.cancelText -> {
                finish()
            }
            R.id.clearAllText -> {
            }
        }
    }


    private fun getNotification() {

        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.GET_NOTIFICATION,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        val dataArray = jsonObj.getJSONArray("data")
                        if (dataArray.length() > 0) {
                            setNotificationValue(dataArray)
                        }

                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                    Log.d("Error.Response", error.toString())
                }
            }
        ) {

            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                Log.e(" JSONData :", params.toString())
                return params

            }
        }

        Log.e(" API :", EndPoints.GET_NOTIFICATION)
        requestQueue.add(stringRequest)
    }

    fun setNotificationValue(jsonArray: JSONArray) {
        NotificationList.clear()
        for (i in 0..(jsonArray.length() - 1)) {
            val jsonObj = jsonArray.getJSONObject(i)
            val notiArray = jsonObj.getJSONArray("data")

            for (j in 0..(notiArray.length() - 1)) {
                val notiObj = notiArray.getJSONObject(j)
                val dataModel = NotificationModel()

                if (j == 0) {
                    dataModel.notificationType = notiObj.getString("type")
                } else {
                    dataModel.notificationType = ""
                }
                dataModel.notificationID = notiObj.getString("id")
                dataModel.notificationTitle = notiObj.getString("title")
                dataModel.notificationTime = notiObj.getString("time")


                NotificationList.add(dataModel)
            }
        }
        adapter!!.notifyDataSetChanged()
    }
}
