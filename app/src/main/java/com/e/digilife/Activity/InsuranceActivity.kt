package com.e.digilife.Activity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.InsuranceAdapter
import com.app.towntalk.Adapter.TicketReceiptAdapter
import com.e.digilife.Model.PersonalModel
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import java.util.ArrayList
import java.util.HashMap

class InsuranceActivity : AppCompatActivity() {

    val TAG = "InsuranceActivity"
    val activity = this@InsuranceActivity
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null

    var bundle: Bundle? = null
    var client_id: String = ""
    var parentFolderId: String = ""

    lateinit var backView: ImageView
    lateinit var titleText: TextView
    lateinit var addView: ImageView
    lateinit var loaderLayout: LinearLayout
    lateinit var listview: ListView
    lateinit var notificationView: ImageView
    lateinit var searchView: ImageView


    var adapter: InsuranceAdapter? = null
    private var dataList = ArrayList<PersonalModel>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_insurance)
        requestQueue = Volley.newRequestQueue(this)
        prefs = this.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)


        bundle = intent.extras
        if (bundle != null) {
            parentFolderId = bundle!!.getString(Utils.PARENT_FOLDER_ID)
        }

        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }

        init()
        adapter = InsuranceAdapter(activity, dataList, onItemClick)
        listview.adapter = adapter

        titleText.text = "Insurance Policies"
    }


    override fun onResume() {
        super.onResume()
        if (Utils.checkInternetConnection(activity))
            getInsuranceList()
        else
            Utils.showMessageDialog(activity, getString(R.string.app_name), getString(R.string.check_internet))
    }

    fun init() {
        listview = findViewById(R.id.listview)
        loaderLayout = findViewById(R.id.loaderLayout)
        titleText = findViewById(R.id.titleText)
        backView = findViewById(R.id.backView)
        addView = findViewById(R.id.plusView)
        notificationView = findViewById(R.id.notificationView)
        searchView = findViewById(R.id.searchView)

        backView.setOnClickListener(clickListener)
        addView.setOnClickListener(clickListener)
        notificationView.setOnClickListener(clickListener)
        searchView.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish()
            }
            R.id.plusView -> {
                val intent = Intent(activity, InsuranceAddingActivity::class.java)
                intent.putExtra(Utils.EDIT_INSURANCE, "0")
                intent.putExtra(Utils.INSURANCE_OBJ, "")
                startActivity(intent)
            }
            R.id.searchView -> {
                val intent = Intent(activity, SearchActivity::class.java)
                startActivity(intent)
            }
            R.id.notificationView -> {
                val intent = Intent(activity, NotificationActivity::class.java)
                startActivity(intent)
            }
        }
    }

    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int

            val intent = Intent(activity, InsuranceAddingActivity::class.java)
            intent.putExtra(Utils.EDIT_INSURANCE, "1")
            intent.putExtra(Utils.INSURANCE_OBJ, dataList.get(position))
            startActivity(intent)
        }
    }


    private fun getInsuranceList() {

        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.GET_INSURANCE,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        val dataArray = jsonObj.getJSONArray("data")
                        if (dataArray.length() > 0) {
                            setInsurance(dataArray)
                        }

                    } else {
                        dataList.clear()
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                        adapter!!.notifyDataSetChanged()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    Log.d("Error.Response", error.toString())
                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                Log.e(" JSONData :", params.toString())
                return params
            }
        }
        Log.e(" API :", EndPoints.GET_INSURANCE)
        requestQueue.add(stringRequest)
    }


    fun setInsurance(jsonArray: JSONArray) {
        dataList.clear()
        for (i in 0..(jsonArray.length() - 1)) {
            val jsonObj = jsonArray.getJSONObject(i)
            val dataModel = PersonalModel()
            dataModel.i_Id = jsonObj.getString("id")
            dataModel.i_StartDate = jsonObj.getString("start_date")
            dataModel.i_EndDate = jsonObj.getString("end_date")
            dataModel.i_PolicyNo = jsonObj.getString("policy_no")
            dataModel.i_PolicyName = jsonObj.getString("policy_company_name")
            dataModel.i_Amount = jsonObj.getString("amount")
            dataModel.i_Receipt = jsonObj.getString("receipt")
            dataModel.i_Attachement = jsonObj.getString("attachment")
            dataModel.i_Days = jsonObj.getString("days")
            dataModel.i_Reminder = jsonObj.getString("reminder")
            dataList.add(dataModel)
        }
        adapter!!.notifyDataSetChanged()
    }


}
