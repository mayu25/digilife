package com.e.digilife.Activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import com.android.volley.*
import com.android.volley.toolbox.Volley
import com.e.digilife.R
import com.e.digilife.View.TextViewPlus
import com.e.digilife.View.Utils
import com.e.digilife.View.VolleyMultipartRequest
import com.shurlock.View.EndPoints
import org.json.JSONObject
import java.io.ByteArrayOutputStream

class ImageUploadActivity : AppCompatActivity() {

    val TAG = "ImageUploadActivity"
    val activity = this@ImageUploadActivity
    var prefs: SharedPreferences? = null
    internal lateinit var requestQueue: RequestQueue

    lateinit var uploadText: TextViewPlus
    lateinit var imageView: ImageView
    lateinit var backView: ImageView
    lateinit var loaderLayout: LinearLayout

    var client_id: String = ""
    var parentFolderId: String = ""
    var Folder_id: String = ""
    var imagePath: String = ""

    var bundle: Bundle? = null

    lateinit var bitmap: Bitmap

    @SuppressLint("WrongThread")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_image_upload)

        prefs = this.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        requestQueue = Volley.newRequestQueue(this)
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")

        }
        init()
        bundle = intent.extras
        if (bundle != null) {
            Folder_id = bundle!!.getString("Folder_id")
            imagePath = bundle!!.getString("imagePath")
            parentFolderId = bundle!!.getString("parentFolderId")
            Log.e("imagePath ::: ", imagePath)
            val imgUri = Uri.parse(imagePath)

            if (!imagePath.equals("")) {
                imageView.setImageURI(imgUri)

                bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, imgUri)
                val byteArrayOutputStream = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
            }
        }
    }

    fun init() {
        loaderLayout = findViewById(R.id.loaderLayout)
        uploadText = findViewById(R.id.uploadText)
        imageView = findViewById(R.id.imageView)
        backView = findViewById(R.id.backView)

        uploadText.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.uploadText -> {
                if (Utils.checkInternetConnection(activity)) {
                    uploadImage()

                    //ImageUploadTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR)
                } else {
                    Utils.showMessageDialog(
                        activity,
                        getString(R.string.app_name),
                        getString(R.string.check_internet)
                    )
                }
            }
            R.id.backView -> {
                finish()
            }
        }
    }

    fun uploadImage() {
        loaderLayout.visibility = View.VISIBLE
        val volleyMultipartRequest = object : VolleyMultipartRequest(
            Request.Method.POST, EndPoints.FILE_UPLOAD,
            Response.Listener { response ->
                loaderLayout.visibility = View.GONE
                Log.d("response", String(response.data))
                var jsonObj = JSONObject(String(response.data))
                val status = jsonObj.getString("status")
                requestQueue.getCache().clear()
                if (status.equals("1")) {
                    finish()
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                }

            },
            Response.ErrorListener { error ->
                Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT).show()
            }) {


            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("client_id", client_id)
                params.put("parent_folder_id", parentFolderId)
                params.put("child_folder_id", Folder_id)
                Log.e(TAG, " JSONData :" + params.toString())
                return params

            }

            override fun getByteData(): Map<String, DataPart> {

                val params = HashMap<String, DataPart>()
                val imagename = System.currentTimeMillis()
                params["file"] = DataPart("$imagename.png", getFileDataFromDrawable(bitmap))
                Log.e(TAG," Image :"+params.toString())
                return params

            }
        }

        volleyMultipartRequest.retryPolicy = DefaultRetryPolicy(
            0,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(volleyMultipartRequest);
    }


    fun getFileDataFromDrawable(bitmap: Bitmap): ByteArray {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream)
        return byteArrayOutputStream.toByteArray()
    }
}
