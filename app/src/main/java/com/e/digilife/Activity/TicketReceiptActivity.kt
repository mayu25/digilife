package com.e.digilife.Activity

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Typeface
import android.media.Image
import android.os.Bundle
import android.os.StrictMode
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.*
import android.widget.*
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.CustomFolderAdapter
import com.app.towntalk.Adapter.TicketReceiptAdapter
import com.e.digilife.Model.CustomFolderModel
import com.e.digilife.Model.PersonalModel
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import java.util.*

class TicketReceiptActivity : AppCompatActivity() {

    val TAG = "TicketReceiptActivity"
    val activity = this@TicketReceiptActivity
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null

    lateinit var backView: ImageView
    lateinit var titleText: TextView
    lateinit var addFolderView: ImageView
    lateinit var loaderLayout: LinearLayout
    lateinit var listview: ListView
    lateinit var notificationView: ImageView
    lateinit var searchView: ImageView

    var client_id: String = ""
    var ticketFolderId: String = ""
    var Title: String = ""
    var docs_id: String = ""

    var adapter: TicketReceiptAdapter? = null
    private var dataList = ArrayList<PersonalModel>()

    var bundle: Bundle? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ticket_receipt)

        requestQueue = Volley.newRequestQueue(this)
        prefs = this.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            val builder = StrictMode.VmPolicy.Builder()
            StrictMode.setVmPolicy(builder.build())
        }

        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
            docs_id = loginObj.getString("Docs_id")
        }

        bundle = intent.extras
        if (bundle != null) {
            ticketFolderId = bundle!!.getString(Utils.PARENT_FOLDER_ID)
            Title = bundle!!.getString(Utils.TITLE)
        }

        init()

        adapter = TicketReceiptAdapter(activity, dataList, onItemClick)
        listview.adapter = adapter

        if (Utils.checkInternetConnection(activity))
            getTicketFolderList("")
        else
            Utils.showMessageDialog(activity, getString(R.string.app_name), getString(R.string.check_internet))


        titleText.text = Title
    }

    fun init() {
        listview = findViewById(R.id.listview)
        loaderLayout = findViewById(R.id.loaderLayout)
        titleText = findViewById(R.id.titleText)
        backView = findViewById(R.id.backView)
        addFolderView = findViewById(R.id.plusView)
        notificationView = findViewById(R.id.notificationView)
        searchView = findViewById(R.id.searchView)

        backView.setOnClickListener(clickListener)
        addFolderView.setOnClickListener(clickListener)
        notificationView.setOnClickListener(clickListener)
        searchView.setOnClickListener(clickListener)
    }

    override fun onResume() {
        super.onResume()
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish()
            }
            R.id.plusView -> {
                showAddFolderDialog()
            }
            R.id.searchView -> {
                val intent = Intent(activity, SearchActivity::class.java)
                startActivity(intent)
            }
            R.id.notificationView -> {
                val intent = Intent(activity, NotificationActivity::class.java)
                startActivity(intent)
            }
        }
    }

    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            val intent = Intent(activity, FileListActivity::class.java)
            val bundle = Bundle()
            bundle.putString(Utils.PARENT_FOLDER_ID, ticketFolderId)
            bundle.putString(Utils.FOLDER_ID, dataList.get(position).personalId)
            bundle.putString(Utils.TITLE, dataList.get(position).personalFolderName)
            intent.putExtras(bundle)
            startActivity(intent)
        }
    }

    private fun getTicketFolderList(type: String) {

        if (type.equals("")) {
            loaderLayout.visibility = View.VISIBLE
        }

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.GET_FOLDER_LIST,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        val dataArray = jsonObj.getJSONArray("data")
                        if (dataArray.length() > 0) {
                            setFolder(dataArray)
                        }

                    } else {
                        dataList.clear()
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                        adapter!!.notifyDataSetChanged()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    Log.d("Error.Response", error.toString())
                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["parent_folder_id"] = ticketFolderId
                Log.e(" JSONData :", params.toString())
                return params
            }
        }
        Log.e(" API :", EndPoints.GET_FOLDER_LIST)
        requestQueue.add(stringRequest)
    }

    fun setFolder(jsonArray: JSONArray) {
        dataList.clear()
        for (i in 0..(jsonArray.length() - 1)) {
            val jsonObj = jsonArray.getJSONObject(i)
            val dataModel = PersonalModel()
            dataModel.personalId = jsonObj.getString("id")
            dataModel.personalFolderName = jsonObj.getString("folder_name")
            dataModel.personalImgPath = jsonObj.getString("image")
            dataModel.description = jsonObj.getString("description")
            dataList.add(dataModel)
        }
        adapter!!.notifyDataSetChanged()
    }

    fun showAddFolderDialog() {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = activity.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.create_folder_view, null)
        dialog.setContentView(view)

        val createText = dialog.findViewById(R.id.createText) as TextView
        val folderNameEdittext = dialog.findViewById(R.id.folderNameEdittext) as EditText
        val doneBtn = dialog.findViewById(R.id.doneBtn) as Button
        folderNameEdittext.setHint("Enter Category Name")
        createText.setText("Create Custom Category")

        val font = Typeface.createFromAsset(activity.assets, resources.getString(R.string.popins_regular))
        folderNameEdittext.setTypeface(font)
        doneBtn.setTypeface(font)

        doneBtn.setOnClickListener {
            if (folderNameEdittext.text.toString().equals("")) {
                Toast.makeText(activity, "Enter Folder Name", Toast.LENGTH_SHORT).show()
            } else {
                if (Utils.checkInternetConnection(activity))
                    addFolder(folderNameEdittext.text.toString())
                else
                    Utils.showMessageDialog(
                        activity,
                        getString(R.string.app_name),
                        getString(R.string.check_internet)
                    )

                dialog.dismiss()
            }
        }
        var window = dialog.getWindow();
        window.setLayout(700, WindowManager.LayoutParams.WRAP_CONTENT)
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }

    private fun addFolder(folderName: String) {

        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.ADD_FOLDER,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        getTicketFolderList("add")
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    } else {
                        loaderLayout.visibility = View.GONE
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["parent_folder_id"] = docs_id
                params["child_folder_id"] = ticketFolderId
                params["folder_name"] = folderName
                Log.e(" JSONData :", params.toString())
                return params

            }
        }
        Log.e(" API :", EndPoints.ADD_FOLDER)
        requestQueue.add(stringRequest)
    }
}
