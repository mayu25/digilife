package com.e.digilife.Activity

import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.e.digilife.R
import com.e.digilife.View.InputValidation
import com.e.digilife.View.Utils
import com.shurlock.View.EndPoints
import org.json.JSONObject
import java.util.*

class ChangePasswordActivity : AppCompatActivity() {

    val TAG = "ChangePasswordActivity"
    val activity = this@ChangePasswordActivity
    lateinit var inputValidation: InputValidation
    internal lateinit var requestQueue: RequestQueue

    lateinit var loaderLayout: LinearLayout
    lateinit var backView: ImageView
    lateinit var newPWEditText: EditText
    lateinit var confirmPWEditText: EditText
    lateinit var submitBtn: Button
    var prefs: SharedPreferences? = null

    var client_id: String = ""
    var oldPW: String = ""
    var newPW: String = ""
    var phone: String = ""

    var bundle: Bundle? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        prefs = this.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        requestQueue = Volley.newRequestQueue(this)
        init()

        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
            oldPW = loginObj.getString("password")
        }

        bundle = intent.extras
        if (bundle != null) {
            phone = bundle!!.getString("phone")
        }
    }

    fun init() {
        inputValidation = InputValidation(activity)
        loaderLayout = findViewById(R.id.loaderLayout)
        backView = findViewById(R.id.backView)
        newPWEditText = findViewById(R.id.newPWEditText)
        confirmPWEditText = findViewById(R.id.confirmPWEditText)
        submitBtn = findViewById(R.id.submitBtn)

        backView.setOnClickListener(clickListener)
        submitBtn.setOnClickListener(clickListener)

        val font = Typeface.createFromAsset(assets, resources.getString(R.string.popins_regular))
        newPWEditText.setTypeface(font)
        confirmPWEditText.setTypeface(font)
        submitBtn.setTypeface(font)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish()
            }
            R.id.submitBtn -> {
                validation()
            }
        }
    }

    private fun changePassword() {

        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.OTP_CHANGE_PASSWORD,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                        val intent = Intent(applicationContext, LoginActivity::class.java)
                        startActivity(intent)
                        finish()

                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    // error
                    // Log.d("Error.Response", ${error.message})
                }
            }
        ) {


            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()

                params["phone"] = phone
                params["password"] = newPW

                return params

                Log.e(" JSONData :", params.toString())
            }
        }
        val socketTimeout = 10000//30 seconds - change to what you want
        val policy = DefaultRetryPolicy(
            socketTimeout,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        stringRequest.setRetryPolicy(policy)
        Log.e(" API :", EndPoints.OTP_CHANGE_PASSWORD)
        requestQueue.add(stringRequest)
    }


    private fun validation() {


        if (!inputValidation.isInputPassword(newPWEditText, getString(R.string.new_password_validation))) {
            return
        }

        if (newPWEditText.text.toString().length < 8 && !inputValidation.isValidPassword(newPWEditText.text.toString())) {
            Toast.makeText(activity, getString(R.string.valid_password), Toast.LENGTH_SHORT).show()
            return
        }



        if (!inputValidation.isInputPassword(confirmPWEditText, getString(R.string.confirm_password_validation))) {
            return
        }

        if (!newPWEditText.getText().toString().trim().equals(confirmPWEditText.getText().toString().trim())) {
            confirmPWEditText.requestFocus()
            Toast.makeText(activity, getString(R.string.confirm_password_not_match), Toast.LENGTH_SHORT).show()
            return

        }

        newPW = newPWEditText.text.toString()

        if (Utils.checkInternetConnection(activity)) {
            changePassword()
        } else {
            Utils.showMessageDialog(
                activity,
                getString(R.string.app_name),
                getString(R.string.check_internet)
            )
        }


    }


    fun fieldValidation(): Boolean {
        var flag = true
        if (!inputValidation.validateString(newPWEditText.getText().toString().trim())) {
            flag = false
            newPWEditText.requestFocus()
            Toast.makeText(activity, getString(R.string.new_password_validation), Toast.LENGTH_SHORT).show()

        } else if (newPWEditText.getText().toString().trim().length < 6) {
            flag = false
            newPWEditText.requestFocus()
            Toast.makeText(activity, getString(R.string.check_password_length), Toast.LENGTH_SHORT).show()

        } else if (!inputValidation.validateString(confirmPWEditText.getText().toString().trim())) {
            flag = false
            confirmPWEditText.requestFocus()
            Toast.makeText(activity, getString(R.string.confirm_password_validation), Toast.LENGTH_SHORT).show()

        } else if (!newPWEditText.getText().toString().trim().equals(confirmPWEditText.getText().toString().trim())) {
            flag = false
            confirmPWEditText.requestFocus()
            Toast.makeText(activity, getString(R.string.confirm_password_not_match), Toast.LENGTH_SHORT).show()

        }
        return flag
    }


}
