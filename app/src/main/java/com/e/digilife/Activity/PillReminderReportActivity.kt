package com.e.digilife.Activity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.PillReminderRecyclerAdapter
import com.e.digilife.Model.PillReminderModel
import com.e.digilife.R
import com.e.digilife.View.InputValidation
import com.e.digilife.View.Utils
import com.e.digilife.View.VolleyMultipartRequest
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import com.e.digilife.MainActivity
import android.widget.ArrayAdapter


class PillReminderReportActivity : AppCompatActivity() {

    val TAG = "PillReminderReportActivity"
    val activity = this@PillReminderReportActivity
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null
    lateinit var inputValidation: InputValidation

    private var medicineList = ArrayList<PillReminderModel>()
    private var reportList = ArrayList<PillReminderModel>()
    var adapter: PillReminderRecyclerAdapter? = null

    lateinit var reportSpinner: Spinner
    lateinit var reportRecyclerView: RecyclerView
    lateinit var backView: ImageView
    lateinit var loaderLayout: LinearLayout
    lateinit var fromeDateLayout: RelativeLayout
    lateinit var toDateLayout: RelativeLayout
    lateinit var fromDateText: TextView
    lateinit var toDateText: TextView
    lateinit var submitView: TextView

    var client_id: String = ""
    var fromDate: String = ""
    var toDate: String = ""
    var medicineName: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pill_reminder_report)
        requestQueue = Volley.newRequestQueue(this)
        prefs = this.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)

        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }
        init()

        val layoutManager2 = LinearLayoutManager(this)
        reportRecyclerView.setLayoutManager(layoutManager2)
        reportRecyclerView.setHasFixedSize(true)
        adapter = PillReminderRecyclerAdapter(activity, reportList, onItemClick)
        reportRecyclerView.adapter = adapter

        getMedicineList()
    }

    fun init() {
        inputValidation = InputValidation(activity)
        loaderLayout = findViewById(R.id.loaderLayout)
        backView = findViewById(R.id.backView)
        reportSpinner = findViewById(R.id.reportSpinner)
        reportRecyclerView = findViewById(R.id.reportRecyclerView)
        submitView = findViewById(R.id.submitView)
        fromeDateLayout = findViewById(R.id.fromeDateLayout)
        toDateLayout = findViewById(R.id.toDateLayout)
        fromDateText = findViewById(R.id.fromDateText)
        toDateText = findViewById(R.id.toDateText)

        fromeDateLayout.setOnClickListener(clickListener)
        toDateLayout.setOnClickListener(clickListener)
        backView.setOnClickListener(clickListener)
        submitView.setOnClickListener(clickListener)

        reportSpinner.setOnItemSelectedListener(object : AdapterView.OnItemSelectedListener {

            override fun onItemSelected(adapterView: AdapterView<*>, arg1: View, pos: Int, arg3: Long) {
                medicineName = adapterView.getItemAtPosition(pos).toString().trim();
            }

            override fun onNothingSelected(arg0: AdapterView<*>) {
                // TODO Auto-generated method stub
            }

        })
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish()
            }
            R.id.fromeDateLayout -> {
                openDatePicker("start")
            }
            R.id.toDateLayout -> {
                openDatePicker("end")
            }
            R.id.submitView -> {
                validation()
            }
        }
    }

    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            val intent = Intent(applicationContext, AddPillActivity::class.java)
            intent.putExtra(Utils.EDIT_PILLS, "1")
            intent.putExtra(Utils.PILL_OBJ, reportList.get(position))
            startActivity(intent)
        }
    }

    //TODO: Medicine name list.....

    fun getMedicineList() {
        loaderLayout.visibility = View.VISIBLE
        val volleyMultipartRequest = object : VolleyMultipartRequest(
            Request.Method.POST, EndPoints.GET_MEDICINE_LIST,
            Response.Listener { response ->
                loaderLayout.visibility = View.GONE
                requestQueue.getCache().clear()
                Log.d("response", String(response.data))
                var jsonObj = JSONObject(String(response.data))

                var status = jsonObj.getString("status")
                if (status.equals("1")) {
                    var dataArray = jsonObj.getJSONArray("data")
                    setMedicineList(dataArray)

                } else {
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT).show()
            }) {


            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("client_id", client_id)

                Log.e(" JSONData :", params.toString())
                return params

            }
        }
        volleyMultipartRequest.retryPolicy = DefaultRetryPolicy(
            0,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(volleyMultipartRequest);
    }

    internal var medicineNameList = ArrayList<String>()

    fun setMedicineList(jsonArray: JSONArray) {
        medicineList.clear()
        medicineNameList.add("Product Name (All)")
        for (i in 0..(jsonArray.length() - 1)) {
            var jObj = jsonArray.getJSONObject(i)
            var reminderModel = PillReminderModel()
            reminderModel.pillId = jObj.getString("pill_id")
            var medicineName = jObj.getString("medicine_name")
            reminderModel.pillName = medicineName
            medicineNameList.add(medicineName)
            medicineList.add(reminderModel)
        }

        reportSpinner.setAdapter(ArrayAdapter<String>(
                this@PillReminderReportActivity,
                android.R.layout.simple_spinner_dropdown_item,
                medicineNameList))
    }


    //TODO: Report List.............

    fun getReportList() {
        loaderLayout.visibility = View.VISIBLE
        val volleyMultipartRequest = object : VolleyMultipartRequest(
            Request.Method.POST, EndPoints.GET_REMINDER_REPORT,
            Response.Listener { response ->
                loaderLayout.visibility = View.GONE
                requestQueue.getCache().clear()
                Log.d("response", String(response.data))
                var jsonObj = JSONObject(String(response.data))

                var status = jsonObj.getString("status")
                if (status.equals("1")) {
                    var dataArray = jsonObj.getJSONArray("data")
                    setReportList(dataArray)

                } else {
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT).show()
            }) {


            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("client_id", client_id)
                params.put("medicine_name", medicineName)
                params.put("from_date", fromDate)
                params.put("to_date", toDate)

                Log.e(" JSONData :", params.toString())
                return params

            }
        }
        volleyMultipartRequest.retryPolicy = DefaultRetryPolicy(
            0,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(volleyMultipartRequest);
    }

    fun setReportList(jsonArray: JSONArray) {
        reportList.clear()
        for (i in 0..(jsonArray.length() - 1)) {
            var jObj = jsonArray.getJSONObject(i)
            var reminderModel = PillReminderModel()
            reminderModel.pillId = jObj.getString("pill_id")
            reminderModel.pillName = jObj.getString("medicine_name")
            reminderModel.fromDate = jObj.getString("from_date")
            reminderModel.toDate = jObj.getString("to_date")
            reminderModel.pillReminderType = jObj.getString("pill_reminder_type")
            reminderModel.pillDays = jObj.getString("pill_days")
            reminderModel.takePills = jObj.getString("take_medicine")
            reminderModel.dailyTime = jObj.getString("daily_time")
            reminderModel.pillRemiderTime = jObj.getString("time")
            reminderModel.doctoreName = jObj.getString("dr_name")
            reminderModel.rxNumber = jObj.getString("rx_number")
            reminderModel.pillStatus = jObj.getString("pill_status")
            reminderModel.note = jObj.getString("description")

            reportList.add(reminderModel)
        }

        adapter!!.notifyDataSetChanged()
    }

    //TODO: Date Convertor............

    fun openDatePicker(type: String) {
        SingleDateAndTimePickerDialog.Builder(activity)
            .bottomSheet()
            .displayMinutes(false)
            .displayHours(false)
            .displayDays(false)
            .displayMonth(true)
            .displayYears(true)
            .displayDaysOfMonth(true)
            .displayListener(object : SingleDateAndTimePickerDialog.DisplayListener {
                override fun onDisplayed(picker: SingleDateAndTimePicker) {
                    //retrieve the SingleDateAndTimePicker
                }
            })
            .title("Select Date")
            .titleTextColor(getResources().getColor(R.color.blackColor))
            .backgroundColor(getResources().getColor(R.color.whiteColor))
            .mainColor(getResources().getColor(R.color.blackColor))
            .curved()
            .listener(object : SingleDateAndTimePickerDialog.Listener {
                override fun onDateSelected(date: Date) {
                    var convertDate = dateConvertor(date)
                    if (type.equals("start")) {
                        fromDate = convertDate
                        fromDateText.setText(convertDate)
                    } else {
                        toDate = convertDate
                        toDateText.setText(convertDate)
                    }
                }
            }).display()
    }

    fun dateConvertor(date: Date): String {
        val outputFormat = SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH)
        outputFormat.timeZone = TimeZone.getTimeZone("UTC")
        val output = outputFormat.format(date)
        println(output)
        return output
    }

    // fun APIServerDate(date: Date): String {
    //     val outputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
    //     outputFormat.timeZone = TimeZone.getTimeZone("UTC")
    //     val output = outputFormat.format(date)
    //     println(output)
    //     return output
    // }

    fun validation() {
        // if (!inputValidation.isFieldBlank(medicineName, getString(R.string.medicine_select_name_validation))) {
        //     return
        // }

        if (Utils.checkInternetConnection(this@PillReminderReportActivity)) {
            getReportList()

        } else {
            Utils.showMessageDialog(
                this@PillReminderReportActivity,
                getString(R.string.app_name),
                getString(R.string.check_internet)
            )
        }
    }

    fun hideKeyboard() {
        val view = this.getCurrentFocus()
        if (view != null) {
            val inputManager: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(view.windowToken, InputMethodManager.SHOW_FORCED)
        }
    }
}
