package com.e.digilife.Activity

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.telephony.TelephonyManager
import android.util.Log
import android.widget.Toast
import com.e.digilife.R
import java.io.FileNotFoundException
import java.io.IOException


class ContactListActivity : AppCompatActivity() {

    var TAG = "ContactListActivity"
    var activity: Activity = this@ContactListActivity
    var wantPermission = Manifest.permission.READ_CONTACTS
    private val PERMISSION_REQUEST_CODE = 1
    var crContacts: Cursor? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_list)

        if (!checkPermission(wantPermission)) {
            requestPermission(wantPermission);
        } else {
            readContacts();
        }
    }

    private fun readContacts() {
        var photo: Bitmap
        var id: String
        var name: String
        var photoUri: String?
        val order = ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME + " ASC"
        val cr = contentResolver
        crContacts = contentResolver.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, order)

        while (crContacts!!.moveToNext()) {
            id = crContacts!!.getString(crContacts!!.getColumnIndex(ContactsContract.Contacts._ID))
            name =
                crContacts!!.getString(crContacts!!.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
            photoUri =
                crContacts!!.getString(crContacts!!.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI))

            if (Integer.parseInt(crContacts!!.getString(crContacts!!.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
                val crPhones = cr.query(
                    ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
                    ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", arrayOf(id), null
                )

                Log.d(TAG, "NAME: $name")

                while (crPhones!!.moveToNext()) {
                    val phone = crPhones.getString(
                        crPhones
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER)
                    )
                    Log.d(TAG, "\tPHONE: $phone")
                }
                crPhones.close()
            }

            val crEmails = cr.query(
                ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", arrayOf(id), null
            )
            while (crEmails!!.moveToNext()) {
                val email = crEmails.getString(
                    crEmails
                        .getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA)
                )
                Log.d(TAG, "\tEMAIL: $email")
            }
            crEmails.close()

            if (photoUri != null) {
                try {
                    photo = MediaStore.Images.Media.getBitmap(this.contentResolver, Uri.parse(photoUri))
                    Log.d(TAG, "\tPHOTO: $photo")
                } catch (e: FileNotFoundException) {
                    e.printStackTrace()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

            }
        }
        crContacts!!.close()
    }

    private fun getPhone(): String {
        val phoneMgr = getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
        return if (ActivityCompat.checkSelfPermission(activity, wantPermission) != PackageManager.PERMISSION_GRANTED) {
            ""
        } else phoneMgr.line1Number
    }

    private fun requestPermission(permission: String) {
        if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permission)) {
            Toast.makeText(
                activity,
                "Read contacts permission allows us to read your contacts. Please allow it for additional functionality.",
                Toast.LENGTH_LONG
            ).show()
        }
        ActivityCompat.requestPermissions(activity, arrayOf(permission), PERMISSION_REQUEST_CODE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            PERMISSION_REQUEST_CODE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                readContacts()
            } else {
                Toast.makeText(activity, "Permission Denied. We can't read contacts.", Toast.LENGTH_LONG).show()
            }
        }
    }

    private fun checkPermission(permission: String): Boolean {
        if (Build.VERSION.SDK_INT >= 23) {
            val result = ContextCompat.checkSelfPermission(activity, permission)
            return if (result == PackageManager.PERMISSION_GRANTED) {
                true
            } else {
                false
            }
        } else {
            return true
        }
    }
}
