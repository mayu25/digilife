package com.e.digilife.Activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Typeface
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.webkit.MimeTypeMap
import android.widget.*
import com.android.volley.*
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.TransactionMemberListRecyclerAdapter
import com.e.digilife.Model.WalletModel
import com.e.digilife.R
import com.e.digilife.View.InputValidation
import com.e.digilife.View.TextViewPlus
import com.e.digilife.View.Utils
import com.e.digilife.View.VolleyMultipartRequest
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import com.shurlock.View.EndPoints
import com.squareup.picasso.Picasso
import org.json.JSONArray
import org.json.JSONObject
import java.io.*
import java.net.URL
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class AddTrascationActivity : AppCompatActivity(), View.OnTouchListener {


    val TAG = "AddTrascationActivity"
    val activity = this@AddTrascationActivity
    lateinit var inputValidation: InputValidation
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null

    lateinit var mainLayout: LinearLayout
    lateinit var titleText: TextView
    lateinit var loaderLayout: LinearLayout
    lateinit var backView: ImageView
    lateinit var saveTextView: TextView
    lateinit var splitView: ImageView
    lateinit var amountEdittext: EditText
    lateinit var splitAmountLayout: LinearLayout
    lateinit var splitAmountEdittext: EditText
    lateinit var addSplitMemberItemView: ImageView
    lateinit var memberListLayout: LinearLayout
    lateinit var memberRecyclerView: RecyclerView
    lateinit var dateSelectionLayout: RelativeLayout
    lateinit var dateTextView: TextView
    lateinit var renewableView: ImageView
    lateinit var renewableLayout: LinearLayout
    lateinit var categoryLayout: RelativeLayout
    lateinit var categoryNameText: TextViewPlus
    lateinit var tvYearly: TextView
    lateinit var tvMonthly: TextView
    lateinit var tvWeekly: TextView
    lateinit var personalTextView: TextView
    lateinit var businessTextview: TextView
    lateinit var attachView: ImageView
    lateinit var imageSelectionView: LinearLayout
    lateinit var cameraView: TextView
    lateinit var viewImg: TextView
    lateinit var galleryView: TextView
    lateinit var cancelView: TextView
    lateinit var documentView: TextView
    lateinit var claimSwitch: Switch
    lateinit var claimTextview: TextView
    lateinit var noteEdittext: EditText

    var client_id: String = ""
    var splitEnable = false
    var RenewableEnable = false

    var memberCount: String = ""
    var isSplit = 0
    var isClaim = 0
    var isRenewable = 0
    var stRenewableType: String = ""
    var purposeType: String = ""
    var selectedDate: String = ""
    var selectedDateFormat: String = ""
    var categoryId: String = ""
    var companyId: String = ""
    var memberJsonArray = JSONArray()

    internal var totalAmountCount: Float? = null
    internal var dataModel = WalletModel()


    var tranEditID: String = ""
    var TYPE: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_trascation)

        requestQueue = Volley.newRequestQueue(this)
        prefs = this.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)

        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }

        init()

        val linearLayoutManager = LinearLayoutManager(activity)
        memberRecyclerView.setLayoutManager(linearLayoutManager)
        adapter = TransactionMemberListRecyclerAdapter(activity, memberList, onDeleteItemClick)
        memberRecyclerView.adapter = adapter

        if (intent != null) {

            TYPE = intent.getStringExtra("transcation")
            if (TYPE.equals("edit")) {
                var fileData = intent.getSerializableExtra(Utils.TRANSCATION_DATA)
                if (!fileData.equals("")) {
                    dataModel = fileData as WalletModel
                    setEditData()
                }
                titleText.setText("Edit Transaction")

            } else {
                selectedDate = SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.getDefault()).format(Date())
                selectedDateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault()).format(Date())
                dateTextView.setText(selectedDate)

                purposeType = Utils.PURPOSE_PERSONAL
                titleText.setText("New Transaction")
            }
        }
    }

    fun init() {
        inputValidation = InputValidation(activity)

        titleText = findViewById(R.id.titleText)
        mainLayout = findViewById(R.id.mainLayout)
        loaderLayout = findViewById(R.id.loaderLayout)
        imageSelectionView = findViewById(R.id.imageSelectionView)
        backView = findViewById(R.id.backView)
        saveTextView = findViewById(R.id.saveTextView)
        splitView = findViewById(R.id.splitView)
        amountEdittext = findViewById(R.id.amountEdittext)
        splitAmountLayout = findViewById(R.id.splitAmountLayout)
        splitAmountEdittext = findViewById(R.id.splitAmountEdittext)
        addSplitMemberItemView = findViewById(R.id.addSplitMemberItemView)
        memberListLayout = findViewById(R.id.memberListLayout)
        memberRecyclerView = findViewById(R.id.memberRecyclerView)
        dateSelectionLayout = findViewById(R.id.dateSelectionLayout)
        dateTextView = findViewById(R.id.dateTextView)
        renewableView = findViewById(R.id.renewableView)
        renewableLayout = findViewById(R.id.renewableLayout)
        categoryLayout = findViewById(R.id.categoryLayout)
        categoryNameText = findViewById(R.id.categoryNameText)
        claimSwitch = findViewById(R.id.claimSwitch)
        claimTextview = findViewById(R.id.claimTextview)
        tvYearly = findViewById(R.id.tvYearly)
        tvMonthly = findViewById(R.id.tvMonthly)
        tvWeekly = findViewById(R.id.tvWeekly)
        personalTextView = findViewById(R.id.personalTextView)
        businessTextview = findViewById(R.id.businessTextview)
        attachView = findViewById(R.id.attachView)
        cameraView = findViewById(R.id.cameraView)
        galleryView = findViewById(R.id.galleryView)
        cancelView = findViewById(R.id.cancelView)
        documentView = findViewById(R.id.documentView)
        noteEdittext = findViewById(R.id.noteEdittext)
        viewImg = findViewById(R.id.viewImg)
        viewImg.visibility = View.VISIBLE


        backView.setOnClickListener(clickListener)
        saveTextView.setOnClickListener(clickListener)
        splitView.setOnClickListener(clickListener)
        addSplitMemberItemView.setOnClickListener(clickListener)
        renewableView.setOnClickListener(clickListener)
        categoryLayout.setOnClickListener(clickListener)
        dateSelectionLayout.setOnClickListener(clickListener)
        tvYearly.setOnClickListener(clickListener)
        tvMonthly.setOnClickListener(clickListener)
        tvWeekly.setOnClickListener(clickListener)
        personalTextView.setOnClickListener(clickListener)
        businessTextview.setOnClickListener(clickListener)
        attachView.setOnClickListener(clickListener)
        cameraView.setOnClickListener(clickListener)
        galleryView.setOnClickListener(clickListener)
        documentView.setOnClickListener(clickListener)
        cancelView.setOnClickListener(clickListener)
        viewImg.setOnClickListener(clickListener)

        mainLayout.setOnTouchListener(this)




        claimSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            Log.e("Switch State=", "" + isChecked);

            if (buttonView.isPressed) {
                if (isChecked == true) {
                    val i = Intent(this@AddTrascationActivity, TranscationCompanyListActivity::class.java)
                    startActivityForResult(i, Utils.COMPANY)
                } else {
                    isClaim = 0
                    companyId = ""
                    claimTextview.setText(activity.resources.getString(R.string.claim_reimbursment))
                }
            }
        }

        amountEdittext!!.addTextChangedListener(object : TextWatcher {

            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {
                amountStatus = true
            }
        })


        amountEdittext.setOnEditorActionListener { v, actionId, event ->
            if (event != null && event.keyCode == KeyEvent.KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) {
                var value = amountEdittext.text.toString()
                amountStatus = false
                hideKeyboard()
                if (!value.isEmpty()) {
                    value = value.replace(",", "")
                    val format = DecimalFormat("#,###,###0.00")
                    val newPrice = format.format(java.lang.Double.parseDouble(value))
                    amountEdittext.setText(newPrice)
                    amountEdittext.setSelection(newPrice.length)
                }

                // hideKeyboard()
            }
            true
        }
    }

    var amountStatus: Boolean = false


    override fun onTouch(v: View?, event: MotionEvent?): Boolean {

        if (v == mainLayout && amountStatus == true) {
            var value = amountEdittext.text.toString()
            amountStatus = false
            if (!value.isEmpty()) {
                value = value.replace(",", "")
                val format = DecimalFormat("#,###,###0.00")
                val newPrice = format.format(java.lang.Double.parseDouble(value))
                amountEdittext.setText(newPrice)
                amountEdittext.setSelection(newPrice.length)
            }

            // hideKeyboard()
            return true
        }
        return false;
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish()
                hideKeyboard()
            }
            R.id.saveTextView -> {
                // Log.e("Member List ::", memberList.toString())
                validation()

            }
            R.id.splitView -> {
                if (splitEnable == false) {
                    splitEnable = true
                    splitAmountLayout.visibility = View.VISIBLE
                } else {
                    splitEnable = false
                    isSplit = 0
                    splitAmountEdittext.setText("0")
                    splitAmountLayout.visibility = View.GONE
                    memberListLayout.visibility = View.GONE
                }
            }
            R.id.addSplitMemberItemView -> {
                memberCount = splitAmountEdittext.text.toString().trim()
                var totalAmountStr = amountEdittext.text.toString().trim()
                if (totalAmountStr.equals("")) {
                    Toast.makeText(activity, resources.getString(R.string.amount_validation), Toast.LENGTH_SHORT).show()
                } else {
                    hideKeyboard()
                    if (!memberCount.equals("") && !memberCount.equals("0")) {
                        isSplit = 1
                        var count = totalAmountStr.replace(",", "")
                        totalAmountCount = count.toFloat()
                        createMemberView(memberCount)
                        memberListLayout.visibility = View.VISIBLE
                    } else {
                        Toast.makeText(activity, resources.getString(R.string.member_validation), Toast.LENGTH_SHORT)
                            .show()
                    }
                }
            }

            R.id.renewableView -> {
                if (RenewableEnable == false) {
                    isRenewable = 1
                    RenewableEnable = true
                    renewableLayout.visibility = View.VISIBLE
                } else {
                    isRenewable = 0
                    RenewableEnable = false
                    renewableLayout.visibility = View.GONE
                    stRenewableType = ""
                    tvYearly.setTextColor(resources.getColor(R.color.greyColor))
                    tvMonthly.setTextColor(resources.getColor(R.color.greyColor))
                    tvWeekly.setTextColor(resources.getColor(R.color.greyColor))
                }

            }
            R.id.dateSelectionLayout -> {
                openDatePicker()
            }
            R.id.categoryLayout -> {
                val i = Intent(this@AddTrascationActivity, TrascationCategoryActivity::class.java)
                i.putExtra("type", "")
                startActivityForResult(i, Utils.CATEGORY)
            }
            R.id.tvYearly -> {
                tvYearly.setTextColor(resources.getColor(R.color.l_blue))
                tvMonthly.setTextColor(resources.getColor(R.color.blackColor))
                tvWeekly.setTextColor(resources.getColor(R.color.blackColor))
                stRenewableType = Utils.TRANSACTION_YEARLY
            }
            R.id.tvWeekly -> {
                tvYearly.setTextColor(resources.getColor(R.color.blackColor))
                tvMonthly.setTextColor(resources.getColor(R.color.blackColor))
                tvWeekly.setTextColor(resources.getColor(R.color.l_blue))
                stRenewableType = Utils.TRANSACTION_WEEKLY
            }
            R.id.tvMonthly -> {
                tvYearly.setTextColor(resources.getColor(R.color.blackColor))
                tvMonthly.setTextColor(resources.getColor(R.color.l_blue))
                tvWeekly.setTextColor(resources.getColor(R.color.blackColor))
                stRenewableType = Utils.TRANSACTION_MONTHLY
            }
            R.id.personalTextView -> {
                personalTextView.setTextColor(resources.getColor(R.color.whiteColor))
                personalTextView.setBackgroundColor(resources.getColor(R.color.colorPrimaryDark))

                businessTextview.setTextColor(resources.getColor(R.color.colorPrimaryDark))
                businessTextview.setBackgroundColor(resources.getColor(R.color.colorPurpose))

                purposeType = Utils.PURPOSE_PERSONAL

            }
            R.id.businessTextview -> {
                businessTextview.setTextColor(resources.getColor(R.color.whiteColor))
                businessTextview.setBackgroundColor(resources.getColor(R.color.colorPrimaryDark))

                personalTextView.setTextColor(resources.getColor(R.color.colorPrimaryDark))
                personalTextView.setBackgroundColor(resources.getColor(R.color.colorPurpose))

                purposeType = Utils.PURPOSE_BUSINESS
            }
            R.id.attachView -> {
                if (ActivityCompat.checkSelfPermission(
                        activity,
                        android.Manifest.permission.CAMERA
                    ) != PackageManager.PERMISSION_GRANTED
                ) {
                    ActivityCompat.requestPermissions(
                        activity, arrayOf(
                            android.Manifest.permission.CAMERA,
                            android.Manifest.permission.READ_EXTERNAL_STORAGE,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ), 0
                    )
                } else {
                    requirePermissions = false
                    imageSelectionView.visibility = View.VISIBLE
                }
            }

            R.id.cameraView -> {
                imageSelectionView.visibility = View.GONE
                preference = Utils.OPEN_CAMERA
                checkStoragePermission()
            }
            R.id.galleryView -> {
                imageSelectionView.visibility = View.GONE
                preference = Utils.OPEN_MEDIA
                checkStoragePermission()
            }
            R.id.documentView -> {
                imageSelectionView.visibility = View.GONE
                preference = Utils.OPEN_DOCUMENT
                checkStoragePermission()
            }
            R.id.cancelView -> {
                imageSelectionView.visibility = View.GONE
            }
            R.id.viewImg -> {
                imageSelectionView.visibility = View.GONE
                val intent = Intent(applicationContext, ZoomImageActivity::class.java)
                val bundle = Bundle()
                bundle.putString("imagePath", imagePath)
                bundle.putString(Utils.TYPE, "")
                bundle.putString("fileID", "")
                bundle.putString("fileName", "")
                intent.putExtras(bundle)
                startActivity(intent)
            }
        }
    }


    //TODO: Edit Transcation ............

    var imagePath: String = ""

    fun setEditData() {

        tranEditID = dataModel.tranId
        var amount = dataModel.tranAmount

        var count = amount.replace(",", "")
        totalAmountCount = count.toFloat()

        if (!amount.isEmpty()) {
            amount = amount.replace(",", "")
            val format = DecimalFormat("#,###,###0.00")
            val newPrice = format.format(java.lang.Double.parseDouble(amount))
            amountEdittext.setText(newPrice)
            amountEdittext.setSelection(newPrice.length)
        }


        isSplit = dataModel.trasSplit.toInt()
        if (isSplit == 1) {
            splitAmountLayout.visibility = View.VISIBLE

            var splitData = dataModel.trasSplitData.replace("\\*", "")
            var jsonArray = JSONArray(splitData)

            memberListLayout.visibility = View.VISIBLE

            createEditMemberView(jsonArray.length().toString(), jsonArray)
        }

        isRenewable = dataModel.trasIsRenewable.toInt()
        if (isRenewable == 1) {
            RenewableEnable = true
            renewableLayout.visibility = View.VISIBLE
            stRenewableType = dataModel.trasRenewableType

            if (stRenewableType.equals(Utils.TRANSACTION_YEARLY)) {
                tvYearly.setTextColor(resources.getColor(R.color.l_blue))
                tvMonthly.setTextColor(resources.getColor(R.color.blackColor))
                tvWeekly.setTextColor(resources.getColor(R.color.blackColor))
            } else if (stRenewableType.equals(Utils.TRANSACTION_WEEKLY)) {
                tvYearly.setTextColor(resources.getColor(R.color.blackColor))
                tvMonthly.setTextColor(resources.getColor(R.color.blackColor))
                tvWeekly.setTextColor(resources.getColor(R.color.l_blue))
            } else if (stRenewableType.equals(Utils.TRANSACTION_MONTHLY)) {
                tvYearly.setTextColor(resources.getColor(R.color.blackColor))
                tvMonthly.setTextColor(resources.getColor(R.color.l_blue))
                tvWeekly.setTextColor(resources.getColor(R.color.blackColor))
            }
        }

        var time = dataModel.transFullDateTime
        selectedDate = dateConvertor(time)
        dateTextView.setText(selectedDate)

        categoryId = dataModel.trasCategoryId
        categoryNameText.text = dataModel.trasCategoryName

        purposeType = dataModel.trasPurpose

        if (purposeType.equals(Utils.PURPOSE_PERSONAL)) {
            personalTextView.setTextColor(resources.getColor(R.color.whiteColor))
            personalTextView.setBackgroundColor(resources.getColor(R.color.colorPrimaryDark))

            businessTextview.setTextColor(resources.getColor(R.color.colorPrimaryDark))
            businessTextview.setBackgroundColor(resources.getColor(R.color.colorPurpose))
        } else if (purposeType.equals(Utils.PURPOSE_BUSINESS)) {
            businessTextview.setTextColor(resources.getColor(R.color.whiteColor))
            businessTextview.setBackgroundColor(resources.getColor(R.color.colorPrimaryDark))

            personalTextView.setTextColor(resources.getColor(R.color.colorPrimaryDark))
            personalTextView.setBackgroundColor(resources.getColor(R.color.colorPurpose))
        }

        var note = dataModel.tranDescription
        noteEdittext.setText(note)

        isClaim = dataModel.tranClaimReimburse.toInt()
        if (isClaim == 1) {

            companyId = dataModel.trasnCompnayId
            claimTextview.setText(dataModel.transCompanyName)
            claimSwitch.isChecked = true
        }

        imagePath = dataModel.trasAttachment
        if (!imagePath.equals("")) {

            imagePath = EndPoints.ROOT + imagePath
            Picasso.with(activity).load(imagePath).placeholder(R.mipmap.place_holder_img)
                .into(attachView)
            convertReceiptBitmap(imagePath)
        }
    }

    @SuppressLint("StaticFieldLeak")
    private fun convertReceiptBitmap(imageUrl: String) {
        object : AsyncTask<Void, Void, Bitmap>() {
            override fun doInBackground(vararg voids: Void): Bitmap {
                var bitmap: Bitmap? = null
                try {
                    val url = URL(imageUrl)
                    bitmap = BitmapFactory.decodeStream(url.openConnection().getInputStream())

                } catch (e: IOException) {
                    println(e)
                }
                return bitmap!!
            }

            override fun onPostExecute(s: Bitmap) {
                receiptBitmap = s
                super.onPostExecute(s)
            }
        }.execute()
    }

    fun dateConvertor(inputText: String): String {

        val outputFormat = SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.US)
        val inputFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)

        var date: Date? = null
        try {
            date = inputFormat.parse(inputText)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val outputText = outputFormat.format(date)

        val outputFormat2 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
        outputFormat2.timeZone = TimeZone.getTimeZone("UTC")
        selectedDateFormat = outputFormat2.format(date)

        return outputText
    }


    fun createEditMemberView(count: String, jsonArray: JSONArray) {

        memberList.clear()
        var mCount = Integer.parseInt(count)
        if (mCount != 0) {
            for (i in 0..(jsonArray.length() - 1)) {
                var jObj = jsonArray.getJSONObject(i)
                val dataModel = WalletModel()

                dataModel.memberName = jObj.getString("name")
                dataModel.memberCount = jObj.getString("price")
                memberList.add(dataModel)
            }
            adapter!!.notifyDataSetChanged()
        }

        splitAmountEdittext.setText(mCount.toString())
        memberCount = splitAmountEdittext.text.toString().trim()
        Log.e("memberCount :::::", memberCount)
    }

    /*...........................End Edit part...................*/

    private var memberList = ArrayList<WalletModel>()
    private var temMemberList = ArrayList<WalletModel>()
    var adapter: TransactionMemberListRecyclerAdapter? = null


    fun createMemberView(count: String) {

        memberList.clear()
        var mCount = Integer.parseInt(count)
        var memberAmount = (totalAmountCount!!.toInt() / mCount)
        if (mCount != 0) {
            for (i in 0..(mCount - 1)) {
                val dataModel = WalletModel()

                if (i == 0) {
                    dataModel.memberName = "Admin"
                } else {
                    dataModel.memberName = ""
                }
                dataModel.memberCount = memberAmount.toString()
                memberList.add(dataModel)
            }

            adapter = TransactionMemberListRecyclerAdapter(activity, memberList, onDeleteItemClick)
            memberRecyclerView.adapter = adapter
//            val linearLayoutManager = LinearLayoutManager(activity)
//            memberRecyclerView.setLayoutManager(linearLayoutManager)
//            memberRecyclerView.setHasFixedSize(true)
//            adapterW = TransactionMemberListRecyclerAdapter(activity, memberList, onDeleteItemClick)
//            memberRecyclerView.adapterW = adapterW
        }
    }


    var onDeleteItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int

            memberCount = splitAmountEdittext.text.toString().trim()
            var mCount = Integer.parseInt(memberCount) - 1
            splitAmountEdittext.setText(mCount.toString())
            if (mCount != 0) {
                var memberAmount = (totalAmountCount!!.toInt() / mCount)
                adapter!!.remove(position)
                for (i in 0..(memberList.size - 1)) {
                    memberList.get(i).memberCount = memberAmount.toString()
                }
                adapter!!.notifyDataSetChanged()
            } else {
                memberListLayout.visibility = View.GONE
                memberList.clear()
                adapter!!.notifyDataSetChanged()
            }
        }
    }


//    var onDeleteItemClick: View.OnClickListener = object : View.OnClickListener {
//        override fun onClick(v: View?) {
//            val position = v!!.getTag() as Int
//            Log.e("position:::", position.toString())
//            // memberList.removeAt(position)
//            temMemberList = memberList
//            memberList.remove(memberList.get(position))
//            adapterW!!.notifyDataSetChanged()
//
//            var mCount = Integer.parseInt(memberCount) - 1
//            splitAmountEdittext.setText(mCount.toString())
//            memberCount = splitAmountEdittext.text.toString().trim()
//
//            if (!memberCount.equals("") && !memberCount.equals("0")) {
//                if (mCount != 0) {
//
//                    if (TYPE.equals("add")) {
//                        // memberList.clear()
//                        var memberAmount = (totalAmountCount!!.toInt() / mCount)
//                        for (i in 0..(memberList.size - 1)) {
//                            memberList.get(i).memberCount = memberAmount.toString()
//                        }
//                        adapterW!!.notifyDataSetChanged()
////                        for (i in 0..(mCount - 1)) {
////                            val dataModel = WalletModel()
////
////                            if (i == 0) {
////                                dataModel.memberName = "Admin"
////                            } else {
////                                dataModel.memberName = ""
////                            }
////                            dataModel.memberCount = memberAmount.toString()
////                            memberList.add(dataModel)
////                        }
////                        adapterW!!.notifyDataSetChanged()
//                    } else {
//                        var memberAmount = (totalAmountCount!!.toInt() / mCount)
//                        for (i in 0..(memberList.size - 1)) {
//                            memberList.get(i).memberCount = memberAmount.toString()
//                        }
//                        adapterW!!.notifyDataSetChanged()
//                    }
//
//                }
//            } else {
//                memberListLayout.visibility = View.GONE
//                memberList.clear()
//                adapterW!!.notifyDataSetChanged()
//            }
//        }
//    }

//
//    var onDeleteItemClick: View.OnClickListener = object : View.OnClickListener {
//        override fun onClick(v: View?) {
//            val position = v!!.getTag() as Int
//            // memberList.remove(memberList.get(position))
//
//            memberList.removeAt(position)
//
//
////            var mCount = Integer.parseInt(memberCount) - 1
////            var memberAmount = (totalAmountCount!!.toInt() / mCount)
////            splitAmountEdittext.setText(mCount.toString())
////            temMemberList = memberList
////           for (i in 0..(memberList.size - 1)) {
////               memberList.get(i).memberCount = memberAmount.toString()
////           }
//            adapterW!!.notifyDataSetChanged()
//        }
//    }


    fun openDatePicker() {
        SingleDateAndTimePickerDialog.Builder(activity)
            .bottomSheet()
            .displayMinutes(true)
            .displayHours(true)
            .displayDays(false)
            .displayMonth(true)
            .displayYears(true)
            .displayDaysOfMonth(true)
            .displayListener(object : SingleDateAndTimePickerDialog.DisplayListener {
                override fun onDisplayed(picker: SingleDateAndTimePicker) {
                    //retrieve the SingleDateAndTimePicker
                }
            })
            .title("Select Date")
            .titleTextColor(getResources().getColor(R.color.blackColor))
            .backgroundColor(getResources().getColor(R.color.whiteColor))
            .mainColor(getResources().getColor(R.color.blackColor))
            .curved()
            .listener(object : SingleDateAndTimePickerDialog.Listener {
                override fun onDateSelected(date: Date) {
                    selectedDate = dateConvertor(date)
                    dateTextView.setText(selectedDate)

                }
            }).display()
    }


    fun dateConvertor(date: Date): String {
        val inputFormat = SimpleDateFormat("E MMM dd yyyy HH:mm:ss 'GMT'z", Locale.ENGLISH)
        // val date = inputFormat.parse(time)
        val outputFormat = SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.ENGLISH)
        outputFormat.timeZone = TimeZone.getTimeZone("UTC")
        val output = outputFormat.format(date)


        val outputFormat2 = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH)
        outputFormat2.timeZone = TimeZone.getTimeZone("UTC")
        selectedDateFormat = outputFormat2.format(date)

        return output
    }


    private fun validation() {


        if (!inputValidation.isFieldEmpty(amountEdittext, getString(R.string.amount_validation))) {
            return
        }
        if (!inputValidation.isFieldBlank(categoryId, getString(R.string.category_validation))) {
            return
        }


        if (isSplit == 1) {
            if (memberList.size == 0) {
                Toast.makeText(activity, R.string.member_validation2, Toast.LENGTH_SHORT).show()
                return
            } else {
                for (i in 0..(memberList.size - 1)) {
                    var mName = memberList.get(i).memberName
                    var nCount = memberList.get(i).memberCount
                    var jObj = JSONObject()
                    jObj.put("name", mName)
                    jObj.put("price", nCount)
                    memberJsonArray.put(jObj)
                }
            }
        }

        if (isRenewable == 1) {
            if (stRenewableType.equals("")) {
                Toast.makeText(activity, R.string.renewable_validation, Toast.LENGTH_SHORT).show()
                return
            }
        }


        var amount = amountEdittext.text.toString().trim().replace(",", "")
        var note = noteEdittext.text.toString().trim()
        // selectedDate = SimpleDateFormat("dd-MMM-yyyy HH:mm:ss", Locale.getDefault()).format(Date())

        if (Utils.checkInternetConnection(this@AddTrascationActivity)) {
            hideKeyboard()
            if (TYPE.equals("edit")) {
                updateTranscation(amount, note)
            } else {
                addTranscation(amount, note)
            }

        } else {
            Utils.showMessageDialog(
                this@AddTrascationActivity,
                getString(R.string.app_name),
                getString(R.string.check_internet)
            )
        }
    }

    fun addTranscation(amount: String, note: String) {
        loaderLayout.visibility = View.VISIBLE

        val volleyMultipartRequest = object : VolleyMultipartRequest(
            Request.Method.POST, EndPoints.ADD_TRANSCATION,
            Response.Listener { response ->
                Log.e("response", String(response.data))
                requestQueue.getCache().clear();
                loaderLayout.visibility = View.GONE

                var jsonObj = JSONObject(String(response.data))
                val status = jsonObj.getString("status")
                if (status.equals("1")) {
                    finish()
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                }

            },
            Response.ErrorListener { error ->
                Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT).show()
            }) {


            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("client_id", client_id)
                params.put("amount", amount)
                params.put("category_id", categoryId)
                params.put("purpose", purposeType)
                params.put("description", note)
                params.put("date_time", selectedDateFormat)
                params.put("is_claim_reimburse", isClaim.toString())
                params.put("company_id", companyId)
                params.put("is_renewable", isRenewable.toString())
                params.put("renewable_type", stRenewableType)
                params.put("is_split", isSplit.toString())
                if (isSplit == 0) {
                    params.put("split_data", "")
                } else {
                    params.put("split_data", memberJsonArray.toString())
                }

                Log.e(" JSONData :", params.toString())
                return params
            }

            override fun getByteData(): Map<String, DataPart> {
                val params = HashMap<String, DataPart>()
                // val imagename = System.currentTimeMillis()

                if (receiptBitmap != null) {
                    params["attachment"] = DataPart("$fileName.$extention", getFileDataFromDrawable(receiptBitmap!!))
                }

                return params
            }
        }
        Log.e(" API :", EndPoints.ADD_TRANSCATION)
        volleyMultipartRequest.retryPolicy = DefaultRetryPolicy(
            0,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(volleyMultipartRequest);
    }

    //Update Transcation..............

    fun updateTranscation(amount: String, note: String) {
        loaderLayout.visibility = View.VISIBLE

        val volleyMultipartRequest = object : VolleyMultipartRequest(
            Request.Method.POST, EndPoints.EDIT_TRANSCATION,
            Response.Listener { response ->
                Log.e("response", String(response.data))
                requestQueue.getCache().clear();
                loaderLayout.visibility = View.GONE

                var jsonObj = JSONObject(String(response.data))
                val status = jsonObj.getString("status")
                if (status.equals("1")) {
                    finish()
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                } else {
                    Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                }

            },
            Response.ErrorListener { error ->
                Toast.makeText(applicationContext, error.message, Toast.LENGTH_SHORT).show()
            }) {


            @Throws(AuthFailureError::class)
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params.put("id", tranEditID)
                params.put("client_id", client_id)
                params.put("amount", amount)
                params.put("category_id", categoryId)
                params.put("purpose", purposeType)
                params.put("description", note)
                params.put("date_time", selectedDateFormat)
                params.put("is_claim_reimburse", isClaim.toString())
                params.put("company_id", companyId)
                params.put("is_renewable", isRenewable.toString())
                params.put("renewable_type", stRenewableType)
                params.put("is_split", isSplit.toString())
                if (isSplit == 0) {
                    params.put("split_data", "")
                } else {
                    params.put("split_data", memberJsonArray.toString())
                }

                Log.e(" JSONData :", params.toString())
                return params
            }

            override fun getByteData(): Map<String, DataPart> {
                val params = HashMap<String, DataPart>()
                // val imagename = System.currentTimeMillis()
                if (receiptBitmap != null) {
                    params["attachment"] = DataPart("$fileName.png", getFileDataFromDrawable(receiptBitmap!!))
                }
                return params
            }
        }
        Log.e(" API :", EndPoints.EDIT_TRANSCATION)

        volleyMultipartRequest.retryPolicy = DefaultRetryPolicy(
            0,
            DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
            DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(volleyMultipartRequest);
    }

    fun hideKeyboard() {
        val view = this.getCurrentFocus()
        if (view != null) {
            val inputManager: InputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputManager.hideSoftInputFromWindow(view.windowToken, InputMethodManager.SHOW_FORCED)
        }
    }


    //TODO: receipt upload images......

    private val GALLERY = 1
    private val CAMERA = 2
    private val DOCUMENT = 3
    var requirePermissions = true
    var preference = 0
    var extention: String = ""
    var fileName: String = ""

    private fun checkStoragePermission() {

        if (ContextCompat.checkSelfPermission(
                this@AddTrascationActivity,
                Manifest.permission.CAMERA
            ) != PackageManager.PERMISSION_GRANTED
        ) {

            ActivityCompat.requestPermissions(
                this@AddTrascationActivity,
                arrayOf(
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ), 100)

        } else {
            chooseFileFromDevice(preference)
        }
    }

    private fun chooseFileFromDevice(preference: Int) {

        if (preference == Utils.OPEN_DOCUMENT) {
            selectFile()

        } else if (preference == Utils.OPEN_MEDIA) {
            val galleryIntent = Intent(
                Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(galleryIntent, GALLERY)
        } else {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            startActivityForResult(intent, CAMERA)
        }
    }

    fun selectFile() {
        val intent = Intent()
        intent.setAction(Intent.ACTION_GET_CONTENT)
        intent.setType("image/*")
        startActivityForResult(Intent.createChooser(intent, "Select Photos"), DOCUMENT)
    }

    // var receiptBytes: ByteArray? = null

    var receiptBitmap: Bitmap? = null

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK && requestCode == Utils.CATEGORY) {
            var name = data!!.getStringExtra("cName")
            var id = data.getStringExtra("cId")
            categoryId = id
            categoryNameText.text = name

        } else if (resultCode == Activity.RESULT_CANCELED && requestCode == Utils.COMPANY) {

            claimSwitch.isChecked = false

        } else if (resultCode == Activity.RESULT_OK && requestCode == Utils.COMPANY) {
            var name = data!!.getStringExtra("cName")
            var id = data.getStringExtra("cId")
            isClaim = 1
            companyId = id
            //claimSwitch.isChecked = true
            claimTextview.text = name

        } else if (requestCode == GALLERY && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                val contentURI = data.data
                try {
                    val file = File(contentURI.path)
                    fileName = file.name

                    val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, contentURI)
                    val path = saveImage(bitmap)

                    val byteArrayOutputStream = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
                    val decoded = BitmapFactory.decodeStream(ByteArrayInputStream(byteArrayOutputStream.toByteArray()));

                    receiptBitmap = decoded
                    // receiptBytes = getFileDataFromDrawable(bitmap)
                    attachView.setImageBitmap(bitmap)
                    extention = "png"

                } catch (e: IOException) {
                    e.printStackTrace()
                    Toast.makeText(this@AddTrascationActivity, "Failed!", Toast.LENGTH_SHORT).show()
                }
            }

        } else if (requestCode == CAMERA && resultCode == Activity.RESULT_OK) {
            val thumbnail = data!!.extras!!.get("data") as Bitmap
            val contentURI = data.data
            val file = File(contentURI.path)
            fileName = file.name
            val byteArrayOutputStream = ByteArrayOutputStream()
            thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
            val decoded = BitmapFactory.decodeStream(ByteArrayInputStream(byteArrayOutputStream.toByteArray()))
            receiptBitmap = decoded
            //receiptBytes = getFileDataFromDrawable(thumbnail)
            attachView.setImageBitmap(thumbnail)
            extention = "png"
            saveImage(thumbnail)
        } else if (requestCode == DOCUMENT && resultCode == Activity.RESULT_OK) {
            if ((data != null) && (data.getData() != null)) {
                var docUri: Uri

                docUri = data.data
                val file = File(docUri.path)
                fileName = file.name

                val bitmap = MediaStore.Images.Media.getBitmap(this.contentResolver, docUri)
                val byteArrayOutputStream = ByteArrayOutputStream()
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
                val decoded = BitmapFactory.decodeStream(ByteArrayInputStream(byteArrayOutputStream.toByteArray()))
                extention = getfileExtension(docUri)!!
                receiptBitmap = decoded  //docUri as Bitmap
                // receiptBytes = Utils.getBytes(docUri, activity)
                attachView.setImageBitmap(bitmap)
            }
        }
    }

    private fun getfileExtension(uri: Uri): String? {
        val extension: String?
        val contentResolver = contentResolver
        val mimeTypeMap = MimeTypeMap.getSingleton()
        extension = mimeTypeMap.getExtensionFromMimeType(contentResolver.getType(uri))
        return extension
    }

    fun getFileDataFromDrawable(bitmap: Bitmap): ByteArray {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream)
        return byteArrayOutputStream.toByteArray()
    }


    fun saveImage(myBitmap: Bitmap): String {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes)
        val wallpaperDirectory = File(
            (Environment.getExternalStorageDirectory()).toString() + "Image_Directory"
        )
        // have the object build the directory structure, if needed.
        Log.d("fee", wallpaperDirectory.toString())
        if (!wallpaperDirectory.exists()) {

            wallpaperDirectory.mkdirs()
        }

        try {
            Log.d("heel", wallpaperDirectory.toString())
            val f = File(
                wallpaperDirectory, ((Calendar.getInstance()
                    .getTimeInMillis()).toString() + ".jpg")
            )
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(
                this,
                arrayOf(f.getPath()),
                arrayOf("image/jpeg"), null
            )
            fo.close()
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath())

            return f.getAbsolutePath()
        } catch (e1: IOException) {
            e1.printStackTrace()
        }

        return ""
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            Utils.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE -> if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                chooseFileFromDevice(preference)
            } else {
                Toast.makeText(
                    this@AddTrascationActivity,
                    resources.getString(R.string.msg_denied_permission),
                    Toast.LENGTH_LONG
                ).show()
            }
        }
    }
}
