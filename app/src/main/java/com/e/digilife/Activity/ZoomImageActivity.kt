package com.e.digilife.Activity

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.NetworkOnMainThreadException
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.e.digilife.CropLibrary.saveToInternalStorage
import com.e.digilife.CropLibrary.shareCacheDirBitmap
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.e.digilife.View.ZoomableImageView
import com.shurlock.View.EndPoints
import com.squareup.picasso.Picasso
import org.json.JSONArray
import org.json.JSONObject
import java.net.HttpURLConnection
import java.net.URL
import java.util.HashMap

class ZoomImageActivity : AppCompatActivity() {

    val TAG = "ZoomImageActivity"
    val activity = this@ZoomImageActivity
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null

    lateinit var loaderLayout: LinearLayout
    lateinit var zoomImgView: ZoomableImageView
    lateinit var closeView: ImageView
    lateinit var shareView: ImageView
    lateinit var deleteView: ImageView
    lateinit var imageNameText: TextView

    var bundle: Bundle? = null
    lateinit var bitmap: Bitmap
    var imagePath: String = ""
    var TYPE: String = ""
    var fileID: String = ""
    var fileName: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_zoom_image)
        bundle = intent.extras
        init()
        prefs = activity.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)

        if (bundle != null) {
            imagePath = bundle!!.getString("imagePath")
            TYPE = bundle!!.getString(Utils.TYPE)
            fileID = bundle!!.getString("fileID")
            fileName = bundle!!.getString("fileName")
            Log.e("imagePath ::: ", imagePath)
            if (!imagePath.equals("")) {
                try {
                    val url = URL(imagePath)
                    val connection = url.openConnection() as HttpURLConnection
                    connection.doInput = true
                    connection.connect()
                    val input = connection.inputStream
                    bitmap = BitmapFactory.decodeStream(input)
                    zoomImgView.setImageBitmap(bitmap)
                } catch (expection: NetworkOnMainThreadException) {

                }
            }

            if (TYPE.equals(Utils.OFFLINE)) {
                deleteView.visibility = View.VISIBLE
                shareView.visibility = View.VISIBLE
            }
        }

        imageNameText.text = fileName
    }

    fun init() {
        loaderLayout = findViewById(R.id.loaderLayout)
        zoomImgView = findViewById(R.id.zoomImgView)
        closeView = findViewById(R.id.closeView)
        shareView = findViewById(R.id.shareView)
        deleteView = findViewById(R.id.deleteView)
        imageNameText = findViewById(R.id.imageNameText)

        closeView.setOnClickListener(clickListener)
        shareView.setOnClickListener(clickListener)
        deleteView.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.closeView -> {
                finish()
            }
            R.id.shareView -> {
                val bitmap = Utils.getBitmapFromURL(imagePath)
                val uri = bitmap!!.saveToInternalStorage(this)
                Toast.makeText(applicationContext, uri.toString(), Toast.LENGTH_SHORT).show()
                this.shareCacheDirBitmap(uri)
            }
            R.id.deleteView -> {
                deleteOfflineFile()
            }
        }
    }

    var offineArray = JSONArray()

    fun deleteOfflineFile() {
        val offlineStr = prefs!!.getString(Utils.SAVE_OFFLINE, "")
        if (!offlineStr.equals("[]")) {
            Utils.OFFLINE_IMG_ARRAY = JSONArray(offlineStr)
            for (i in 0..(Utils.OFFLINE_IMG_ARRAY.length() - 1)) {
                val jsonObj = Utils.OFFLINE_IMG_ARRAY.getJSONObject(i)
                val id = jsonObj.getString(Utils.OFFLINE_FILE_ID)
                if (fileID.equals(id)) {
                    Utils.OFFLINE_IMG_ARRAY.remove(i)
                    break
                }
            }
        }
        if (offineArray.length() == 0) {
            Utils.storeJSONArraylist(prefs, Utils.SAVE_OFFLINE, null)
        } else {
            Utils.storeJSONArraylist(prefs, Utils.SAVE_OFFLINE, Utils.OFFLINE_IMG_ARRAY)
        }

        finish()
        Toast.makeText(activity, "File Deleted Successfully !", Toast.LENGTH_SHORT).show()
    }
}
