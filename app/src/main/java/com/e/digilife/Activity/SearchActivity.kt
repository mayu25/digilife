package com.e.digilife.Activity

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.*
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.ImageGridviewAdapter
import com.app.towntalk.Adapter.SearchGridviewAdapter
import com.e.digilife.Model.DataModel
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import java.util.HashMap

class SearchActivity : AppCompatActivity() {

    val TAG = "SearchActivity"
    val activity = this@SearchActivity
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null

    lateinit var backView: ImageView
    lateinit var loaderLayout: LinearLayout
    lateinit var gridview: GridView
    lateinit var searchEditText: EditText

    var client_id: String = ""
    var searchText: String = ""
    private var searchList = ArrayList<DataModel>()
    var adapter: SearchGridviewAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        prefs = activity.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }
        requestQueue = Volley.newRequestQueue(activity)
        init()


        adapter = SearchGridviewAdapter(activity, searchList, onItemClick)
        gridview.adapter = adapter
    }

    fun init() {
        gridview = findViewById(R.id.gridview)
        loaderLayout = findViewById(R.id.loaderLayout)
        backView = findViewById(R.id.backView)
        searchEditText = findViewById(R.id.searchEditText)

        backView.setOnClickListener(clickListener)
        val font = Typeface.createFromAsset(assets, resources.getString(R.string.popins_regular))
        searchEditText.setTypeface(font)

        searchEditText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {
                searchText = editable.toString()

                if (searchText.equals("")){
                    searchList.clear()
                    adapter!!.notifyDataSetChanged()
                }else{
                    getSearchFiles()
                }

            }
        })
    }

    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int

            var fileType = searchList.get(position).fileType

            if (fileType.equals(Utils.FAVOURITE_MUSIC)) {
                val intent = Intent(applicationContext, ImageViewerActivity::class.java)
                intent.putExtra(Utils.FILE_DATA, searchList.get(position))
                intent.putExtra(Utils.TYPE, "")
                intent.putExtra(Utils.FILE_TYPE, Utils.FAVOURITE_MUSIC)
                startActivity(intent)
            } else if (fileType.equals(Utils.DOCUMENT)) {
                val intent = Intent(applicationContext, ImageViewerActivity::class.java)
                intent.putExtra(Utils.FILE_DATA, searchList.get(position))
                intent.putExtra(Utils.TYPE, "")
                intent.putExtra(Utils.FILE_TYPE, Utils.DOCUMENT)
                startActivity(intent)
            } else {
                val intent = Intent(applicationContext, ImageViewerActivity::class.java)
                intent.putExtra(Utils.FILE_DATA, searchList.get(position))
                intent.putExtra(Utils.TYPE, "")
                intent.putExtra(Utils.FILE_TYPE, "")
                startActivity(intent)
            }
        }
    }


    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish()
            }
        }
    }


    private fun getSearchFiles() {

       // loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.SEARCH_FILES,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                  //  loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        val dataArray = jsonObj.getJSONArray("data")
                        if (dataArray.length() > 0) {
                            setSearchFiles(dataArray)
                        }

                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_SHORT).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                    Log.d("Error.Response", error.toString())
                }
            }
        ) {

            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["file_name"] = searchText
                Log.e(" JSONData :", params.toString())
                return params

            }
        }

        Log.e(" API :", EndPoints.SEARCH_FILES)
        requestQueue.add(stringRequest)
    }


    fun setSearchFiles(jsonArray: JSONArray) {
        searchList.clear()
        for (i in 0..(jsonArray.length() - 1)) {
            val jsonObj = jsonArray.getJSONObject(i)
            val searchModel = DataModel()
            searchModel.fileId = jsonObj.getString("id")
            searchModel.fileName = jsonObj.getString("file_name")
            var filePath = jsonObj.getString("file_path")
            filePath = filePath.replace(" ", "%20")
            searchModel.filePath = filePath
            var extention = getFileExtention(filePath)
            if (extention.equals("mp3")) {
                searchModel.fileType = Utils.FAVOURITE_MUSIC
            } else if (extention.equals("pdf")) {
                searchModel.fileType = Utils.DOCUMENT
            } else {
                searchModel.fileType = ""
            }
            searchModel.isFav = jsonObj.getString("is_fav")
           // emrModel.fileSize = jsonObj.getString("file_size")
            searchModel.parentFolderId = jsonObj.getString("parent_folder_id")
            searchModel.childFolderId = jsonObj.getString("child_folder_id")
            searchModel.createTime = jsonObj.getString("DateTime")
          //  emrModel.modifiedTime = jsonObj.getString("ModifiedTime")
            searchList.add(searchModel)
        }

        adapter!!.notifyDataSetChanged()
    }

    fun getFileExtention(fileName: String): String {
        val filenameArray = fileName.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        var extention = filenameArray[filenameArray.size - 1]
        return extention
    }

}
