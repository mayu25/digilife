package com.e.digilife.Activity

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.support.v4.app.ActivityCompat
import android.support.v4.content.FileProvider
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.webkit.WebView
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import com.e.digilife.R
import com.webviewtopdf.PdfView
import java.io.File


class ReportShareActivity : AppCompatActivity() {

    val TAG = "ReportShareActivity"
    val activity = this@ReportShareActivity
    var reportStr: String = ""
    var pdfURL: String = ""

    lateinit var loaderLayout: LinearLayout
    lateinit var reportView: WebView
    lateinit var shareView: ImageView
    lateinit var backView: ImageView

    internal var permissions =
        arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(com.e.digilife.R.layout.activity_report_share)
        init()
        if (intent != null) {
            reportStr = intent.getStringExtra("ReportStr")
        }

        reportView.loadDataWithBaseURL(null, reportStr, "text/html", "utf-8", null)


        //or without a callback
    }

    fun init() {
        loaderLayout = findViewById(R.id.loaderLayout)
        reportView = findViewById(R.id.reportView)
        backView = findViewById(R.id.backView)
        shareView = findViewById(R.id.shareView)

        backView.setOnClickListener(clickListener)
        shareView.setOnClickListener(clickListener)
    }


    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                finish()
            }
            R.id.shareView -> {

                if (ActivityCompat.checkSelfPermission(
                        activity, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(
                        activity, arrayOf(
                            android.Manifest.permission.READ_EXTERNAL_STORAGE,
                            android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ), 0
                    )
                } else {
                    val directory_path = Environment.getExternalStorageDirectory().path + "/DigiLife/"
                    val file = File(directory_path)
                    if (!file.exists()) {
                        file.mkdirs()
                    }

                    val imagename = System.currentTimeMillis()
                    val targetPdf = "$imagename.pdf"

                    loaderLayout.visibility = View.VISIBLE

                    PdfView.createWebPrintJob(this@ReportShareActivity, reportView, File(directory_path), targetPdf, object :
                        PdfView.Callback {

                        override fun success(path: String) {
                            loaderLayout.visibility = View.GONE
                            Toast.makeText(this@ReportShareActivity, "Pdf is successfully Saved!", Toast.LENGTH_LONG).show()
                            SaveandShare(directory_path + targetPdf)
                        }

                        override fun failure() {
                            loaderLayout.visibility = View.GONE

                        }
                    })
                }

            }
        }
    }


    private fun SaveandShare(targetPdf: String) {
        try {
            val U = FileProvider.getUriForFile(
                this@ReportShareActivity.getApplicationContext(),
                "com.e.digilife.provider", File(targetPdf)) as Uri
            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_SUBJECT, "Digi life")
            intent.putExtra(Intent.EXTRA_STREAM, U)
            startActivity(Intent.createChooser(intent, "choose one"))
        } catch (e: Exception) {
            Log.e("error", e.message)
        }

    }



}
