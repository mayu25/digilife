package com.e.digilife.Fragment

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.*
import android.widget.*
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.EMRListAdapter
import com.e.digilife.Activity.DashboardActivity
import com.e.digilife.Activity.FileListActivity
import com.e.digilife.Activity.NotificationActivity
import com.e.digilife.Activity.SearchActivity
import com.e.digilife.Model.EMRModel
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import java.util.HashMap
import kotlin.collections.ArrayList
import kotlin.collections.set

class EMRFragment : Fragment() {

    private val TAG = "EMRFragment"
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null

    var adapter: EMRListAdapter? = null
    private var emrList = ArrayList<EMRModel>()

    lateinit var emrListview: ListView
    lateinit var loaderLayout: LinearLayout

    var client_id: String = ""
    var emr_id: String = ""

    lateinit var addFolder: ImageView
    lateinit var searchView: ImageView
    lateinit var notificationView: ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_emr, container, false)
        // (activity as DashboardActivity).setActionBarTitle(resources.getString(R.string.emr))
        setHasOptionsMenu(true)


        val toolbar: Toolbar = (activity as DashboardActivity).findViewById(R.id.toolbar)
        var addFolderView = toolbar.findViewById(R.id.addFolderView) as ImageView
        var shareView = toolbar.findViewById(R.id.shareView) as ImageView
        addFolder = toolbar.findViewById(R.id.plusView) as ImageView
        searchView = toolbar.findViewById(R.id.searchView)
        notificationView = toolbar.findViewById(R.id.notificationView)
        addFolderView.visibility = View.GONE
        addFolder.visibility = View.VISIBLE
        shareView.visibility = View.GONE
        var titleText = toolbar.findViewById(R.id.titleText) as TextView
        titleText.setText(resources.getString(R.string.emr))

        requestQueue = Volley.newRequestQueue(activity)
        init(view)

        prefs = activity!!.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
            emr_id = loginObj.getString("EMR_id")
        }

        adapter = EMRListAdapter(context!!, emrList, onItemClick)
        emrListview.adapter = adapter

        if (Utils.checkInternetConnection(context!!))
            getEMRList("")
        else
            Utils.showMessageDialog(activity!!, getString(R.string.app_name), getString(R.string.check_internet))

        return view
    }


    fun init(view: View) {
        loaderLayout = view.findViewById(R.id.loaderLayout)
        emrListview = view.findViewById(R.id.listview)

        addFolder.setOnClickListener(clickListener)
        searchView.setOnClickListener(clickListener)
        notificationView.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.plusView -> {
                showAddFolderDialog()
            }
            R.id.searchView -> {
                val intent = Intent(activity, SearchActivity::class.java)
                startActivity(intent)
            }
            R.id.notificationView -> {
                val intent = Intent(activity, NotificationActivity::class.java)
                startActivity(intent)
            }
        }
    }


    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int

            val intent = Intent(activity, FileListActivity::class.java)
            val bundle = Bundle()
            bundle.putString(Utils.PARENT_FOLDER_ID, emr_id)
            bundle.putString(Utils.FOLDER_ID, emrList.get(position).emrId)
            bundle.putString(Utils.TITLE, emrList.get(position).emrFolderName)
            intent.putExtras(bundle)
            startActivity(intent)

        }
    }


    private fun getEMRList(type: String) {

        if (type.equals("")){
            loaderLayout.visibility = View.VISIBLE
        }

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.GET_FOLDER_LIST,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    //dialog.dismiss()

                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        val dataArray = jsonObj.getJSONArray("data")
                        if (dataArray.length() > 0)
                            setEMRList(dataArray)

                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {

            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["parent_folder_id"] = emr_id
                return params
                Log.e(" JSONData :", params.toString())
            }
        }

        Log.e(" API :", EndPoints.GET_FOLDER_LIST)
        requestQueue.add(stringRequest)
    }


    fun setEMRList(jsonArray: JSONArray) {

        emrList.clear()
        for (i in 0..(jsonArray.length() - 1)) {
            val jsonObj = jsonArray.getJSONObject(i)
            val emrModel = EMRModel()
            emrModel.emrId = jsonObj.getString("id")
            emrModel.emrFolderName = jsonObj.getString("folder_name")
            emrList.add(emrModel)
        }

        adapter!!.notifyDataSetChanged()

    }

    fun showAddFolderDialog() {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = activity!!.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.create_folder_view, null)
        dialog.setContentView(view)

        val createText = dialog.findViewById(R.id.createText) as TextView
        val folderNameEdittext = dialog.findViewById(R.id.folderNameEdittext) as EditText
        val doneBtn = dialog.findViewById(R.id.doneBtn) as Button
        folderNameEdittext.setHint("Enter Report Name")
        createText.setText("Create Custom Report")

        val font = Typeface.createFromAsset(activity!!.assets, resources.getString(R.string.popins_regular))
        folderNameEdittext.setTypeface(font)
        doneBtn.setTypeface(font)

        doneBtn.setOnClickListener {
            if (folderNameEdittext.text.toString().equals("")) {
                Toast.makeText(activity, "Enter Report Name", Toast.LENGTH_SHORT).show()
            } else {
                if (Utils.checkInternetConnection(context!!))
                    addFolder(folderNameEdittext.text.toString())
                else
                    Utils.showMessageDialog(
                        activity!!,
                        getString(R.string.app_name),
                        getString(R.string.check_internet)
                    )

                dialog.dismiss()
            }
        }

        var window = dialog.getWindow();
        window.setLayout(700, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }

    private fun addFolder(folderName: String) {

        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.ADD_FOLDER,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        getEMRList("add")
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    } else {
                        loaderLayout.visibility = View.GONE
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["parent_folder_id"] = emr_id
                params["folder_name"] = folderName
                return params
                Log.e(" JSONData :", params.toString())
            }
        }
        Log.e(" API :", EndPoints.ADD_FOLDER)
        requestQueue.add(stringRequest)
    }


}