package com.e.digilife.Fragment


import android.Manifest
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.content.pm.PackageManager.PERMISSION_GRANTED
import android.graphics.Typeface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.telephony.SmsManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.ContactRecyclerAdapter
import com.e.digilife.Activity.AddContactActivity

import com.e.digilife.Model.ContactModel
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import java.net.URLEncoder
import java.util.*


class Contact_Fragment : Fragment() {

    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null
    private var animation: Animation? = null


    lateinit var searchText: EditText
    lateinit var contactRecyclerView: RecyclerView
    lateinit var loaderLayout: LinearLayout

    var client_id: String = ""

    private var contactList = ArrayList<ContactModel>()
    private var searchList = ArrayList<ContactModel>()
    private var tempContactList = ArrayList<ContactModel>()
    var adapter: ContactRecyclerAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_contact, container, false)
        init(view)
        requestQueue = Volley.newRequestQueue(activity)
        prefs = activity!!.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        animation = AnimationUtils.loadAnimation(activity, R.anim.fade_in)
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")

        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }
        init(view)

        val linearLayoutManager = LinearLayoutManager(activity)
        contactRecyclerView.setLayoutManager(linearLayoutManager)
        contactRecyclerView.setHasFixedSize(true)
        adapter = ContactRecyclerAdapter(
            context!!,
            contactList,
            onItemClick,
            onMessageItemClick,
            onCallItemClick,
            onWhatsAppItemClick,
            onDeleteItemClick,
            onShareItemClick
        )
        contactRecyclerView.adapter = adapter

        return view
    }


    fun init(view: View) {
        loaderLayout = view.findViewById(R.id.loaderLayout)
        searchText = view.findViewById(R.id.searchText)
        contactRecyclerView = view.findViewById(R.id.contactRecyclerView)

        var font = Typeface.createFromAsset(context!!.assets, resources.getString(R.string.roboto_regular))
        searchText.setTypeface(font)

        searchText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun onTextChanged(charSequence: CharSequence, i: Int, i1: Int, i2: Int) {

            }

            override fun afterTextChanged(editable: Editable) {
                var searchText = editable.toString()

                if (searchText.equals("")) {
                    searchList.clear()
                    tempContactList = contactList
                    adapter = ContactRecyclerAdapter(
                        context!!,
                        contactList,
                        onItemClick,
                        onMessageItemClick,
                        onCallItemClick,
                        onWhatsAppItemClick,
                        onDeleteItemClick,
                        onShareItemClick
                    )
                    contactRecyclerView.adapter = adapter
                } else {
                    getSearchValue(searchText)
                }

            }
        })


    }

    override fun onResume() {
        super.onResume()
        callAPI()
    }

    fun callAPI() {
        contactList.clear()
        adapter!!.notifyDataSetChanged()

        if (Utils.contactIndex == 0) {
            getAllContact()
        } else if (Utils.contactIndex == 1) {
            getGroupContact()
        } else if (Utils.contactIndex == 2) {
            getEmergencyContact()
        }
    }


    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            Log.e("position ::", position.toString())
            Log.e("ID ::", tempContactList.get(position).contactId)

            var intent = Intent(activity, AddContactActivity::class.java)
            intent.putExtra(Utils.EDIT_CONTACT, "1")
            intent.putExtra(Utils.CONTACT_OBJ, tempContactList.get(position))
            startActivity(intent)
        }
    }

    var onMessageItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            Log.e("position ::", position.toString())
            var phone = tempContactList.get(position).phone_1
            //  SmsManager.getDefault().sendTextMessage(phone, null, "Hello", null,null);
            val uri = Uri.parse("smsto:$phone")
            val intent = Intent(Intent.ACTION_SENDTO, uri)
            intent.putExtra("sms_body", "Hello")
            startActivity(intent)
        }
    }

    var onCallItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            Log.e("position ::", position.toString())
            var phone = tempContactList.get(position).phone_1
            checkCallPermission(phone)

        }
    }

    fun checkCallPermission(phone: String) {
        val callIntent = Intent(Intent.ACTION_CALL)
        callIntent.data = Uri.parse("tel:" + phone)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.CALL_PHONE
            ) != PERMISSION_GRANTED
        ) {
            requestPermissions(arrayOf(Manifest.permission.CALL_PHONE), Utils.CALL_PERMISSION)
        } else {

            try {
                startActivity(callIntent)
            } catch (e: SecurityException) {
                e.printStackTrace();
            }
        }

    }

    var onWhatsAppItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            Log.e("position ::", position.toString())
            var name = tempContactList.get(position).contactName
            var phone = tempContactList.get(position).phone_1



            try {
                val sendMsg = Intent(Intent.ACTION_VIEW)
                val url = "https://api.whatsapp.com/send?phone=" + "+91" + phone + "&text=" + URLEncoder.encode(
                    "Hello",
                    "UTF-8"
                )
                sendMsg.setPackage("com.whatsapp")
                sendMsg.data = Uri.parse(url)
                if (sendMsg.resolveActivity(context!!.getPackageManager()) != null) {
                    startActivity(sendMsg)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }


        }
    }

    var onDeleteItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            Log.e("position ::", position.toString())
            showDeleteMessageDialog("Are you sure you want to delete this contact ?", position)
        }
    }

    var onShareItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            Log.e("position ::", position.toString())

            var name = tempContactList.get(position).contactName
            var phone = tempContactList.get(position).phone_1

            val intent = Intent(Intent.ACTION_SEND)
            intent.type = "text/plain"
            intent.putExtra(Intent.EXTRA_TEXT, "Name :" + name + " \n Phone Number :" + phone)
            activity!!.startActivity(intent)
        }
    }

    fun getSearchValue(text: String) {
        searchList.clear()
        for (i in 0..(contactList.size - 1)) {
            var name = contactList.get(i).contactName
            var number = contactList.get(i).phone_1
            if ((name.toLowerCase()).contains(text.toLowerCase()) || (number.toLowerCase()).contains(text.toLowerCase())) {
                searchList.add(contactList.get(i))
            }
        }
        tempContactList = searchList
        adapter = ContactRecyclerAdapter(
            context!!,
            tempContactList,
            onItemClick,
            onMessageItemClick,
            onCallItemClick,
            onWhatsAppItemClick,
            onDeleteItemClick,
            onShareItemClick
        )
        contactRecyclerView.adapter = adapter

    }

    //TODO: All Contact........

    private fun getAllContact() {
        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.GET_ALL_CONTACT,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        val dataArray = jsonObj.getJSONArray("data")
                        setContactData(dataArray)
                    } else {
                        contactList.clear()
                        adapter!!.notifyDataSetChanged()
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                Log.e(" JSONData :", params.toString())
                return params
            }
        }

        Log.e(" API :", EndPoints.GET_ALL_CONTACT)
        requestQueue.add(stringRequest)
    }


    //TODO: Emergency Contact........

    private fun getEmergencyContact() {
        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.GET_EMERGENCY_CONTACT,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        val dataArray = jsonObj.getJSONArray("data")
                        setContactData(dataArray)
                    } else {
                        contactList.clear()
                        adapter!!.notifyDataSetChanged()
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                Log.e(" JSONData :", params.toString())
                return params
            }
        }

        Log.e(" API :", EndPoints.GET_EMERGENCY_CONTACT)
        requestQueue.add(stringRequest)
    }

    //TODO: Group Contact........

    private fun getGroupContact() {
        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.GET_GROUP_CONTACT,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        val dataArray = jsonObj.getJSONArray("data")
                        setGroupContactData(dataArray)
                    } else {
                        contactList.clear()
                        adapter!!.notifyDataSetChanged()
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                Log.e(" JSONData :", params.toString())
                return params
            }
        }

        Log.e(" API :", EndPoints.GET_GROUP_CONTACT)
        requestQueue.add(stringRequest)
    }


    fun setContactData(jsonArray: JSONArray) {
        contactList.clear()
        for (i in 0..(jsonArray.length() - 1)) {
            val jsonObj = jsonArray.getJSONObject(i)
            var contactModel = ContactModel()
            contactModel.isTitle = ""
            contactModel.contactId = jsonObj.getString("id")
            contactModel.groupId = jsonObj.getString("group_id")
            contactModel.groupName = jsonObj.getString("group_name")
            var contactName = jsonObj.getString("contact_name")
            contactModel.contactName = contactName
            var firstLetter = contactName.substring(0, 1)
            contactModel.nameFirstLetter = firstLetter
            contactModel.contactPic = jsonObj.getString("profile_pic")
            contactModel.organization_1 = jsonObj.getString("organization")
            contactModel.organization_2 = jsonObj.getString("organization_2")
            contactModel.phone_1 = jsonObj.getString("phone_no")
            contactModel.phone_2 = jsonObj.getString("phone_no_2")
            contactModel.email_1 = jsonObj.getString("email")
            contactModel.email_2 = jsonObj.getString("email_2")
            contactModel.address_1 = jsonObj.getString("address")
            contactModel.address_2 = jsonObj.getString("address_2")
            contactModel.webAddress_1 = jsonObj.getString("web_address")
            contactModel.webAddress_2 = jsonObj.getString("web_address_2")
            contactModel.createAt = jsonObj.getString("created_at")
            contactModel.isEmergency = jsonObj.getString("emergency")

            contactList.add(contactModel)
        }

        tempContactList = contactList
        adapter!!.notifyDataSetChanged()
    }

    fun setGroupContactData(jsonArray: JSONArray) {
        contactList.clear()
        for (k in 0..(jsonArray.length() - 1)) {
            val contactObj = jsonArray.getJSONObject(k)
            val contactArray = contactObj.getJSONArray("contact_list")
            if (contactArray.length() > 0) {
                for (i in 0..(contactArray.length() - 1)) {
                    val jsonObj = contactArray.getJSONObject(i)
                    var contactModel = ContactModel()
                    if (i == 0) {
                        contactModel.isTitle = contactObj.getString("group_name")
                    } else {
                        contactModel.isTitle = ""
                    }
                    contactModel.contactId = jsonObj.getString("id")
                    contactModel.groupId = jsonObj.getString("group_id")
                    var contactName = jsonObj.getString("contact_name")
                    contactModel.contactName = contactName
                    contactModel.groupName = contactObj.getString("group_name")
                    var firstLetter = contactName.substring(0, 1)
                    contactModel.nameFirstLetter = firstLetter
                    contactModel.contactPic = jsonObj.getString("profile_pic")
                    contactModel.organization_1 = jsonObj.getString("organization")
                    contactModel.organization_2 = jsonObj.getString("organization_2")
                    contactModel.phone_1 = jsonObj.getString("phone_no")
                    contactModel.phone_2 = jsonObj.getString("phone_no_2")
                    contactModel.email_1 = jsonObj.getString("email")
                    contactModel.email_2 = jsonObj.getString("email_2")
                    contactModel.address_1 = jsonObj.getString("address")
                    contactModel.address_2 = jsonObj.getString("address_2")
                    contactModel.webAddress_1 = jsonObj.getString("web_address")
                    contactModel.webAddress_2 = jsonObj.getString("web_address_2")
                    contactModel.createAt = jsonObj.getString("created_at")
                    contactModel.isEmergency = jsonObj.getString("emergency")

                    contactList.add(contactModel)
                }
            }
        }
        tempContactList = contactList
        adapter!!.notifyDataSetChanged()
    }


    //TODO: Remove Contact........

    private fun removeContact(id: String) {
        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.REMOVE_CONTACT,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        callAPI()
                    } else {
                        contactList.clear()
                        adapter!!.notifyDataSetChanged()
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["contact_id"] = id
                Log.e(" JSONData :", params.toString())
                return params
            }
        }

        Log.e(" API :", EndPoints.REMOVE_CONTACT)
        requestQueue.add(stringRequest)
    }

    fun showDeleteMessageDialog(message: String, position: Int) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = activity!!.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.confirmation_dialog, null)
        dialog.setContentView(view)

        val cancleBtn = dialog.findViewById(R.id.cancleText) as TextView
        val okBtn = dialog.findViewById(R.id.okBtn) as Button
        val dialogText = dialog.findViewById(R.id.dialogText) as TextView
        dialogText.setText(message)

        val font = Typeface.createFromAsset(activity!!.assets, resources.getString(R.string.popins_semi_bold))
        okBtn.setTypeface(font)

        okBtn.setOnClickListener {
            removeContact(tempContactList.get(position).contactId)
            dialog.dismiss()
        }

        cancleBtn.setOnClickListener {
            dialog.dismiss()
        }

        var window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }


    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {

        if (requestCode == Utils.CALL_PERMISSION) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            } else {
                Toast.makeText(activity, resources.getString(R.string.msg_denied_permission), Toast.LENGTH_LONG).show()
            }
        }

    }


}




