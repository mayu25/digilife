package com.e.digilife.Fragment

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.*
import android.widget.*
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.PersonalListAdapter
import com.e.digilife.Activity.*
import com.e.digilife.Model.PersonalModel
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import java.util.HashMap
import kotlin.collections.ArrayList
import kotlin.collections.set

class PersonalFragment : Fragment() {

    private val TAG = "EMRFragment"
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null

    var adapter: PersonalListAdapter? = null
    private var personalList = ArrayList<PersonalModel>()

    lateinit var emrListview: ListView
    lateinit var loaderLayout: LinearLayout

    var client_id: String = ""
    var docs_id: String = ""

    lateinit var addFolder: ImageView
    lateinit var searchView: ImageView
    lateinit var notificationView: ImageView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_emr, container, false)
        // (activity as DashboardActivity).setActionBarTitle(resources.getString(R.string.personal))
        setHasOptionsMenu(true)

        val toolbar: Toolbar = (activity as DashboardActivity).findViewById(R.id.toolbar)
        var titleText = toolbar.findViewById(R.id.titleText) as TextView
        titleText.setText(resources.getString(R.string.personal))
        var addFolderView = toolbar.findViewById(R.id.addFolderView) as ImageView
        addFolder = toolbar.findViewById(R.id.plusView) as ImageView
        searchView = toolbar.findViewById(R.id.searchView)
        var shareView = toolbar.findViewById(R.id.shareView) as ImageView
        notificationView = toolbar.findViewById(R.id.notificationView)
        addFolderView.visibility = View.GONE
        addFolder.visibility = View.VISIBLE
        shareView.visibility = View.GONE

        requestQueue = Volley.newRequestQueue(activity)

        init(view)

        prefs = activity!!.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
            docs_id = loginObj.getString("Docs_id")
        }

        adapter = PersonalListAdapter(context!!, personalList, onItemClick, onDeleteItemClick)
        emrListview.adapter = adapter



        return view
    }

    override fun onResume() {
        super.onResume()
        if (Utils.checkInternetConnection(context!!))
            getPersonalList("")
        else
            Utils.showMessageDialog(activity!!, getString(R.string.app_name), getString(R.string.check_internet))
    }

    fun init(view: View) {
        loaderLayout = view.findViewById(R.id.loaderLayout)
        emrListview = view.findViewById(R.id.listview)

        addFolder.setOnClickListener(clickListener)
        searchView.setOnClickListener(clickListener)
        notificationView.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.plusView -> {
                showAddFolderDialog()
            }
            R.id.searchView -> {
                val intent = Intent(activity, SearchActivity::class.java)
                startActivity(intent)
            }
            R.id.notificationView -> {
                val intent = Intent(activity, NotificationActivity::class.java)
                startActivity(intent)
            }
        }
    }

    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int

            if (personalList.get(position).personalFolderName.equals("Ticket-Receipt")) {
                val intent = Intent(activity, TicketReceiptActivity::class.java)
                val bundle = Bundle()
                bundle.putString(Utils.PARENT_FOLDER_ID, personalList.get(position).personalId)
                bundle.putString(Utils.TITLE, personalList.get(position).personalFolderName)
                intent.putExtras(bundle)
                startActivity(intent)

            } else if (personalList.get(position).personalFolderName.equals("Insurance Policy")) {
                val intent = Intent(activity, InsuranceActivity::class.java)
                val bundle = Bundle()
                bundle.putString(Utils.PARENT_FOLDER_ID, personalList.get(position).personalId)
                intent.putExtras(bundle)
                startActivity(intent)

            } else {
                val intent = Intent(activity, FileListActivity::class.java)
                val bundle = Bundle()
                bundle.putString(Utils.PARENT_FOLDER_ID, docs_id)
                bundle.putString(Utils.FOLDER_ID, personalList.get(position).personalId)
                bundle.putString(Utils.TITLE, personalList.get(position).personalFolderName)
                intent.putExtras(bundle)
                startActivity(intent)
            }
        }
    }

    var onDeleteItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            Log.e("position:::", position.toString())
            showDeleteMessageDialog("Are You sure you want to delete ?", position)
        }
    }


    private fun getPersonalList(type: String) {

        if (type.equals("")) {
            loaderLayout.visibility = View.VISIBLE
        }

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.GET_FOLDER_LIST,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    //dialog.dismiss()

                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        val dataArray = jsonObj.getJSONArray("data")
                        if (dataArray.length() > 0)
                            setPersonalList(dataArray)

                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["parent_folder_id"] = docs_id
                Log.e(" JSONData :", params.toString())
                return params

            }
        }
        Log.e(" API :", EndPoints.GET_FOLDER_LIST)
        requestQueue.add(stringRequest)
    }


    fun setPersonalList(jsonArray: JSONArray) {

        personalList.clear()
        for (i in 0..(jsonArray.length() - 1)) {
            val jsonObj = jsonArray.getJSONObject(i)
            val personalModel = PersonalModel()
            personalModel.personalId = jsonObj.getString("id")
            personalModel.personalFolderName = jsonObj.getString("folder_name")
            personalModel.personalImgPath = jsonObj.getString("image")
            personalModel.description = jsonObj.getString("description")
            personalList.add(personalModel)
        }
        adapter!!.notifyDataSetChanged()

    }


    //TODO: Delete Image API........
    private fun deleteFolder(id: String, position: Int) {

        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.DELETE_FILE,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        personalList.removeAt(position)
                        adapter!!.notifyDataSetChanged()
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["id"] = id
                params["type"] = "folder"
                return params
                Log.e(" JSONData :", params.toString())
            }
        }

        Log.e(" API :", EndPoints.DELETE_FILE)
        requestQueue.add(stringRequest)
    }


    fun showAddFolderDialog() {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = activity!!.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.create_folder_view, null)
        dialog.setContentView(view)

        val createText = dialog.findViewById(R.id.createText) as TextView
        val folderNameEdittext = dialog.findViewById(R.id.folderNameEdittext) as EditText
        val doneBtn = dialog.findViewById(R.id.doneBtn) as Button
        folderNameEdittext.setHint("Enter Folder Name")
        createText.setText("Create Custom Folder")

        val font = Typeface.createFromAsset(activity!!.assets, resources.getString(R.string.popins_regular))
        folderNameEdittext.setTypeface(font)
        doneBtn.setTypeface(font)

        doneBtn.setOnClickListener {
            if (folderNameEdittext.text.toString().equals("")) {
                Toast.makeText(activity, "Enter Folder Name", Toast.LENGTH_SHORT).show()
            } else {
                if (Utils.checkInternetConnection(context!!))
                    addFolder(folderNameEdittext.text.toString())
                else
                    Utils.showMessageDialog(
                        activity!!,
                        getString(R.string.app_name),
                        getString(R.string.check_internet)
                    )

                dialog.dismiss()
            }
        }
        var window = dialog.getWindow();
        window.setLayout(700, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }

    private fun addFolder(folderName: String) {

        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.ADD_FOLDER,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        getPersonalList("add")
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    } else {
                        loaderLayout.visibility = View.GONE
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["parent_folder_id"] = docs_id
                params["folder_name"] = folderName
                return params
                Log.e(" JSONData :", params.toString())
            }
        }
        Log.e(" API :", EndPoints.ADD_FOLDER)
        requestQueue.add(stringRequest)
    }

    // delete dialog.......

    fun showDeleteMessageDialog(message: String, position: Int) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = activity!!.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.confirmation_dialog, null)
        dialog.setContentView(view)

        val cancleBtn = dialog.findViewById(R.id.cancleText) as TextView
        val okBtn = dialog.findViewById(R.id.okBtn) as Button
        val dialogText = dialog.findViewById(R.id.dialogText) as TextView
        dialogText.setText(message)

        val font = Typeface.createFromAsset(activity!!.assets, resources.getString(R.string.popins_semi_bold))
        okBtn.setTypeface(font)

        okBtn.setOnClickListener {

            deleteFolder(personalList.get(position).personalId, position)
            dialog.dismiss()
        }

        cancleBtn.setOnClickListener {
            dialog.dismiss()
        }

        var window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }

}