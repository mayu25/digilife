package com.e.digilife.Fragment

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.ImageGridviewAdapter
import com.e.digilife.Activity.*
import com.e.digilife.Model.DataModel
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import java.util.HashMap
import kotlin.collections.ArrayList
import kotlin.collections.set

class RecentFragment : Fragment() {

    private val TAG = "EMRFragment"
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null

    var adapter: ImageGridviewAdapter? = null
    private var recentList = ArrayList<DataModel>()

    lateinit var gridview: GridView
    lateinit var loaderLayout: LinearLayout
    lateinit var noDataView: TextView
    lateinit var searchView: ImageView
    lateinit var notificationView: ImageView
    lateinit var shareView:ImageView

    var client_id: String = ""


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_grid_view, container, false)
        setHasOptionsMenu(true)
        val toolbar: Toolbar = (activity as DashboardActivity).findViewById(R.id.toolbar)
        var titleText = toolbar.findViewById(R.id.titleText) as TextView
        var addFolderView = toolbar.findViewById(R.id.addFolderView) as ImageView
        var plusView = toolbar.findViewById(R.id.plusView) as ImageView
        searchView = toolbar.findViewById(R.id.searchView)
        notificationView = toolbar.findViewById(R.id.notificationView)
        shareView = toolbar.findViewById(R.id.shareView)
        addFolderView.visibility = View.GONE
        plusView.visibility = View.GONE
        shareView.visibility = View.VISIBLE
        titleText.setText(resources.getString(R.string.recent))

        requestQueue = Volley.newRequestQueue(activity)
        init(view)

        prefs = activity!!.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }

        adapter = ImageGridviewAdapter(context!!, recentList, onItemClick, onSelectClick, onUnSelectClick)
        gridview.adapter = adapter


        return view
    }

    override fun onResume() {
        super.onResume()
        if (Utils.checkInternetConnection(context!!))
            getRecentList()
        else
            Utils.showMessageDialog(activity!!, getString(R.string.app_name), getString(R.string.check_internet))
    }

    fun init(view: View) {
        noDataView = view.findViewById(R.id.noDataView)
        loaderLayout = view.findViewById(R.id.loaderLayout)
        gridview = view.findViewById(R.id.gridview)
        searchView.setOnClickListener(clickListener)
        notificationView.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {

            R.id.searchView -> {
                val intent = Intent(activity, SearchActivity::class.java)
                startActivity(intent)
            }
            R.id.notificationView -> {
                val intent = Intent(activity, NotificationActivity::class.java)
                startActivity(intent)
            }

        }
    }


    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            var fileType = recentList.get(position).fileType
            if (fileType.equals(Utils.FAVOURITE_MUSIC)) {
                val intent = Intent(context, ImageViewerActivity::class.java)
                intent.putExtra(Utils.FILE_DATA, recentList.get(position))
                intent.putExtra(Utils.TYPE, "")
                intent.putExtra(Utils.FILE_TYPE, Utils.FAVOURITE_MUSIC)
                startActivity(intent)

            } else if (fileType.equals(Utils.DOCUMENT)) {
                val intent = Intent(context, ImageViewerActivity::class.java)
                intent.putExtra(Utils.FILE_DATA, recentList.get(position))
                intent.putExtra(Utils.TYPE, "")
                intent.putExtra(Utils.FILE_TYPE, Utils.DOCUMENT)
                startActivity(intent)
            } else {
                val intent = Intent(context, ImageViewerActivity::class.java)
                intent.putExtra(Utils.FILE_DATA, recentList.get(position))
                intent.putExtra(Utils.TYPE, "")
                intent.putExtra(Utils.FILE_TYPE, "")
                startActivity(intent)
            }
        }
    }

    var onSelectClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            //imageUris.add(Utils.ImageNameClass.geturi(emrList.get(position).emr_filePath, activity))
            recentList.get(position).isSelected = true
        }
    }


    var onUnSelectClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            recentList.get(position).isSelected = false
        }
    }


    private fun getRecentList() {

        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.RECENT_FILES,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    //dialog.dismiss()

                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        val dataArray = jsonObj.getJSONArray("data")
                        if (dataArray.length() > 0) {
                            setRecentList(dataArray)
                        }


                    } else {
                        recentList.clear()
                        noDataView.visibility = View.VISIBLE
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                        adapter!!.notifyDataSetChanged()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["limit"] = "10"
                params["page_no"] = "1"
                Log.e(" JSONData :", params.toString())
                return params
            }
        }

        Log.e(" API :", EndPoints.RECENT_FILES)
        requestQueue.add(stringRequest)
    }


    fun setRecentList(jsonArray: JSONArray) {

        recentList.clear()
        for (i in 0..(jsonArray.length() - 1)) {
            val jsonObj = jsonArray.getJSONObject(i)
            val dataModel = DataModel()
            dataModel.fileId = jsonObj.getString("id")
            dataModel.fileName = jsonObj.getString("file_name")
            var filePath = jsonObj.getString("file_path")
            filePath = filePath.replace(" ", "%20")
            dataModel.filePath = filePath
            var extention = getFileExtention(filePath)
            if (extention.equals("mp3")) {
                dataModel.fileType = Utils.FAVOURITE_MUSIC
            } else if (extention.equals("pdf")) {
                dataModel.fileType = Utils.DOCUMENT
            } else {
                dataModel.fileType = ""
            }
            dataModel.isFav = jsonObj.getString("is_fav")
            dataModel.fileSize = jsonObj.getString("file_size")
            dataModel.parentFolderId = jsonObj.getString("parent_folder_id")
            dataModel.childFolderId = jsonObj.getString("child_folder_id")
            dataModel.createTime = jsonObj.getString("DateTime")
            dataModel.modifiedTime = jsonObj.getString("ModifiedTime")
            recentList.add(dataModel)
        }

        adapter!!.notifyDataSetChanged()
    }

    fun getFileExtention(fileName: String): String {
        val filenameArray = fileName.split("\\.".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        var extention = filenameArray[filenameArray.size - 1]
        return extention
    }

}