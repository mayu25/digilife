package com.e.digilife.Fragment


import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.W_BudgetCategoryRecyclerAdapter
import com.app.towntalk.Adapter.W_BudgetRecyclerAdapter
import com.e.digilife.Activity.AddTrascationActivity
import com.e.digilife.Activity.DashboardActivity
import com.e.digilife.Activity.EditBudgetActivity
import com.e.digilife.Model.WalletModel
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class Wallet_BudgetFragment : Fragment() {


    private var animation: Animation? = null
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null
    internal lateinit var calendar: Calendar

    var client_id: String = ""
    var currency: String = ""
    var currentYear: String = ""
    var currentMonthName: String = ""
    var currentMonth: String = ""
    var saveAmount: String = ""
    var totalAmount: String = ""

    private var dataList = ArrayList<WalletModel>()
    private var categoryBudgetList = ArrayList<WalletModel>()

    var adapter: W_BudgetRecyclerAdapter? = null
    var categoryAdapter: W_BudgetCategoryRecyclerAdapter? = null

    lateinit var editText: TextView
    lateinit var previousView: ImageView
    lateinit var nextView: ImageView
    lateinit var displayText: TextView
    lateinit var monthYearNameText: TextView
    lateinit var topProgressBar: ProgressBar
    lateinit var categoryRecyclerView: RecyclerView
    lateinit var budgetRecyclerView: RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_wallet_budget, container, false)
        init(view)

        requestQueue = Volley.newRequestQueue(activity)
        prefs = activity!!.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)

        animation = AnimationUtils.loadAnimation(activity, R.anim.fade_in)
        currency = prefs!!.getString(Utils.CURRENCY, "")
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }

        calendar = Calendar.getInstance()
        currentYear = calendar.get(Calendar.YEAR).toString()
        @SuppressLint("SimpleDateFormat")
        val month_date = SimpleDateFormat("M")
        @SuppressLint("SimpleDateFormat")
        val month_name = SimpleDateFormat("MMMM")
        currentMonth = month_date.format(calendar.time).toString()
        currentMonthName = month_name.format(calendar.getTime())


        if ((getActivity()) != null) {
            setDisplayText(currentMonthName, currentYear)
        }

        val linearLayoutManager = LinearLayoutManager(activity)
        budgetRecyclerView.setLayoutManager(linearLayoutManager)
        budgetRecyclerView.setHasFixedSize(true)
        adapter = W_BudgetRecyclerAdapter(context!!, dataList, onItemClick, onDeleteItemClick)
        budgetRecyclerView.adapter = adapter

        val linearLayoutManager2 = LinearLayoutManager(activity)
        categoryRecyclerView.setLayoutManager(linearLayoutManager2)
        categoryRecyclerView.setHasFixedSize(true)
        categoryAdapter = W_BudgetCategoryRecyclerAdapter(context!!, categoryBudgetList, onCategoryDeleteItemClick)
        categoryRecyclerView.adapter = categoryAdapter


        return view
    }

    fun init(view: View) {
        displayText = view.findViewById(R.id.displayText)
        previousView = view.findViewById(R.id.previousView)
        nextView = view.findViewById(R.id.nextView)
        monthYearNameText = view.findViewById(R.id.monthYearNameText)
        topProgressBar = view.findViewById(R.id.topProgressBar)
        categoryRecyclerView = view.findViewById(R.id.categoryRecyclerView)
        budgetRecyclerView = view.findViewById(R.id.budgetRecyclerView)
        editText = view.findViewById(R.id.editText)


        previousView.setOnClickListener(clickListener)
        nextView.setOnClickListener(clickListener)
        editText.setOnClickListener(clickListener)
    }

    override fun onResume() {
        super.onResume()
        callBudgetListApi()
        callBudgetCategoryApi()
    }

    fun setDisplayText(month: String, year: String) {
        monthYearNameText.setText("(" + month + " - " + year + ")")
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.editText -> {
                val intent = Intent(activity, EditBudgetActivity::class.java)
                intent.putExtra("month", currentMonth)
                intent.putExtra("year", currentYear)
                intent.putExtra("total_amount", totalAmount)
                intent.putExtra("save_amount", saveAmount)
                startActivity(intent)
            }
            R.id.nextView -> {
                var month = Integer.valueOf(currentMonth)
                var year = Integer.valueOf(currentYear)
                if (month == 12) {
                    month = 0
                    year = year + 1
                }
                month = month + 1
                getUpdatedMonth(month, year.toString())
                callBudgetListApi()
                callBudgetCategoryApi()
            }
            R.id.previousView -> {
                var month = Integer.valueOf(currentMonth)
                var year = Integer.valueOf(currentYear)
                if (month == 1) {
                    month = 13
                    year = year - 1
                }
                month = month - 1
                getUpdatedMonth(month, year.toString())
                callBudgetListApi()
                callBudgetCategoryApi()
            }
        }
    }

    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            Log.e("position ::", position.toString())
            var intent = Intent(activity, AddTrascationActivity::class.java)
            intent.putExtra("transcation", "edit")
            intent.putExtra(Utils.TRANSCATION_DATA, dataList.get(position))
            startActivity(intent)
        }
    }

    var onDeleteItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            Log.e("position:::", position.toString())
            showDeleteMessageDialog("Are you sure want to delete this transcation ?", position)
        }
    }

    var onCategoryDeleteItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            Log.e("position:::", position.toString())
            showCategoryDeleteDialog("Are you sure want to delete this category ?", position)
        }
    }

    //TODO: Budget API........

    private fun callBudgetListApi() {

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.GET_BUDGET,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    Log.e("Budget List :::", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        val dataObj = jsonObj.getJSONObject("data")
                        setBudgetList(dataObj)
                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["month"] = currentMonth
                params["year"] = currentYear
                Log.e(" JSONData :", params.toString())
                return params
            }
        }

        Log.e(" API :", EndPoints.GET_BUDGET)
        requestQueue.add(stringRequest)
    }

    var progressBarValue: Int = 0
    fun setBudgetList(dataObj: JSONObject) {
        totalAmount = dataObj.getString("total_amount")
        var left_amount = dataObj.getString("left_amount")
        saveAmount = dataObj.getString("save_amount")

        var amount = left_amount.substring(0, 1)

        if (totalAmount.equals("0")) {
            progressBarValue = 0

            displayText.text = currency + " " + getFloatValue(totalAmount)
        } else {
            if (amount.equals("-")) {
                progressBarValue = 100
                displayText.text = currency + " " +getFloatValue(totalAmount)
            } else {
                var totalValue = totalAmount.toDouble()
                var leftValue = totalAmount.toDouble() - left_amount.toDouble()
                var value = (leftValue / totalValue) * 100
                progressBarValue = value.toInt()
                displayText.text = currency + " " +getFloatValue(left_amount)
            }
        }

        topProgressBar.setProgress(progressBarValue)

        var budgetArray = dataObj.getJSONArray("budget")

        if (budgetArray.length() > 0) {
            dataList.clear()

            for (i in 0..(budgetArray.length() - 1)) {
                val jsonObj = budgetArray.getJSONObject(i)
                val dataArray = jsonObj.getJSONArray("data")
                if (dataArray.length() > 0) {
                    for (j in 0..(dataArray.length() - 1)) {
                        val dataObj = dataArray.getJSONObject(j)
                        val dataModel = WalletModel()
                        if (j == 0) {
                            dataModel.isTitle = jsonObj.getString("key")
                        } else {
                            dataModel.isTitle = ""
                        }

                        dataModel.currency = currency
                        dataModel.tranId = dataObj.getString("id")
                        dataModel.tranAmount = dataObj.getString("amount")
                        dataModel.tranDescription = dataObj.getString("description")
                        dataModel.trasAttachment = dataObj.getString("attachment")
                        dataModel.tranClaimReimburse = dataObj.getString("is_claim_reimburse")
                        dataModel.trasPurpose = dataObj.getString("purpose")
                        dataModel.trasCategoryId = dataObj.getString("category_id")
                        dataModel.transCompanyName = dataObj.getString("company_name")
                        dataModel.trasnCompnayId = dataObj.getString("company_id")
                        dataModel.trasRenewableType = dataObj.getString("renewable_type")
                        dataModel.trasIsRenewable = dataObj.getString("is_renewable")
                        dataModel.transFullDateTime = dataObj.getString("date_time")
                        var sdDate = dateConvertor(dataObj.getString("date_time"))
                        dataModel.transDate = sdDate
                        dataModel.trasSplit = dataObj.getString("is_split")
                        dataModel.trasSplitData = dataObj.getString("split_data")
                        dataModel.trasCategoryName = dataObj.getString("category_name")
                        dataModel.trasCategoryType = dataObj.getString("category_type")

                        dataList.add(dataModel)
                    }

                }
            }
            adapter!!.notifyDataSetChanged()


        } else {
            dataList.clear()
            adapter!!.notifyDataSetChanged()
        }
    }

    fun getFloatValue(value: String): String {
        val format = DecimalFormat("#,###,###0.00")
        var newPrice = format.format(java.lang.Double.parseDouble(value))
        return  newPrice
    }

    //TODO: Budget Category API........

    private fun callBudgetCategoryApi() {

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.BUDGET_CATEGORY_LIST,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    Log.e("Category List ::", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        val dataArray = jsonObj.getJSONArray("data")
                        if (dataArray.length() > 0) {
                            setBudgetCategoryList(dataArray)
                        } else {
                            categoryBudgetList.clear()
                            categoryAdapter!!.notifyDataSetChanged()
                        }

                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["month"] = currentMonth
                params["year"] = currentYear
                params["cat_id"] = "1"
                Log.e(" JSONData :", params.toString())
                return params
            }
        }

        Log.e(" API :", EndPoints.BUDGET_CATEGORY_LIST)
        requestQueue.add(stringRequest)
    }

    fun setBudgetCategoryList(dataArray: JSONArray) {
        categoryBudgetList.clear()
        for (i in 0..(dataArray.length() - 1)) {
            val jsonObj = dataArray.getJSONObject(i)
            val dataModel = WalletModel()
            dataModel.currency = currency
            dataModel.tranId = jsonObj.getString("id")
            dataModel.categoryID = jsonObj.getString("category_id")
            dataModel.budgetName = jsonObj.getString("name")
            var budgetAmount = jsonObj.getString("amount")
            budgetAmount = getFloatValue(budgetAmount)
            dataModel.budgetAmount = budgetAmount

            var budgetExpense = jsonObj.getString("expense")
            budgetExpense = getFloatValue(budgetExpense)
            dataModel.budgetExpense = budgetExpense
            dataModel.categoryType = jsonObj.getString("category_type")

            categoryBudgetList.add(dataModel)
        }
        categoryAdapter!!.notifyDataSetChanged()
    }

    //TODO : Delete Budget.....................

    private fun deleteTranscation(id: String, pos: Int) {

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.DELETE_TRANSCATION,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        // dataList.removeAt(pos)
                        // adapterW!!.notifyDataSetChanged()
                        callBudgetListApi()
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["id"] = id

                Log.e(" JSONData :", params.toString())
                return params
            }
        }

        Log.e(" API :", EndPoints.DELETE_TRANSCATION)
        requestQueue.add(stringRequest)
    }

    //TODO: Delete Category.............

    private fun deleteCategory(id: String) {

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.DELETE_BUDGET_CATEGORY,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        // dataList.removeAt(pos)
                        // adapterW!!.notifyDataSetChanged()
                        callBudgetCategoryApi()
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["id"] = id

                Log.e(" JSONData :", params.toString())
                return params
            }
        }

        Log.e(" API :", EndPoints.DELETE_BUDGET_CATEGORY)
        requestQueue.add(stringRequest)
    }


    // delete dialog.......

    fun showDeleteMessageDialog(message: String, position: Int) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = activity!!.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.confirmation_dialog, null)
        dialog.setContentView(view)

        val cancleBtn = dialog.findViewById(R.id.cancleText) as TextView
        val okBtn = dialog.findViewById(R.id.okBtn) as Button
        val dialogText = dialog.findViewById(R.id.dialogText) as TextView
        dialogText.setText(message)

        val font = Typeface.createFromAsset(activity!!.assets, resources.getString(R.string.popins_semi_bold))
        okBtn.setTypeface(font)

        okBtn.setOnClickListener {
            deleteTranscation(dataList.get(position).tranId, position)
            dialog.dismiss()
        }

        cancleBtn.setOnClickListener {
            dialog.dismiss()
        }

        var window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }

    fun showCategoryDeleteDialog(message: String, position: Int) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = activity!!.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.confirmation_dialog, null)
        dialog.setContentView(view)

        val cancleBtn = dialog.findViewById(R.id.cancleText) as TextView
        val okBtn = dialog.findViewById(R.id.okBtn) as Button
        val dialogText = dialog.findViewById(R.id.dialogText) as TextView
        dialogText.setText(message)

        val font = Typeface.createFromAsset(activity!!.assets, resources.getString(R.string.popins_semi_bold))
        okBtn.setTypeface(font)

        okBtn.setOnClickListener {
            deleteCategory(categoryBudgetList.get(position).tranId)
            dialog.dismiss()
        }

        cancleBtn.setOnClickListener {
            dialog.dismiss()
        }

        var window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }


    fun dateConvertor(inputText: String): String {

        val outputFormat = SimpleDateFormat("E, MMM dd yyyy", Locale.US)
        val inputFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
        var date: Date? = null
        try {
            date = inputFormat.parse(inputText)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        val outputText = outputFormat.format(date)
        return outputText
    }


    @SuppressLint("SetTextI18n")
    private fun getUpdatedMonth(month: Int, year: String) {
        currentYear = year
        if (month == 1) {
            currentMonth = "1"
            setDisplayText("January", year)
        } else if (month == 2) {
            setDisplayText("February", year)
            currentMonth = "2"
        } else if (month == 3) {
            setDisplayText("March", year)
            currentMonth = "3"
        } else if (month == 4) {
            setDisplayText("April", year)
            currentMonth = "4"
        } else if (month == 5) {
            setDisplayText("May", year)
            currentMonth = "5"
        } else if (month == 6) {
            setDisplayText("June", year)
            currentMonth = "6"
        } else if (month == 7) {
            setDisplayText("July", year)
            currentMonth = "7"
        } else if (month == 8) {
            setDisplayText("August", year)
            currentMonth = "8"
        } else if (month == 9) {
            setDisplayText("September", year)
            currentMonth = "9"
        } else if (month == 10) {
            setDisplayText("October", year)
            currentMonth = "10"
        } else if (month == 11) {
            setDisplayText("November", year)
            currentMonth = "11"
        } else if (month == 12) {
            setDisplayText("December", year)
            currentMonth = "12"
        }

    }


}




