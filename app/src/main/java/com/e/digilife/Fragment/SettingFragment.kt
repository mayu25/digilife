package com.e.digilife.Fragment


import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.app.Fragment
import android.view.*

import android.view.ViewGroup

import android.view.LayoutInflater
import android.widget.EditText
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import com.e.digilife.Activity.MainTaskListActivity
import com.e.digilife.Activity.TagListActivity

import com.e.digilife.R
import com.e.digilife.View.Utils


class SettingFragment : Fragment() {

    lateinit var shareView: ImageView
    lateinit var backView: ImageView
    lateinit var titleText: TextView
    lateinit var lay_tag: LinearLayout
    lateinit var lay_category: LinearLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_setting_layout, container, false)
        init(view)

        return view
    }

    fun init(view: View) {
        shareView = view.findViewById(R.id.shareView)
        backView = view.findViewById(R.id.backView)
        titleText = view.findViewById(R.id.titleText)
        lay_tag = view.findViewById(R.id.lay_tag)
        lay_category = view.findViewById(R.id.lay_category)
        titleText.setText(R.string.settings)
        shareView.visibility = View.GONE;
        backView.visibility = View.GONE;
        lay_tag.setOnClickListener(clickListener)
        lay_category.setOnClickListener(clickListener)

    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {

            R.id.lay_tag -> {

                val intent = Intent(activity, TagListActivity::class.java)
                startActivity(intent)
            }
            R.id.lay_category -> {

                val intent = Intent(activity, MainTaskListActivity::class.java)
                startActivity(intent)
            }

        }
    }

}




