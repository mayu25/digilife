package com.e.digilife.Fragment


import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*


import android.view.ViewGroup
import android.view.LayoutInflater
import android.widget.RelativeLayout
import android.widget.TextView
import com.e.digilife.Activity.*

import com.e.digilife.R
import com.e.digilife.View.Utils


class Wallet_SettingFragment : Fragment() {

    lateinit var currencyLayout: RelativeLayout
    lateinit var currencyText: TextView
    lateinit var categoryLayout: RelativeLayout
    lateinit var renewableTransLayout: RelativeLayout
    lateinit var paymentReceivedLayout: RelativeLayout
    lateinit var reportLayout:RelativeLayout

    var prefs: SharedPreferences? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_wallet_setting, container, false)
        init(view)
        prefs = activity!!.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        var currency = prefs!!.getString(Utils.CURRENCY, "")
        currencyText.setText(currency)
        return view
    }

    fun init(view: View) {
        currencyLayout = view.findViewById(R.id.currencyLayout)
        currencyText = view.findViewById(R.id.currencyText)
        categoryLayout = view.findViewById(R.id.categoryLayout)
        renewableTransLayout = view.findViewById(R.id.renewableTransLayout)
        paymentReceivedLayout = view.findViewById(R.id.paymentReceivedLayout)
        reportLayout = view.findViewById(R.id.reportLayout)

        currencyLayout.setOnClickListener(clickListener)
        categoryLayout.setOnClickListener(clickListener)
        renewableTransLayout.setOnClickListener(clickListener)
        paymentReceivedLayout.setOnClickListener(clickListener)
        reportLayout.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.currencyLayout -> {
                val intent = Intent(activity, CurrencyActivity::class.java)
                startActivityForResult(intent, 101);

            }
            R.id.categoryLayout -> {
                val i = Intent(activity, TrascationCategoryActivity::class.java)
                i.putExtra("type", "Setting")
                startActivityForResult(i, Utils.CATEGORY)
            }
            R.id.renewableTransLayout -> {
                val i = Intent(activity, RenewableTranscationActivity::class.java)
                startActivity(i)
            }
            R.id.paymentReceivedLayout -> {
                val i = Intent(activity, PaymentReceivableActivity::class.java)
                startActivity(i)
            }
            R.id.reportLayout ->{
                val i = Intent(activity, ReportActivity::class.java)
                startActivity(i)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == 101) {
            var symbol = data!!.getStringExtra("currency")
            currencyText.setText(symbol)
            Utils.storeString(prefs, Utils.CURRENCY, symbol)
        }
    }


}




