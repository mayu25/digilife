package com.e.digilife.Fragment

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.GridView
import android.widget.ImageView
import android.widget.TextView
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.OfflineAdpater
import com.e.digilife.Activity.*
import com.e.digilife.Model.DataModel
import com.e.digilife.R
import com.e.digilife.View.Utils
import org.json.JSONArray
import org.json.JSONObject
import kotlin.collections.ArrayList

class OfflineFragment : Fragment() {

    private val TAG = "EMRFragment"
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null

    var adapter: OfflineAdpater? = null
    private var offlineList = ArrayList<DataModel>()

    var client_id: String = ""

    lateinit var gridview: GridView
    lateinit var noDataView: TextView
    lateinit var searchView: ImageView
    lateinit var notificationView: ImageView


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_grid_view, container, false)
        setHasOptionsMenu(true)
        val toolbar: Toolbar = (activity as DashboardActivity).findViewById(R.id.toolbar)
        var titleText = toolbar.findViewById(R.id.titleText) as TextView
        var addFolderView = toolbar.findViewById(R.id.addFolderView) as ImageView
        var plusView = toolbar.findViewById(R.id.plusView) as ImageView
        var shareView = toolbar.findViewById(R.id.shareView) as ImageView
        searchView = toolbar.findViewById(R.id.searchView)
        notificationView = toolbar.findViewById(R.id.notificationView)
        addFolderView.visibility = View.GONE
        plusView.visibility = View.GONE
        shareView.visibility = View.GONE
        titleText.setText(resources.getString(R.string.offline))

        requestQueue = Volley.newRequestQueue(activity)
        init(view)

        prefs = activity!!.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)

        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }

        adapter = OfflineAdpater(context!!, offlineList, onItemClick)
        gridview.adapter = adapter


        return view
    }

    override fun onResume() {
        super.onResume()
        val offlineStr = prefs!!.getString(Utils.SAVE_OFFLINE, "")
        if (!offlineStr.equals("null")) {
            Utils.OFFLINE_IMG_ARRAY = JSONArray()
            Utils.OFFLINE_IMG_ARRAY = JSONArray(offlineStr)
            setOfflineList(Utils.OFFLINE_IMG_ARRAY)
        } else {
            offlineList.clear()
            adapter!!.notifyDataSetChanged()
            noDataView.visibility = View.VISIBLE
        }
    }

    fun init(view: View) {
        noDataView = view.findViewById(R.id.noDataView)
        gridview = view.findViewById(R.id.gridview)

        searchView.setOnClickListener(clickListener)
        notificationView.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.searchView -> {
                val intent = Intent(activity, SearchActivity::class.java)
                startActivity(intent)
            }
            R.id.notificationView -> {
                val intent = Intent(activity, NotificationActivity::class.java)
                startActivity(intent)
            }
        }
    }

    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            var fileType = offlineList.get(position).offlineFileType


            if (fileType.equals(Utils.FAVOURITE_MUSIC)) {
                val intent = Intent(activity, MusicPlayerActivity::class.java)
                val bundle = Bundle()
                bundle.putString("filePath", offlineList.get(position).offlineImagePath)
                bundle.putString(Utils.TYPE, Utils.OFFLINE)
                bundle.putString("fileID", offlineList.get(position).offlineFileId)
                bundle.putString("fileName", offlineList.get(position).offlineFileName)
                intent.putExtras(bundle)
                startActivity(intent)

            } else if (fileType.equals(Utils.DOCUMENT)) {

                val intent = Intent(activity, PDFViewerActivity::class.java)
                val bundle = Bundle()
                bundle.putString("filePath", offlineList.get(position).offlineImagePath)
                bundle.putString(Utils.TYPE, Utils.OFFLINE)
                bundle.putString("fileID",  offlineList.get(position).offlineFileId)
                bundle.putString("fileName", offlineList.get(position).offlineFileName)
                intent.putExtras(bundle)
                startActivity(intent)
            } else {
                val intent = Intent(activity, ZoomImageActivity::class.java)
                val bundle = Bundle()
                bundle.putString("imagePath", offlineList.get(position).offlineImagePath)
                bundle.putString(Utils.TYPE, Utils.OFFLINE)
                bundle.putString("fileID", offlineList.get(position).offlineFileId)
                bundle.putString("fileName", offlineList.get(position).offlineFileName)
                intent.putExtras(bundle)
                startActivity(intent)
            }
        }
    }

    fun setOfflineList(jsonArray: JSONArray) {

        offlineList.clear()

        if (jsonArray.length() > 0) {
            for (i in 0..(jsonArray.length() - 1)) {
                val jsonObj = jsonArray.getJSONObject(i)

                if (client_id.equals(jsonObj.getString(Utils.OFFLINE_FILE_CLIENT_ID))) {
                    val dataModel = DataModel()
                    dataModel.offlineClientId = jsonObj.getString(Utils.OFFLINE_FILE_CLIENT_ID)
                    dataModel.offlineImagePath = jsonObj.getString(Utils.OFFLINE_FILE_PATH)
                    dataModel.offlineFileName = jsonObj.getString(Utils.OFFLINE_FILE_NAME)
                    dataModel.offlineFileId = jsonObj.getString(Utils.OFFLINE_FILE_ID)
                    dataModel.offlineFileType = jsonObj.getString(Utils.OFFLINE_FILE_TYPE)
                    offlineList.add(dataModel)
                }
            }
            adapter!!.notifyDataSetChanged()
        }
    }
}