package com.e.digilife.Fragment

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.Dialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.*

import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.support.annotation.RequiresApi
import android.support.v4.app.DialogFragment
import android.support.v4.content.ContextCompat
import android.support.v4.graphics.drawable.DrawableCompat
import android.support.v7.widget.Toolbar
import android.text.method.PasswordTransformationMethod
import android.util.Log

import android.widget.*
import android.view.ViewGroup
import android.widget.PopupWindow
import android.view.LayoutInflater
import android.widget.RadioGroup
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.DashbordTaskListAdapter
import com.app.towntalk.Adapter.DashbordTaskListDateAdapter
import com.app.towntalk.Adapter.EMRListAdapter
import com.e.digilife.Activity.*
import com.e.digilife.Model.DashbordTaskListModel
import com.e.digilife.Model.DashbordtaskListDateModel
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.github.florent37.singledateandtimepicker.SingleDateAndTimePicker
import com.github.florent37.singledateandtimepicker.dialog.SingleDateAndTimePickerDialog
import com.shurlock.View.EndPoints

import kotlinx.android.synthetic.main.fragment_task_layout.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class TaskFragment : Fragment() {

    private val TAG = "TaskFragment"
    lateinit var list_menu: ImageView
    lateinit var img_close: ImageView
    lateinit var editMessage: EditText
    lateinit var lay_reminder: LinearLayout
    lateinit var img_popup: ImageView
    lateinit var img_add_task: ImageView
    lateinit var mainrl: RelativeLayout
    lateinit var popupWindow: PopupWindow
    lateinit var radio_group: RadioGroup
    lateinit var time: SingleDateAndTimePickerDialog
    lateinit var radio_btn_cuxtom: RadioButton
    lateinit var toolbar_title: TextView
    lateinit var img_plus: ImageView
    lateinit var txt_clear_completed: TextView
    lateinit var buttonSend: ImageView
    lateinit var listview: ListView
    internal lateinit var requestQueue: RequestQueue
    private var taskList = ArrayList<DashbordtaskListDateModel>()
    private var List_wise = ArrayList<DashbordTaskListModel>()
    private var emrList4 = ArrayList<DashbordtaskListDateModel>()
    var prefs: SharedPreferences? = null
    var client_id: String = ""
    lateinit var loaderLayout: LinearLayout
    var adapter_date: DashbordTaskListDateAdapter? = null
    var adapter_list: DashbordTaskListAdapter? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_task_layout, container, false)
        requestQueue = Volley.newRequestQueue(activity)

        init(view)
        prefs = activity!!.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")
        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")

        }
        //  adapter_list = DashbordTaskListAdapter(context!!, List_wise, onItemClick)
        //  listview.adapter = adapter_list

        // get_List_wise_task(" ")
        adapter_date = DashbordTaskListDateAdapter(context!!, taskList, onItemClick)
        listview.adapter = adapter_date

        if (Utils.checkInternetConnection(context!!)) {
            get_task("")
        } else {
            Utils.showMessageDialog(
                context!!,
                getString(R.string.app_name),
                getString(R.string.check_internet)
            )
        }

        focus(view)
        val RadioGroup = view.findViewById(R.id.radio_group) as RadioGroup
        RadioGroup.setOnCheckedChangeListener(object : RadioGroup.OnCheckedChangeListener {
            override fun onCheckedChanged(group: RadioGroup, checkedId: Int) {
                when (checkedId) {
                    R.id.radio_btn_tomorrow -> {
                        radio_btn_tomorrow.setTextColor(getResources().getColor(R.color.whiteColor));
                        radio_btn_nextweek.setTextColor(getResources().getColor(R.color.colorLightTransparent));
                        radio_btn_cuxtom.setTextColor(getResources().getColor(R.color.colorLightTransparent));

                        val drawable_white =
                            context?.let { ContextCompat.getDrawable(it, R.drawable.ic_notification_white) }
                        val drawable_greay =
                            context?.let { ContextCompat.getDrawable(it, R.drawable.ic_notifications_greay) }
                        radio_btn_tomorrow.setCompoundDrawablesWithIntrinsicBounds(drawable_white, null, null, null)
                        radio_btn_nextweek.setCompoundDrawablesWithIntrinsicBounds(drawable_greay, null, null, null)
                        radio_btn_cuxtom.setCompoundDrawablesWithIntrinsicBounds(drawable_greay, null, null, null)

                    }
                    R.id.radio_btn_nextweek -> {

                        radio_btn_tomorrow.setTextColor(getResources().getColor(R.color.colorLightTransparent));
                        radio_btn_nextweek.setTextColor(getResources().getColor(R.color.whiteColor));
                        radio_btn_cuxtom.setTextColor(getResources().getColor(R.color.colorLightTransparent));
                        val drawable_white =
                            context?.let { ContextCompat.getDrawable(it, R.drawable.ic_notification_white) }
                        val drawable_greay =
                            context?.let { ContextCompat.getDrawable(it, R.drawable.ic_notifications_greay) }
                        radio_btn_nextweek.setCompoundDrawablesWithIntrinsicBounds(drawable_white, null, null, null)
                        radio_btn_tomorrow.setCompoundDrawablesWithIntrinsicBounds(drawable_greay, null, null, null)
                        radio_btn_cuxtom.setCompoundDrawablesWithIntrinsicBounds(drawable_greay, null, null, null)

                    }
                    R.id.radio_btn_cuxtom -> {
                        radio_btn_tomorrow.setTextColor(getResources().getColor(R.color.colorLightTransparent));
                        radio_btn_nextweek.setTextColor(getResources().getColor(R.color.colorLightTransparent));
                        radio_btn_cuxtom.setTextColor(getResources().getColor(R.color.whiteColor));
                        val drawable_white =
                            context?.let { ContextCompat.getDrawable(it, R.drawable.ic_notification_white) }
                        val drawable_greay =
                            context?.let { ContextCompat.getDrawable(it, R.drawable.ic_notifications_greay) }
                        radio_btn_cuxtom.setCompoundDrawablesWithIntrinsicBounds(drawable_white, null, null, null)
                        radio_btn_nextweek.setCompoundDrawablesWithIntrinsicBounds(drawable_greay, null, null, null)
                        radio_btn_tomorrow.setCompoundDrawablesWithIntrinsicBounds(drawable_greay, null, null, null)
                        select_date_time(view)
                    }

                }
            }
        })

        return view
    }


    fun init(view: View) {
        list_menu = view.findViewById(R.id.list_menu)
        img_close = view.findViewById(R.id.img_close)
        img_popup = view.findViewById(R.id.img_popup)
        listview = view.findViewById(R.id.listview)

        //img_add_task = view.findViewById(R.id.img_add_task)
        img_plus = view.findViewById(R.id.img_plus)
        toolbar_title = view.findViewById(R.id.toolbar_title)
        editMessage = view.findViewById(R.id.editMessage)
        lay_reminder = view.findViewById(R.id.lay_reminder)
        radio_group = view.findViewById(R.id.radio_group)
        radio_btn_cuxtom = view.findViewById(R.id.radio_btn_cuxtom)
        mainrl = view.findViewById(R.id.mainrl)
        buttonSend = view.findViewById(R.id.buttonSend)
        loaderLayout = view.findViewById(R.id.loaderLayout)
        list_menu.setOnClickListener(clickListener)
        img_popup.setOnClickListener(clickListener)
        img_close.setOnClickListener(clickListener)
        //img_add_task.setOnClickListener(clickListener)
        buttonSend.setOnClickListener(clickListener)

        lay_reminder.visibility = View.GONE
        img_popup.visibility = View.VISIBLE
        img_close.visibility = View.VISIBLE
        img_plus.visibility = View.GONE

        toolbar_title.setText(R.string.alltasks)

    }


    fun select_date_time(view: View) {
        SingleDateAndTimePickerDialog.Builder(context)
            .displayListener(object : SingleDateAndTimePickerDialog.DisplayListener {
                override fun onDisplayed(picker: SingleDateAndTimePicker) {
                    //retrieve the SingleDateAndTimePicker
                }
            })
            .title("Select Date")
            .titleTextColor(getResources().getColor(R.color.whiteColor))
            .backgroundColor(getResources().getColor(R.color.whiteColor))
            .mainColor(getResources().getColor(R.color.blackColor))
            .mustBeOnFuture()
            .curved()
            .listener(object : SingleDateAndTimePickerDialog.Listener {
                override fun onDateSelected(date: Date) {
                    val dateFormat = SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy")
                    var sourceDate: Date? = null
                    try {
                        sourceDate = dateFormat.parse(date.toString())
                    } catch (e: ParseException) {
                        e.printStackTrace()
                    }
                    val targetFormat = SimpleDateFormat("yyyy-MM-dd HH:mm")
                    radio_btn_cuxtom.setText(targetFormat.format(sourceDate));


                }
            }).display()


    }

    fun focus(view: View) {
        editMessage.onFocusChangeListener = View.OnFocusChangeListener { view, hasFocus ->
            if (!hasFocus) {
                lay_reminder.visibility = View.GONE;

            } else {
                lay_reminder.visibility = View.VISIBLE;

            }
        }
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.list_menu -> {
                val intent = Intent(activity, MainTaskListActivity::class.java)
                startActivity(intent)
            }
            R.id.img_popup -> {
                val view2 = layoutInflater.inflate(R.layout.popup_layout, null, false)
                requestQueue = Volley.newRequestQueue(activity)


                popupWindow =
                    PopupWindow(view2, ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
                popupWindow.showAtLocation(view, Gravity.CENTER, 100, 100)
                popupWindow.setFocusable(true);
                popupWindow.update();
                popupWindow.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT));

                txt_clear_completed = view2.findViewById(R.id.txt_clear_completed)
                txt_clear_completed.setOnClickListener {

                    if (Utils.checkInternetConnection(context!!)) {
                        Clear_completed_task("")

                    } else {
                        Utils.showMessageDialog(
                            context!!,
                            getString(R.string.app_name),
                            getString(R.string.check_internet)
                        )
                    }
                }

            }
            R.id.img_close -> {
                activity?.finish()
            }

//            R.id.img_add_task -> {
//                val intent = Intent(activity, AddTaskActivity::class.java)
//                startActivity(intent)
//            }
            R.id.buttonSend -> {
                val intent = Intent(activity, AddTaskActivity::class.java)
                startActivity(intent)
            }


        }
    }

    private fun Clear_completed_task(type: String) {

        if (type.equals("")) {
            loaderLayout.visibility = View.VISIBLE
        }

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.CLEAR_ALL_COMPLETED_TASK,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    loaderLayout.visibility = View.GONE
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {


                    } else {

                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    loaderLayout.visibility = View.GONE
                }
            }
        ) {

            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                return params

            }
        }

        requestQueue.add(stringRequest)
    }

    private fun get_task(type: String) {

        if (type.equals("")) {
            loaderLayout.visibility = View.VISIBLE
        }

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.GET_TASK_LIST_DASHBORD_DATE,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    //dialog.dismiss()

                    loaderLayout.visibility = View.GONE
                    Log.e("response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")

                    if (status.equals("1")) {
                        taskList.clear()
                        Log.d("status", "1")
                        val datajsonobject = jsonObj.getJSONObject("data")

                        val dashbordTaskListDate = DashbordtaskListDateModel()
                        dashbordTaskListDate.isTitle = "Today"
                        taskList.add(dashbordTaskListDate)

                        val dataArray1 = datajsonobject.getJSONArray("today")
                        if (dataArray1.length() > 0) {
                            setValue(dataArray1)
                        }

                        val dashbordTaskListDate2 = DashbordtaskListDateModel()
                        dashbordTaskListDate2.isTitle = "Tomorrow"
                        taskList.add(dashbordTaskListDate2)

                        val dataArray2 = datajsonobject.getJSONArray("tomorrow")
                        if (dataArray2.length() > 0) {
                            setValue(dataArray2)
                        }

                        val dashbordTaskListDate3 = DashbordtaskListDateModel()
                        dashbordTaskListDate3.isTitle = "Upcoming"
                        taskList.add(dashbordTaskListDate3)

                        val dataArray3 = datajsonobject.getJSONArray("upcoming")
                        if (dataArray3.length() > 0) {
                            setValue(dataArray3)
                        }

                        val dashbordTaskListDate4 = DashbordtaskListDateModel()
                        dashbordTaskListDate4.isTitle = "Pending"
                        taskList.add(dashbordTaskListDate4)

                        val dataArray4 = datajsonobject.getJSONArray("pending")
                        if (dataArray4.length() > 0) {
                            setValue(dataArray4)
                        }
                        adapter_date!!.notifyDataSetChanged()


                    } else {

                        taskList.clear()

                        Log.d("sadjagsjgd", "0")
                        //  Toast.makeText(this@AddTaskActivity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {
                    Log.d("sadjagsjgd", error.toString())
                }
            }
        ) {

            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id

                return params
                Log.e(" JSONData :", params.toString())
            }
        }

        Log.e(" API :", EndPoints.GET_FOLDER_LIST)
        requestQueue.add(stringRequest)
    }


    fun setValue(dataArray: JSONArray) {
        for (i in 0..(dataArray.length() - 1)) {
            val jsonObj4 = dataArray.getJSONObject(i)
            val dashbordTaskListDate = DashbordtaskListDateModel()
            dashbordTaskListDate.isTitle = ""
            dashbordTaskListDate.task_id = jsonObj4.getString("task_id")
            dashbordTaskListDate.share_task_id = jsonObj4.getString("share_task_id")
            dashbordTaskListDate.client_id = jsonObj4.getString("client_id")
            dashbordTaskListDate.owner_id = jsonObj4.getString("owner_id")
            dashbordTaskListDate.task_title = jsonObj4.getString("task_title")
            dashbordTaskListDate.task_tag = jsonObj4.getString("task_tag")
            dashbordTaskListDate.reminder_type = jsonObj4.getString("reminder_type")
            dashbordTaskListDate.onetime_type = jsonObj4.getString("onetime_type")
            dashbordTaskListDate.register_date_time = jsonObj4.getString("register_date_time")
            dashbordTaskListDate.update_date_time = jsonObj4.getString("update_date_time")
            dashbordTaskListDate.repeat_type = jsonObj4.getString("repeat_type")
            dashbordTaskListDate.repeat_day = jsonObj4.getString("repeat_day")
            dashbordTaskListDate.repeat_week = jsonObj4.getString("repeat_week")
            dashbordTaskListDate.repeat_on = jsonObj4.getString("repeat_on")
            dashbordTaskListDate.repeat_month = jsonObj4.getString("repeat_month")
            dashbordTaskListDate.repeat_year = jsonObj4.getString("repeat_year")
            dashbordTaskListDate.loc_address = jsonObj4.getString("loc_address")
            dashbordTaskListDate.curr_lat = jsonObj4.getString("curr_lat")
            dashbordTaskListDate.curr_long = jsonObj4.getString("curr_long")
            dashbordTaskListDate.add_lat = jsonObj4.getString("add_lat")
            dashbordTaskListDate.add_long = jsonObj4.getString("add_long")
            dashbordTaskListDate.cat_id = jsonObj4.getString("cat_id")
            dashbordTaskListDate.loc_reminder_type = jsonObj4.getString("loc_reminder_type")
            dashbordTaskListDate.loc_type = jsonObj4.getString("loc_type")
            dashbordTaskListDate.adress = jsonObj4.getString("adress")
            dashbordTaskListDate.latitude = jsonObj4.getString("latitude")
            dashbordTaskListDate.longitude = jsonObj4.getString("longitude")
            dashbordTaskListDate.sub_task_name = jsonObj4.getString("sub_task_name")
            dashbordTaskListDate.notes = jsonObj4.getString("notes")
            dashbordTaskListDate.attachment = jsonObj4.getString("attachment")
            dashbordTaskListDate.task_status = jsonObj4.getString("task_status")
            dashbordTaskListDate.noti_status = jsonObj4.getString("noti_status")
            dashbordTaskListDate.share_status = jsonObj4.getString("share_status")
            dashbordTaskListDate.create_at = jsonObj4.getString("create_at")
            dashbordTaskListDate.update_at = jsonObj4.getString("update_at")
            dashbordTaskListDate.cat_name = jsonObj4.getString("cat_name")
            taskList.add(dashbordTaskListDate)
        }
    }

    private fun get_List_wise_task(type: String) {

        if (type.equals("")) {
            loaderLayout.visibility = View.VISIBLE
        }

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.GET_CATEGORY_TASK_LIST,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {
                    //dialog.dismiss()

                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        val dataArray = jsonObj.getJSONArray("data")
                        if (dataArray.length() > 0)
                            set_list_wise_task(dataArray)

                    } else {
                        //  Toast.makeText(this@AddTaskActivity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {

            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id

                return params
                Log.e(" JSONData :", params.toString())
            }
        }

        Log.e(" API :", EndPoints.GET_FOLDER_LIST)
        requestQueue.add(stringRequest)
    }


    fun set_list_wise_task(jsonArray: JSONArray) {

        List_wise.clear()
        for (i in 0..(jsonArray.length() - 1)) {
            val jsonObj = jsonArray.getJSONObject(i)
            val maintask = DashbordTaskListModel()
            maintask.name = jsonObj.getString("name")
            val dataArray2 = jsonObj.getJSONArray("sub_data")
            if (dataArray2.length() > 0) {
                for (i in 0..(dataArray2.length() - 1)) {
                    val jsonObj2 = dataArray2.getJSONObject(i)
                    maintask.task_id = jsonObj2.getString("task_id")
                    maintask.task_title = jsonObj2.getString("task_title")
                    Log.d("sdfsdsfsfsff", jsonObj2.getString("task_id"))
                }

            }

            List_wise.add(maintask)
        }

        adapter_list!!.notifyDataSetChanged()

    }

    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val intent = Intent(activity, AddTaskActivity::class.java)
            startActivity(intent)


        }
    }

    override fun onResume() {
        super.onResume()

        if (Utils.checkInternetConnection(context!!)) {
            get_task("")
        } else {
            Utils.showMessageDialog(
                context!!,
                getString(R.string.app_name),
                getString(R.string.check_internet)
            )
        }

    }
}




