package com.e.digilife.Fragment

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.NetworkImageView
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.e.digilife.Activity.*
import com.e.digilife.Adapter.DashboardViewpagerAdapter
import com.e.digilife.Model.ArticleModel
import com.e.digilife.Model.DataModel
import com.e.digilife.R
import com.e.digilife.View.CustomVolleyRequest
import com.e.digilife.View.Utils
import com.google.android.gms.vision.text.Line
import com.shurlock.View.EndPoints
import com.squareup.picasso.Picasso
import org.json.JSONArray
import org.json.JSONObject
import org.w3c.dom.Text
import java.util.HashMap

class DashboardFragment : Fragment() {

    private val TAG = "DashboardFragment"
    internal lateinit var requestQueue: RequestQueue

    private val TOP_SLIDER_IMAGES = arrayOf<Int>(
        R.mipmap.dashboard_bg_big,
        R.mipmap.dashboard_bg_big,
        R.mipmap.dashboard_bg_big,
        R.mipmap.dashboard_bg_big
    )

    lateinit var viewPager: ViewPager
    lateinit var loaderLayout: LinearLayout

    lateinit var walletLayout: LinearLayout
    lateinit var emrLayout: LinearLayout
    lateinit var contactLayout: LinearLayout
    lateinit var taskLayout: LinearLayout
    lateinit var personalLayout: LinearLayout

    lateinit var favouritesLayout: LinearLayout
    lateinit var recentLayout: LinearLayout
    lateinit var addFolder: ImageView
    lateinit var searchView: ImageView
    lateinit var notificationView: ImageView

    //For article .......
    lateinit var articleViewLayout: LinearLayout
    lateinit var articleView: NetworkImageView
    lateinit var articleTimeText: TextView
    lateinit var articleTitleText: TextView
    lateinit var articleDescriptionText: TextView
    lateinit var articleShareLayout: LinearLayout

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_dashboard, container, false)
        setHasOptionsMenu(true)
        requestQueue = Volley.newRequestQueue(activity)

        val toolbar: Toolbar = (activity as DashboardActivity).findViewById(R.id.toolbar)
        addFolder = toolbar.findViewById(R.id.addFolderView)
        var titleText = toolbar.findViewById(R.id.titleText) as TextView
        var plusView = toolbar.findViewById(R.id.plusView) as ImageView
        var shareView = toolbar.findViewById(R.id.shareView) as ImageView
        notificationView = toolbar.findViewById(R.id.notificationView)
        searchView = toolbar.findViewById(R.id.searchView)
        addFolder.visibility = View.VISIBLE
        plusView.visibility = View.GONE
        shareView.visibility = View.GONE
        titleText.setText(resources.getString(R.string.app_name))

        init(view)
        initTopViewPager()

        if (Utils.checkInternetConnection(context!!))
            getArticleList()
        else
            Utils.showMessageDialog(activity!!, getString(R.string.app_name), getString(R.string.check_internet))

        return view
    }

    fun init(view: View) {
        viewPager = view.findViewById(R.id.viewPager)
        loaderLayout = view.findViewById(R.id.loaderLayout)

        walletLayout = view.findViewById(R.id.walletLayout)
        emrLayout = view.findViewById(R.id.emrLayout)
        contactLayout = view.findViewById(R.id.contactLayout)
        personalLayout = view.findViewById(R.id.personalLayout)
        favouritesLayout = view.findViewById(R.id.favouritesLayout)
        recentLayout = view.findViewById(R.id.recentLayout)
        taskLayout = view.findViewById(R.id.taskLayout)

        walletLayout.setOnClickListener(clickListener)
        emrLayout.setOnClickListener(clickListener)
        contactLayout.setOnClickListener(clickListener)
        taskLayout.setOnClickListener(clickListener)
        personalLayout.setOnClickListener(clickListener)
        favouritesLayout.setOnClickListener(clickListener)
        recentLayout.setOnClickListener(clickListener)
        addFolder.setOnClickListener(clickListener)
        searchView.setOnClickListener(clickListener)
        notificationView.setOnClickListener(clickListener)

        //For article....
        articleViewLayout = view.findViewById(R.id.articleViewLayout)
        articleView = view.findViewById(R.id.articleView)
        articleTimeText = view.findViewById(R.id.articleTimeText)
        articleTitleText = view.findViewById(R.id.articleTitleText)
        articleDescriptionText = view.findViewById(R.id.articleDescriptionText)
        articleShareLayout = view.findViewById(R.id.articleShareLayout)

        articleViewLayout.setOnClickListener(clickListener)
        articleShareLayout.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.addFolderView -> {
                val intent = Intent(activity, CustomFolderActivity::class.java)
                startActivity(intent)
            }
            R.id.searchView -> {
                val intent = Intent(activity, SearchActivity::class.java)
                startActivity(intent)
            }
            R.id.notificationView -> {
                val intent = Intent(activity, NotificationActivity::class.java)
                startActivity(intent)
            }
            R.id.taskLayout -> {
                val intent = Intent(activity, TaskMainActivity::class.java)
                startActivity(intent)
            }
            R.id.walletLayout -> {
                val intent = Intent(activity, WalletActivity::class.java)
                startActivity(intent)
            }
            R.id.emrLayout -> {
                val emrFragment = EMRFragment()
                replaceFragment(emrFragment)
                Utils.navItemIndex = 4
            }
            R.id.personalLayout -> {
                val personalFragment = PersonalFragment()
                replaceFragment(personalFragment)
                Utils.navItemIndex = 5
            }
            R.id.contactLayout -> {
                val intent = Intent(activity, ContactActivity::class.java)
                startActivity(intent)
            }
            R.id.favouritesLayout -> {
                val favFragment = FavoriteFragment()
                replaceFragment(favFragment)
                Utils.navItemIndex = 10
            }
            R.id.recentLayout -> {
                val recentFragment = RecentFragment()
                replaceFragment(recentFragment)
                Utils.navItemIndex = 11
            }
            R.id.articleViewLayout -> {
                val intent = Intent(activity, ArticleListActivity::class.java)
                val bundle = Bundle()
                bundle.putSerializable("ArticleList", articleList)
                intent.putExtras(bundle)
                startActivity(intent)
            }
            R.id.articleShareLayout -> {
                try {
                    val shareIntent = Intent(Intent.ACTION_SEND)
                    shareIntent.type = "text/plain"
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Digilife")
                    var shareMessage = articleList.get(0).articleTitle
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                    startActivity(Intent.createChooser(shareIntent, "choose one"))
                } catch (e: Exception) {
                    //e.toString();
                }

            }
        }
    }

    private var articleList = ArrayList<ArticleModel>()


    private fun getArticleList() {

        loaderLayout.visibility = View.VISIBLE

        val stringRequest = object : StringRequest(
            Request.Method.GET, EndPoints.ARTICLE_LIST,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    loaderLayout.visibility = View.GONE
                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {

                        val dataArray = jsonObj.getJSONArray("data")
                        if (dataArray.length() > 0) {
                            setArticlesData(dataArray)
                        }

                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }) {

        }

        Log.e(" API :", EndPoints.ARTICLE_LIST)
        requestQueue.add(stringRequest)
    }


    fun setArticlesData(jsonArray: JSONArray) {
        articleList.clear()
        for (i in 0..(jsonArray.length() - 1)) {
            val jsonObj = jsonArray.getJSONObject(i)
            val articleModel = ArticleModel()
            articleModel.articleId = jsonObj.getString("id")
            articleModel.articleTitle = jsonObj.getString("article_title")
            articleModel.articleImage = jsonObj.getString("article_image")
            articleModel.articleDescription = jsonObj.getString("article_description")
            articleModel.articelDate = jsonObj.getString("article_date")
            articleList.add(articleModel)
        }

        articleTimeText.text = articleList.get(0).articelDate
        articleTitleText.text = articleList.get(0).articleTitle
        articleDescriptionText.text = articleList.get(0).articleDescription

        var articleImgPath = articleList.get(0).articleImage
        var imageLoader = CustomVolleyRequest.getInstance(context).getImageLoader();


        if (!articleImgPath.equals("")) {
            imageLoader!!.get(
                articleImgPath,
                ImageLoader.getImageListener(articleView, R.mipmap.place_holder_img, R.mipmap.place_holder_img)
            )
            articleView.setImageUrl(articleImgPath, imageLoader)

        }
    }

    private fun initTopViewPager() {
        val viewpagerAdapter = DashboardViewpagerAdapter(
            context,
            TOP_SLIDER_IMAGES, R.layout.dashboard_viewpager
        )

        viewPager.setAdapter(viewpagerAdapter)
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                Log.e(TAG, "onPageScrolled==position==$position")
            }

            override fun onPageSelected(position: Int) {
                Log.e(TAG, "onPageSelected==state==$position")
            }

            override fun onPageScrollStateChanged(state: Int) {
                Log.e(TAG, "onPageScrollStateChanged==state==$state")
            }
        })
    }

    fun replaceFragment(fragment: Fragment) {
        activity!!.supportFragmentManager.beginTransaction().replace(R.id.frameLayout, fragment).commit()
    }

    override fun onResume() {
        super.onResume()
    }

}