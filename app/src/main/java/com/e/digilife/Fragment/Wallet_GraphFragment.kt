package com.e.digilife.Fragment


import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.W_GraphRecyclerAdapter
import com.e.digilife.Model.WalletModel
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.shurlock.View.EndPoints
import org.json.JSONArray
import org.json.JSONObject
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class Wallet_GraphFragment : Fragment() {

    private var animation: Animation? = null
    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null
    lateinit var firstTextview: TextView
    lateinit var middleTextview: TextView
    lateinit var lastTextview: TextView
    lateinit var previousView: ImageView
    lateinit var nextView: ImageView
    lateinit var displayText: TextView
    lateinit var pie_Chart: PieChart
    lateinit var graphRecyclerView: RecyclerView
    lateinit var noValueText: TextView

    var client_id: String = ""
    var currency: String = ""
    var currentYear: String = ""
    var currentMonthName: String = ""
    var currentMonth: String = ""
    var transactionType: String = ""
    var weekNumber: Int = 0
    var weeksOfYear: Int = 0
    var startDate: String = ""
    var endDate: String = ""


    internal lateinit var calendar: Calendar

    private var dataList = ArrayList<WalletModel>()
    var adapter: W_GraphRecyclerAdapter? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_wallet_graph, container, false)
        init(view)
        requestQueue = Volley.newRequestQueue(activity)
        prefs = activity!!.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)

        animation = AnimationUtils.loadAnimation(activity, R.anim.fade_in)
        currency = prefs!!.getString(Utils.CURRENCY, "")
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")

        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }

        calendar = Calendar.getInstance()
        currentYear = calendar.get(Calendar.YEAR).toString()
        @SuppressLint("SimpleDateFormat")
        val month_date = SimpleDateFormat("MM")
        @SuppressLint("SimpleDateFormat")
        val month_name = SimpleDateFormat("MMMM")
        currentMonth = month_date.format(calendar.time)
        currentMonthName = month_name.format(calendar.getTime())

        if ((getActivity()) != null) {
            displayText.text = currentMonthName
        }

        transactionType = Utils.TRANSACTION_MONTHLY

        weekNumber = calendar.get(Calendar.WEEK_OF_YEAR)
        weeksOfYear = calendar.getActualMaximum(Calendar.WEEK_OF_YEAR)


        val linearLayoutManager = LinearLayoutManager(activity)
        graphRecyclerView.setLayoutManager(linearLayoutManager)
        graphRecyclerView.setHasFixedSize(true)
        adapter = W_GraphRecyclerAdapter(context!!, dataList)
        graphRecyclerView.adapter = adapter



        callGraphListApi()




        return view
    }

    fun init(view: View) {
        noValueText = view.findViewById(R.id.noValueText)
        firstTextview = view.findViewById(R.id.firstTextview)
        middleTextview = view.findViewById(R.id.middleTextview)
        lastTextview = view.findViewById(R.id.lastTextview)
        displayText = view.findViewById(R.id.displayText)
        previousView = view.findViewById(R.id.previousView)
        nextView = view.findViewById(R.id.nextView)

        pie_Chart = view.findViewById(R.id.pie_Chart)

        graphRecyclerView = view.findViewById(R.id.graphRecyclerView)

        firstTextview.setOnClickListener(clickListener)
        lastTextview.setOnClickListener(clickListener)
        previousView.setOnClickListener(clickListener)
        nextView.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {

            R.id.nextView -> {
                if (middleTextview.getText().toString().equals(Utils.lbl_week, ignoreCase = true)) {
                    //getUpdateDate(+7, aysBeforeDate)
                    if (weekNumber == weeksOfYear) {
                        weekNumber = 1
                        getUpdateWeek(weekNumber)
                    } else {
                        weekNumber = weekNumber + 1
                        getUpdateWeek(weekNumber)
                    }

                } else if (middleTextview.getText().toString().equals(Utils.lbl_month, ignoreCase = true)) {

                    var month = Integer.valueOf(currentMonth)
                    if (month == 12) {
                        month = 0
                    }
                    month = month + 1
                    getUpdatedMonth(month)
                } else if (middleTextview.getText().toString().equals(Utils.lbl_year, ignoreCase = true)) {
                    var Year = Integer.valueOf(currentYear)
                    Year = Year + 1
                    getUpdatedYear(Year)
                }
                callGraphListApi()
            }
            R.id.previousView -> {
                if (middleTextview.getText().toString().equals(Utils.lbl_week, ignoreCase = true)) {
                    if (weekNumber == 1) {
                        weekNumber = weeksOfYear
                        getUpdateWeek(weekNumber)
                    } else {
                        weekNumber = weekNumber - 1
                        getUpdateWeek(weekNumber)
                    }
                } else if (middleTextview.getText().toString().equals(Utils.lbl_month, ignoreCase = true)) {

                    var month = Integer.valueOf(currentMonth)
                    if (month == 1) {
                        month = 13
                    }
                    month = month - 1
                    getUpdatedMonth(month)
                } else if (middleTextview.getText().toString().equals(Utils.lbl_year, ignoreCase = true)) {
                    var Year = Integer.valueOf(currentYear)
                    Year = Year - 1
                    getUpdatedYear(Year)
                }
                callGraphListApi()
            }
            R.id.firstTextview -> {
                if (middleTextview.getText().toString().equals(Utils.lbl_week, ignoreCase = true)) {
                    middleTextview.setText(firstTextview.getText())
                } else if (middleTextview.getText().toString().equals(Utils.lbl_month, ignoreCase = true)) {
                    middleTextview.setText(firstTextview.getText())
                } else if (middleTextview.getText().toString().equals(Utils.lbl_year, ignoreCase = true)) {
                    middleTextview.setText(firstTextview.getText())
                }

                if (middleTextview.getText().toString().equals(Utils.lbl_week, ignoreCase = true)) {
                    lastTextview.setText(Utils.lbl_year)
                    firstTextview.setText(Utils.lbl_month)
                } else if (middleTextview.getText().toString().equals(Utils.lbl_month, ignoreCase = true)) {
                    firstTextview.setText(Utils.lbl_week)
                    lastTextview.setText(Utils.lbl_year)
                } else if (middleTextview.getText().toString().equals(Utils.lbl_year, ignoreCase = true)) {
                    firstTextview.setText(Utils.lbl_week)
                    lastTextview.setText(Utils.lbl_month)
                }

                if (middleTextview.getText().toString().equals(Utils.lbl_week, ignoreCase = true)) {
                    displayText.setText("Week " + weekNumber)
                    getDateFromWeek(weekNumber, calendar.get(Calendar.YEAR))
                    transactionType = Utils.TRANSACTION_WEEKLY
                } else if (middleTextview.getText().toString().equals(Utils.lbl_month, ignoreCase = true)) {
                    displayText.setText(currentMonthName)
                    transactionType = Utils.TRANSACTION_MONTHLY
                    setCurrentMonthValue(currentMonthName)
                } else if (middleTextview.getText().toString().equals(Utils.lbl_year, ignoreCase = true)) {
                    currentYear = calendar.get(Calendar.YEAR).toString()
                    displayText.setText(currentYear)
                    transactionType = Utils.TRANSACTION_YEARLY
                }

                middleTextview.startAnimation(animation)
                firstTextview.startAnimation(animation)
                displayText.startAnimation(animation)
                callGraphListApi()


            }
            R.id.lastTextview -> {


                if (middleTextview.getText().toString().equals(Utils.lbl_week, ignoreCase = true)) {
                    middleTextview.setText(lastTextview.getText())
                } else if (middleTextview.getText().toString().equals(Utils.lbl_month, ignoreCase = true)) {
                    middleTextview.setText(lastTextview.getText())
                } else if (middleTextview.getText().toString().equals(Utils.lbl_year, ignoreCase = true)) {
                    middleTextview.setText(lastTextview.getText())
                }

                if (middleTextview.getText().toString().equals(Utils.lbl_week, ignoreCase = true)) {
                    lastTextview.setText(Utils.lbl_year)
                    firstTextview.setText(Utils.lbl_month)
                    transactionType = Utils.TRANSACTION_WEEKLY
                } else if (middleTextview.getText().toString().equals(Utils.lbl_month, ignoreCase = true)) {
                    firstTextview.setText(Utils.lbl_week)
                    lastTextview.setText(Utils.lbl_year)
                    transactionType = Utils.TRANSACTION_MONTHLY
                } else if (middleTextview.getText().toString().equals(Utils.lbl_year, ignoreCase = true)) {
                    firstTextview.setText(Utils.lbl_week)
                    lastTextview.setText(Utils.lbl_month)
                    transactionType = Utils.TRANSACTION_YEARLY
                }

                if (middleTextview.getText().toString().equals(Utils.lbl_week, ignoreCase = true)) {
                    displayText.setText("Week " + weekNumber)
                    getDateFromWeek(weekNumber, calendar.get(Calendar.YEAR))
                } else if (middleTextview.getText().toString().equals(Utils.lbl_month, ignoreCase = true)) {
                    displayText.setText(currentMonthName)
                    setCurrentMonthValue(currentMonthName)
                } else if (middleTextview.getText().toString().equals(Utils.lbl_year, ignoreCase = true)) {
                    currentYear = calendar.get(Calendar.YEAR).toString()
                    displayText.setText(currentYear)
                }

                middleTextview.startAnimation(animation)
                lastTextview.startAnimation(animation)
                displayText.startAnimation(animation)

                callGraphListApi()
            }
        }
    }

    //TODO: Expense Income API........

    private fun callGraphListApi() {

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.GET_GRAPH,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        val dataArray = jsonObj.getJSONArray("data")
                        if (dataArray.length() > 0) {
                            setGraphList(dataArray)
                        }


                    } else {
                        noValueText.visibility = View.VISIBLE
                        pie_Chart.visibility = View.GONE
                        dataList.clear()
                        adapter!!.notifyDataSetChanged()
                        pie_Chart.clear()
                        pie_Chart.notifyDataSetChanged()
                        pie_Chart.invalidate()
                        // Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["type"] = transactionType
                params["start_date"] = startDate
                params["month_no"] = currentMonth
                params["year"] = currentYear
                Log.e(" JSONData :", params.toString())
                return params
            }
        }

        Log.e(" API :", EndPoints.GET_GRAPH)
        requestQueue.add(stringRequest)
    }


    fun setGraphList(jsonArray: JSONArray) {
        dataList.clear()
        for (i in 0..(jsonArray.length() - 1)) {
            val jsonObj = jsonArray.getJSONObject(i)

            val dataArray = jsonObj.getJSONArray("data")

            for (j in 0..(dataArray.length() - 1)) {
                val dataObj = dataArray.getJSONObject(j)
                val dataModel = WalletModel()
                if (j == 0) {
                    dataModel.expenseDate = jsonObj.getString("date")
                    dataModel.isExpense = "1"
                } else {
                    dataModel.isExpense = ""
                }
                dataModel.currency = currency
                dataModel.tranId = dataObj.getString("id")
                dataModel.tranAmount = getFloatValue(dataObj.getString("amount"))
                var sdDate = dateConvertor(dataObj.getString("date1"))
                dataModel.transDate = sdDate
                dataModel.trasCategoryName = dataObj.getString("category_name")
                dataModel.trasPersentage = dataObj.getString("percentage")
                dataModel.percentageColor = 0
                dataList.add(dataModel)
            }
            adapter!!.notifyDataSetChanged()
        }

        setUpPieChartData()

    }

    fun getFloatValue(value: String): String {
        val format = DecimalFormat("#,###,###0.00")
        var newPrice = format.format(java.lang.Double.parseDouble(value))
        return newPrice
    }


    private fun setUpPieChartData() {
        noValueText.visibility = View.GONE
        pie_Chart.visibility = View.VISIBLE

        val yVals = ArrayList<PieEntry>()

        val colors = java.util.ArrayList<Int>()

        for (j in 0..(dataList.size - 1)) {
            val rnd = Random()
            var currentStrokeColor = Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))

            var persentage = dataList.get(j).trasPersentage
            var per = (persentage.replace("%", "") + "f").toFloat()
            dataList.get(j).percentageColor = currentStrokeColor
            colors.add(currentStrokeColor)
            yVals.add(PieEntry(per))
        }
        adapter!!.notifyDataSetChanged()

        pie_Chart.notifyDataSetChanged()
        pie_Chart.invalidate()

        val dataSet = PieDataSet(yVals, "")
        dataSet.valueTextSize = 0f


        dataSet.setColors(colors)

        val data = PieData(dataSet)
        pie_Chart.data = data
        pie_Chart.centerTextRadiusPercent = 0f
        pie_Chart.isDrawHoleEnabled = false
        pie_Chart.legend.isEnabled = false
        pie_Chart.animateY(1000)
        pie_Chart.description.isEnabled = false
    }


    fun dateConvertor(inputText: String): String {

        val outputFormat = SimpleDateFormat("E, MMM dd yyyy", Locale.US)
        val inputFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)

        var date: Date? = null
        try {
            date = inputFormat.parse(inputText)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val outputText = outputFormat.format(date)

        return outputText
    }


    //TODO: Year Month and week  ........

    fun getUpdateWeek(weekNo: Int) {
        displayText.text = "Week " + weekNo
        getDateFromWeek(weekNo, calendar.get(Calendar.YEAR))
    }


    fun getDateFromWeek(weekNo: Int, year: Int) {
        val cal = GregorianCalendar.getInstance()
        cal.clear()
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.WEEK_OF_YEAR, weekNo)
        startDate = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(cal.time)
        cal.add(Calendar.DAY_OF_YEAR, 6)
        endDate = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(cal.time)
    }

    @SuppressLint("SetTextI18n")
    private fun getUpdatedMonth(month: Int) {
        if (month == 1) {
            currentMonth = "01"
            displayText.setText("January")
        } else if (month == 2) {
            displayText.setText("February")
            currentMonth = "02"
        } else if (month == 3) {
            displayText.setText("March")
            currentMonth = "03"
        } else if (month == 4) {
            displayText.setText("April")
            currentMonth = "04"
        } else if (month == 5) {
            displayText.setText("May")
            currentMonth = "05"
        } else if (month == 6) {
            displayText.setText("June")
            currentMonth = "06"
        } else if (month == 7) {
            displayText.setText("July")
            currentMonth = "07"
        } else if (month == 8) {
            displayText.setText("August")
            currentMonth = "08"
        } else if (month == 9) {
            displayText.setText("September")
            currentMonth = "09"
        } else if (month == 10) {
            displayText.setText("October")
            currentMonth = "10"
        } else if (month == 11) {
            displayText.setText("November")
            currentMonth = "11"
        } else if (month == 12) {
            displayText.setText("December")
            currentMonth = "12"
        }

        transactionType = Utils.TRANSACTION_MONTHLY
    }

    private fun getUpdatedYear(year: Int) {
        displayText.setText(year.toString())
        currentYear = year.toString()

        transactionType = Utils.TRANSACTION_YEARLY
    }


    private fun setCurrentMonthValue(currentMonthName: String) {
        if (currentMonthName.equals("January", ignoreCase = true)) {
            currentMonth = "01"
        } else if (currentMonthName.equals("February", ignoreCase = true)) {
            currentMonth = "02"
        } else if (currentMonthName.equals("March", ignoreCase = true)) {
            currentMonth = "03"
        } else if (currentMonthName.equals("April", ignoreCase = true)) {
            currentMonth = "04"
        } else if (currentMonthName.equals("May", ignoreCase = true)) {
            currentMonth = "05"
        } else if (currentMonthName.equals("June", ignoreCase = true)) {
            currentMonth = "06"
        } else if (currentMonthName.equals("July", ignoreCase = true)) {
            currentMonth = "07"
        } else if (currentMonthName.equals("August", ignoreCase = true)) {
            currentMonth = "08"
        } else if (currentMonthName.equals("September", ignoreCase = true)) {
            currentMonth = "09"
        } else if (currentMonthName.equals("October", ignoreCase = true)) {
            currentMonth = "10"
        } else if (currentMonthName.equals("November", ignoreCase = true)) {
            currentMonth = "11"
        } else if (currentMonthName.equals("December", ignoreCase = true)) {
            currentMonth = "12"
        }
    }


}






