package com.e.digilife.Fragment


import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Typeface
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.*
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.Response
import com.android.volley.VolleyError
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.app.towntalk.Adapter.W_TransactionRecyclerAdapter
import com.e.digilife.Activity.AddTrascationActivity
import com.e.digilife.Model.WalletModel
import com.e.digilife.R
import com.e.digilife.View.Utils
import com.shurlock.View.EndPoints
import org.json.JSONObject
import java.text.DecimalFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


class Wallet_TransactionFragment : Fragment() {

    internal lateinit var requestQueue: RequestQueue
    var prefs: SharedPreferences? = null
    private var animation: Animation? = null

    lateinit var backView: ImageView
    lateinit var NoValueTextview: TextView
    lateinit var walletRecyclerView: RecyclerView
    lateinit var firstTextview: TextView
    lateinit var middleTextview: TextView
    lateinit var lastTextview: TextView
    lateinit var previousView: ImageView
    lateinit var nextView: ImageView
    lateinit var displayText: TextView
    lateinit var addTransactionView: ImageView
    lateinit var incomeText: TextView
    lateinit var expenseText: TextView
    lateinit var balanceText: TextView

    var client_id: String = ""
    var currentYear: String = ""
    var currentMonthName: String = ""
    var currentMonth: String = ""
    var weekNumber: Int = 0
    var weeksOfYear: Int = 0
    var startDate: String = ""
    var endDate: String = ""
    var transactionType: String = ""
    var currency: String = ""

    internal lateinit var calendar: Calendar

    private var dataList = ArrayList<WalletModel>()
    var adapter: W_TransactionRecyclerAdapter? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_wallet_transaction, container, false)
        init(view)
        requestQueue = Volley.newRequestQueue(activity)
        prefs = activity!!.getSharedPreferences(Utils.PREF_NAME, Context.MODE_PRIVATE)
        animation = AnimationUtils.loadAnimation(activity, R.anim.fade_in)
        currency = prefs!!.getString(Utils.CURRENCY, "")
        val loginRes = prefs!!.getString(Utils.LOGIN_OBJ, "")

        if (!loginRes.equals("")) {
            val loginObj = JSONObject(loginRes)
            client_id = loginObj.getString("id")
        }


        calendar = Calendar.getInstance()
        currentYear = calendar.get(Calendar.YEAR).toString()
        @SuppressLint("SimpleDateFormat")
        val month_date = SimpleDateFormat("MM")
        @SuppressLint("SimpleDateFormat")
        val month_name = SimpleDateFormat("MMMM")
        currentMonth = month_date.format(calendar.time)
        currentMonthName = month_name.format(calendar.getTime())


        if ((getActivity()) != null) {
            displayText.text = currentMonthName
        }
        transactionType = Utils.TRANSACTION_MONTHLY

        weekNumber = calendar.get(Calendar.WEEK_OF_YEAR)
        weeksOfYear = calendar.getActualMaximum(Calendar.WEEK_OF_YEAR)
        val linearLayoutManager = LinearLayoutManager(activity)
        walletRecyclerView.setLayoutManager(linearLayoutManager)
        walletRecyclerView.setHasFixedSize(true)
        adapter = W_TransactionRecyclerAdapter(context!!, dataList, onItemClick, onDeleteItemClick)
        walletRecyclerView.adapter = adapter

        return view
    }

    override fun onResume() {
        super.onResume()
        callExpenseIncomeListApi()
    }

    fun init(view: View) {
        backView = view.findViewById(R.id.backView)
        NoValueTextview = view.findViewById(R.id.NoValueTextview)
        walletRecyclerView = view.findViewById(R.id.walletRecyclerView)
        firstTextview = view.findViewById(R.id.firstTextview)
        middleTextview = view.findViewById(R.id.middleTextview)
        lastTextview = view.findViewById(R.id.lastTextview)
        displayText = view.findViewById(R.id.displayText)
        previousView = view.findViewById(R.id.previousView)
        nextView = view.findViewById(R.id.nextView)
        addTransactionView = view.findViewById(R.id.addTransactionView)
        incomeText = view.findViewById(R.id.incomeText)
        expenseText = view.findViewById(R.id.expenseText)
        balanceText = view.findViewById(R.id.balanceText)

        backView.setOnClickListener(clickListener)
        firstTextview.setOnClickListener(clickListener)
        lastTextview.setOnClickListener(clickListener)
        previousView.setOnClickListener(clickListener)
        nextView.setOnClickListener(clickListener)
        addTransactionView.setOnClickListener(clickListener)
    }

    private val clickListener: View.OnClickListener = View.OnClickListener { view ->
        when (view.id) {
            R.id.backView -> {
                activity!!.finish()
            }
            R.id.addTransactionView -> {
                var intent = Intent(activity, AddTrascationActivity::class.java)
                intent.putExtra("transcation", "add")
                startActivity(intent)
            }
            R.id.nextView -> {
                if (middleTextview.getText().toString().equals(Utils.lbl_week, ignoreCase = true)) {
                    if (weekNumber == weeksOfYear) {
                        weekNumber = 1
                        getUpdateWeek(weekNumber)
                    } else {
                        weekNumber = weekNumber + 1
                        getUpdateWeek(weekNumber)
                    }

                } else if (middleTextview.getText().toString().equals(Utils.lbl_month, ignoreCase = true)) {

                    var month = Integer.valueOf(currentMonth)
                    if (month == 12) {
                        month = 0
                    }
                    month = month + 1
                    getUpdatedMonth(month)
                } else if (middleTextview.getText().toString().equals(Utils.lbl_year, ignoreCase = true)) {
                    var Year = Integer.valueOf(currentYear)
                    Year = Year + 1
                    getUpdatedYear(Year)
                }
                callExpenseIncomeListApi()
            }
            R.id.previousView -> {
                if (middleTextview.getText().toString().equals(Utils.lbl_week, ignoreCase = true)) {
                    if (weekNumber == 1) {
                        weekNumber = weeksOfYear
                        getUpdateWeek(weekNumber)
                    } else {
                        weekNumber = weekNumber - 1
                        getUpdateWeek(weekNumber)
                    }

                } else if (middleTextview.getText().toString().equals(Utils.lbl_month, ignoreCase = true)) {

                    var month = Integer.valueOf(currentMonth)
                    if (month == 1) {
                        month = 13
                    }
                    month = month - 1
                    getUpdatedMonth(month)
                } else if (middleTextview.getText().toString().equals(Utils.lbl_year, ignoreCase = true)) {
                    var Year = Integer.valueOf(currentYear)
                    Year = Year - 1
                    getUpdatedYear(Year)
                }
                callExpenseIncomeListApi()
            }
            R.id.firstTextview -> {
                if (middleTextview.getText().toString().equals(Utils.lbl_week, ignoreCase = true)) {
                    middleTextview.setText(firstTextview.getText())
                } else if (middleTextview.getText().toString().equals(Utils.lbl_month, ignoreCase = true)) {
                    middleTextview.setText(firstTextview.getText())
                } else if (middleTextview.getText().toString().equals(Utils.lbl_year, ignoreCase = true)) {
                    middleTextview.setText(firstTextview.getText())
                }

                if (middleTextview.getText().toString().equals(Utils.lbl_week, ignoreCase = true)) {
                    lastTextview.setText(Utils.lbl_year)
                    firstTextview.setText(Utils.lbl_month)
                } else if (middleTextview.getText().toString().equals(Utils.lbl_month, ignoreCase = true)) {
                    firstTextview.setText(Utils.lbl_week)
                    lastTextview.setText(Utils.lbl_year)
                } else if (middleTextview.getText().toString().equals(Utils.lbl_year, ignoreCase = true)) {
                    firstTextview.setText(Utils.lbl_week)
                    lastTextview.setText(Utils.lbl_month)
                }

                if (middleTextview.getText().toString().equals(Utils.lbl_week, ignoreCase = true)) {
                    displayText.setText("Week " + weekNumber)
                    getDateFromWeek(weekNumber, calendar.get(Calendar.YEAR))
                    transactionType = Utils.TRANSACTION_WEEKLY
                } else if (middleTextview.getText().toString().equals(Utils.lbl_month, ignoreCase = true)) {
                    displayText.setText(currentMonthName)
                    transactionType = Utils.TRANSACTION_MONTHLY
                    setCurrentMonthValue(currentMonthName)
                } else if (middleTextview.getText().toString().equals(Utils.lbl_year, ignoreCase = true)) {
                    currentYear = calendar.get(Calendar.YEAR).toString()
                    displayText.setText(currentYear)
                    transactionType = Utils.TRANSACTION_YEARLY
                }

                middleTextview.startAnimation(animation)
                firstTextview.startAnimation(animation)
                displayText.startAnimation(animation)

                callExpenseIncomeListApi()

            }
            R.id.lastTextview -> {

                if (middleTextview.getText().toString().equals(Utils.lbl_week, ignoreCase = true)) {
                    middleTextview.setText(lastTextview.getText())
                } else if (middleTextview.getText().toString().equals(Utils.lbl_month, ignoreCase = true)) {
                    middleTextview.setText(lastTextview.getText())
                } else if (middleTextview.getText().toString().equals(Utils.lbl_year, ignoreCase = true)) {
                    middleTextview.setText(lastTextview.getText())
                }

                if (middleTextview.getText().toString().equals(Utils.lbl_week, ignoreCase = true)) {
                    lastTextview.setText(Utils.lbl_year)
                    firstTextview.setText(Utils.lbl_month)
                    transactionType = Utils.TRANSACTION_WEEKLY
                } else if (middleTextview.getText().toString().equals(Utils.lbl_month, ignoreCase = true)) {
                    firstTextview.setText(Utils.lbl_week)
                    lastTextview.setText(Utils.lbl_year)
                    transactionType = Utils.TRANSACTION_MONTHLY
                } else if (middleTextview.getText().toString().equals(Utils.lbl_year, ignoreCase = true)) {
                    firstTextview.setText(Utils.lbl_week)
                    lastTextview.setText(Utils.lbl_month)
                    transactionType = Utils.TRANSACTION_YEARLY
                }

                if (middleTextview.getText().toString().equals(Utils.lbl_week, ignoreCase = true)) {
                    displayText.setText("Week " + weekNumber)
                    getDateFromWeek(weekNumber, calendar.get(Calendar.YEAR))
                } else if (middleTextview.getText().toString().equals(Utils.lbl_month, ignoreCase = true)) {
                    displayText.setText(currentMonthName)
                    setCurrentMonthValue(currentMonthName)
                } else if (middleTextview.getText().toString().equals(Utils.lbl_year, ignoreCase = true)) {
                    currentYear = calendar.get(Calendar.YEAR).toString()
                    displayText.setText(currentYear)
                }

                middleTextview.startAnimation(animation)
                lastTextview.startAnimation(animation)
                displayText.startAnimation(animation)

                callExpenseIncomeListApi()
            }
        }
    }

    var onItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            Log.e("position ::", position.toString())
            var intent = Intent(activity, AddTrascationActivity::class.java)
            intent.putExtra("transcation", "edit")
            intent.putExtra(Utils.TRANSCATION_DATA, dataList.get(position))
            startActivity(intent)
        }
    }

    var onDeleteItemClick: View.OnClickListener = object : View.OnClickListener {
        override fun onClick(v: View?) {
            val position = v!!.getTag() as Int
            Log.e("position:::", position.toString())
            showDeleteMessageDialog("Are you sure want to delete this transcation ?", position)
        }
    }

    //TODO: Expense Income API........

    private fun callExpenseIncomeListApi() {

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.EXPENSE_INCOME_LIST,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        val dataObj = jsonObj.getJSONObject("data")
                        setExpenseIncomeList(dataObj)
                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["type"] = transactionType
                params["start_date"] = startDate
                params["end_date"] = endDate
                params["month_no"] = currentMonth
                params["year"] = currentYear
                Log.e(" JSONData :", params.toString())
                return params
            }
        }

        Log.e(" API :", EndPoints.EXPENSE_INCOME_LIST)
        requestQueue.add(stringRequest)
    }


    fun setExpenseIncomeList(dataObj: JSONObject) {
        var incomeAmount = dataObj.getString("income")
        var expenseAmount = dataObj.getString("expense")
        var balanceAmount = dataObj.getString("balance")

        incomeText.text = currency + " " + getFloatValue(incomeAmount)
        expenseText.text = currency + " " + getFloatValue(expenseAmount)
        balanceText.text = currency + " " + getFloatValue(balanceAmount)

        var expenseArray = dataObj.getJSONArray("expense_list")

        if (expenseArray.length() > 0) {
            dataList.clear()
            NoValueTextview.visibility = View.GONE
            for (i in 0..(expenseArray.length() - 1)) {
                val jsonObj = expenseArray.getJSONObject(i)
                val dataArray = jsonObj.getJSONArray("data")

                for (j in 0..(dataArray.length() - 1)) {
                    val dataObj = dataArray.getJSONObject(j)
                    val dataModel = WalletModel()
                    if (j == 0) {
                        dataModel.expenseDate = jsonObj.getString("date")
                        dataModel.expenseTotal = getFloatValue (jsonObj.getString("total"))
                        dataModel.isExpense = "1"
                    } else {
                        dataModel.isExpense = ""
                    }
                    dataModel.currency = currency
                    dataModel.tranId = dataObj.getString("id")
                    dataModel.tranAmount = getFloatValue (dataObj.getString("amount"))
                    dataModel.tranDescription = dataObj.getString("description")
                    dataModel.trasAttachment = dataObj.getString("attachment")
                    dataModel.tranClaimReimburse = dataObj.getString("is_claim_reimburse")
                    dataModel.trasPurpose = dataObj.getString("purpose")
                    dataModel.trasCategoryId = dataObj.getString("category_id")
                    dataModel.transCompanyName = dataObj.getString("company_name")
                    dataModel.trasnCompnayId = dataObj.getString("company_id")
                    dataModel.trasRenewableType = dataObj.getString("renewable_type")
                    dataModel.trasIsRenewable = dataObj.getString("is_renewable")
                    dataModel.transFullDateTime = dataObj.getString("date_time")
                    var sdDate = dateConvertor(dataObj.getString("date_time"))
                    dataModel.transDate = sdDate
                    dataModel.trasSplit = dataObj.getString("is_split")
                    dataModel.trasSplitData = dataObj.getString("split_data")
                    dataModel.trasCategoryName = dataObj.getString("category_name")
                    dataModel.trasCategoryType = dataObj.getString("category_type")

                    dataList.add(dataModel)
                }
            }
            adapter!!.notifyDataSetChanged()
        } else {
            dataList.clear()
            adapter!!.notifyDataSetChanged()
            NoValueTextview.visibility = View.VISIBLE
        }
    }

    fun getFloatValue(value: String): String {
        val format = DecimalFormat("#,###,###0.00")
        var newPrice = format.format(java.lang.Double.parseDouble(value))
        return newPrice
    }

    private fun deleteTranscation(id: String, pos: Int) {

        val stringRequest = object : StringRequest(
            Request.Method.POST, EndPoints.DELETE_TRANSCATION,
            object : Response.Listener<String> {
                override fun onResponse(response: String) {

                    Log.e("Response", response)
                    val jsonObj = JSONObject(response)
                    val status = jsonObj.getString("status")
                    if (status.equals("1")) {
                        // dataList.removeAt(pos)
                        // adapterW!!.notifyDataSetChanged()
                        callExpenseIncomeListApi()
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    } else {
                        Toast.makeText(activity, jsonObj.getString("message"), Toast.LENGTH_LONG).show()
                    }
                }
            },
            object : Response.ErrorListener {
                override fun onErrorResponse(error: VolleyError) {

                }
            }
        ) {
            override fun getParams(): Map<String, String> {
                val params = HashMap<String, String>()
                params["client_id"] = client_id
                params["id"] = id

                Log.e(" JSONData :", params.toString())
                return params
            }
        }

        Log.e(" API :", EndPoints.DELETE_TRANSCATION)
        requestQueue.add(stringRequest)
    }

    // delete dialog.......

    fun showDeleteMessageDialog(message: String, position: Int) {
        val dialog = Dialog(activity)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        val inflater = activity!!.getSystemService(AppCompatActivity.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val view = inflater.inflate(R.layout.confirmation_dialog, null)
        dialog.setContentView(view)

        val cancleBtn = dialog.findViewById(R.id.cancleText) as TextView
        val okBtn = dialog.findViewById(R.id.okBtn) as Button
        val dialogText = dialog.findViewById(R.id.dialogText) as TextView
        dialogText.setText(message)

        val font = Typeface.createFromAsset(activity!!.assets, resources.getString(R.string.popins_semi_bold))
        okBtn.setTypeface(font)

        okBtn.setOnClickListener {
            deleteTranscation(dataList.get(position).tranId, position)
            dialog.dismiss()
        }

        cancleBtn.setOnClickListener {
            dialog.dismiss()
        }

        var window = dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawableResource(R.color.colorTransparent);
        window.setGravity(Gravity.CENTER);
        dialog.show()
    }

    //TODO: Year Month and week  ........

    fun dateConvertor(inputText: String): String {

        val outputFormat = SimpleDateFormat("E, MMM dd yyyy", Locale.US)
        val inputFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)

        var date: Date? = null
        try {
            date = inputFormat.parse(inputText)
        } catch (e: ParseException) {
            e.printStackTrace()
        }

        val outputText = outputFormat.format(date)

        return outputText
    }


    fun getUpdateWeek(weekNo: Int) {
        displayText.text = "Week " + weekNo
        getDateFromWeek(weekNo, calendar.get(Calendar.YEAR))
    }

    fun getDateFromWeek(weekNo: Int, year: Int) {
        val cal = GregorianCalendar.getInstance()
        cal.clear()
        cal.set(Calendar.YEAR, year)
        cal.set(Calendar.WEEK_OF_YEAR, weekNo)
        startDate = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(cal.time)
        cal.add(Calendar.DAY_OF_YEAR, 6)
        endDate = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(cal.time)
    }


//    fun getUpdateDate(updatedValue: Int, aysBeforeDate: Date) {
//        val cal = GregorianCalendar.getInstance()
//        cal.time = aysBeforeDate
//        cal.add(Calendar.DAY_OF_YEAR, updatedValue)
//        this.aysBeforeDate = cal.time
//
//        updatedDate = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(this.aysBeforeDate)
//        displayText.text = updatedDate
//    }

    @SuppressLint("SetTextI18n")
    private fun getUpdatedMonth(month: Int) {
        if (month == 1) {
            currentMonth = "01"
            displayText.setText("January")
        } else if (month == 2) {
            displayText.setText("February")
            currentMonth = "02"
        } else if (month == 3) {
            displayText.setText("March")
            currentMonth = "03"
        } else if (month == 4) {
            displayText.setText("April")
            currentMonth = "04"
        } else if (month == 5) {
            displayText.setText("May")
            currentMonth = "05"
        } else if (month == 6) {
            displayText.setText("June")
            currentMonth = "06"
        } else if (month == 7) {
            displayText.setText("July")
            currentMonth = "07"
        } else if (month == 8) {
            displayText.setText("August")
            currentMonth = "08"
        } else if (month == 9) {
            displayText.setText("September")
            currentMonth = "09"
        } else if (month == 10) {
            displayText.setText("October")
            currentMonth = "10"
        } else if (month == 11) {
            displayText.setText("November")
            currentMonth = "11"
        } else if (month == 12) {
            displayText.setText("December")
            currentMonth = "12"
        }

        transactionType = Utils.TRANSACTION_MONTHLY
    }

    private fun getUpdatedYear(year: Int) {
        displayText.setText(year.toString())
        currentYear = year.toString()
        transactionType = Utils.TRANSACTION_YEARLY
    }


    private fun setCurrentMonthValue(currentMonthName: String) {
        if (currentMonthName.equals("January", ignoreCase = true)) {
            currentMonth = "01"
        } else if (currentMonthName.equals("February", ignoreCase = true)) {
            currentMonth = "02"
        } else if (currentMonthName.equals("March", ignoreCase = true)) {
            currentMonth = "03"
        } else if (currentMonthName.equals("April", ignoreCase = true)) {
            currentMonth = "04"
        } else if (currentMonthName.equals("May", ignoreCase = true)) {
            currentMonth = "05"
        } else if (currentMonthName.equals("June", ignoreCase = true)) {
            currentMonth = "06"
        } else if (currentMonthName.equals("July", ignoreCase = true)) {
            currentMonth = "07"
        } else if (currentMonthName.equals("August", ignoreCase = true)) {
            currentMonth = "08"
        } else if (currentMonthName.equals("September", ignoreCase = true)) {
            currentMonth = "09"
        } else if (currentMonthName.equals("October", ignoreCase = true)) {
            currentMonth = "10"
        } else if (currentMonthName.equals("November", ignoreCase = true)) {
            currentMonth = "11"
        } else if (currentMonthName.equals("December", ignoreCase = true)) {
            currentMonth = "12"
        }
    }


}




