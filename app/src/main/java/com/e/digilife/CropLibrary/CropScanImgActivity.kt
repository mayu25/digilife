package com.e.digilife.CropLibrary

import android.annotation.SuppressLint
import android.app.Activity
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.graphics.*
import android.media.ExifInterface
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.RelativeLayout
import com.e.digilife.Activity.ImageUploadActivity
import com.e.digilife.R
import me.pqpo.smartcropperlib.view.CropImageView
import java.io.*
import java.text.SimpleDateFormat
import java.util.*


class CropScanImgActivity : AppCompatActivity(), View.OnClickListener {

    private val TAG = "CropScanImageActivity"


    lateinit var ivCropResult: ImageView
    lateinit var ivCropView: CropImageView
    lateinit var btOk: Button
    lateinit var btCancel: Button
    lateinit var rlMain: RelativeLayout

    lateinit var lci_rlMain: RelativeLayout
    lateinit var lci_ivCropResult: ImageView
    lateinit var lci_btCancel: Button
    lateinit var lci_btOk: Button

    private var mFromAlbum: Boolean = false
    val isCropResult = false
    private var isCameraOrGallery = false

    private var file: File? = null
    private var selectedBitmap: Bitmap? = null

    lateinit var colorMatrix: ColorMatrix

    lateinit var options: BitmapFactory.Options

    val REQUEST_CODE_TAKE_PHOTO = 100
    val REQUEST_CODE_SELECT_ALBUM = 200
    val EXTRA_FROM_ALBUM = "extra_from_album"
    val EXTRA_CROPPED_FILE = "extra_cropped_file"

    var emrFolder_id = ""

    fun getJumpIntent(context: Context, fromAlbum: Boolean, emrFolder_id: String): Intent {
        val intent = Intent(context, CropScanImageActivity::class.java)
        intent.putExtra(EXTRA_FROM_ALBUM, fromAlbum)
        intent.putExtra("emrFolder_id", emrFolder_id)
        //intent.putExtra(EXTRA_CROPPED_FILE, croppedFile);
        return intent
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_crop_scan_image)
        init()

        btCancel.setOnClickListener(this)
        btOk.setOnClickListener(this)

        rlMain.visibility = View.VISIBLE

        mFromAlbum = intent.getBooleanExtra(EXTRA_FROM_ALBUM, true)

        file = createImageFile()
        val isDirectoryCreated = file!!.parentFile.mkdirs()
        Log.e(TAG, "openCamera: isDirectoryCreated: $isDirectoryCreated")

        //file = (File) getIntent().getSerializableExtra(EXTRA_CROPPED_FILE);
        if (file == null) {
            setResult(Activity.RESULT_CANCELED)
            finish()
            return
        }
        selectPhoto()
    }


    fun init() {
        ivCropResult = findViewById(R.id.acsi_ivCropResult)
        btOk = findViewById(R.id.acsi_btOk)
        btCancel = findViewById(R.id.acsi_btCancel)
        rlMain = findViewById(R.id.acsi_rlMain)
        ivCropView = findViewById(R.id.acsi_ivCropView)


        lci_rlMain = findViewById(R.id.lci_rlMain)
        lci_ivCropResult = findViewById(R.id.lci_ivCropResult)
        lci_btCancel = findViewById(R.id.lci_btCancel)
        lci_btOk = findViewById(R.id.lci_btOk)
    }

    private fun CropBitmapBrightness(bitmap: Bitmap): Bitmap {
        val ret = Bitmap.createBitmap(bitmap.width, bitmap.height, bitmap.config)

        val canvas = Canvas(ret)

        val paint = Paint()
        paint.colorFilter = ColorMatrixColorFilter(colorMatrix)
        canvas.drawBitmap(bitmap, 0f, 0f, paint)

        return ret
    }


    private fun selectPhoto() {
        if (mFromAlbum) {
            val selectIntent = Intent(Intent.ACTION_PICK)
            selectIntent.type = "image/*"
            if (selectIntent.resolveActivity(packageManager) != null) {
                startActivityForResult(selectIntent, REQUEST_CODE_SELECT_ALBUM)
            }
        } else {
            /*Intent startCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mCroppedFile));
            if (startCameraIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(startCameraIntent, REQUEST_CODE_TAKE_PHOTO);
            }*/


            val cameraIntent = Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                val tempFileUri = FileProvider.getUriForFile(
                    applicationContext,
                    "com.e.digilife.provider", // As defined in Manifest
                    file!!
                )
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, tempFileUri)
            } else {
                val tempFileUri = Uri.fromFile(file)
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, tempFileUri)
            }
            if (cameraIntent.resolveActivity(packageManager) != null) {
                startActivityForResult(cameraIntent, REQUEST_CODE_TAKE_PHOTO)
            }
        }
    }

    val UPLOAD_FILE_PATH = Environment.getExternalStorageDirectory().path + "/DigiLife/" + "Upload Files"


    private fun createImageFile(): File {
        clearTempImages()
        @SuppressLint("SimpleDateFormat") val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        //fileUri = Uri.fromFile(file);
        return File(
            UPLOAD_FILE_PATH, "IMG_" + timeStamp +
                    ".jpg"
        )
    }


    private fun clearTempImages() {
        try {
            val tempFolder = File(UPLOAD_FILE_PATH)
            for (f in tempFolder.listFiles())
                f.delete()
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) {
            setResult(Activity.RESULT_CANCELED)
            finish()
            return
        }
        //Bitmap bitmap = getBitmap(fileUri);
        //CAMERA IMAGE
        if (requestCode == REQUEST_CODE_TAKE_PHOTO && file!!.exists()) {

            isCameraOrGallery = false
            getCameraImageHeightWidth()
        } else if (requestCode == REQUEST_CODE_SELECT_ALBUM && data != null && data.data != null) {
            isCameraOrGallery = true
            val cr = contentResolver
            val bmpUri = data.data
            getGalleryImageHeightWidth(cr, bmpUri)
        }
        if (selectedBitmap != null) {


            try {
                val orientationColumn = arrayOf(MediaStore.Images.Media.ORIENTATION)
                val uri = getUri(this@CropScanImgActivity, selectedBitmap!!)

                val filePath = RealPathUtil.getPath(this@CropScanImgActivity, uri)

                val rotateImage = getCameraPhotoOrientation(this@CropScanImgActivity, uri, filePath)
                val matrix = Matrix()
                if (options.outHeight < options.outWidth) {
                    matrix.setRotate(90f)
                }
                //matrix.postRotate(rotateImage, (float) selectedBitmap.getWidth(), (float) selectedBitmap.getHeight());
                val rotatedBitmap =
                    Bitmap.createBitmap(selectedBitmap!!, 0, 0, options.outWidth, options.outHeight, matrix, true)
                var fos: FileOutputStream? = null
                fos = FileOutputStream(file)
                rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
                selectedBitmap = rotatedBitmap
                fos.flush()
                fos.close()


                Log.e(TAG, "rotateImage==$rotateImage")

            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            } catch (e: IOException) {
                e.printStackTrace()
            }


            /*Cursor cur = managedQuery(uri, orientationColumn, null, null, null);
            int orientation = -1;
            if (cur != null && cur.moveToFirst()) {
                orientation = cur.getInt(cur.getColumnIndex(orientationColumn[0]));
            }*/

            /*            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);
            Log.e(TAG, "orientation==" + orientation);*/


            ivCropView.setImageToCrop(selectedBitmap!!)
            ivCropView.setFullImgCrop()
            Log.e(TAG, "points==" + Arrays.toString(ivCropView.cropPoints))
            //ivCropResult.setImageBitmap(selectedBitmap);
        }
    }


    fun getCameraPhotoOrientation(context: Context, imageUri: Uri, imagePath: String?): Int {
        var rotate = 0
        try {
            context.contentResolver.notifyChange(imageUri, null)
            val imageFile = File(imagePath)

            val exif = ExifInterface(imageFile.absolutePath)
            val orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)

            when (orientation) {
                ExifInterface.ORIENTATION_ROTATE_270 -> rotate = 270
                ExifInterface.ORIENTATION_ROTATE_180 -> rotate = 180
                ExifInterface.ORIENTATION_ROTATE_90 -> rotate = 90
            }

            Log.i("RotateImage", "Exif orientation: $orientation")
            Log.i("RotateImage", "Rotate value: $rotate")


        } catch (e: Exception) {
            e.printStackTrace()
        }

        return rotate
    }


    private fun getCameraImageHeightWidth() {
        options = BitmapFactory.Options()
        options.inJustDecodeBounds = true
        BitmapFactory.decodeFile(file!!.path, options)
        options.inJustDecodeBounds = false
        options.inSampleSize = calculateSampleSize(options)
        selectedBitmap = BitmapFactory.decodeFile(file!!.path, options)
    }

    private fun getGalleryImageHeightWidth(cr: ContentResolver, bmpUri: Uri?) {

        try {

            options = BitmapFactory.Options()
            options.inJustDecodeBounds = true
            BitmapFactory.decodeStream(cr.openInputStream(bmpUri!!), Rect(), options)
            options.inJustDecodeBounds = false
            options.inSampleSize = calculateSampleSize(options)
            selectedBitmap = BitmapFactory.decodeStream(cr.openInputStream(bmpUri), Rect(), options)

        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        }


    }


    private fun saveImage(bitmap: Bitmap, saveFile: File?) {
        try {
            val fos = FileOutputStream(saveFile)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos)
            fos.flush()
            fos.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }

    }

    private fun calculateSampleSize(options: BitmapFactory.Options): Int {
        val outHeight = options.outHeight
        val outWidth = options.outWidth
        var sampleSize = 1
        val destHeight = 1000
        val destWidth = 1000
        if (outHeight > destHeight || outWidth > destHeight) {
            if (outHeight > outWidth) {
                sampleSize = outHeight / destHeight
            } else {
                sampleSize = outWidth / destWidth
            }
        }
        if (sampleSize < 1) {
            sampleSize = 1
        }
        return sampleSize
    }

    override fun onClick(view: View) {
        if (view === btCancel || view === lci_btCancel) {
            setResult(Activity.RESULT_CANCELED)
            finish()
        } else if (view === btOk || view === lci_btOk) {
            if (ivCropView.canRightCrop()) {
                val crop = ivCropView.crop()
                val uri = getUri(this@CropScanImgActivity, crop)
                val intent = Intent(this@CropScanImgActivity, ImageUploadActivity::class.java)
                val bundle = Bundle()
                bundle.putString("imagePath", uri.toString())
                bundle.putString("emrFolder_id", "")
                intent.putExtras(bundle)
                startActivity(intent)

                // lci_ivCropResult.setImageBitmap(crop);
                saveImage(crop, file)
            }

        }
    }

    fun getUri(context: Context, bitmap: Bitmap): Uri {
        val bytes = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val path = MediaStore.Images.Media.insertImage(context.contentResolver, bitmap, "Title", null)
        return Uri.parse(path)
    }

}



