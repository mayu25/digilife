package com.e.digilife.CropLibrary;

import android.annotation.SuppressLint;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.*;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.e.digilife.Activity.ImageUploadActivity;
import com.e.digilife.R;
import me.pqpo.smartcropperlib.view.CropImageView;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class CropScanImageActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "CropScanImageActivity";


    ImageView ivCropResult;
    CropImageView ivCropView;
    Button btOk;
    Button btCancel;
    RelativeLayout rlMain;

    RelativeLayout lci_rlMain;
    ImageView lci_ivCropResult;
    Button lci_btCancel;
    Button lci_btOk;

    private boolean mFromAlbum, isCropResult = false, isCameraOrGallery = false;

    private File file;
    private Bitmap selectedBitmap = null;

    static ColorMatrix colorMatrix;

    BitmapFactory.Options options;

    public static final int REQUEST_CODE_TAKE_PHOTO = 100;
    public static final int REQUEST_CODE_SELECT_ALBUM = 200;
    public static final String EXTRA_FROM_ALBUM = "extra_from_album";
    public static final String EXTRA_CROPPED_FILE = "extra_cropped_file";

    public static String Folder_id = "";
    public static String parentFolderId = "";

    public static Intent getJumpIntent(Context context, boolean fromAlbum, String Folder_id, String parentFolderId) {
        Intent intent = new Intent(context, CropScanImageActivity.class);
        intent.putExtra("Folder_id", Folder_id);
        intent.putExtra("parentFolderId", parentFolderId);
        intent.putExtra(EXTRA_FROM_ALBUM, fromAlbum);
        //intent.putExtra(EXTRA_CROPPED_FILE, croppedFile);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crop_scan_image);
        init();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {


        }

        btCancel.setOnClickListener(this);
        btOk.setOnClickListener(this);

        rlMain.setVisibility(View.VISIBLE);

        mFromAlbum = getIntent().getBooleanExtra(EXTRA_FROM_ALBUM, true);
        Folder_id = getIntent().getStringExtra("Folder_id");
        parentFolderId = getIntent().getStringExtra("parentFolderId");

        file = createImageFile();
        boolean isDirectoryCreated = file.getParentFile().mkdirs();
        Log.e(TAG, "openCamera: isDirectoryCreated: " + isDirectoryCreated);

        //file = (File) getIntent().getSerializableExtra(EXTRA_CROPPED_FILE);
        if (file == null) {
            setResult(RESULT_CANCELED);
            finish();
            return;
        }

        selectPhoto();
    }


    public void init() {
        ivCropResult = findViewById(R.id.acsi_ivCropResult);
        btOk = findViewById(R.id.acsi_btOk);
        btCancel = findViewById(R.id.acsi_btCancel);
        rlMain = findViewById(R.id.acsi_rlMain);
        ivCropView = findViewById(R.id.acsi_ivCropView);


        lci_rlMain = findViewById(R.id.lci_rlMain);
        lci_ivCropResult = findViewById(R.id.lci_ivCropResult);
        lci_btCancel = findViewById(R.id.lci_btCancel);
        lci_btOk = findViewById(R.id.lci_btOk);

    }


    public static Bitmap changeBitmapContrastBrightness(Bitmap bmp, float contrast, float brightness) {
        colorMatrix = new ColorMatrix(new float[]
                {
                        contrast, 0, 0, 0, brightness,
                        0, contrast, 0, 0, brightness,
                        0, 0, contrast, 0, brightness,
                        0, 0, 0, 1, 0
                });

        /*Bitmap ret = Bitmap.createBitmap(bmp.getWidth(), bmp.getHeight(), bmp.getConfig());

        Canvas canvas = new Canvas(ret);

        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(bmp, 0, 0, paint);*/

        return CropBitmapBrightness(bmp);
    }


    private static Bitmap CropBitmapBrightness(Bitmap bitmap) {
        Bitmap ret = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());

        Canvas canvas = new Canvas(ret);

        Paint paint = new Paint();
        paint.setColorFilter(new ColorMatrixColorFilter(colorMatrix));
        canvas.drawBitmap(bitmap, 0, 0, paint);

        return ret;
    }


    private void selectPhoto() {
        if (mFromAlbum) {
            Intent selectIntent = new Intent(Intent.ACTION_PICK);
            selectIntent.setType("image/*");
            if (selectIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(selectIntent, REQUEST_CODE_SELECT_ALBUM);
            }
        } else {
            /*Intent startCameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startCameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(mCroppedFile));
            if (startCameraIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(startCameraIntent, REQUEST_CODE_TAKE_PHOTO);
            }*/


            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Uri tempFileUri = FileProvider.getUriForFile(getApplicationContext(),
                        "com.e.digilife.provider", // As defined in Manifest
                        file);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, tempFileUri);
            } else {
                Uri tempFileUri = Uri.fromFile(file);
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, tempFileUri);
            }
            if (cameraIntent.resolveActivity(getPackageManager()) != null) {
                startActivityForResult(cameraIntent, REQUEST_CODE_TAKE_PHOTO);
            }
        }
    }

    public final static String UPLOAD_FILE_PATH = Environment.getExternalStorageDirectory().getPath() + "/DigiLife/" + "Upload Files";


    private File createImageFile() {
        clearTempImages();
        @SuppressLint("SimpleDateFormat") String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        //fileUri = Uri.fromFile(file);
        return new File(UPLOAD_FILE_PATH, "IMG_" + timeStamp +
                ".jpg");
    }


    private void clearTempImages() {
        try {
            File tempFolder = new File(UPLOAD_FILE_PATH);
            for (File f : tempFolder.listFiles())
                f.delete();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            setResult(RESULT_CANCELED);
            finish();
            return;
        }
        //Bitmap bitmap = getBitmap(fileUri);
        //CAMERA IMAGE
        if (requestCode == REQUEST_CODE_TAKE_PHOTO && file.exists()) {

            isCameraOrGallery = false;
            getCameraImageHeightWidth();
        } else if (requestCode == REQUEST_CODE_SELECT_ALBUM && data != null && data.getData() != null) {
            isCameraOrGallery = true;
            ContentResolver cr = getContentResolver();
            Uri bmpUri = data.getData();
            getGalleryImageHeightWidth(cr, bmpUri);
        }
        if (selectedBitmap != null) {

            try {
                String[] orientationColumn = {MediaStore.Images.Media.ORIENTATION};
                Uri uri = getUri(CropScanImageActivity.this, selectedBitmap);

                String filePath = RealPathUtil.getPath(CropScanImageActivity.this, uri);

                int rotateImage = getCameraPhotoOrientation(CropScanImageActivity.this, uri, filePath);
                Matrix matrix = new Matrix();
                if (options.outHeight < options.outWidth) {
                    matrix.setRotate(90);
                }
                //matrix.postRotate(rotateImage, (float) selectedBitmap.getWidth(), (float) selectedBitmap.getHeight());
                Bitmap rotatedBitmap = Bitmap.createBitmap(selectedBitmap, 0, 0, options.outWidth, options.outHeight, matrix, true);
                FileOutputStream fos = null;
                fos = new FileOutputStream(file);
                rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                selectedBitmap = rotatedBitmap;
                fos.flush();
                fos.close();

                Log.e(TAG, "rotateImage==" + rotateImage);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }


            /*Cursor cur = managedQuery(uri, orientationColumn, null, null, null);
            int orientation = -1;
            if (cur != null && cur.moveToFirst()) {
                orientation = cur.getInt(cur.getColumnIndex(orientationColumn[0]));
            }*/

/*            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);
            Log.e(TAG, "orientation==" + orientation);*/


            ivCropView.setImageToCrop(selectedBitmap);
            ivCropView.setFullImgCrop();
            Log.e(TAG, "points==" + Arrays.toString(ivCropView.getCropPoints()));
            //ivCropResult.setImageBitmap(selectedBitmap);
        }
    }


    public int getCameraPhotoOrientation(Context context, Uri imageUri, String imagePath) {
        int rotate = 0;
        try {
            context.getContentResolver().notifyChange(imageUri, null);
            File imageFile = new File(imagePath);

            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }

            Log.i("RotateImage", "Exif orientation: " + orientation);
            Log.i("RotateImage", "Rotate value: " + rotate);


        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }


    private void getCameraImageHeightWidth() {
        options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(file.getPath(), options);
        options.inJustDecodeBounds = false;
        options.inSampleSize = calculateSampleSize(options);
        selectedBitmap = BitmapFactory.decodeFile(file.getPath(), options);
    }

    private void getGalleryImageHeightWidth(ContentResolver cr, Uri bmpUri) {

        try {

            options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(cr.openInputStream(bmpUri), new Rect(), options);
            options.inJustDecodeBounds = false;
            options.inSampleSize = calculateSampleSize(options);
            selectedBitmap = BitmapFactory.decodeStream(cr.openInputStream(bmpUri), new Rect(), options);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


    }


    private void saveImage(Bitmap bitmap, File saveFile) {
        try {
            FileOutputStream fos = new FileOutputStream(saveFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int calculateSampleSize(BitmapFactory.Options options) {
        int outHeight = options.outHeight;
        int outWidth = options.outWidth;
        int sampleSize = 1;
        int destHeight = 1000;
        int destWidth = 1000;
        if (outHeight > destHeight || outWidth > destHeight) {
            if (outHeight > outWidth) {
                sampleSize = outHeight / destHeight;
            } else {
                sampleSize = outWidth / destWidth;
            }
        }
        if (sampleSize < 1) {
            sampleSize = 1;
        }
        return sampleSize;
    }

    @Override
    public void onClick(View view) {
        if (view == btCancel || view == lci_btCancel) {
            setResult(RESULT_CANCELED);
            finish();
        } else if (view == btOk || view == lci_btOk) {
            if (ivCropView.canRightCrop()) {
                Bitmap crop = ivCropView.crop();
                Uri uri = getUri(CropScanImageActivity.this, crop);
                Intent intent = new Intent(CropScanImageActivity.this, ImageUploadActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("imagePath", uri.toString());
                bundle.putString("Folder_id", Folder_id);
                bundle.putString("parentFolderId", parentFolderId);
                intent.putExtras(bundle);
                startActivity(intent);
                finish();
                // lci_ivCropResult.setImageBitmap(crop);
                saveImage(crop, file);
            }
        }
    }

    public static Uri getUri(Context context, Bitmap bitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "Title", null);
        return Uri.parse(path);
    }

}
