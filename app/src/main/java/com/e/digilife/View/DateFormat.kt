package com.e.digilife.View
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.ArrayList as ArrayList1


object DateFormat {
    fun dateFormat(inputFormat:String, outputFormat:String, inputDate:String): String? {
        var parsed: Date? = null
        var outputDate: String? = null
        val df_input = SimpleDateFormat(inputFormat, Locale.getDefault())
        val df_output = SimpleDateFormat(outputFormat, Locale.getDefault())
        try
        {
            parsed = df_input.parse(inputDate)
            outputDate = df_output.format(parsed)

        }
        catch (e:ParseException) {
        }
        return outputDate
    }
}



