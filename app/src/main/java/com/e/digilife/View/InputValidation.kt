package com.e.digilife.View

import android.app.Activity
import android.content.Context
import android.support.v7.app.AlertDialog
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import com.e.digilife.R
import java.util.regex.Matcher
import java.util.regex.Pattern

/**
 * Created by Mayuri on 29/5/18.
 */
class InputValidation(private val context: Context) {

    fun isInputEmail(textInputEditText: EditText, message: String): Boolean {
        val value = textInputEditText.text.toString().trim()
        if (value.isEmpty()) {
            Toast.makeText(context, context.getString(R.string.email_validation), Toast.LENGTH_SHORT).show()
            // showAlert(context.getString(R.string.email_validation))
            hideKeyboardFrom(textInputEditText)
            return false
        } else if (value.length > 0 && !android.util.Patterns.EMAIL_ADDRESS.matcher(value).matches()) {
            Toast.makeText(context, context.getString(R.string.email_is_not_valid), Toast.LENGTH_SHORT).show()
            //showAlert(context.getString(R.string.email_is_not_valid))
            hideKeyboardFrom(textInputEditText)
            return false
        }
        return true
    }

    fun isInputPassword(textInputEditText: EditText, message: String): Boolean {
        val value = textInputEditText.text.toString().trim()
        if (value.isEmpty()) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
            //showAlert(message)
            hideKeyboardFrom(textInputEditText)
            return false
        } else if (value.length < 6) {
            Toast.makeText(context, context.getString(R.string.check_password_length), Toast.LENGTH_SHORT).show()
            // showAlert(context.getString(R.string.check_password_length))
            hideKeyboardFrom(textInputEditText)
            return false
        }
        return true
    }


    fun validateString(`object`: String?): Boolean {
        var flag = false
        if (`object` != null && !`object`.isEmpty()
            && `object`.trim { it <= ' ' }.isNotEmpty()
            && !`object`.equals("null", ignoreCase = true)
            && !`object`.equals("", ignoreCase = true)
        ) {
            flag = true
        }
        return flag
    }



    fun isValidPassword(value: String): Boolean {
        var pattern: Pattern
        var matcher: Matcher
        var strPattern = "^(?=.*[A-Za-z])(?=.\\d)(?=.[\$@\$!%#?&])[A-Za-z\\d\$@\$!%#?&]{4,}\$"
       // var strPattern="^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#\$%^&+=!])(?=\\\\S+\$).{4,}\$"
        // var password_patter = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%^&+=])(?=\\S+$).{4,}$"
        pattern = Pattern.compile(strPattern)
        matcher = pattern.matcher(value);

        return matcher.matches();
    }

    fun isFieldEmpty(textInputEditText: EditText, message: String): Boolean {
        val value = textInputEditText.text.toString().trim()
        if (value.isEmpty()) {
            //showAlert(message)
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
            hideKeyboardFrom(textInputEditText)
            return false
        }
        return true
    }

    fun isFieldBlank(value: String, message: String): Boolean {

        if (value.isEmpty()) {
            //showAlert(message)
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show()

            return false
        }
        return true
    }


    fun showAlert(message: String) {
        val dialog = AlertDialog.Builder(context).setTitle(context.getString(R.string.app_name)).setMessage(message)
            .setPositiveButton("Ok", { dialog, i ->
                dialog.dismiss()
            })

        dialog.show()
    }


    fun hideKeyboardFrom(view: View) {
        val imm = context.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    }
}