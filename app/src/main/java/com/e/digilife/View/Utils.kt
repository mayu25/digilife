package com.e.digilife.View

import android.app.Activity
import android.content.Context
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import android.net.Uri
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import com.e.digilife.R
import org.json.JSONArray
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.URL


/**
 * Created by Mayuri on 10/5/19.
 */
object Utils {


    val PREF_NAME = "DigiLife"
    var VISIBLE_CHECKBOX = "0"
    var navItemIndex = 0
    var contactIndex = 0

    val LOGIN_OBJ = "LOGIN_OBJ"
    val PARENT_FOLDER_ID = "PARENT_FOLDER_ID"
    val FOLDER_ID = "FOLDER_ID"

    //Contact.....
    var EDIT_CONTACT = "EDIT_CONTACT"
    var CONTACT_OBJ = "CONTACT_OBJ"
    val GROUP = 201
    val CONTACT = 202
    val CALL_PERMISSION = 203

    //Pill Reminder
    var EDIT_PILLS = "EDIT_PILLS"
    var PILL_OBJ = "PILL_OBJ"

    /*.......Favourite Music........*/
    var FAVOURITE_MUSIC = "Favourite Music"
    var FAVOURITE_PHOTOS = "Favourite Photo"
    var DOCUMENT = "DOCUMENT"

    /*......offline Images.....*/
    val SAVE_OFFLINE = "SAVE_OFFLINE"
    var OFFLINE_IMG_ARRAY = JSONArray()

    val OFFLINE_FILE_ID = "OFFLINE_FILE_ID"
    val OFFLINE_FILE_NAME = "OFFLINE_FILE_NAME"
    val OFFLINE_FILE_PATH = "OFFLINE_FILE_PATH"
    val OFFLINE_FILE_CLIENT_ID = "OFFLINE_FILE_CLIENT_ID"
    val OFFLINE_FILE_TYPE = "OFFLINE_FILE_TYPE"

    // Personal........
    val FILE_DATA = "FILE_DATA"
    val FILE_TYPE = "FILE_TYPE"
    val TYPE = "TYPE"
    val MOVE_FILE_ID = "MOVE_FILE_ID"
    val COPY_FILE_ID = "COPY_FILE_ID"
    val TITLE = "TITLE"

    // Insurance....
    val EDIT_INSURANCE = "EDIT_INSURANCE"
    val INSURANCE_OBJ = "INSURANCE_OBJ"

    val TRASH = "TRASH"
    val DELETE = "DELETE"
    val OFFLINE = "OFFLINE"

    //Wallet...
    val lbl_week = "Week"
    val lbl_month = "Month"
    val lbl_year = "Year"
    val TRANSACTION_WEEKLY = "weekly"
    val TRANSACTION_MONTHLY = "monthly"
    val TRANSACTION_YEARLY = "yearly"
    val PURPOSE_PERSONAL = "personal"
    val PURPOSE_BUSINESS = "business"
    val CURRENCY = "CURRENCY"
    val TRANSACTION_EXPENSE = "expense"
    val TRANSACTION_INCOME = "income"
    val CATEGORY = 201
    val COMPANY = 202
    val TRANSCATION_DATA = "TRANSCATION_DATA"

    val TAG_NAME = "TAGNAME"
    //REQUEST CODE
    val SELECT_FILE = 101
    val SELECT_PDF_FILE = 1001
    val OPEN_DOCUMENT = 102
    val OPEN_CAMERA = 4
    val OPEN_MEDIA = 5
    val MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 100


    fun storeString(sharedPreferences: SharedPreferences?, key: String, value: String) {
        val editor = sharedPreferences?.edit()
        editor?.putString(key, value)
        editor?.commit()
    }

    fun storeJSONArraylist(sharedPreferences: SharedPreferences?, key: String, value: JSONArray?) {
        val editor = sharedPreferences?.edit()
        editor?.putString(key, value.toString())
        editor?.commit()
    }


    fun storeJSONObj(sharedPreferences: SharedPreferences?, key: String, value: JSONObject?) {
        val editor = sharedPreferences?.edit()
        editor?.putString(key, value.toString())
        editor?.commit()
    }

    fun checkInternetConnection(context: Context): Boolean {

        val cm = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        return if (cm.activeNetworkInfo != null
            && cm.activeNetworkInfo.isAvailable
            && cm.activeNetworkInfo.isConnected
        ) {
            true
        } else {
            // Toast.makeText(context, "Conex�o com a internet indispon�vel.",
            // Toast.LENGTH_SHORT).show();
            false
        }
    }

    fun showMessageDialog(context: Context, title: String, message: String?) {
        if (message != null && message.trim { it <= ' ' }.length > 0) {
            val builder = android.app.AlertDialog.Builder(context)
            builder.setTitle(title)
            builder.setCancelable(false)
            builder.setMessage(message)
            builder.setPositiveButton(
                "Ok"
            ) { dialog, id -> dialog.dismiss() }
            // create alert dialog
            val alertDialog = builder.create()
            // show it
            alertDialog.show()
        }
    }

    class ImageNameClass {
        companion object {
            fun geturi(name: String, ctx: Context): Uri {
                var pname = ctx.packageName
                // val uri1 = Uri.parse("android.resource://" + pname + "/drawable/" + name)
                val uri = Uri.parse("android.resource://" + pname + "/drawable/" + name)

                Log.d("uri", uri.toString())
                // Toast.makeText(ctx, uri.toString(), Toast.LENGTH_SHORT).show()
                return uri
            }
        }
    }

    fun getBitmapFromURL(src: String): Bitmap? {
        try {
            val url = URL(src)
            val connection = url.openConnection() as HttpURLConnection
            connection.setDoInput(true)
            connection.connect()
            val input = connection.getInputStream()
            return BitmapFactory.decodeStream(input)
        } catch (e: IOException) {
            e.printStackTrace()
            return null
        }
    }

    @Throws(IOException::class)
    fun getBytes(uri: Uri, activity: Context): ByteArray? {
        val iStream = activity.contentResolver.openInputStream(uri)
        try {
            return getBytes(iStream!!)
        } finally {
            // close the stream
            try {
                iStream!!.close()
            } catch (ignored: IOException) { /* do nothing */
            }

        }
    }

    @Throws(IOException::class)
    fun getBytes(inputStream: InputStream): ByteArray? {

        val out = ByteArrayOutputStream()
        var read = 0
        val buffer = ByteArray(1024)
        while (read != -1) {
            read = `inputStream`.read(buffer)
            if (read != -1)
                out.write(buffer, 0, read)
        }
        out.close()
        return out.toByteArray()
    }


}