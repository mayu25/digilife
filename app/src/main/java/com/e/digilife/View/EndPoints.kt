package com.shurlock.View

import java.net.URL

/**
 * Created by Mayuri on 10/5/19.
 */
object EndPoints {

    //val URL_ROOT = "https://digihealth.cloud/subdomains/digiLife/console/api/v1/"
    val URL_ROOT = "https://digiLife.cloud/console/api/v1/"

    // val ROOT = "https://digihealth.cloud/subdomains/digiLife/console/"
    val ROOT = "https://digiLife.cloud/console/"

    val LOGIN = URL_ROOT + "login.php"
    val SIGNUP = URL_ROOT + "registerclient.php"
    val FORGOTPASSWORD = URL_ROOT + "forgotpassword.php"
    val CHANGE_PASSWORD = URL_ROOT + "changePassword.php"
    val OTP_CHANGE_PASSWORD = URL_ROOT + "otpChangePassword.php"
    val EDIT_PROFILE = URL_ROOT + "manageprofile.php"
    val SEND_OTP = URL_ROOT + "sendOtp.php"
    val SEARCH_FILES = URL_ROOT + "searchfile.php"
    val ARTICLE_LIST = URL_ROOT + "articlelist.php"

    val GET_PROFILE = URL_ROOT + "getProfile.php"
    val GET_FOLDER_LIST = URL_ROOT + "getallfolder.php"


    val GET_EMR_DOCS_LIST = URL_ROOT + "getfiles.php"
    val ADD_FOLDER = URL_ROOT + "addfolder.php"
    val GET_NOTIFICATION = URL_ROOT + "getNotification.php"
    val GET_INSURANCE = URL_ROOT + "policylist.php"
    val ADD_INSURANCE = URL_ROOT + "addpolicy.php"
    val EDIT_INSURANCE = URL_ROOT + "editPolicy.php"

    val ADD_TAG = URL_ROOT + "addTag.php"
    val CLEAR_ALL_COMPLETED_TASK = URL_ROOT + "clearCloseTask.php"
    val GET_TAG_LIST = URL_ROOT + "getTag.php"
    val GET_CATEGORY_TASK_LIST = URL_ROOT + "categoryTaskList.php"
    val GET_TASK_LIST_DASHBORD_DATE = URL_ROOT + "getTasklist.php"
    val ADD_TASK_GET_CATEGORY_TASK_LIST = URL_ROOT + "taskCategoryList.php"
    val ADD_TASK = URL_ROOT + "addTask.php"
    val ADD_TASK_CATEGORY = URL_ROOT + "addTaskCategory.php"


    val FILE_UPLOAD = URL_ROOT + "uploadfile.php"
    val GET_FAV_LIST = URL_ROOT + "favouritefiles.php"
    val RECENT_FILES = URL_ROOT + "recentdocs.php"
    val IS_FAV = URL_ROOT + "addfavourite.php"
    val EDIT_FILE_NAME = URL_ROOT + "editfilename.php"
    val MOVE_FILE = URL_ROOT + "movefile.php"
    val COPY_FILE = URL_ROOT + "copyfile.php"
    val TRASH_FILE = URL_ROOT + "trash.php"
    val DELETE_FILE = URL_ROOT + "delete.php"
    val GET_TRASH_FILES = URL_ROOT + "trashfilelist.php"

    //Wallet....
    val EXPENSE_INCOME_LIST = URL_ROOT + "showaccount.php"
    val GET_CATEGORY = URL_ROOT + "transcategorylist.php"
    val DELETE_CATEGORY = URL_ROOT + "deleteTransactionCategory.php"
    val GET_COMPANY = URL_ROOT + "companylist.php"
    val ADD_TRANSCATION = URL_ROOT + "addtransaction.php"
    val ADD_COMPANY = URL_ROOT + "addcompany.php"
    val ADD_CATEGORY = URL_ROOT + "addtranscategory.php"
    val DELETE_TRANSCATION = URL_ROOT + "deleteTransaction.php"
    val EDIT_TRANSCATION = URL_ROOT + "editTransaction.php"
    val GET_GRAPH = URL_ROOT + "graph.php"
    val GET_BUDGET = URL_ROOT + "showbudget.php"
    val BUDGET_CATEGORY_LIST = URL_ROOT + "showTransaction.php"
    val GET_BUDGET_CATEGORY = URL_ROOT + "getBudgetCategory.php"
    val DELETE_BUDGET_CATEGORY = URL_ROOT + "deleteBudgetCategory.php"
    val ADD_INCOME = URL_ROOT + "addincome.php"
    val ADD_BUDGET_CATEGORY = URL_ROOT + "addBudgetCategory.php"
    val EDIT_BUDGET_CATEGORY = URL_ROOT + "editBudgetCategory.php"
    val EDIT_CATEGORY = URL_ROOT + "editTransationCategory.php"
    val GET_RENEWABLE_TRASCATION = URL_ROOT + "renualTransaction.php"
    val GET_PAYMENT_RECEIVABLE = URL_ROOT + "paymentreceivable.php"
    val DELETE_SPLIT_TRASCATION = URL_ROOT + "deleteSplitTransaction.php"
    val GET_REPORT = URL_ROOT + "showreport.php"


    //Contact.....
    val GET_ALL_CONTACT = URL_ROOT + "contactlist.php"
    val GET_GROUP_CONTACT = URL_ROOT + "groupcontactlist.php"
    val GET_EMERGENCY_CONTACT = URL_ROOT + "emergencyContact.php"
    val ADD_CONTACT = URL_ROOT + "addcontact.php"
    val GET_GROUP_LIST = URL_ROOT + "grouplist.php"
    val CREATE_GROUP = URL_ROOT + "creategroup.php"
    val EDIT_CONTACT = URL_ROOT + "updatecontact.php"
    var REMOVE_CONTACT = URL_ROOT + "removecontact.php"

    //Pill Reminder.....
    val GET_PILL_LIST = URL_ROOT + "getPillReminder.php"
    val GET_TODAYS_REMINDER = URL_ROOT + "getTimewisePillReminderNew.php"
    val ADD_PILLS = URL_ROOT + "addPillReminder.php"
    val EDIT_PILLS = URL_ROOT + "editPillReminder.php"
    val DELETE_PILLS = URL_ROOT + "deletePillReminder.php"
    val GET_MEDICINE_LIST = URL_ROOT + "getMedicineName.php"
    val GET_REMINDER_REPORT = URL_ROOT + "pillReminderReport.php"
}