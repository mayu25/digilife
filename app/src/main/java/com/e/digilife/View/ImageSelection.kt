package com.e.digilife.View

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Parcelable
import android.provider.MediaStore
import android.util.Log
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.util.*

/**
 * Created by Mayuri on 29/5/18.
 */
class ImageSelection(private val context: Context) {

    var selectedImagePath: String = ""
    private var mCropImageUri: Uri? = null
    var requirePermissions = true


    fun getPickImageChooserIntent(): Intent {

        val outputFileUri = getCaptureImageOutputUri()
        val allIntents = ArrayList<Intent>()
        val packageManager = context!!.packageManager
        // collect all camera intents
        val captureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val listCam = packageManager.queryIntentActivities(captureIntent, 0)
        for (res in listCam) {
            val intent = Intent(captureIntent)
            intent.component = ComponentName(res.activityInfo.packageName, res.activityInfo.name)
            intent.setPackage(res.activityInfo.packageName)
            if (outputFileUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri)
            }
            allIntents.add(intent)
        }

        // the main intent is the last in the  list so pickup the useless one
        var mainIntent = allIntents[allIntents.size - 1]
        for (intent in allIntents) {
            if (intent.component!!.className == "com.android.documentsui.DocumentsActivity") {
                mainIntent = intent
                break
            }
        }
        allIntents.remove(mainIntent)
        val chooserIntent = Intent.createChooser(mainIntent, "Select source")
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toTypedArray<Parcelable>())

        return chooserIntent
    }

    private fun getCaptureImageOutputUri(): Uri? {
        var outputFileUri: Uri? = null
        val getImage = context.externalCacheDir
        if (getImage != null) {
            outputFileUri = Uri.fromFile(File(getImage.path, "pickImageResult.jpeg"))
        }
        return outputFileUri
    }

    fun getPickImageChooserGalleryIntent(): Intent {

        // Uri outputFileUri = getCaptureImageOutputUri();
        val allIntents = ArrayList<Intent>()
        val packageManager = context.packageManager

        // collect all gallery intents
        val galleryIntent = Intent(Intent.ACTION_GET_CONTENT)
        galleryIntent.type = "image/*"
        val listGallery = packageManager.queryIntentActivities(galleryIntent, 0)
        for (res in listGallery) {
            val intent = Intent(galleryIntent)
            intent.component = ComponentName(res.activityInfo.packageName, res.activityInfo.name)
            intent.setPackage(res.activityInfo.packageName)
            allIntents.add(intent)
        }

        // the main intent is the last in the  list so pickup the useless one
        var mainIntent = allIntents[allIntents.size - 1]
        for (intent in allIntents) {
            if (intent.component!!.className == "com.android.documentsui.DocumentsActivity") {
                mainIntent = intent
                break
            }
        }
        allIntents.remove(mainIntent)
        val chooserIntent = Intent.createChooser(mainIntent, "Select source")
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toTypedArray<Parcelable>())

        return chooserIntent
    }

    fun getPickImageResultUri(data: Intent?): Uri? {
        var isCamera = true
        if (data != null && data.data != null) {
            val action = data.action
            isCamera = action != null && action == MediaStore.ACTION_IMAGE_CAPTURE
        }
        return if (isCamera) getCaptureImageOutputUri() else data!!.data
    }

    fun isUriRequiresPermissions(uri: Uri): Boolean {
        try {
            val resolver = context.contentResolver
            val stream = resolver.openInputStream(uri)
            stream!!.close()
            return false
        } catch (e: FileNotFoundException) {

        } catch (e: Exception) {
        }

        return false
    }

    //TODO: Compress Image...
    fun compressImg(newPath: String) {
        val MAX_IMAGE_SIZE = 200 * 1024 // max final file size
        var bmpPic = BitmapFactory.decodeFile(newPath)
        if ((bmpPic.getWidth() >= 1024) && (bmpPic.getHeight() >= 1024)) {
            val bmpOptions = BitmapFactory.Options()
            bmpOptions.inSampleSize = 1
            while ((bmpPic.getWidth() >= 1024) && (bmpPic.getHeight() >= 1024)) {
                bmpOptions.inSampleSize++
                bmpPic = BitmapFactory.decodeFile(newPath, bmpOptions)
            }
            Log.e("ComressImg", "Resize: " + bmpOptions.inSampleSize)
        }
        var compressQuality = 104 // quality decreasing by 5 every loop. (start from 99)
        var streamLength = MAX_IMAGE_SIZE
        while (streamLength >= MAX_IMAGE_SIZE) {
            val bmpStream = ByteArrayOutputStream()
            compressQuality -= 5
            Log.e("ComressImg", "Quality: " + compressQuality)
            bmpPic.compress(Bitmap.CompressFormat.JPEG, compressQuality, bmpStream)
            val bmpPicByteArray = bmpStream.toByteArray()
            streamLength = bmpPicByteArray.size
            Log.e("ComressImg", "Size: " + streamLength)
        }
        try {
            val bmpFile = FileOutputStream(newPath)
            bmpPic.compress(Bitmap.CompressFormat.JPEG, compressQuality, bmpFile)
            bmpFile.flush()
            bmpFile.close()
        } catch (e: Exception) {
            Log.e("ComressImg", "Error on saving file")
        }
    }
}