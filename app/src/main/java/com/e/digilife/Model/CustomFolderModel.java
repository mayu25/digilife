package com.e.digilife.Model;

import java.io.Serializable;

//
// Created by Mayuri on 24/05/19.
//

public class CustomFolderModel implements Serializable {

    String customFolderId, customFolderName, customFolderPath, customFolderDescription;

    public String getCustomFolderId() {
        return customFolderId;
    }

    public void setCustomFolderId(String customFolderId) {
        this.customFolderId = customFolderId;
    }

    public String getCustomFolderName() {
        return customFolderName;
    }

    public void setCustomFolderName(String customFolderName) {
        this.customFolderName = customFolderName;
    }

    public String getCustomFolderPath() {
        return customFolderPath;
    }

    public void setCustomFolderPath(String customFolderPath) {
        this.customFolderPath = customFolderPath;
    }

    public String getCustomFolderDescription() {
        return customFolderDescription;
    }

    public void setCustomFolderDescription(String customFolderDescription) {
        this.customFolderDescription = customFolderDescription;
    }
}

