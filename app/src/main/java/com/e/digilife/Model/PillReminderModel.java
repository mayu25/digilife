package com.e.digilife.Model;

import java.io.Serializable;


public class PillReminderModel implements Serializable {

    String TodayType, pill1, pill2, pill3, medicineTime, isData;

    String dayLetter, isDaySelected, dayId;
    String pillId, pillName, pillRemiderTime, pillTimeAM_PM, fromDate, toDate, pillReminderType, pillDays, takePills, dailyTime,
            doctoreName, rxNumber, pillStatus, note, pillCount, pillType;


    public String getPillType() {
        return pillType;
    }

    public void setPillType(String pillType) {
        this.pillType = pillType;
    }

    public String getPillCount() {
        return pillCount;
    }

    public void setPillCount(String pillCount) {
        this.pillCount = pillCount;
    }

    public String getIsData() {
        return isData;
    }

    public void setIsData(String isData) {
        this.isData = isData;
    }

    public String getTodayType() {
        return TodayType;
    }

    public void setTodayType(String todayType) {
        TodayType = todayType;
    }

    public String getPill1() {
        return pill1;
    }

    public void setPill1(String pill1) {
        this.pill1 = pill1;
    }

    public String getPill2() {
        return pill2;
    }

    public void setPill2(String pill2) {
        this.pill2 = pill2;
    }

    public String getPill3() {
        return pill3;
    }

    public void setPill3(String pill3) {
        this.pill3 = pill3;
    }

    public String getMedicineTime() {
        return medicineTime;
    }

    public void setMedicineTime(String medicineTime) {
        this.medicineTime = medicineTime;
    }

    public String getPillName() {
        return pillName;
    }

    public void setPillName(String pillName) {
        this.pillName = pillName;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    public String getPillReminderType() {
        return pillReminderType;
    }

    public void setPillReminderType(String pillReminderType) {
        this.pillReminderType = pillReminderType;
    }

    public String getPillDays() {
        return pillDays;
    }

    public void setPillDays(String pillDays) {
        this.pillDays = pillDays;
    }

    public String getTakePills() {
        return takePills;
    }

    public void setTakePills(String takePills) {
        this.takePills = takePills;
    }

    public String getDailyTime() {
        return dailyTime;
    }

    public void setDailyTime(String dailyTime) {
        this.dailyTime = dailyTime;
    }

    public String getDoctoreName() {
        return doctoreName;
    }

    public void setDoctoreName(String doctoreName) {
        this.doctoreName = doctoreName;
    }

    public String getRxNumber() {
        return rxNumber;
    }

    public void setRxNumber(String rxNumber) {
        this.rxNumber = rxNumber;
    }

    public String getPillStatus() {
        return pillStatus;
    }

    public void setPillStatus(String pillStatus) {
        this.pillStatus = pillStatus;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getIsDaySelected() {
        return isDaySelected;
    }

    public void setIsDaySelected(String isDaySelected) {
        this.isDaySelected = isDaySelected;
    }

    public String getPillId() {
        return pillId;
    }

    public void setPillId(String pillId) {
        this.pillId = pillId;
    }

    public String getDayLetter() {
        return dayLetter;
    }

    public void setDayLetter(String dayLetter) {
        this.dayLetter = dayLetter;
    }

    public String getPillRemiderTime() {
        return pillRemiderTime;
    }

    public void setPillRemiderTime(String pillRemiderTime) {
        this.pillRemiderTime = pillRemiderTime;
    }

    public String getPillTimeAM_PM() {
        return pillTimeAM_PM;
    }

    public void setPillTimeAM_PM(String pillTimeAM_PM) {
        this.pillTimeAM_PM = pillTimeAM_PM;
    }

    public String getDayId() {
        return dayId;
    }

    public void setDayId(String dayId) {
        this.dayId = dayId;
    }
}
