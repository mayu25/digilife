package com.e.digilife.Model;

import java.io.Serializable;

//
// Created by Mayuri on 03/06/19.
//

public class ArticleModel implements Serializable {

   String articleId, articleTitle, articleImage, articleDescription, articelDate;

    public String getArticleId() {
        return articleId;
    }

    public void setArticleId(String articleId) {
        this.articleId = articleId;
    }

    public String getArticleTitle() {
        return articleTitle;
    }

    public void setArticleTitle(String articleTitle) {
        this.articleTitle = articleTitle;
    }

    public String getArticleImage() {
        return articleImage;
    }

    public void setArticleImage(String articleImage) {
        this.articleImage = articleImage;
    }

    public String getArticleDescription() {
        return articleDescription;
    }

    public void setArticleDescription(String articleDescription) {
        this.articleDescription = articleDescription;
    }

    public String getArticelDate() {
        return articelDate;
    }

    public void setArticelDate(String articelDate) {
        this.articelDate = articelDate;
    }
}
