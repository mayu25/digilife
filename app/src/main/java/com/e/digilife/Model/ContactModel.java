package com.e.digilife.Model;

import java.io.Serializable;


public class ContactModel implements Serializable {

    String contactId, groupId, contactName, contactPic, organization_1, organization_2, phone_1, phone_2, email_1, email_2, address_1,
            address_2, webAddress_1, webAddress_2, createAt;
    String nameFirstLetter, groupName, isEmergency, isTitle;

    public String getIsTitle() {
        return isTitle;
    }

    public void setIsTitle(String isTitle) {
        this.isTitle = isTitle;
    }

    public String getIsEmergency() {
        return isEmergency;
    }

    public void setIsEmergency(String isEmergency) {
        this.isEmergency = isEmergency;
    }

    public String getNameFirstLetter() {
        return nameFirstLetter;
    }

    public void setNameFirstLetter(String nameFirstLetter) {
        this.nameFirstLetter = nameFirstLetter;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getContactId() {
        return contactId;
    }

    public void setContactId(String contactId) {
        this.contactId = contactId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPic() {
        return contactPic;
    }

    public void setContactPic(String contactPic) {
        this.contactPic = contactPic;
    }

    public String getOrganization_1() {
        return organization_1;
    }

    public void setOrganization_1(String organization_1) {
        this.organization_1 = organization_1;
    }

    public String getOrganization_2() {
        return organization_2;
    }

    public void setOrganization_2(String organization_2) {
        this.organization_2 = organization_2;
    }

    public String getPhone_1() {
        return phone_1;
    }

    public void setPhone_1(String phone_1) {
        this.phone_1 = phone_1;
    }

    public String getPhone_2() {
        return phone_2;
    }

    public void setPhone_2(String phone_2) {
        this.phone_2 = phone_2;
    }

    public String getEmail_1() {
        return email_1;
    }

    public void setEmail_1(String email_1) {
        this.email_1 = email_1;
    }

    public String getEmail_2() {
        return email_2;
    }

    public void setEmail_2(String email_2) {
        this.email_2 = email_2;
    }

    public String getAddress_1() {
        return address_1;
    }

    public void setAddress_1(String address_1) {
        this.address_1 = address_1;
    }

    public String getAddress_2() {
        return address_2;
    }

    public void setAddress_2(String address_2) {
        this.address_2 = address_2;
    }

    public String getWebAddress_1() {
        return webAddress_1;
    }

    public void setWebAddress_1(String webAddress_1) {
        this.webAddress_1 = webAddress_1;
    }

    public String getWebAddress_2() {
        return webAddress_2;
    }

    public void setWebAddress_2(String webAddress_2) {
        this.webAddress_2 = webAddress_2;
    }

    public String getCreateAt() {
        return createAt;
    }

    public void setCreateAt(String createAt) {
        this.createAt = createAt;
    }
}
