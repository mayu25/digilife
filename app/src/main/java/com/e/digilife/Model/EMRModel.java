package com.e.digilife.Model;

import android.net.Uri;

import java.io.Serializable;

//
// Created by Mayuri on 14/05/19.
//

public class EMRModel implements Serializable {

    String emrId, emrFolderName;
    String emr_fileId, emr_fileName, emr_filePath;
    public boolean selected;

    public String getEmrId() {
        return emrId;
    }

    public void setEmrId(String emrId) {
        this.emrId = emrId;
    }

    public String getEmrFolderName() {
        return emrFolderName;
    }

    public void setEmrFolderName(String emrFolderName) {
        this.emrFolderName = emrFolderName;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    //EMR Docs...


    public String getEmr_fileId() {
        return emr_fileId;
    }

    public void setEmr_fileId(String emr_fileId) {
        this.emr_fileId = emr_fileId;
    }

    public String getEmr_fileName() {
        return emr_fileName;
    }

    public void setEmr_fileName(String emr_fileName) {
        this.emr_fileName = emr_fileName;
    }

    public String getEmr_filePath() {
        return emr_filePath;
    }

    public void setEmr_filePath(String emr_filePath) {
        this.emr_filePath = emr_filePath;
    }
}
