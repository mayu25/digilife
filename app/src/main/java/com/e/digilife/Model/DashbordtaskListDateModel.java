package com.e.digilife.Model;

import java.io.Serializable;

public class DashbordtaskListDateModel implements Serializable {

    String task_id, repeat_day, repeat_week, task_status, share_status, cat_name, update_at, create_at, noti_status, attachment, repeat_on, notes, sub_task_name, longitude, adress, latitude, loc_type, loc_reminder_type, repeat_year, cat_id, add_long, add_lat, curr_long, loc_address, curr_lat, repeat_month, share_task_id, client_id, owner_id, task_title, task_tag, reminder_type, onetime_type, register_date_time, update_date_time, repeat_type;
    String isTitle;

    public String getIsTitle() {
        return isTitle;
    }

    public void setIsTitle(String isTitle) {
        this.isTitle = isTitle;
    }

    public String getTask_id() {
        return task_id;
    }

    public void setTask_id(String task_id) {
        this.task_id = task_id;
    }

    public String getRepeat_day() {
        return repeat_day;
    }

    public void setRepeat_day(String repeat_day) {
        this.repeat_day = repeat_day;
    }

    public String getRepeat_week() {
        return repeat_week;
    }

    public void setRepeat_week(String repeat_week) {
        this.repeat_week = repeat_week;
    }

    public String getTask_status() {
        return task_status;
    }

    public void setTask_status(String task_status) {
        this.task_status = task_status;
    }

    public String getShare_status() {
        return share_status;
    }

    public void setShare_status(String share_status) {
        this.share_status = share_status;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    public String getUpdate_at() {
        return update_at;
    }

    public void setUpdate_at(String update_at) {
        this.update_at = update_at;
    }

    public String getCreate_at() {
        return create_at;
    }

    public void setCreate_at(String create_at) {
        this.create_at = create_at;
    }

    public String getNoti_status() {
        return noti_status;
    }

    public void setNoti_status(String noti_status) {
        this.noti_status = noti_status;
    }

    public String getAttachment() {
        return attachment;
    }

    public void setAttachment(String attachment) {
        this.attachment = attachment;
    }

    public String getRepeat_on() {
        return repeat_on;
    }

    public void setRepeat_on(String repeat_on) {
        this.repeat_on = repeat_on;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public String getSub_task_name() {
        return sub_task_name;
    }

    public void setSub_task_name(String sub_task_name) {
        this.sub_task_name = sub_task_name;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLoc_type() {
        return loc_type;
    }

    public void setLoc_type(String loc_type) {
        this.loc_type = loc_type;
    }

    public String getLoc_reminder_type() {
        return loc_reminder_type;
    }

    public void setLoc_reminder_type(String loc_reminder_type) {
        this.loc_reminder_type = loc_reminder_type;
    }

    public String getRepeat_year() {
        return repeat_year;
    }

    public void setRepeat_year(String repeat_year) {
        this.repeat_year = repeat_year;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getAdd_long() {
        return add_long;
    }

    public void setAdd_long(String add_long) {
        this.add_long = add_long;
    }

    public String getAdd_lat() {
        return add_lat;
    }

    public void setAdd_lat(String add_lat) {
        this.add_lat = add_lat;
    }

    public String getCurr_long() {
        return curr_long;
    }

    public void setCurr_long(String curr_long) {
        this.curr_long = curr_long;
    }

    public String getLoc_address() {
        return loc_address;
    }

    public void setLoc_address(String loc_address) {
        this.loc_address = loc_address;
    }

    public String getCurr_lat() {
        return curr_lat;
    }

    public void setCurr_lat(String curr_lat) {
        this.curr_lat = curr_lat;
    }

    public String getRepeat_month() {
        return repeat_month;
    }

    public void setRepeat_month(String repeat_month) {
        this.repeat_month = repeat_month;
    }

    public String getShare_task_id() {
        return share_task_id;
    }

    public void setShare_task_id(String share_task_id) {
        this.share_task_id = share_task_id;
    }

    public String getClient_id() {
        return client_id;
    }

    public void setClient_id(String client_id) {
        this.client_id = client_id;
    }

    public String getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(String owner_id) {
        this.owner_id = owner_id;
    }

    public String getTask_title() {
        return task_title;
    }

    public void setTask_title(String task_title) {
        this.task_title = task_title;
    }

    public String getTask_tag() {
        return task_tag;
    }

    public void setTask_tag(String task_tag) {
        this.task_tag = task_tag;
    }

    public String getReminder_type() {
        return reminder_type;
    }

    public void setReminder_type(String reminder_type) {
        this.reminder_type = reminder_type;
    }

    public String getOnetime_type() {
        return onetime_type;
    }

    public void setOnetime_type(String onetime_type) {
        this.onetime_type = onetime_type;
    }

    public String getRegister_date_time() {
        return register_date_time;
    }

    public void setRegister_date_time(String register_date_time) {
        this.register_date_time = register_date_time;
    }

    public String getUpdate_date_time() {
        return update_date_time;
    }

    public void setUpdate_date_time(String update_date_time) {
        this.update_date_time = update_date_time;
    }

    public String getRepeat_type() {
        return repeat_type;
    }

    public void setRepeat_type(String repeat_type) {
        this.repeat_type = repeat_type;
    }
}
