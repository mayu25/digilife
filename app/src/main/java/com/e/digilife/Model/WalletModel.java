package com.e.digilife.Model;

import java.io.Serializable;

//
// Created by Mayuri on 05/06/19.
//

public class WalletModel implements Serializable {

    String expenseTotal, expenseDate, isExpense, currency, isTitle;
    String tranId, tranAmount, tranDescription, trasAttachment, tranClaimReimburse, trasPurpose, trasCategoryId, trasnCompnayId,
            trasRenewableType, transDate, trasSplit, trasCategoryName, trasCategoryType, trasSplitData, trasIsRenewable, transFullDateTime,
            transCompanyName, trasPersentage;
    String budgetAmount, budgetExpense, budgetName;

    String memberName, memberCount;

    int percentageColor;

    public int getPercentageColor() {
        return percentageColor;
    }

    public void setPercentageColor(int percentageColor) {
        this.percentageColor = percentageColor;
    }

    public String getIsTitle() {
        return isTitle;
    }

    public void setIsTitle(String isTitle) {
        this.isTitle = isTitle;
    }

    public String getBudgetAmount() {
        return budgetAmount;
    }

    public void setBudgetAmount(String budgetAmount) {
        this.budgetAmount = budgetAmount;
    }

    public String getBudgetExpense() {
        return budgetExpense;
    }

    public void setBudgetExpense(String budgetExpense) {
        this.budgetExpense = budgetExpense;
    }

    public String getBudgetName() {
        return budgetName;
    }

    public void setBudgetName(String budgetName) {
        this.budgetName = budgetName;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getMemberCount() {
        return memberCount;
    }

    public void setMemberCount(String memberCount) {
        this.memberCount = memberCount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getIsExpense() {
        return isExpense;
    }

    public void setIsExpense(String isExpense) {
        this.isExpense = isExpense;
    }

    public String getExpenseTotal() {
        return expenseTotal;
    }

    public void setExpenseTotal(String expenseTotal) {
        this.expenseTotal = expenseTotal;
    }

    public String getExpenseDate() {
        return expenseDate;
    }

    public void setExpenseDate(String expenseDate) {
        this.expenseDate = expenseDate;
    }

    public String getTranId() {
        return tranId;
    }

    public void setTranId(String tranId) {
        this.tranId = tranId;
    }

    public String getTranAmount() {
        return tranAmount;
    }

    public void setTranAmount(String tranAmount) {
        this.tranAmount = tranAmount;
    }

    public String getTranDescription() {
        return tranDescription;
    }

    public String getTrasAttachment() {
        return trasAttachment;
    }

    public void setTrasAttachment(String trasAttachment) {
        this.trasAttachment = trasAttachment;
    }

    public void setTranDescription(String tranDescription) {
        this.tranDescription = tranDescription;
    }

    public String getTranClaimReimburse() {
        return tranClaimReimburse;
    }

    public void setTranClaimReimburse(String tranClaimReimburse) {
        this.tranClaimReimburse = tranClaimReimburse;
    }

    public String getTrasPurpose() {
        return trasPurpose;
    }

    public void setTrasPurpose(String trasPurpose) {
        this.trasPurpose = trasPurpose;
    }

    public String getTrasCategoryId() {
        return trasCategoryId;
    }

    public void setTrasCategoryId(String trasCategoryId) {
        this.trasCategoryId = trasCategoryId;
    }

    public String getTrasnCompnayId() {
        return trasnCompnayId;
    }

    public void setTrasnCompnayId(String trasnCompnayId) {
        this.trasnCompnayId = trasnCompnayId;
    }


    public String getTrasPersentage() {
        return trasPersentage;
    }

    public void setTrasPersentage(String trasPersentage) {
        this.trasPersentage = trasPersentage;
    }

    public String getTransCompanyName() {
        return transCompanyName;
    }

    public void setTransCompanyName(String transCompanyName) {
        this.transCompanyName = transCompanyName;
    }

    public String getTrasRenewableType() {
        return trasRenewableType;
    }

    public void setTrasRenewableType(String trasRenewableType) {
        this.trasRenewableType = trasRenewableType;
    }

    public String getTransDate() {
        return transDate;
    }

    public void setTransDate(String transDate) {
        this.transDate = transDate;
    }

    public String getTrasSplit() {
        return trasSplit;
    }

    public void setTrasSplit(String trasSplit) {
        this.trasSplit = trasSplit;
    }

    public String getTrasCategoryName() {
        return trasCategoryName;
    }

    public void setTrasCategoryName(String trasCategoryName) {
        this.trasCategoryName = trasCategoryName;
    }

    public String getTrasCategoryType() {
        return trasCategoryType;
    }

    public void setTrasCategoryType(String trasCategoryType) {
        this.trasCategoryType = trasCategoryType;
    }

    public String getTrasSplitData() {
        return trasSplitData;
    }

    public void setTrasSplitData(String trasSplitData) {
        this.trasSplitData = trasSplitData;
    }

    public String getTrasIsRenewable() {
        return trasIsRenewable;
    }

    public void setTrasIsRenewable(String trasIsRenewable) {
        this.trasIsRenewable = trasIsRenewable;
    }

    public String getTransFullDateTime() {
        return transFullDateTime;
    }

    public void setTransFullDateTime(String transFullDateTime) {
        this.transFullDateTime = transFullDateTime;
    }

    //TODO:Category..........

    String type, categoryID, categoryName, categoryType, categoryStatus;


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getCategoryType() {
        return categoryType;
    }

    public void setCategoryType(String categoryType) {
        this.categoryType = categoryType;
    }

    public String getCategoryStatus() {
        return categoryStatus;
    }

    public void setCategoryStatus(String categoryStatus) {
        this.categoryStatus = categoryStatus;
    }

    //TODO: Company.........

    String companyId, companyName;

    public String getCompanyId() {
        return companyId;
    }

    public void setCompanyId(String companyId) {
        this.companyId = companyId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }


    //TODO: Payment Receivable.........

    String p_MemberName, p_Amount, p_Id, p_Key, p_Split, p_IsSplit, p_TransID;

    public String getP_TransID() {
        return p_TransID;
    }

    public void setP_TransID(String p_TransID) {
        this.p_TransID = p_TransID;
    }

    public String getP_MemberName() {
        return p_MemberName;
    }

    public void setP_MemberName(String p_MemberName) {
        this.p_MemberName = p_MemberName;
    }

    public String getP_Amount() {
        return p_Amount;
    }

    public void setP_Amount(String p_Amount) {
        this.p_Amount = p_Amount;
    }

    public String getP_Id() {
        return p_Id;
    }

    public void setP_Id(String p_Id) {
        this.p_Id = p_Id;
    }

    public String getP_Key() {
        return p_Key;
    }

    public void setP_Key(String p_Key) {
        this.p_Key = p_Key;
    }

    public String getP_Split() {
        return p_Split;
    }

    public void setP_Split(String p_Split) {
        this.p_Split = p_Split;
    }

    public String getP_IsSplit() {
        return p_IsSplit;
    }

    public void setP_IsSplit(String p_IsSplit) {
        this.p_IsSplit = p_IsSplit;
    }
}
