package com.e.digilife.Model;

import java.io.Serializable;

//
// Created by Mayuri on 20/05/19.
//

public class DataModel implements Serializable {

    String fileId, fileName, fileSize, filePath, isFav, parentFolderId, childFolderId, createTime, modifiedTime, fileType;
    public boolean selected;

    //Offline Image
    String offlineClientId, offlineImagePath, offlineFileId, offlineFileName, offlineFileType;

    public String getOfflineClientId() {
        return offlineClientId;
    }

    public void setOfflineClientId(String offlineClientId) {
        this.offlineClientId = offlineClientId;
    }

    public String getOfflineImagePath() {
        return offlineImagePath;
    }

    public void setOfflineImagePath(String offlineImagePath) {
        this.offlineImagePath = offlineImagePath;
    }

    public String getOfflineFileId() {
        return offlineFileId;
    }

    public void setOfflineFileId(String offlineFileId) {
        this.offlineFileId = offlineFileId;
    }

    public String getOfflineFileName() {
        return offlineFileName;
    }

    public void setOfflineFileName(String offlineFileName) {
        this.offlineFileName = offlineFileName;
    }


    public String getOfflineFileType() {
        return offlineFileType;
    }

    public void setOfflineFileType(String offlineFileType) {
        this.offlineFileType = offlineFileType;
    }

    public String getFileId() {
        return fileId;
    }

    public void setFileId(String fileId) {
        this.fileId = fileId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getIsFav() {
        return isFav;
    }

    public void setIsFav(String isFav) {
        this.isFav = isFav;
    }

    public String getParentFolderId() {
        return parentFolderId;
    }

    public void setParentFolderId(String parentFolderId) {
        this.parentFolderId = parentFolderId;
    }

    public String getChildFolderId() {
        return childFolderId;
    }

    public void setChildFolderId(String childFolderId) {
        this.childFolderId = childFolderId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(String modifiedTime) {
        this.modifiedTime = modifiedTime;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }
}
