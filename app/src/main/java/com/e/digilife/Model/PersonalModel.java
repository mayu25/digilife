package com.e.digilife.Model;

import java.io.Serializable;

//
// Created by Mayuri on 21/05/19.
//

public class PersonalModel implements Serializable {

    String personalId, personalFolderName, personalImgPath, description;

    public String getPersonalId() {
        return personalId;
    }

    public void setPersonalId(String personalId) {
        this.personalId = personalId;
    }

    public String getPersonalFolderName() {
        return personalFolderName;
    }

    public void setPersonalFolderName(String personalFolderName) {
        this.personalFolderName = personalFolderName;
    }

    public String getPersonalImgPath() {
        return personalImgPath;
    }

    public void setPersonalImgPath(String personalImgPath) {
        this.personalImgPath = personalImgPath;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    //For Insurance......
    String  i_Id, i_StartDate, i_EndDate, i_PolicyNo, i_PolicyName, i_Amount, i_Attachement, i_Receipt, i_Days, i_Reminder;

    public String getI_Id() {
        return i_Id;
    }

    public void setI_Id(String i_Id) {
        this.i_Id = i_Id;
    }

    public String getI_StartDate() {
        return i_StartDate;
    }

    public void setI_StartDate(String i_StartDate) {
        this.i_StartDate = i_StartDate;
    }

    public String getI_EndDate() {
        return i_EndDate;
    }

    public void setI_EndDate(String i_EndDate) {
        this.i_EndDate = i_EndDate;
    }

    public String getI_PolicyNo() {
        return i_PolicyNo;
    }

    public void setI_PolicyNo(String i_PolicyNo) {
        this.i_PolicyNo = i_PolicyNo;
    }

    public String getI_PolicyName() {
        return i_PolicyName;
    }

    public void setI_PolicyName(String i_PolicyName) {
        this.i_PolicyName = i_PolicyName;
    }

    public String getI_Amount() {
        return i_Amount;
    }

    public void setI_Amount(String i_Amount) {
        this.i_Amount = i_Amount;
    }

    public String getI_Attachement() {
        return i_Attachement;
    }

    public void setI_Attachement(String i_Attachement) {
        this.i_Attachement = i_Attachement;
    }

    public String getI_Receipt() {
        return i_Receipt;
    }

    public void setI_Receipt(String i_Receipt) {
        this.i_Receipt = i_Receipt;
    }

    public String getI_Days() {
        return i_Days;
    }

    public void setI_Days(String i_Days) {
        this.i_Days = i_Days;
    }

    public String getI_Reminder() {
        return i_Reminder;
    }

    public void setI_Reminder(String i_Reminder) {
        this.i_Reminder = i_Reminder;
    }
}

